Declare @IndexName nvarchar(100)

DECLARE index_cursor CURSOR FOR
SELECT name FROM sys.indexes 
where object_id = object_id('Positions') 
and name is not null

OPEN index_cursor

-- Perform the first fetch.
FETCH NEXT FROM index_cursor into @IndexName

WHILE @@FETCH_STATUS = 0
BEGIN

if left(@IndexName,2) = 'PK'
BEGIN
	Exec( 'ALTER TABLE ' + 'Positions' +	' DROP CONSTRAINT ' + @IndexName )
END

ELSE
BEGIN
-- This is executed as long as the previous fetch succeeds.
Exec('Drop index ' + @IndexName + ' on ' + 'Positions')
END

FETCH NEXT FROM index_cursor into @IndexName

END

CLOSE index_cursor
DEALLOCATE index_cursor