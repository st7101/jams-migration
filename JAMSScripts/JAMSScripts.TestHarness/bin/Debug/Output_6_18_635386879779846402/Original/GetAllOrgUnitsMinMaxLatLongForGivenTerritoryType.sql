SELECT 
OUAE.OrgUnit_Id as OrgUnitId,
Max(AE.AlignmentEntity_Latitude) as MaxLatitude, 
Min(AE.AlignmentEntity_Latitude) as MinLatitude,
Max(AE.AlignmentEntity_Longitude) as MaxLongitude, 
Min(AE.AlignmentEntity_Longitude) as MinLongitude 
FROM OrgUnitAlignmentEntitys as OUAE
inner join AlignmentEntitys as AE on OUAE.AlignmentEntity_id = AE.AlignmentEntity_id
inner join OrgUnits ou on ou.OrgUnit_Id = OUAE.OrgUnit_Id
inner join TerritoryTypes tt on tt.OrgUnitLevel_Id = ou.OrgUnitLevel_Id
where (AE.AlignmentEntity_Longitude != 0) AND (AE.AlignmentEntity_Latitude != 0) AND (tt.TerritoryType_Id = '146')
group by OUAE.OrgUnit_Id