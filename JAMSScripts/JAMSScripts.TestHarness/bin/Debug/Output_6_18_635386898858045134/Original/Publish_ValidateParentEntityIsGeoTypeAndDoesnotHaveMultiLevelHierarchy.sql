/*Query to get count of records whose parent entity type is not Geo and Geo type entites having parent defined. 
1 belongs to geo type which will be passed from server side.*/

select AlignmentEntityType_id, TerritoryType_Id  from AlignmentEntityTypeTerritoryTypes
where AlignmentEntityType_ParentId <> 1
	or (AlignmentEntityType_id = 1 and AlignmentEntityType_ParentId is not null)