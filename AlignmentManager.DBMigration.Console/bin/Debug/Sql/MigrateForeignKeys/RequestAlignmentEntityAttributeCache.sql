
PRINT N'Updating RequestAlignmentEntityAttributeCache ...';

UPDATE raeac
SET TerritoryType_Id=tt.TerritoryType_Id,AlignmentEntity_Id=ae.AlignmentEntity_Id
FROM dbo.RequestAlignmentEntityAttributeCache raeac INNER JOIN 
dbo.AlignmentEntitys ae ON raeac.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON raeac.TerritoryType_ExternalID=tt.TerritoryType_ExternalID
