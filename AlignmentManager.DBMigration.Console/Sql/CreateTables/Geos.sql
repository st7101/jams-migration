

PRINT N'Starting rebuilding table [dbo].[Geos]...';

CREATE TABLE [dbo].[tmp_ms_xx_Geos] (
    [Geo_Id]        BIGINT NOT NULL,
    [Geo_ExternalId] NVARCHAR(100) NOT NULL,
    [Geo_Name]      NVARCHAR (255) NULL,
    [Geo_Status]    NVARCHAR (255) NULL,
    CONSTRAINT [tmp_ms_xx_PK_Geos] PRIMARY KEY CLUSTERED ([Geo_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Geos])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Geos] WITH (TABLOCK) (Geo_Id, [Geo_ExternalId], [Geo_Name], [Geo_Status])
        SELECT ae.AlignmentEntity_Id,
		g.[Geo_Id],
               [Geo_Name],
               [Geo_Status]
        FROM   [dbo].[Geos] g
		INNER JOiN dbo.AlignmentEntitys ae ON g.Geo_Id=ae.AlignmentEntity_ExternalId
    END

DROP TABLE [dbo].[Geos];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Geos]', N'Geos';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PK_Geos]', N'PK_Geos', N'OBJECT';

