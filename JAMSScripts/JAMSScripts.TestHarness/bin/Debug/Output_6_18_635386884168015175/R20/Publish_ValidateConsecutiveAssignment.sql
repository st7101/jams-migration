DECLARE @tempPositionPersonnel TABLE(
		ID int IDENTITY(1,1) PRIMARY KEY,
		Position_Id BIGINT,
		Personnel_Id nvarchar(100)
	);

INSERT INTO @tempPositionPersonnel
select Position_id, Personnel_id from PositionPersonnel  
order by Position_id,EffectiveDate asc


SELECT T1.Position_Id,T1.Personnel_Id from @tempPositionPersonnel T1
JOIN @tempPositionPersonnel T2
ON T1.ID+1=T2.ID
AND T1.Position_Id=T2.Position_Id
WHERE (T1.Personnel_Id = T2.Personnel_Id) OR (T1.Personnel_Id IS NULL AND T2.Personnel_Id IS NULL)
