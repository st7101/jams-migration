-- =============================================
-- Author:		Steve Tetrault
-- Create date: May 21, 2014
-- Description:	Log Client ID changes to the EntityIDMappingHistory
-- =============================================
CREATE TRIGGER dbo.LogClientIDChange 
   ON  dbo.EntityIDMapping 
   AFTER UPDATE
AS 
BEGIN
	SET NOCOUNT ON;
	IF UPDATE ([EntityIDMapping_ClientID])
	BEGIN

    INSERT  INTO [dbo].[EntityIDMappingHistory]
            ( [EntityIDMapping_TypeID] ,
              [EntityIDMapping_InternalID] ,
              [EntityIDMapping_OldClientID] ,
              [EntityIDMapping_NewClientID] ,
              [EntityIDMapping_OldEffectiveDate] ,
              [EntityIDMapping_NewEffectiveDate] ,
              [EntityIDMapping_CreationDate]
            )
            SELECT  d.[EntityIDMapping_TypeID] ,
                    d.[EntityIDMapping_InternalID] ,
                    d.[EntityIDMapping_ClientID] ,
                    i.[EntityIDMapping_ClientID] ,
                    d.[EntityIDMapping_EffectiveDate] ,
                    i.[EntityIDMapping_EffectiveDate] ,
                    d.[EntityIDMapping_CreationDate]
            FROM    Inserted i
            INNER JOIN Deleted d ON d.[EntityIDMapping_InternalID] = i.[EntityIDMapping_InternalID]
                                    AND d.[EntityIDMapping_TypeID] = i.[EntityIDMapping_TypeID]

	END

END
