PRINT N'Updating Positions ...';

UPDATE p
SET OrgUnit_Id=ou.OrgUnit_Id
FROM dbo.Positions p INNER JOIN 
dbo.OrgUnits ou ON p.OrgUnit_ExternalID=ou.OrgUnit_ExternalID
