/***** DROP EXISTING TEMPORARY TABLE IF THERE IS ANY *******/
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedEntitiesWithAttributes') 
DROP TABLE [TempUpdatedEntitiesWithAttributes];

/**** FIND OUT ALL ENTITIES THAT HAD ANY NUMERIC ATTRIBUTES BEING UPDATED IN A REQUEST *********/
WITH CTE_Temp AS (
SELECT DISTINCT eao.EditAttributeOverride_AlignmentEntityId as AlignmentEntity_Id, at.AttributeType_Id, 
CAST(ISNULL(eao.EditAttributeOverride_NewAttributeValue, '0') AS FLOAT) - CAST(ISNULL(eao.EditAttributeOverride_OldAttributeValue, '0') AS FLOAT) AS ChangeInValue 
FROM EditAttributeOverrides eao
INNER JOIN AttributeTypes at ON at.AttributeType_Id = eao.AttributeType_Id
WHERE (at.AttributeType_DataType = 'Number' OR at.AttributeType_DataType = 'Currency')
and eao.Request_Id = 101
)

SELECT * INTO TempUpdatedEntitiesWithAttributes FROM(
/*** THIS WOULD FIND OUT ALL THE UPDATED ENTITIES WHICH ARE ALSO INDEPENDENT IN ANY SINGLE TERRITORY TYPE (i.e. TERRITORYTYPE_ID NULL IN CACHE TABLE) ***/
SELECT temp.AlignmentEntity_Id, NULL AS TerritoryType_Id, temp.AttributeType_Id, temp.ChangeInValue FROM CTE_Temp temp
INNER JOIN AlignmentEntitys ae on ae.alignmententity_Id = temp.AlignmentEntity_Id
WHERE 
EXISTS	--IS THERE ANY TERRITORY TYPE TO WHICH THIS ENTITY BELONGS AND IT IS NOT A TERRITORY TYPE SUPPORTING GEO ASSOCIATION
(SELECT DISTINCT territorytype_id FROM AlignmentEntityTypeTerritoryTypes aet2
WHERE aet2.AlignmentEntityType_id = ae.AlignmentEntityType_id
EXCEPT
SELECT aet1.TerritoryType_id from AlignmentEntityTypeTerritoryTypes aet1
WHERE aet1.AlignmentEntityType_ParentId = ae.AlignmentEntityType_id
)
UNION
/* PARENTS OF ALL UPDATED ALIGNMENT ENTITYS WITH AGGREGATED CHANGED VALUE OF ALL CHILDREN GROUPED IN EACH TERRITORY TYPE */
SELECT ae.AlignmentEntity_ParentId, ge.TerritoryType_id, temp.AttributeType_Id, SUM(temp.ChangeInValue) AS ChangeInValue FROM CTE_Temp temp
INNER JOIN GroupedEntities ge ON temp.AlignmentEntity_Id = ge.AlignmentEntity_id
INNER JOIN AlignmentEntitys ae ON ae.AlignmentEntity_Id = temp.AlignmentEntity_Id
GROUP BY ae.AlignmentEntity_ParentId, ge.TerritoryType_id, temp.AttributeType_Id
) t