
PRINT N'Starting rebuilding table [dbo].[Customers]...';

CREATE TABLE [dbo].[tmp_ms_xx_Customers] (
    [Customer_Id]         BIGINT NOT NULL,
    [Customer_ExternalId]  NVARCHAR(100)   NOT NULL,
    [Customer_Name]       NVARCHAR (255) NULL,
    [Customer_Address1]   NVARCHAR (255) NULL,
    [Customer_Address2]   NVARCHAR (255) NULL,
    [Customer_City]       NVARCHAR (255) NULL,
    [Customer_State]      NVARCHAR (255) NULL,
    [Customer_Country]    NVARCHAR (255) NULL,
    [Customer_Latitude]   FLOAT (53)     NULL,
    [Customer_Longitude]  FLOAT (53)     NULL,
    [Customer_Quality]    INT            NULL,
    [Customer_PostalCode] NVARCHAR (255) NULL,
    [Customer_Status]     NVARCHAR (255) NULL,
    [CustomerType_Id]     BIGINT         NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Customers] PRIMARY KEY CLUSTERED ([Customer_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Customers])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Customers] WITH (TABLOCK) ([Customer_Id], [Customer_ExternalId],[Customer_Name], [Customer_Address1], [Customer_Address2], [Customer_City], [Customer_State], [Customer_Country], [Customer_Latitude], [Customer_Longitude], [Customer_Quality], [Customer_PostalCode], [Customer_Status], [CustomerType_Id])
        SELECT ae.AlignmentEntity_Id, c.[Customer_Id],
               c.[Customer_Name],
               c.[Customer_Address1],
               c.[Customer_Address2],
               c.[Customer_City],
               c.[Customer_State],
               c.[Customer_Country],
               c.[Customer_Latitude],
               c.[Customer_Longitude],
               c.[Customer_Quality],
               c.[Customer_PostalCode],
               c.[Customer_Status],
               c.[CustomerType_Id]
        FROM   [dbo].[Customers] c
		INNER JOIN dbo.AlignmentEntitys ae ON c.Customer_Id=AlignmentEntity_ExternalId;
    END

DROP TABLE [dbo].[Customers];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Customers]', N'Customers';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Customers]', N'PK_Customers', N'OBJECT';

