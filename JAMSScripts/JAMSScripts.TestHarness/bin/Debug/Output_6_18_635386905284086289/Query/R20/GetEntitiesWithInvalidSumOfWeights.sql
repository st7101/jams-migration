SELECT DISTINCT ResultTable.AlignmentEntity_Id
FROM (SELECT Temp.AlignmentEntity_Id AS AlignmentEntity_Id, Temp.TerritoryType_Id AS TerritoryType_Id 
	  FROM (SELECT OAE.AlignmentEntity_Id, OAE.OrgUnitAlignmentEntity_Weight, OAE.OrgUnit_Id, OAE.TerritoryType_Id
		    FROM OrgUnitAlignmentEntitys OAE
			INNER JOIN AlignmentEntitys AE ON OAE.AlignmentEntity_id = AE.AlignmentEntity_Id
			INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
			AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND OAE.TerritoryType_Id = AETTT.TerritoryType_id
			WHERE AETTT.ToleratePartialAlignment = 0) AS Temp
	  GROUP BY Temp.TerritoryType_Id, Temp.AlignmentEntity_Id
	  HAVING SUM(Temp.OrgUnitAlignmentEntity_Weight) <> 100
	  EXCEPT (SELECT Temp2.AlignmentEntity_Id AS AlignmentEntity_Id, Temp2.TerritoryType_Id AS TerritoryType_Id
	          FROM (SELECT OAE.AlignmentEntity_Id, OAE.OrgUnitAlignmentEntity_Weight, OAE.OrgUnit_Id, OAE.TerritoryType_Id
				    FROM OrgUnitAlignmentEntitys OAE
				    INNER JOIN AlignmentEntitys AE ON OAE.AlignmentEntity_id = AE.AlignmentEntity_Id
				    INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
				    AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND OAE.TerritoryType_Id = AETTT.TerritoryType_id
				    WHERE AETTT.ToleratePartialAlignment = 0) AS Temp2 
		      WHERE Temp2.OrgUnitAlignmentEntity_Weight = 0 OR Temp2.OrgUnitAlignmentEntity_Weight IS NULL)
	 ) As ResultTable