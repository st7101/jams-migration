﻿select entity.AlignmentEntity_ParentId, count(entity.AlignmentEntity_Id) 
from AlignmentEntitys entity 
inner join GroupedEntities groupedEntity on entity.AlignmentEntity_Id = groupedEntity.AlignmentEntity_Id 
where entity.AlignmentEntity_Status = :status 
and entity.AlignmentEntity_ParentId in (:parentIds) 
and groupedEntity.TerritoryType_Id = :territoryTypeId 
group by entity.AlignmentEntity_ParentId