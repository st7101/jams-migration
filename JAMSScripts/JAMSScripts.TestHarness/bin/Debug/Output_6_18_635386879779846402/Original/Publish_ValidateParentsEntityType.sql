--Query to determine if an alignment entity type specifies a parent alignment entity type that is not present in the same territory type

with invalidEntityParent as 
(
	select distinct AlignmentEntityType_id,TerritoryType_id,AlignmentEntityType_ParentId  
	from AlignmentEntityTypeTerritoryTypes aettt3
	where AlignmentEntityType_ParentId is not null except 
	(
	select distinct aettt1.AlignmentEntityType_id, aettt1.TerritoryType_id, aettt1.AlignmentEntityType_ParentId   
	from AlignmentEntityTypeTerritoryTypes aettt1
	inner join AlignmentEntityTypeTerritoryTypes aettt2 
	on aettt1.AlignmentEntityType_ParentId=aettt2.AlignmentEntityType_id 
	and aettt1.TerritoryType_id = aettt2.TerritoryType_id 
	)
)

select * from invalidEntityParent