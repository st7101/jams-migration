

PRINT N'Starting rebuilding table [dbo].[RequestOrgUnitAttributeCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_RequestOrgUnitAttributeCache]
    (
      [Id] BIGINT NOT NULL ,
      [AttributeType_Value] FLOAT(53) NOT NULL ,
      [Request_Id] BIGINT NULL ,
      [OrgUnit_ExternalId] NVARCHAR(100) NULL ,
      [OrgUnit_Id] BIGINT NULL ,
      [AttributeType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_RequestOrgUnitAttributeCache] PRIMARY KEY CLUSTERED
        ( [Id] ASC ) WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[RequestOrgUnitAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_RequestOrgUnitAttributeCache] WITH (TABLOCK)
                ( [Id] ,
                  [AttributeType_Value] ,
                  [Request_Id] ,
                  [OrgUnit_ExternalId] ,
                  [AttributeType_Id]
                )
                SELECT  [Id] ,
                        [AttributeType_Value] ,
                        [Request_Id] ,
                        [OrgUnit_Id] ,
                        [AttributeType_Id]
                FROM    [dbo].[RequestOrgUnitAttributeCache]
                ORDER BY [Id] ASC;
    END
DrOP TABLE [dbo].[RequestOrgUnitAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_RequestOrgUnitAttributeCache]',
    N'RequestOrgUnitAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_RequestOrgUnitAttributeCache]',
    N'PK_RequestOrgUnitAttributeCache', N'OBJECT';
