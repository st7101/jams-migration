
CREATE TABLE dbo.EntityIDMappingHistory
	(
	EntityIDMapping_TypeID int NOT NULL ,
	EntityIDMapping_InternalID BIGINT NOT NULL,
	EntityIDMapping_OldClientID NVARCHAR(100) NULL,
	EntityIDMapping_NewClientID NVARCHAR(100) NULL,
	EntityIDMapping_OldEffectiveDate DATETIME NOT NULL DEFAULT GETDATE(),
	EntityIDMapping_NewEffectiveDate DATETIME NOT NULL DEFAULT GETDATE(),
	EntityIDMapping_CreationDate DATETIME NOT NULL DEFAULT GETDATE(),
	EntityIDMapping_UpdateDate DATETIME NOT NULL DEFAULT GETDATE(),
	CONSTRAINT PK_EntityIDMappingHistory PRIMARY KEY CLUSTERED 
	(
	EntityIDMapping_TypeID,EntityIDMapping_InternalID,EntityIDMapping_UpdateDate
	) WITH( STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON,DATA_COMPRESSION=PAGE)
	)  ON PS_EntityType(EntityIDMapping_TypeID)

ALTER TABLE dbo.EntityIDMappingHistory ADD CONSTRAINT
	FK_EntityIDMappingHistory_EntityType FOREIGN KEY
	(
	EntityIDMapping_TypeID
	) REFERENCES dbo.EntityType
	(
	EntityType_ID
	) ON UPDATE  NO ACTION 
	 ON DELETE  NO ACTION 
	
