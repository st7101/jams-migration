INSERT INTO [dbo].[AlignmentEntitys]
           ([AlignmentEntity_Id]
           ,[AlignmentEntity_ExternalId]
           ,[AlignmentEntity_Name]
           ,[AlignmentEntity_Status]
           ,[AlignmentEntity_ParentExternalId]
           ,[AlignmentEntityType_Id])
SELECT 
ROW_NUMBER() OVER (ORDER BY ALIGNMENTENTITY_ID ASC),ALIGNMENTENTITY_ID,ALIGNMENTENTITY_NAME
, 'Normal',ALIGNMENTENTITY_PARENTID,ALIGNMENTENTITYTYPE_ID
FROM dbo.AlignmentEntitys_Temp

UPDATE ae
SET AlignmentEntity_ParentId =aeparent.AlignmentEntity_Id
FROM dbo.AlignmentEntitys ae
INNER JOIN dbo.AlignmentEntitys aeParent ON ae.[AlignmentEntity_ParentExternalId] =aeparent.AlignmentEntity_ExternalId
WHERE ae.AlignmentEntity_ParentExternalId IS NOT NULL

