-- get explicit alignments
select 
	alignments.AlignmentEntity_Id as AlignmentEntity_Id, 
	alignments.TerritoryType_Id as TerritoryType_Id,
	alignments.OrgUnit_Id as OrgUnit_Id, 
	alignments.OrgUnitAlignmentEntity_Weight as OrgUnitAlignmentEntity_Weight
		
from [{0}].dbo.OrgUnitAlignmentEntitys alignments
join [{0}].dbo.AlignmentEntitys entitys on alignments.AlignmentEntity_Id = entitys.AlignmentEntity_Id
where entitys.AlignmentEntity_Status = 'Normal'

union all

-- get derived alignments
select 
	alignments.AlignmentEntity_Id as AlignmentEntity_Id, 
	alignments.TerritoryType_Id as TerritoryType_Id, 
	alignments.OrgUnit_Id as OrgUnit_Id, 
	alignments.OrgUnitAlignmentEntity_Weight as OrgUnitAlignmentEntity_Weight
		
from [{0}].dbo.OrgUnitAlignmentEntitys alignments
inner join [{0}].dbo.AlignmentEntitys parents on alignments.AlignmentEntity_Id = parents.AlignmentEntity_Id

INNER  JOIN [{0}].dbo.AlignmentEntitys entitys on parents.alignmentEntity_Id = entitys.alignmentEntity_ParentId
where entitys.AlignmentEntity_Status = 'Normal'
	and parents.AlignmentEntity_Status = 'Normal';