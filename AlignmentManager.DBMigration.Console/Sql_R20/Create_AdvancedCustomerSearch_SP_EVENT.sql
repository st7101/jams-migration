-- =============================================
-- Author:		Steve Tetrault
-- Create date: Jun. 17, 2014
-- Description:	Advanced Customer Search 
--				Replacement for: AdvancedCustomerSearchQuery.sql
-- @spanRestriction		-1: EntireFieldForce
--						0: OutOfSpanOfControl
--						1: WithinSpanOfControl
-- =============================================
CREATE PROCEDURE [dbo].[AdvancedCustomerSearch]
	-- Add the parameters for the stored procedure here
    @territoryTypeId BIGINT ,
    @aeStatus NVARCHAR(255) = NULL ,
    @PageSize INT ,
    @GroupedEntityTypeIds VARCHAR(MAX) ,
    @IndependantEntityTypeIds VARCHAR(MAX) ,
    @SearchString NVARCHAR(100) = NULL ,
    @totalNumberOfRowsShown INT ,
    @spanRestriction INT ,
    @sortStatement NVARCHAR(MAX) = NULL ,
    @lastShownRow INT = 0 ,
    @debug BIT = 0 ,
    @treeLeft INT ,
    @treeRight INT
AS
    BEGIN

        DECLARE @sql NVARCHAR(MAX)= '
		WITH independentCustomer AS
		(
		SELECT DISTINCT TOP ' + CAST(@totalNumberOfRowsShown AS NVARCHAR) + '
				 ae.AlignmentEntity_ExternalId   AS Id,
                 ae.AlignmentEntity_Name   AS Name,
				 null AS ParentId
		FROM AlignmentEntitys ae
		inner join OrgUnitAlignmentEntitys oae
			ON ae.AlignmentEntity_Id = oae.AlignmentEntity_id 
			and oae.TerritoryType_Id =' + CAST(@territoryTypeId AS NVARCHAR)
            + CHAR(13)
        IF ( @spanRestriction = 1 )
            BEGIN
                SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and (( ou.OrgUnit_TreeLeft >= ' + CAST(@treeLeft AS NVARCHAR)
                    + ' and ou.OrgUnit_TreeRight <= '
                    + CAST(@treeRight AS NVARCHAR) + ') 
		or ou.OrgUnit_IsUnassigned = 1) 
		'
            END
        ELSE
            IF ( @spanRestriction = 0 )
                BEGIN
                    SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and (( ou.OrgUnit_TreeLeft < ' + CAST(@treeLeft AS NVARCHAR)
                        + ' or ou.OrgUnit_TreeRight > '
                        + CAST(@treeRight AS NVARCHAR) + ') 
		AND ou.OrgUnit_IsUnassigned = 1)' 

                END	
	

        SET @sql = @sql + '
	WHERE ae.AlignmentEntityType_Id in ( '
            + CAST(@IndependantEntityTypeIds AS NVARCHAR) + ')' + CHAR(13)
        IF ( @SearchString IS NOT NULL
             AND @SearchString != ''
           )
            BEGIN
                SET @sql = @sql + '	and (LEFT(ae.AlignmentEntity_Name,'
                    + CAST(LEN(@SearchString) AS NVARCHAR) + ') = '''
                    + @SearchString
                    + ''' or LEFT(ae.AlignmentEntity_ExternalId,'
                    + CAST(LEN(@SearchString) AS NVARCHAR) + ') = '''
                    + @SearchString + ''')' + CHAR(13)
            END
        IF ( @aeStatus IS NOT NULL
             AND @aeStatus != ''
           )
            BEGIN
                SET @sql = @sql + ' and	ae.AlignmentEntity_Status = '''
                    + @aeStatus + ''''
            END

        SET @sql = @sql + ' ),
groupedCustomer AS
(
SELECT DISTINCT TOP ' + CAST(@totalNumberOfRowsShown AS NVARCHAR) + '
				 ae.AlignmentEntity_ExternalId   AS Id,
                 ae.AlignmentEntity_Name   AS Name,
				 ae.AlignmentEntity_ParentId AS ParentId
		FROM GroupedEntities ge
		inner join AlignmentEntitys ae 
			ON ge.TerritoryType_Id =' + CAST(@territoryTypeId AS NVARCHAR) + '
			AND ae.AlignmentEntity_Id = ge.AlignmentEntity_id
		inner join OrgUnitAlignmentEntitys oae
			ON oae.TerritoryType_Id =' + CAST(@territoryTypeId AS NVARCHAR)
            + ' 
			AND ae.AlignmentEntity_ParentId = oae.AlignmentEntity_id 
			'

        IF ( @spanRestriction = 1 )
            BEGIN
                SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and (( ou.OrgUnit_TreeLeft >= ' + CAST(@treeLeft AS NVARCHAR)
                    + ' and ou.OrgUnit_TreeRight <= '
                    + CAST(@treeRight AS NVARCHAR) + ') 
		or ou.OrgUnit_IsUnassigned = 1) 
		'
            END
        ELSE
            IF ( @spanRestriction = 0 )
                BEGIN
                    SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and (( ou.OrgUnit_TreeLeft < ' + CAST(@treeLeft AS NVARCHAR)
                        + ' or ou.OrgUnit_TreeRight > '
                        + CAST(@treeRight AS NVARCHAR) + ') 
		AND ou.OrgUnit_IsUnassigned = 1)' 

                END	

        SET @sql = @sql + '
	WHERE ae.AlignmentEntityType_Id in ( '
            + CAST(@GroupedEntityTypeIds AS NVARCHAR) + ')' + CHAR(13)
        IF ( @SearchString IS NOT NULL
             AND @SearchString != ''
           )
            BEGIN
                SET @sql = @sql + '	and (LEFT(ae.AlignmentEntity_Name,'
                    + CAST(LEN(@SearchString) AS NVARCHAR) + ') = '''
                    + @SearchString
                    + ''' or LEFT(ae.AlignmentEntity_ExternalId,'
                    + CAST(LEN(@SearchString) AS NVARCHAR) + ') = '''
                    + @SearchString + ''')' + CHAR(13)
            END
        IF ( @aeStatus IS NOT NULL
             AND @aeStatus != ''
           )
            BEGIN
                SET @sql = @sql + ' and	ae.AlignmentEntity_Status = '''
                    + @aeStatus + ''''
            END

        SET @sql = @sql + ' )
SELECT TOP (' + CAST(@pageSize AS NVARCHAR)
            + ') customers.Id, customers.Name ,ROW_NUMBER() OVER(ORDER BY  Id ASC ) AS SortRow
		FROM   (SELECT  Id, Name,	ROW_NUMBER() OVER(ORDER BY '
        IF ( @sortStatement IS NOT NULL
             AND @sortStatement != ''
           )
            BEGIN
                SET @sql = @sql + @sortStatement + ') AS SortRow '
            END
        ELSE
            BEGIN
                SET @sql = @sql + 'Id) AS SortRow '

            END
        SET @sql = @sql + '		FROM   (SELECT  Id, Name
				FROM   independentCustomer
				UNION ALL
				SELECT  Id, Name
				FROM   groupedCustomer) AS combinedCustomers ) AS customers
				WHERE SortRow > ' + CAST(@lastShownRow AS NVARCHAR) + '
ORDER  BY SortRow ASC
'
        EXEC (@sql)

        IF ( @debug = 1 )
            SELECT  @sql
    END

