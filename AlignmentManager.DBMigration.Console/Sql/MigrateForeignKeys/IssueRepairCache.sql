
PRINT N'Updating IssueRepairCache ...';

UPDATE irc
SET AlignmentEntity_Id=ae.AlignmentEntity_Id,TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.IssueRepairCache irc INNER JOIN 
dbo.AlignmentEntitys ae ON irc.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON irc.TerritoryType_ExternalID=tt.TerritoryType_ExternalID

