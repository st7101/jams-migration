﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMSScripts.TestHarness
{
    class Program
    {
        static void Main(string[] args)
        {
            string[] arg = new string[] { "Server=sa-qsql08.zssd.local;Database=QA_AlignmentManager_0012e_Event_31700;Trusted_Connection=True;", @"C:\Users\st7101\Documents\GitHub\DBMigration\JAMSScripts\JAMSDBScripts\Done\Query" ,
                "Server=sa-qsql08.zssd.local;Database=QA_AlignmentManager_0012e_Event_31700_original;Trusted_Connection=True;", @"C:\Users\st7101\Documents\GitHub\DBMigration\JAMSScripts\JAMSDBScripts\Original\Query" };
            using (var scriptTest = new TestHarness(arg))
                scriptTest.DocumentScripts();
                
        }
    }
}
