

PRINT N'Starting rebuilding table [dbo].[AlignmentEntityStates]...';

CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityStates] (
    [AlignmentEntityState_Id]              BIGINT         NOT NULL,
    [AlignmentEntityState_Name]            NVARCHAR (255) NULL,
    [AlignmentEntityState_ParentExternalId]        NVARCHAR (255) NULL,
    [AlignmentEntityState_ParentId] BIGINT         NULL,
    [AlignmentEntityState_Address1]        NVARCHAR (255) NULL,
    [AlignmentEntityState_Address2]        NVARCHAR (255) NULL,
    [AlignmentEntityState_City]            NVARCHAR (255) NULL,
    [AlignmentEntityState_State]           NVARCHAR (255) NULL,
    [AlignmentEntityState_Country]         NVARCHAR (255) NULL,
    [AlignmentEntityState_Latitude]        FLOAT (53)     NULL,
    [AlignmentEntityState_Longitude]       FLOAT (53)     NULL,
    [AlignmentEntityState_PostalCode]      NVARCHAR (255) NULL,
    [AlignmentEntityState_Quality]         INT            NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityStates] PRIMARY KEY CLUSTERED ([AlignmentEntityState_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[AlignmentEntityStates])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_AlignmentEntityStates] WITH (TABLOCK) ([AlignmentEntityState_Id], [AlignmentEntityState_Name], [AlignmentEntityState_ParentId], [AlignmentEntityState_Address1], [AlignmentEntityState_Address2], [AlignmentEntityState_City], [AlignmentEntityState_State], [AlignmentEntityState_Country], [AlignmentEntityState_Latitude], [AlignmentEntityState_Longitude], [AlignmentEntityState_PostalCode], [AlignmentEntityState_Quality])
        SELECT   [AlignmentEntityState_Id],
                 [AlignmentEntityState_Name],
                 [AlignmentEntityState_ParentId],
                 [AlignmentEntityState_Address1],
                 [AlignmentEntityState_Address2],
                 [AlignmentEntityState_City],
                 [AlignmentEntityState_State],
                 [AlignmentEntityState_Country],
                 [AlignmentEntityState_Latitude],
                 [AlignmentEntityState_Longitude],
                 [AlignmentEntityState_PostalCode],
                 [AlignmentEntityState_Quality]
        FROM     [dbo].[AlignmentEntityStates]
        ORDER BY [AlignmentEntityState_Id] ASC;
    END

DROP TABLE [dbo].[AlignmentEntityStates];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityStates]', N'AlignmentEntityStates';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityStates]', N'PK_AlignmentEntityStates', N'OBJECT';
