/******** UPDATE OrgUnitAlignmentEntitys TO REPLACE NULL WEIGHTS WITH 100 FOR ENTITIES WITH TOLERATE PARTIAL ALIGNMENT SET AS FALSE *****************************/
UPDATE OrgUnitAlignmentEntitys SET OrgUnitAlignmentEntity_Weight = 100 WHERE OrgUnitAlignmentEntity_Id IN 
(SELECT OUAE.OrgUnitAlignmentEntity_Id
FROM OrgUnitAlignmentEntitys OUAE
INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OUAE.OrgUnit_id
INNER JOIN TerritoryTypes TT ON TT.OrgUnitLevel_id = OU.OrgUnitLevel_id
INNER JOIN AlignmentEntitys AE ON OUAE.AlignmentEntity_id = AE.AlignmentEntity_Id
INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND TT.TerritoryType_Id = AETTT.TerritoryType_id
WHERE AETTT.ToleratePartialAlignment = 0 AND OUAE.OrgUnitAlignmentEntity_Weight IS NULL)

/******** UPDATE OrgUnitAlignmentEntitys TO REPLACE NULL WEIGHTS WITH 0 FOR ENTITIES (WITH SPLIT ASSIGNMENT) WITH TOLERATE PARTIAL ALIGNMENT SET AS TRUE *****************************/
UPDATE OrgUnitAlignmentEntitys SET OrgUnitAlignmentEntity_Weight = 0 WHERE OrgUnitAlignmentEntity_Id IN 
(SELECT OUAE.OrgUnitAlignmentEntity_Id
FROM OrgUnitAlignmentEntitys OUAE
INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OUAE.OrgUnit_id
INNER JOIN TerritoryTypes TT ON TT.OrgUnitLevel_id = OU.OrgUnitLevel_id
INNER JOIN AlignmentEntitys AE ON OUAE.AlignmentEntity_id = AE.AlignmentEntity_Id
INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND TT.TerritoryType_Id = AETTT.TerritoryType_id
WHERE AETTT.ToleratePartialAlignment = 1 AND OUAE.OrgUnitAlignmentEntity_Weight IS NULL)

/******** UPDATING NULL WEIGHTS FOR ENTITIES WITH SINGLE ALIGNMENT TO 100.**********/
UPDATE OrgUnitAlignmentEntitys SET OrgUnitAlignmentEntity_Weight = 100 WHERE OrgUnitAlignmentEntity_Id IN
(SELECT OUAE.OrgUnitAlignmentEntity_Id FROM OrgUnitAlignmentEntitys OUAE 
INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OUAE.OrgUnit_id
INNER JOIN TerritoryTypes TT ON TT.OrgUnitLevel_id = OU.OrgUnitLevel_id
INNER JOIN (SELECT OUAE.AlignmentEntity_id AS aeId, TT.TerritoryType_Id AS ttId
FROM OrgUnitAlignmentEntitys OUAE
INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OUAE.OrgUnit_id
INNER JOIN TerritoryTypes TT ON TT.OrgUnitLevel_id = OU.OrgUnitLevel_id
GROUP BY OUAE.AlignmentEntity_id, TT.TerritoryType_Id
HAVING COUNT(*) = 1) TEMP
ON TEMP.aeId = OUAE.AlignmentEntity_id and TEMP.ttId = TT.TerritoryType_Id WHERE
OUAE.OrgUnitAlignmentEntity_Weight IS NULL)

/******** DELETING ROWS WITH INVALID (0 OR NEGATIVE) WEIGHTS FOR ENTITIES WITH TOLERATE PARTIAL ALIGNMENT SET AS FALSE **********/
DELETE FROM OrgUnitAlignmentEntitys WHERE OrgUnitAlignmentEntity_Id IN 
(SELECT OUAE.OrgUnitAlignmentEntity_Id
FROM OrgUnitAlignmentEntitys OUAE
INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OUAE.OrgUnit_id
INNER JOIN TerritoryTypes TT ON TT.OrgUnitLevel_id = OU.OrgUnitLevel_id
INNER JOIN AlignmentEntitys AE ON OUAE.AlignmentEntity_id = AE.AlignmentEntity_Id
INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND TT.TerritoryType_Id = AETTT.TerritoryType_id
WHERE AETTT.ToleratePartialAlignment = 0 AND OUAE.OrgUnitAlignmentEntity_Weight <= 0)

/********* GENERATE TEMPORARY TABLE REPLICATING OrgUnitAlignmentEntitys WITH TerritoryType_Id COLUMN ********************/
 SELECT * INTO #TempTableWithTerritoryType FROM(
	  SELECT OAE.[AlignmentEntity_Id],OAE.[OrgUnitAlignmentEntity_Weight],OU.OrgUnit_Id, TT.TerritoryType_Id
	  FROM [OrgUnitAlignmentEntitys] OAE
	  INNER JOIN [OrgUnits] OU ON OU.OrgUnit_Id = OAE.OrgUnit_Id
	  INNER JOIN [TerritoryTypes] TT ON OU.OrgUnitLevel_Id = TT.OrgUnitLevel_Id
	  INNER JOIN [AlignmentEntitys] AE ON OAE.AlignmentEntity_id = AE.AlignmentEntity_Id
	  INNER JOIN [AlignmentEntityTypeTerritoryTypes] AETTT ON 
	  AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND TT.TerritoryType_Id = AETTT.TerritoryType_id
	  WHERE AETTT.ToleratePartialAlignment = 0)
	  AS T

/********** GENERATE ANOTHER TEMPORARY TABLE STORING FACTOR TO BE MULTIPLIED TO EACH WEIGHT **************/
  SELECT T.[AlignmentEntity_Id] AS AlignmentEntity_Id,
  100/SUM(T.[OrgUnitAlignmentEntity_Weight]) AS Factor, 
  T.[TerritoryType_Id] AS TerritoryType_Id
  INTO #TempTableWithFactor
  FROM #TempTableWithTerritoryType T
  GROUP BY T.[TerritoryType_Id], T.[AlignmentEntity_Id]
  HAVING SUM(T.[OrgUnitAlignmentEntity_Weight]) <> 100   

/************ SKIP NORMALIZATION IF ALL RECORDS SUM TO 100****************/
 IF EXISTS ( SELECT * FROM #TempTableWithFactor)
 BEGIN
	/************ UPDATE PRIMARY TEMPORARY TABLE WEIGHT COLUMN BY MULTIPLYING THE FACTOR FROM SECOND TEMPORAY TABLE****************/
	UPDATE #TempTableWithTerritoryType
	SET OrgUnitAlignmentEntity_Weight = CAST((OrgUnitAlignmentEntity_Weight * T2.Factor) AS DECIMAL(5,2))
	FROM #TempTableWithTerritoryType T1 
	INNER JOIN #TempTableWithFactor T2 
	ON T1.TerritoryType_Id = T2.TerritoryType_Id 
	AND T1.AlignmentEntity_Id = T2.AlignmentEntity_Id
	  
	/************* FIX WEIGHTS NOT SUMING TO 100 *********************/
	UPDATE #TempTableWithTerritoryType
	SET OrgUnitAlignmentEntity_Weight = OrgUnitAlignmentEntity_Weight + T2.Add_Factor
	FROM 
	#TempTableWithTerritoryType T1
	INNER JOIN 
	(SELECT T.[AlignmentEntity_Id] AS AlignmentEntity_Id,
	MAX(T.[OrgUnit_Id]) AS OrgUnit_Id,
	(100 - SUM(T.[OrgUnitAlignmentEntity_Weight])) AS Add_Factor
	FROM #TempTableWithTerritoryType T
	GROUP BY T.[TerritoryType_Id], T.[AlignmentEntity_Id]
	HAVING SUM(T.[OrgUnitAlignmentEntity_Weight]) <> 100) T2		-- THIS TABLE FINDS OUT Alignmententity_Id AND OrgUnit_Id PAIR WITH WEIGHT TO BE ADDED TO MAKE THE SUM 100
	ON T1.[OrgUnit_Id] = T2.[OrgUnit_Id] 
	AND T1.AlignmentEntity_Id = T2.AlignmentEntity_Id
	  
	/************ MERGE THE UPDATED WEIGHTS TO ACTUAL OrgUnitAlignmentEntitys TABLE ******************/
	MERGE OrgUnitAlignmentEntitys AS TARGET
		USING (SELECT AlignmentEntity_Id, OrgUnit_Id, OrgUnitAlignmentEntity_Weight 
			FROM #TempTableWithTerritoryType) AS SOURCE
		ON TARGET.AlignmentEntity_Id = SOURCE.AlignmentEntity_Id AND TARGET.OrgUnit_Id = SOURCE.OrgUnit_Id
		WHEN MATCHED THEN
			UPDATE SET TARGET.OrgUnitAlignmentEntity_Weight = SOURCE.OrgUnitAlignmentEntity_Weight;
            
END
            
/************* DROP TEMPORARY TABLES********/  
DROP TABLE #TempTableWithTerritoryType
DROP TABLE #TempTableWithFactor