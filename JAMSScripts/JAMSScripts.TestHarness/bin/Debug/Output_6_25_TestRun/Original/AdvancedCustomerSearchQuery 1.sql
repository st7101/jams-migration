WITH independentCustomer AS
(
	SELECT DISTINCT TOP(20) ae.AlignmentEntity_Id        AS Id,
		   ae.AlignmentEntity_Name      AS Name,
		   null AS ParentId
		FROM AlignmentEntitys ae
		inner join OrgUnitAlignmentEntitys oae
			ON ae.AlignmentEntity_Id = oae.AlignmentEntity_id
		inner join OrgUnits ou
			ON oae.OrgUnit_id = ou.OrgUnit_Id
			
		inner join TerritoryTypes tt
			ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
			and tt.TerritoryType_Id = '157'
		WHERE ae.AlignmentEntity_Status = 'Normal'
			and ae.AlignmentEntityType_Id in (1,2)
			and (ae.AlignmentEntity_Name like 'o' +'%' escape '\' 
			or ae.AlignmentEntity_Id like 'o' +'%' escape '\')
		ORDER BY  Id ASC 
),
groupedCustomer AS
(
	SELECT DISTINCT TOP(20) ae.AlignmentEntity_Id        AS Id,
		   ae.AlignmentEntity_Name      AS Name,
		   ae.AlignmentEntity_ParentId AS ParentId
		FROM GroupedEntities ge
		inner join AlignmentEntitys ae 
			ON ae.AlignmentEntity_Id = ge.AlignmentEntity_id
		inner join OrgUnitAlignmentEntitys oae
			ON ae.AlignmentEntity_ParentId = oae.AlignmentEntity_id
		inner join OrgUnits ou 
			ON oae.OrgUnit_id = ou.OrgUnit_Id
			
		inner join TerritoryTypes tt 
			ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
			and tt.TerritoryType_Id = '157'
		WHERE ae.AlignmentEntity_Status = 'Normal'
			and ae.AlignmentEntityType_Id in (1,2)
			and (ae.AlignmentEntity_Name like 'o' +'%' escape '\' 
			or ae.AlignmentEntity_Id like 'o' +'%' escape '\')
			and ge.TerritoryType_id = '157'
		ORDER BY  Id ASC 
)
	

SELECT TOP (20) customers.Id, customers.Name 
FROM   (SELECT Id, Name,
			   ROW_NUMBER() OVER(ORDER BY  Id ASC ) AS SortRow
		FROM   (SELECT  Id, Name
				FROM   independentCustomer
				UNION ALL
				SELECT  Id, Name
				FROM   groupedCustomer) AS combinedCustomers) AS customers
WHERE  customers.SortRow > 5
ORDER  BY customers.SortRow ASC


OPTION (RECOMPILE)