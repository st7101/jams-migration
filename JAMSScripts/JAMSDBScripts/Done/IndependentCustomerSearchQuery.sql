/*
Handle Parameters that the script has which are not required
:alignmentEntityTypeIds
*/
EXECUTE  [dbo].IndependantCustomerSearchQuery 
   @showAssigned=:showAssigned
  ,@showUnassigned=:showUnassigned
  ,@treeLeft= :treeLeft
  ,@treeRight= :treeRight
  ,@territoryTypeId=:terrTypeId
  ,@aeStatus=:aeStatus
  ,@MaxResult=:maxResult
  ,@GeoEntityTypeId=:geoEntityTypeId
  ,@alignmentEntityTypeIds=:alignmentEntityTypeIds
  ,@SearchString=:searchString


