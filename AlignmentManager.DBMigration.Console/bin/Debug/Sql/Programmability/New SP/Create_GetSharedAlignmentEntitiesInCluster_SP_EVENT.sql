IF (OBJECT_ID('[GetSharedAlignmentEntitiesInCluster]') IS NOT NULL)
  DROP PROCEDURE [GetSharedAlignmentEntitiesInCluster]
GO
-- =============================================
-- Author:		Steve Tetrault
-- Create date: Jun. 17, 2014
-- Description:	Modified Logic for GetSharedAlignmentEntitiesInCluster SP
-- =============================================
CREATE PROCEDURE [dbo].[GetSharedAlignmentEntitiesInCluster]
	-- Add the parameters for the stored procedure here
    @minLat FLOAT ,
    @maxLat FLOAT ,
    @minLng FLOAT ,
    @maxLng FLOAT ,
    @territoryTypeId BIGINT ,
    @aeStatus NVARCHAR(255) = NULL ,
    @IsGeo BIT = 1 ,
    @OrgUnitId BIGINT
AS
    BEGIN

        DECLARE @sql NVARCHAR(MAX)= '
WITH SharedEntities AS
(
	SELECT oae.AlignmentEntity_id
	FROM  [OrgUnitAlignmentEntitys] oae
	inner join AlignmentEntitys ae
	ON ae.AlignmentEntity_Id = oae.AlignmentEntity_id
	and oae.TerritoryType_Id = ' + CAST(@territoryTypeId AS VARCHAR)
            + ' 
	where AlignmentEntity_Latitude is not null and ae.AlignmentEntity_Latitude <> 0
	 and ae.AlignmentEntity_Longitude <> 0 and ae.AlignmentEntity_Longitude Is NOT NULL
	and ae.AlignmentEntity_Longitude between ' + CAST(@minLng AS VARCHAR)
            + ' and ' + CAST(@maxLng AS VARCHAR) + '
	and ae.AlignmentEntity_Latitude between ' + CAST(@minLat AS VARCHAR)
            + ' and ' + CAST(@maxLat AS VARCHAR) + CHAR(13)
        IF ( @isgeo = 1 )
            BEGIN
                SET @sql = @sql + 'and (ae.AlignmentEntityType_Id = 1)'
            END
        ELSE
            BEGIN
                SET @sql = @sql + 'and (ae.AlignmentEntityType_Id <> 1)'
            END
        IF ( @aeStatus IS NOT NULL
             AND @aeStatus != ''
           )
            BEGIN
                SET @sql = @sql + ' and	ae.AlignmentEntity_Status = '''
                    + @aeStatus + ''''
            END
        SET @sql = @sql
            + '

	GROUP BY oae.AlignmentEntity_id
	HAVING COUNT(oae.AlignmentEntity_id) >1
)

SELECT AlignmentEntity_Id        AS Id,
	   AlignmentEntity_Name      AS Name,
	   AlignmentEntity_Latitude  AS Latitude,
	   AlignmentEntity_Longitude AS Longitude
FROM   (
		SELECT [oae].AlignmentEntity_Id,
			   [oae].AlignmentEntity_Name,
			   [oae].AlignmentEntity_Latitude,
			   [oae].AlignmentEntity_Longitude,
			   Left(oae.orgUnitsList,Len(oae.orgUnitsList)-1) AS orgUnitIds
		FROM   
		(
			SELECT distinct [ae].AlignmentEntity_Id,
							[ae].AlignmentEntity_Name,
							[ae].AlignmentEntity_Latitude,
							[ae].AlignmentEntity_Longitude,
							(SELECT CAST(oae1.OrgUnit_id AS VARCHAR) + '','' AS [text()]
								FROM   OrgUnitAlignmentEntitys oae1
								WHERE  oae1.TerritoryType_Id = '
            + CAST(@territoryTypeId AS VARCHAR)
            + ' AND oae1.AlignmentEntity_id = oae2.AlignmentEntity_id
								ORDER  BY oae1.OrgUnit_Id
								For XML PATH ('''')
							) [orgUnitsList]
			FROM  OrgUnitAlignmentEntitys AS oae2
			inner join AlignmentEntitys as ae
			on ae.AlignmentEntity_Id = oae2.AlignmentEntity_id
			inner join SharedEntities se
			on se.AlignmentEntity_Id = oae2.AlignmentEntity_id 
		) [oae]
	) AS EntitiesWithOrgunits   
WHERE  orgUnitIds = ' + CAST(@OrgUnitId AS VARCHAR)
        EXEC (@sql)
    END

