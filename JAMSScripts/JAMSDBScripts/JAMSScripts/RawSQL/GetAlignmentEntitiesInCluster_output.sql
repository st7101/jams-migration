/*This query gets the cluster details for non-shared entities. */

SELECT oae.AlignmentEntity_Id        AS Id,
	   Min(ae.AlignmentEntity_Name)      AS Name,
	   Min(ae.AlignmentEntity_Latitude)  AS Latitude,
	   Min(ae.AlignmentEntity_Longitude) AS Longitude
FROM   
		OrgUnitAlignmentEntitys AS oae
		inner join AlignmentEntitys as ae
		on ae.AlignmentEntity_Id = oae.AlignmentEntity_id
		inner join [OrgUnits] ou
		ON ou.OrgUnit_Id = oae.OrgUnit_id
		inner join [TerritoryTypes] tt
		ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id
		and tt.TerritoryType_Id = 6
		and ae.AlignmentEntity_Longitude between -73.4377403259277 and -66.9500045776367
		and ae.AlignmentEntity_Latitude between 42.6969776153564 and 47.4596862792969
		and ((False = 1 and ae.AlignmentEntityType_Id = 1)	or (False = 0 and ae.AlignmentEntityType_Id <> 1))
		and ae.AlignmentEntity_Status = 'Normal'
		and (ae.AlignmentEntity_Latitude <> 0 and ae.AlignmentEntity_Longitude <> 0)
		and (ae.AlignmentEntity_Latitude Is NOT NULL and ae.AlignmentEntity_Longitude Is NOT NULL)
GROUP BY oae.AlignmentEntity_Id
HAVING COUNT(oae.AlignmentEntity_id) = 1 
	and Min(oae.OrgUnit_id) =  2,2433,2434,2985,3484,4036,4590,2435,2491,2559,2618,2686,2747,2811,2872,2928,2986
