
PRINT N'Updating PositionPersonnelOverrides ...';

UPDATE ppo
SET [PositionPersonnelOverride_PositionId]=p.Position_Id
FROM dbo.PositionPersonnelOverrides ppo INNER JOIN 
dbo.Positions p ON ppo.[PositionPersonnelOverride_ExternalPositionId]=p.Position_ExternalID

