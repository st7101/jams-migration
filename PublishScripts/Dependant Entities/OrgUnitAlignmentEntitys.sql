INSERT INTO [dbo].[OrgUnitAlignmentEntitys]
           ([OrgUnitAlignmentEntity_Id]
           ,[OrgUnitAlignmentEntity_Weight]
           ,[AlignmentEntity_Id]
           ,[AlignmentEntity_ExternalId]
           ,[OrgUnit_Id]
           ,[OrgUnit_ExternalId])
 SELECT 
	ROW_NUMBER() OVER (ORDER BY ouaeT.ALIGNMENTENTITY_ID,ouaeT.ORGUNIT_ID),
	ouaeT.ORGUNITALIGNMENTENTITY_WEIGHT,
	ae.AlignmentEntity_Id,
	ouaeT.ALIGNMENTENTITY_ID,
	ou.OrgUnit_Id,
	ouaeT.ORGUNIT_ID
FROM dbo.OrgUnitAlignmentEntitys_Temp ouaeT
	INNER JOIN dbo.AlignmentEntitys ae 
		ON ouaet.ALIGNMENTENTITY_ID=ae.AlignmentEntity_ExternalId
	INNER JOIN dbo.OrgUnits ou 
		ON ouaeT.ORGUNIT_ID=ou.OrgUnit_ExternalId


