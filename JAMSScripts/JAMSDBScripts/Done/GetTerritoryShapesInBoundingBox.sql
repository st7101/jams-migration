
EXECUTE  [dbo].[GetTerritoryShapesInBoundingBox] 
@zoomLevel=:zoomLevel
  ,@ShapeWKB=:ShapeWKB
  ,@ShapeWKBType=:ShapeWKBType
  ,@tolerance=:reduceTolerance
  ,@territoryTypeId=:territoryTypeId
  ,@showAssigned=:showAssigned
  ,@showUnassigned=:showUnassigned
  ,@TreeLeft=:treeLeft
  ,@treeRight=:treeRight
