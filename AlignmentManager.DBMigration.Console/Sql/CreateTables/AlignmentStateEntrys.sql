
PRINT N'Starting rebuilding table [dbo].[AlignmentStateEntrys]...';

CREATE TABLE [dbo].[tmp_ms_xx_AlignmentStateEntrys] (
    [AlignmentStateEntry_Id]               BIGINT         NOT NULL,
    [AlignmentStateEntry_ExternalOrgUnitId]        NVARCHAR (255) NULL,
    [AlignmentStateEntry_OrgUnitId] BIGINT         NULL,
    [AlignmentStateEntry_Weight]           DECIMAL (5, 2) NULL,
    [AlignmentState_Id]                    BIGINT         NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentStateEntrys] PRIMARY KEY CLUSTERED ([AlignmentStateEntry_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[AlignmentStateEntrys])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_AlignmentStateEntrys] WITH (TABLOCK) ([AlignmentStateEntry_Id], [AlignmentStateEntry_ExternalOrgUnitId], [AlignmentStateEntry_Weight], [AlignmentState_Id])
        SELECT   [AlignmentStateEntry_Id],
                 [AlignmentStateEntry_OrgUnitId],
                 [AlignmentStateEntry_Weight],
                 [AlignmentState_Id]
        FROM     [dbo].[AlignmentStateEntrys]
        ORDER BY [AlignmentStateEntry_Id] ASC;
    END

DROP TABLE [dbo].[AlignmentStateEntrys];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentStateEntrys]', N'AlignmentStateEntrys';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentStateEntrys]', N'PK_AlignmentStateEntrys', N'OBJECT';
