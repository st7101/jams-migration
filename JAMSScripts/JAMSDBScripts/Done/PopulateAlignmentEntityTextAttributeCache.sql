﻿/* DROP EXISTING AlignmentEntityTextAttributeCache TABLE */
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'AlignmentEntityTextAttributeCache') 
DROP TABLE [AlignmentEntityTextAttributeCache]

/* DROP EXISTING TempAlignmentEntityTextAttributeCache TABLE */
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempAlignmentEntityTextAttributeCache') 
DROP TABLE [TempAlignmentEntityTextAttributeCache]

/* CREATE TEMPORARY TABLE */
CREATE TABLE [TempAlignmentEntityTextAttributeCache] 
( AlignmentEntityTextAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
AlignmentEntityTextAttributeCache_Value nvarchar(2000) NULL ,
AlignmentEntity_id BIGINT NULL , 
AttributeType_id bigint NULL , 
CONSTRAINT PK_TempAlignmentEntityTextAttributeCache_AlignmentEntityTextAttributeCache_Id 
PRIMARY KEY CLUSTERED (AlignmentEntityTextAttributeCache_Id))

/* Populate Temporary table */
DECLARE @attributeTypeId VARCHAR(200) -- Attribute Type Id
DECLARE @attributeTypeName VARCHAR(200) -- Attribute Type Name
DECLARE @sql NVARCHAR(MAX)

DECLARE Attribute_Cursor CURSOR FOR
SELECT AttributeType_Id, AttributeType_Name FROM [AttributeTypes]
WHERE AttributeType_Entity = 'AlignmentEntity' AND
	(AttributeType_DataType = 'Text')

OPEN Attribute_Cursor
FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @sql = N'
	INSERT INTO [TempAlignmentEntityTextAttributeCache] with(tablock)
	(AlignmentEntity_id, AttributeType_id, AlignmentEntityTextAttributeCache_Value) 
	(SELECT ae.AlignmentEntity_id AS AlignmentEntity_id,' + 
			@attributeTypeId + ' AS AttributeType_id, 
			ae.[' + @attributeTypeName + '] AS AlignmentEntityTextAttributeCache_Value
			FROM [AlignmentEntitys] ae
			WHERE EXISTS (Select 1 from [OrgUnitAlignmentEntitys] oae
				Where ae.AlignmentEntity_Id = oae.AlignmentEntity_id )
	);'

	exec sp_executesql @sql;

	FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName
END


CLOSE Attribute_Cursor   
DEALLOCATE Attribute_Cursor


/* POPULATE ORIGINAL TABLE */
CREATE TABLE [AlignmentEntityTextAttributeCache] 
( AlignmentEntityTextAttributeCache_Id bigint NOT NULL, 
AlignmentEntityTextAttributeCache_Value nvarchar(2000) NULL ,
AlignmentEntity_id BIGINT NULL , 
AttributeType_id INT NULL , 
CONSTRAINT PK_AlignmentEntityTextAttributeCache_AlignmentEntityTextAttributeCache_Id 
PRIMARY KEY CLUSTERED (AlignmentEntityTextAttributeCache_Id)WITH (DATA_COMPRESSION=PAGE));

INSERT INTO [AlignmentEntityTextAttributeCache] with(tablock) (AlignmentEntityTextAttributeCache_Id, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id)
SELECT AlignmentEntityTextAttributeCache_Id, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id FROM [TempAlignmentEntityTextAttributeCache]; 

CREATE NONCLUSTERED INDEX [IDX_ALIGNMENTENTITYTEXTATTRIBUTECACHE_AttributeTypeId] ON [dbo].[AlignmentEntityTextAttributeCache] ([AttributeType_Id] ASC) WITH ( ONLINE = OFF, DATA_COMPRESSION = PAGE) ON PS_AENAC(AttributeType_Id);
CREATE NONCLUSTERED INDEX [IDX_ALIGNMENTENTITYTEXTATTRIBUTECACHE_AlignmentEntityTextAttributeCacheValue_AlignmentEntityId_AttributeTypeId] ON [dbo].[AlignmentEntityTextAttributeCache] ([AlignmentEntityTextAttributeCache_Value] ASC,[AlignmentEntity_Id] ASC,[AttributeType_Id] ASC) WITH ( ONLINE = OFF, DATA_COMPRESSION = PAGE);
CREATE NONCLUSTERED INDEX [IDX_ALIGNMENTENTITYTEXTATTRIBUTECACHE_AlignmentEntityId_INC_AlignmentEntityTextAttributeCacheValue_AttributeTypeId] ON [dbo].[AlignmentEntityTextAttributeCache] ([AlignmentEntity_Id] ASC)  INCLUDE ([AlignmentEntityTextAttributeCache_Value],[AttributeType_Id])WITH ( ONLINE = OFF, DATA_COMPRESSION = PAGE);


/* Drop Temporary Table */
DROP TABLE [TempAlignmentEntityTextAttributeCache]

/* Insert object in HiLo table */

DELETE FROM HiLoUniqueKey WHERE ObjectType = 'AlignmentEntityTextAttributeCache';

INSERT INTO HiLoUniqueKey
(NextHi, ObjectType)
VALUES
((SELECT ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0)+1 
				FROM [AlignmentEntityTextAttributeCache]), 'AlignmentEntityTextAttributeCache');