﻿IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedOrgUnitIds') 
DROP TABLE [TempUpdatedOrgUnitIds];

CREATE TABLE [dbo].[TempUpdatedOrgUnitIds](
	[OrgUnit_Id] BIGINT NOT NULL PRIMARY KEY
) WITH (DATA_COMPRESSION=PAGE);


INSERT INTO TempUpdatedOrgUnitIds 
	SELECT DISTINCT ouae.OrgUnit_id 
	FROM TempUpdatedEntitys tue
	inner join OrgUnitAlignmentEntitys ouae
		ON ouae.AlignmentEntity_id = tue.AlignmentEntity_id
	inner join OrgUnits ou
		ON ou.OrgUnit_Id = ouae.OrgUnit_id