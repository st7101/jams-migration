DECLARE @TableName NVARCHAR(100) = REPLACE(':TableName','_Temp','') 

SET @TableName = REPLACE(@TableName,'[','')
SET @TableName = REPLACE(@TableName,']','')
DECLARE @ColumnName NVARCHAR(100) = @TableName
IF (@columnName LIKE '%[_]2%')
BEGIN
SET @columnName=SUBSTRING(@ColumnName,1,CHARINDEX('_',@columnName,1)-1)
SET @columnName=@columnName + SUBSTRING(@TableName,CHARINDEX('_',@TableName,1)+5,LEN (@TableName))
END
DECLARE @sqlCreate NVARCHAR(MAX) 

SET @sqlCreate='CREATE TABLE [dbo].['+@TableName+'](
	[ALIGNMENTENTITY_ID] [NVARCHAR](250) NOT NULL,
	['+@ColumnName+'] [NUMERIC](28, 10) NULL,
 CONSTRAINT [PK_'+@TableName+'] PRIMARY KEY CLUSTERED 
(
	[ALIGNMENTENTITY_ID] ASC
)
) ON [PRIMARY]'

EXEC (  @sqlCreate)

DECLARE @sqlInsert NVARCHAR(MAX)
SET @SqlInsert ='
INSERT INTO [dbo].['+@TableName+']
           ([ALIGNMENTENTITY_ID]
           ,['+@ColumnName+'])
SELECT 
	ae.AlignmentEntity_Id
	,mt.'+@ColumnName+'
FROM dbo.'+@TableName+'_Temp mt
INNER JOIN dbo.AlignmentEntitys ae 
ON mt.ALIGNMENTENTITY_ID=ae.AlignmentEntity_ExternalId
'
exec (@sqlInsert)

