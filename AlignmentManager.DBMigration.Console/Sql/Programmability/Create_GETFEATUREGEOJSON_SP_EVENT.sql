IF (OBJECT_ID('[GetAlignmentEntityClustersOfTypeInBoundingBox]') IS NOT NULL)
  DROP PROCEDURE [GetAlignmentEntityClustersOfTypeInBoundingBox]
DECLARE @sqlCommand NVARCHAR(MAX)='
-- =======================================
-- Author:		Steve Tetrault
-- Create date: Jun. 11, 2014
-- Description:	Modified Logic for GetAlignmentEntityClustersOfTypeInBoundingBox SP
-- =============================================
CREATE PROCEDURE [dbo].[GetAlignmentEntityClustersOfTypeInBoundingBox]
	-- Add the parameters for the stored procedure here
	@showAssigned BIT,@showUnassigned BIT, @isGeo BIT, @treeLeft INT, @treeRight INT, @territoryTypeId BIGINT,
	@minLat FLOAT, @maxLat FLOAT, @minLng FLOAT, @maxLng FLOAT, @ZoomLevel INT, @aeStatus NVARCHAR(255)
	
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
  DECLARE @factor int = POWER(2,20-@ZoomLevel)
  DECLARE @minX INT, @maxX INT, @minY INT, @maxY INT
  DECLARE @sql NVARCHAR(MAX)  
  SELECT @MinX = dbo.[ConvertLongitudeToTileX](@minLng,@ZoomLevel), @MaxX=Dbo.ConvertLongitudeToTileX(@maxLng,@ZoomLevel),
  @MinY = dbo.[ConvertLatitudeToTileY](@maxLat,@ZoomLevel), @MaxY=Dbo.ConvertLatitudeToTileY(@minLat,@ZoomLevel)
  SELECT @MinX = dbo.[ConvertLongitudeToTileX](@minLng,@ZoomLevel)*@factor, @MaxX=Dbo.ConvertLongitudeToTileX(@maxLng,@ZoomLevel)*@factor,
  @MinY = dbo.[ConvertLatitudeToTileY](@maxLat,@ZoomLevel)*@factor, @MaxY=Dbo.ConvertLatitudeToTileY(@minLat,@ZoomLevel)*@factor
  IF (@minX=@maxX)
  BEGIN
	SET @maxX = @minX + @factor -1
  END
    IF (@minY=@maxY)
  BEGIN
	SET @maxY = @minY + @factor -1
  END

  SET @sql = ''	WITH AllEntities AS
		(
			SELECT ae.AlignmentEntity_id,
				   ae.AlignmentEntity_Latitude,
				   ae.AlignmentEntity_Longitude,
				   ae.TileX_20,
				   ae.TileY_20,
				   ou.OrgUnit_Id,
				   InSpan=Case ''					
			IF (@showUnassigned = 1)
			BEGIN
			SET @sql = @sql +''WHEN (ou.OrgUnit_IsUnassigned = 1) THEN 1 ''
			END	ELSE	
			IF (@showAssigned = 1)
			BEGIN
			SET @sql = @sql +''WHEN (ou.OrgUnit_TreeLeft>= ''+CAST(@treeLeft AS VARCHAR)+'' and ou.OrgUnit_TreeRight<= ''+CAST(@treeRight AS VARCHAR)+'') THEN 1 ''
			END ELSE BEGIN
			SET @sql = @sql +''WHEN 1=0 THEN 0 '' 
			END

								
			SET @sql = @sql +'' else 0 end
			FROM [AlignmentEntitys] ae WITH(NOLOCK)
			inner join [OrgUnitAlignmentEntitys] ouae WITH (NOLOCK)	
			ON ae.TileX_20 BETWEEN ''+CAST(@MinX AS VARCHAR) +'' AND ''+CAST(@MaxX AS VARCHAR) +'' AND TileY_20 BETWEEN ''+CAST(@minY AS VARCHAR) +'' AND ''+CAST(@maxY AS VARCHAR) +''
			AND ''
			IF (@isGeo = 1)
			BEGIN
			SET @sql = @sql +'' ae.AlignmentEntityType_Id = 1''
			END	ELSE BEGIN
			SET @sql = @sql +'' ae.AlignmentEntityType_Id <> 1''
			END
			SET @sql = @sql + '' AND ae.AlignmentEntity_Status=''''''+@aeStatus +'''''' AND ae.AlignmentEntity_id = ouae.AlignmentEntity_id  ''
			SET @sql = @sql +''			inner join [OrgUnits] ou WITH(NOLOCK)
				ON ouae.OrgUnit_id = ou.OrgUnit_id
				inner join TerritoryTypes tt 
			ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
	and tt.TerritoryType_Id ='' + CAST(@territoryTypeId AS NVARCHAR)+CHAR(13)
	SET @sql=@sql++'')
	SELECT	COUNT(1) AS ClusterCount,
			AVG(AlignmentEntity_Latitude) AS ClusterLat,
			AVG(AlignmentEntity_Longitude) AS ClusterLong,		
			Max(AlignmentEntity_Latitude)  AS MaxLat,
			Max(AlignmentEntity_Longitude) AS MaxLong,
			Min(AlignmentEntity_Latitude)  AS MinLat,
			Min(AlignmentEntity_Longitude) AS MinLong,
			(Case WHEN COUNT(1) = 1 then CAST(Min(AlignmentEntity_Id) as varchar)
			else (CAST(OrgUnitId AS VARCHAR) + ''''|'''' + CAST(GroupByX AS varchar) + ''''|'''' + CAST(GroupByY AS varchar) + ''''|'''' + ''''''+CAST(@isGeo AS varchar)+'''''') end) AS EntityId,
			OrgUnitId AS OrgUnits,
			GroupByX AS TileX,
			GroupByY AS TileY
	FROM
	(
		SELECT Min(ae.AlignmentEntity_Latitude) as AlignmentEntity_Latitude,
			   Min(ae.AlignmentEntity_Longitude) as AlignmentEntity_Longitude,
			   Min(TileX_20)/''+CAST(@factor AS VARCHAR)+'' AS GroupByX, 
			   Min(TileY_20)/''+CAST(@factor AS VARCHAR)+'' AS GroupByY,
			   ae.AlignmentEntity_Id,
			   (Case WHEN COUNT(ae.OrgUnit_id) = 1 then
					cast(Min(ae.OrgUnit_id) as varchar)
				 WHEN COUNT(ae.OrgUnit_id) > 1 then
					STUFF((SELECT '''','''' + CAST(ouae.OrgUnit_Id as VARCHAR(MAX))
					FROM  [OrgUnitAlignmentEntitys] ouae WITH (NOLOCK)					
					WHERE  
					ouae.TerritoryType_Id =''+ CAST(@territoryTypeId AS VARCHAR)+'' and 
					ae.AlignmentEntity_id = ouae.AlignmentEntity_id
					ORDER BY ouae.OrgUnit_id					   
					FOR XML PATH('''''''')),1,1,'''''''')					
				end) AS OrgUnitId
		FROM AllEntities as ae
		GROUP BY ae.AlignmentEntity_id
		HAVING Max(InSpan)=1
	) as ClusterResults_Normal
	GROUP BY OrgUnitId,GroupByX,GroupByY
''

EXEC( @sql)
	END

'
EXEC (@sqlCommand)