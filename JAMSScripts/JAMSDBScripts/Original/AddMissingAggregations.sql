﻿declare @currentMaxOrgUnitAttributeIdCount int;
declare @missingOrgUnitAttributeCacheCount int;

declare @sql varchar(4000);

Set @sql='';
select @currentMaxOrgUnitAttributeIdCount= (MAX(OrgUnitAttributeCache_id)+1) from OrgUnitAttributeCache;

select OrgUnit_id,AttributeType_id,OrgUnitAttributeCache_Value  into  TempOrgUnitAttributeCache from OrgUnitAttributeCache where 1=2;

set @sql='Alter Table TempOrgUnitAttributeCache 
ADD OrgUnitAttributeCache_id int Identity( ' + Cast(@currentMaxOrgUnitAttributeIdCount as CHAR) +' ,1) ;';

exec(@sql);

insert into TempOrgUnitAttributeCache ( OrgUnit_id,AttributeType_id,OrgUnitAttributeCache_Value) 
select o.OrgUnit_Id,a.AttributeType_Id,0 from OrgUnits as o, AttributeTypes as a where not exists 
(select 1 from OrgUnitAttributeCache as oac where o.OrgUnit_Id=oac.OrgUnit_id and 
a.AttributeType_Id=oac.AttributeType_id) and a.AttributeType_Entity='AlignmentEntity' and a.AttributeType_DataType <> 'Text';

select @missingOrgUnitAttributeCacheCount= COUNT(1) from TempOrgUnitAttributeCache

IF @missingOrgUnitAttributeCacheCount > 0
BEGIN
	insert into OrgUnitAttributeCache (OrgUnitAttributeCache_Id, OrgUnit_id,AttributeType_id,OrgUnitAttributeCache_Value) 
	select OrgUnitAttributeCache_Id, OrgUnit_id,AttributeType_id,OrgUnitAttributeCache_Value from TempOrgUnitAttributeCache;
	
	update HiLoUniqueKey set NextHi= @missingOrgUnitAttributeCacheCount + NextHi*10 where
	ObjectType='OrgUnitAttributeCache'
END

drop table TempOrgUnitAttributeCache