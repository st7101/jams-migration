
PRINT N'Updating OrgUnitOverrides ...';

UPDATE ouo
SET [OrgUnitOverride_OrgUnitId]=ou.OrgUnit_Id,[OrgUnitOverride_ParentOrgUnitId]=ouParent.OrgUnit_Id
FROM dbo.OrgUnitOverrides ouo 
LEFT OUTER JOIN dbo.OrgUnits ou ON ouo.OrgUnitOverride_OrgUnitExternalId=ou.OrgUnit_ExternalID
LEFT OUTER JOIN dbo.OrgUnits ouParent ON ouo.OrgUnitOverride_ExternalParentOrgUnitId=ouParent.OrgUnit_ExternalID
