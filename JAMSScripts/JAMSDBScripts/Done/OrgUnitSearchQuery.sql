/*
Handle Parameters that the script has which are not required
:alignmentEntityTypeIds
*/
EXECUTE  [dbo].[OrgUnitSearchQuery]
   @showAssigned=:showAssigned
  ,@showUnassigned=:showUnassigned
  ,@treeLeft= :treeLeft
  ,@treeRight= :treeRight
  ,@territoryTypeId=:terrTypeId
  ,@MaxResult=:maxResult
  ,@SearchString=:searchString


