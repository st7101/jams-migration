select entity.AlignmentEntity_ParentId, count(entity.AlignmentEntity_Id) 
from AlignmentEntitys entity With (INDEX([ParentId]))
inner join GroupedEntities groupedEntity on entity.AlignmentEntity_Id = groupedEntity.AlignmentEntity_Id 
where entity.AlignmentEntity_Status = 'Normal' 
and entity.AlignmentEntity_ParentId in ('16820','29620','31001','36310','38601','70510','70511','04406','54405','76621','17301','67510','20606','58001','21001','28315','39730','42201','45101','57401','57402','83210','98520','21005','35440','79311','68001','67410') 
and groupedEntity.TerritoryType_Id = '120' 
group by entity.AlignmentEntity_ParentId