/*This query gets the cluster details for shared entities. */

/********* GET SHARED ALIGNMENT ENTITIES FOR THE SELECTED TERRITORY TYPE ********************/

EXECUTE [dbo].[GetSharedAlignmentEntitiesInCluster] 
   @minLat=42.6969776153564
  ,@maxLat=47.4596862792969
  ,@minLng=-73.4377403259277
  ,@maxLng=-66.9500045776367
    ,@territoryTypeId=15
  ,@aeStatus='Normal'
  ,@IsGeo=0
  ,@OrgUnitId=38