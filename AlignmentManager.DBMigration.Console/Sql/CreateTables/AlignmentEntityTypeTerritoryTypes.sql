
PRINT N'Starting rebuilding table [dbo].[AlignmentEntityTypeTerritoryTypes_Internal]...';


CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityTypeTerritoryTypes]
    (
      [AlignmentEntityTypeTerritoryType_Id] BIGINT NOT NULL ,
      [ToleratePartialAlignment] BIT
        CONSTRAINT DF_tmp_ms_xx_AlignmentEntityTypeTerritoryTypes_ToleratePartialAlignment
        DEFAULT ( (0) )
        NOT NULL ,
      [AlignmentEntityType_Id] BIGINT NULL ,
      [AlignmentEntityType_ParentId] BIGINT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityTypeTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [AlignmentEntityTypeTerritoryType_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AlignmentEntityTypeTerritoryTypes] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_AlignmentEntityTypeTerritoryTypes] WITH (TABLOCK)
                ( [AlignmentEntityTypeTerritoryType_Id] ,
                  [ToleratePartialAlignment] ,
                  [AlignmentEntityType_Id] ,
                  [AlignmentEntityType_ParentId] ,
                  [TerritoryType_ExternalId]
                )
                SELECT  [AlignmentEntityTypeTerritoryType_Id] ,
                        [ToleratePartialAlignment] ,
                        [AlignmentEntityType_Id] ,
                        [AlignmentEntityType_ParentId] ,
                        [TerritoryType_Id]
                FROM    [dbo].[AlignmentEntityTypeTerritoryTypes]
                ORDER BY [AlignmentEntityTypeTerritoryType_Id] ASC;
    END
DROP TABLE [dbo].[AlignmentEntityTypeTerritoryTypes];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityTypeTerritoryTypes]',
    N'AlignmentEntityTypeTerritoryTypes';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityTypeTerritoryTypes]',
    N'PK_AlignmentEntityTypeTerritoryTypes', N'OBJECT';
EXECUTE sp_rename N'[dbo].[DF_tmp_ms_xx_AlignmentEntityTypeTerritoryTypes_ToleratePartialAlignment]',
    N'DF_AlignmentEntityTypeTerritoryTypes_ToleratePartialAlignment',
    N'OBJECT';

