WITH OrgUnitWithSingleAlignment
AS
(
	SELECT OUAE.AlignmentEntity_id AS aeId, OUAE.TerritoryType_Id AS ttId
		FROM OrgUnitAlignmentEntitys OUAE
		INNER JOIN AlignmentEntitys AE ON OUAE.AlignmentEntity_id = AE.AlignmentEntity_Id
		INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
		AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND OUAE.TerritoryType_Id = AETTT.TerritoryType_id
		WHERE AETTT.ToleratePartialAlignment = 0
		GROUP BY OUAE.AlignmentEntity_id, OUAE.TerritoryType_Id
		HAVING COUNT(*) = 1
)
SELECT DISTINCT OUAE.AlignmentEntity_id FROM OrgUnitAlignmentEntitys OUAE 
	INNER JOIN OrgUnitWithSingleAlignment
	ON OrgUnitWithSingleAlignment.aeId = OUAE.AlignmentEntity_id and OrgUnitWithSingleAlignment.ttId = OUAE.TerritoryType_Id WHERE
	OUAE.OrgUnitAlignmentEntity_Weight = 0
