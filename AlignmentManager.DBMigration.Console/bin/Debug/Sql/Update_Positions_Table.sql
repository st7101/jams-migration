PRINT N'Starting rebuilding table [dbo].[Positions]...';

/*Base table structure */
DECLARE @CreateTable VARCHAR(MAX) ,
    @InsertTable VARCHAR(MAX)


SET @CreateTable = '
CREATE TABLE {DBIdentifier}.[dbo].[tmp_ms_xx_Positions] (
    [Position_ExternalId]        NVARCHAR (100) NOT NULL,
    [Position_Id] BIGINT      NOT NULL,
    [Position_LegacyId]  NVARCHAR (255) NULL,
    [Position_Effort]    DECIMAL (5, 2) CONSTRAINT DF_tmp_ms_xx_Positions_Position_Effort DEFAULT (100) NOT NULL,
    [OrgUnit_ExternalId]         NVARCHAR (100) NULL,
    [OrgUnit_Id]  BIGINT         NULL,
    [Role_Id]            NVARCHAR (100) NULL,
    CONSTRAINT [tmp_ms_xx_PK_Positions] PRIMARY KEY CLUSTERED ([Position_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);'
EXEC ( @CreateTable )


/* creating the insert list*/
SET @InsertTable = '
IF EXISTS (SELECT TOP 1 1 FROM {DBIdentifier}.[dbo].[Positions])
BEGIN

        INSERT INTO {DBIdentifier}.[dbo].[tmp_ms_xx_Positions] WITH (TABLOCK) ([Position_Id],[Position_ExternalId], [Position_LegacyId], [Position_Effort], [OrgUnit_ExternalId], [Role_Id])
        SELECT ROW_NUMBER() OVER (ORDER BY Position_Id ASC),
				[Position_Id],
               [Position_LegacyId],
               [Position_Effort],
               [OrgUnit_Id],
               [Role_Id]
        FROM   {DBIdentifier}.[dbo].[Positions] p 
END
'


EXEC (@insertTable)
DROP TABLE {DBIdentifier}.[dbo].Positions;

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_Positions]', N'Positions';

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_PK_Positions]', N'PK_Positions', N'OBJECT';

