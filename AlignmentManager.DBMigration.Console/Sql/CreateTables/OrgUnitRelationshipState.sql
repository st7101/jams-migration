PRINT N'Starting rebuilding table [dbo].[OrgUnitRelationshipStates]...';

CREATE TABLE dbo.Tmp_OrgUnitRelationshipStates ([OrgUnitRelationshipState_Id] bigint NOT NULL,
CONSTRAINT PK_Tmp_OrgUnitRelationshipStates PRIMARY KEY CLUSTERED ([OrgUnitRelationshipState_Id])
)  ON [PRIMARY] WITH (DATA_COMPRESSION=PAGE)

IF EXISTS(SELECT * FROM [dbo].[OrgUnitRelationshipStates])
	 EXEC('INSERT INTO dbo.Tmp_OrgUnitRelationshipStates ([OrgUnitRelationshipState_Id])
		SELECT OrgUnitRelationshipState_Id FROM dbo.[OrgUnitRelationshipStates] WITH (HOLDLOCK TABLOCKX)')
DROP TABLE [dbo].[OrgUnitRelationshipStates];
EXECUTE sp_rename N'[dbo].[Tmp_OrgUnitRelationshipStates]',
    N'OrgUnitRelationshipStates';
EXECUTE sp_rename N'[dbo].[PK_Tmp_OrgUnitRelationshipStates]',
    N'PK_OrgUnitRelationshipStates', N'OBJECT';