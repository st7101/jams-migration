DECLARE @nextHi bigint; 

SET @nextHi = ((SELECT COUNT(*) FROM OrgUnits) / 100)+1;
UPDATE HiLoUniqueKey SET NextHi=@nextHi
WHERE ObjectType='OrgUnits'

SET @nextHi = ((SELECT COUNT(*) FROM AlignmentEntitys) / 100)+1;
UPDATE HiLoUniqueKey SET NextHi=@nextHi
WHERE ObjectType='AlignmentEntitys'

SET @nextHi = ((SELECT COUNT(*) FROM OrgUnitLevels) / 100)+1;
UPDATE HiLoUniqueKey SET NextHi=@nextHi
WHERE ObjectType='OrgUnitLevels'

SET @nextHi = ((SELECT COUNT(*) FROM TerritoryTypes) / 100)+1;
UPDATE HiLoUniqueKey SET NextHi=@nextHi
WHERE ObjectType='TerritoryTypes'

SET @nextHi = ((SELECT COUNT(*) FROM Positions) / 100)+1;
UPDATE HiLoUniqueKey SET NextHi=@nextHi
WHERE ObjectType='Positions'