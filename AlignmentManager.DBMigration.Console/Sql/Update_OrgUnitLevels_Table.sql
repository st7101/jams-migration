PRINT N'Starting rebuilding table [dbo].[OrgUnitLevels]...';

/*Base table structure */
DECLARE @CreateTable VARCHAR(MAX) ,
    @InsertTable VARCHAR(MAX)


SET @CreateTable = '
CREATE TABLE {DBIdentifier}.[dbo].[tmp_ms_xx_OrgUnitLevels] (
    [OrgUnitLevel_Id]       BIGINT        NOT NULL,
    [OrgUnitLevel_ExternalId]              NVARCHAR (100) NOT NULL,
    [OrgUnitLevel_Name]            NVARCHAR (255) NULL,
    [UsesOrgUnitLegacyId]          BIT            NULL,
    [UsesPositionLegacyId]         BIT            NULL,
    [OrgUnitLevel_IsLeafLevel]     BIT            NULL,
    [OrgUnitLevel_ParentExternalId]        NVARCHAR (100) NULL,
    [OrgUnitLevel_ParentId] BIGINT         NULL,
    [OrgUnitLevel_TreeLeft]        INT            NULL,
    [OrgUnitLevel_TreeRight]       INT            NULL,
    CONSTRAINT [tmp_ms_xx_PK_OrgUnitLevels] PRIMARY KEY CLUSTERED ([OrgUnitLevel_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);'
EXEC ( @CreateTable )


/* creating the insert list*/
SET @InsertTable = '
IF EXISTS (SELECT TOP 1 1 FROM {DBIdentifier}.[dbo].[OrgUnitLevels])
BEGIN

        INSERT INTO {DBIdentifier}.[dbo].[tmp_ms_xx_OrgUnitLevels] WITH (TABLOCK) ([OrgUnitLevel_Id],OrgUnitLevel_ExternalId, [OrgUnitLevel_Name], [UsesOrgUnitLegacyId], [UsesPositionLegacyId], [OrgUnitLevel_IsLeafLevel], [OrgUnitLevel_ParentExternalId], [OrgUnitLevel_TreeLeft], [OrgUnitLevel_TreeRight])
        SELECT ROW_NUMBER() OVER (ORDER BY OrgUnitLevel_Id ASC),
		[OrgUnitLevel_Id],
               [OrgUnitLevel_Name],
               [UsesOrgUnitLegacyId],
               [UsesPositionLegacyId],
               [OrgUnitLevel_IsLeafLevel],
               [OrgUnitLevel_ParentId],
               [OrgUnitLevel_TreeLeft],
               [OrgUnitLevel_TreeRight]
        FROM   {DBIdentifier}.[dbo].[OrgUnitLevels] oul 
END
'


EXEC (@insertTable)
DROP TABLE {DBIdentifier}.[dbo].OrgUnitLevels;

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_OrgUnitLevels]', N'OrgUnitLevels';

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_PK_OrgUnitLevels]', N'PK_OrgUnitLevels', N'OBJECT';

