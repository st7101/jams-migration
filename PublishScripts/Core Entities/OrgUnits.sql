INSERT INTO [dbo].[OrgUnits]
           ([OrgUnit_Id]
           ,[OrgUnit_ExternalId]
           ,[OrgUnit_Name]
           ,[OrgUnit_LegacyId]
           ,[OrgUnit_IsUnassigned]
           ,[OrgUnitLevel_Id]
           ,[OrgUnitLevel_ExternalId]
           ,[OrgUnit_ParentExternalId] )
 SELECT 
	ROW_NUMBER() OVER (ORDER BY ouTemp.ORGUNIT_ID),
	ouTemp.ORGUNIT_ID,
	ouTemp.ORGUNIT_NAME,
	ouTemp.ORGUNIT_LEGACYID,
	ouTemp.ORGUNIT_ISUNASSIGNED,
	oul.OrgUnitLevel_Id,
	ouTemp.ORGUNITLEVEL_ID,
	ouTemp.ORGUNIT_PARENTID	
FROM dbo.OrgUnits_Temp ouTemp 
	INNER JOIN dbo.OrgUnitLevels oul ON ouTemp.ORGUNITLEVEL_ID=oul.OrgUnitLevel_ExternalId
 
 GO

UPDATE ou
SET OrgUnit_ParentId=ouParent.OrgUnit_Id
FROM orgunits ou
INNER JOIN orgunits ouParent ON ouparent.OrgUnit_ExternalId=ou.OrgUnit_ExternalId
WHERE ou.OrgUnit_ParentExternalId IS NOT NULL

GO

SELECT * FROM dbo.OrgUnits
