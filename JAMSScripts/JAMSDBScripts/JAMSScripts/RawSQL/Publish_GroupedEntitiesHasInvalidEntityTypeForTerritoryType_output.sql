/*Query to check that Grouped Entities has records only for Entity type which has derived alignment for that territorytype.*/ 

with invalidEntityType as 
(
	select ae.AlignmentEntityType_id, ge.TerritoryType_id 
	from GroupedEntities ge
	inner join AlignmentEntitys ae
		on ae.AlignmentEntity_Id = ge.AlignmentEntity_id 

	except
	
	select aettt.AlignmentEntityType_id, aettt.TerritoryType_id 
	from AlignmentEntityTypeTerritoryTypes aettt
	where aettt.AlignmentEntityType_ParentId is not null
)

Select * from invalidEntityType

