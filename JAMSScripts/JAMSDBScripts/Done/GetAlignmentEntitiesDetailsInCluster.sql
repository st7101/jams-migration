﻿/*This query gets the cluster details for shared entities. */

/********* GET SHARED ALIGNMENT ENTITIES FOR THE SELECTED TERRITORY TYPE ********************/

EXECUTE [dbo].[GetNonSharedAlignmentEntitiesDetailsInCluster] 
   @minLat=:minLat
  ,@maxLat=:maxLat
  ,@minLng=:minLng
  ,@maxLng=:maxLng
    ,@territoryTypeId=:territoryTypeId
  ,@aeStatus=:aeStatus
  ,@IsGeo=:isGeo
  ,@OrgUnitId=:orgUnitIds