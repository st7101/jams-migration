﻿/*Query to check All the entities which are present in GroupedEntitys table should not be present in OrgUnitAlignmentEntity
 for the territoryTypes having derived alignment.*/

select ouae.AlignmentEntity_id, tt.TerritoryType_id
from OrgUnitAlignmentEntitys ouae
inner join AlignmentEntitys ae
	on ouae.AlignmentEntity_id = ae.AlignmentEntity_Id
inner join AlignmentEntityTypeTerritoryTypes aettt
	on aettt.AlignmentEntityType_id = ae.AlignmentEntityType_id
	and ouae.TerritoryType_id = aettt.TerritoryType_id 
	and aettt.AlignmentEntityType_ParentId is not NULL
    INNER JOIN  OrgUnits as OU ON OU.OrgUnit_Id = ouae.OrgUnit_Id
INNER JOIN OrgUnitLevels AS OUL
ON OU.OrgUnitLevel_id = OUL.OrgUnitLevel_id
INNER JOIN dbo.TerritoryTypes tt ON tt.OrgUnitLevel_Id = OUL.OrgUnitLevel_Id