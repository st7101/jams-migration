PRINT N'Starting rebuilding table [dbo].[TerritoryTypes]...';

/*Base table structure */
DECLARE @CreateTable VARCHAR(MAX) ,
    @InsertTable VARCHAR(MAX)


SET @CreateTable = '
CREATE TABLE {DBIdentifier}.[dbo].[tmp_ms_xx_TerritoryTypes] (
    [TerritoryType_ExternalId]                            NVARCHAR (100) NOT NULL,
    [TerritoryType_Id]                     BIGINT      NOT NULL,
    [TerritoryType_Name]                          NVARCHAR (255) NULL,
    [TerritoryType_AreAllAttributesVisible]       BIT            NULL,
    [OrgUnitLevel_ExternalId]                             NVARCHAR (100) NULL,
    [OrgUnitLevel_Id]                      BIGINT         NULL,
    [UnassignedOrgUnit_ExternalId]                        NVARCHAR (100) NULL,
    [UnassignedOrgUnit_Id]                 BIGINT         NULL,
    [TerritoryType_SingleAssignmentWeightEnabled] BIT            CONSTRAINT DF_tmp_ms_xx_TerritoryTypes_TerritoryType_SingleAssignmentWeightEnabled DEFAULT ((0)) NOT NULL,
    [TerritoryType_ZeroWeightToleranceEnabled]    BIT            CONSTRAINT DF_tmp_ms_xx_TerritoryTypes_ZeroWeightToleranceEnabled DEFAULT ((0)) NOT NULL,
    [TerritoryType_WeightSumNotEqualTo100Enabled] BIT            CONSTRAINT DF_tmp_ms_xx_TerritoryTypes_WeightSumNotEqualTo100Enabled DEFAULT ((0)) NOT NULL,
    CONSTRAINT [tmp_ms_xx_PK_TerritoryTypes] PRIMARY KEY CLUSTERED ([TerritoryType_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
); '
EXEC ( @CreateTable )


/* creating the insert list*/
SET @InsertTable = '
IF EXISTS (SELECT TOP 1 1 FROM {DBIdentifier}.[dbo].[TerritoryTypes])
BEGIN

        INSERT INTO {DBIdentifier}.[dbo].[tmp_ms_xx_TerritoryTypes] WITH (TABLOCK) (TerritoryType_Id, [TerritoryType_ExternalId], [TerritoryType_Name], [TerritoryType_AreAllAttributesVisible], [OrgUnitLevel_ExternalId], [UnassignedOrgUnit_ExternalId], [TerritoryType_SingleAssignmentWeightEnabled], [TerritoryType_ZeroWeightToleranceEnabled], [TerritoryType_WeightSumNotEqualTo100Enabled])
        SELECT ROW_NUMBER() OVER (ORDER BY TerritoryType_Id ASC),
				[TerritoryType_Id],
               [TerritoryType_Name],
               [TerritoryType_AreAllAttributesVisible],
               [OrgUnitLevel_Id],
               [UnassignedOrgUnit_Id],
               [TerritoryType_SingleAssignmentWeightEnabled],
               [TerritoryType_ZeroWeightToleranceEnabled],
               [TerritoryType_WeightSumNotEqualTo100Enabled]
        FROM   {DBIdentifier}.[dbo].[TerritoryTypes] tt
END
'


EXEC (@insertTable)
DROP TABLE {DBIdentifier}.[dbo].[TerritoryTypes];

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_TerritoryTypes]', N'TerritoryTypes';

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_PK_TerritoryTypes]', N'PK_TerritoryTypes', N'OBJECT';

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[DF_tmp_ms_xx_TerritoryTypes_TerritoryType_SingleAssignmentWeightEnabled]', N'DF_TerritoryTypes_TerritoryType_SingleAssignmentWeightEnabled', N'OBJECT';

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[DF_tmp_ms_xx_TerritoryTypes_ZeroWeightToleranceEnabled]', N'DF_TerritoryTypes_ZeroWeightToleranceEnabled', N'OBJECT';

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[DF_tmp_ms_xx_TerritoryTypes_WeightSumNotEqualTo100Enabled]', N'DF_TerritoryTypes_WeightSumNotEqualTo100Enabled', N'OBJECT';

