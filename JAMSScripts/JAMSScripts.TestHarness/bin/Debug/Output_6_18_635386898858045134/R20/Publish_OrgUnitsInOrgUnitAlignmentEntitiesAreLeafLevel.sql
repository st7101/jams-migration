-- Query to determine if all the OrgUnits listed in OrgUnitAlignmentEntities table are leaf level

	select ou.OrgUnit_Id, ouae.AlignmentEntity_id, oul.OrgUnitLevel_Name 
	from OrgUnitAlignmentEntitys ouae 
	inner join OrgUnits ou 
	ON ouae.OrgUnit_id=ou.OrgUnit_Id 
	inner join OrgUnitLevels oul 
	ON ou.OrgUnitLevel_id=oul.OrgUnitLevel_Id 
	where oul.OrgUnitLevel_IsLeafLevel!=1
