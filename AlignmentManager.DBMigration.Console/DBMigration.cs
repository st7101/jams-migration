﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Data.Sql;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using System.Timers;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Threading;



namespace AlignmentManager.DBMigration.Console
{
    class DBMigrationConsole : IDisposable
    {
        private string[] _args;
        private string _metaDbconnectionString;
        private Int64 _version;
        private DataTable _eventInfo = new DataTable();
        private StreamWriter _log;
        private StreamWriter _longLog;
        private LimitedConcurrencyTaskScheduler _ts;
        private TaskFactory _factory;
        private int _maxDegreeOfParallelismEvent;
        private int _maxDegreeOfParallelismTask;
        private CountdownEvent _eventsLeft;
        private int _totalEvents;

        public DBMigrationConsole(string[] argv)
        {
            _args = argv;
            ParseArguments(_args);
            _ts = new LimitedConcurrencyTaskScheduler(5);
            _factory = new TaskFactory(_ts);
            _log = new StreamWriter("DBMigrationLog.txt");
            _longLog = new StreamWriter("DBMigrationLongOperations.txt");
        }

        public void ParseArguments(string[] argv)
        {
            _metaDbconnectionString = argv[0];
            _version = (argv.Count() > 1) ? Int64.Parse(argv[1]) : -1;
            _maxDegreeOfParallelismEvent = (argv.Count() > 2) ? int.Parse(argv[2]) : 1;
            _maxDegreeOfParallelismTask = (argv.Count() > 3) ? int.Parse(argv[3]) : 1;

        }

        public int Run()
        {
            #region update the metaDbSchema
            using (SqlConnection metaCon = SetupConnection(_metaDbconnectionString, ""))
            {
                System.Console.WriteLine(String.Format("metaDBUpdate Started at: {0}", DateTime.Now));
                metaCon.Open();

                using (SqlCommand sqlCmd = new SqlCommand(GetSqlFromFile("Create_ArrayTypes.sql"), metaCon))
                {
                    sqlCmd.ExecuteNonQuery();
                }

                using (SqlCommand sqlCmd = new SqlCommand(GetSqlFromFile("Get_Event_DB_Info.sql"), metaCon))
                {
                    _eventInfo.Load(sqlCmd.ExecuteReader());

                }
                System.Console.WriteLine(String.Format("metaDBUpdate Ended at: {0}", DateTime.Now));
            #endregion
                _totalEvents = _eventInfo.Rows.Count;
                _eventsLeft = new CountdownEvent(_totalEvents);
                if (_eventsLeft.CurrentCount > 0)
                {
                    _factory.StartNew(() => { ProcessEvent(0); });
                }
                #region compress meta DB tables
                using (SqlCommand sqlCmd = new SqlCommand(GetSqlFromFile("CompressMetaDb.sql"), metaCon))
                {
                    sqlCmd.CommandTimeout = 0;
                    sqlCmd.ExecuteNonQuery();
                }
                #endregion
            }
            _eventsLeft.Wait();
            return 1;
        }

        private void ProcessEvent(int eventIndex)
        {
            ConcurrentQueue<Exception> exceptions = new ConcurrentQueue<Exception>();
            TaskFactory taskFact = new TaskFactory(new LimitedConcurrencyTaskScheduler(16));
            List<Task> taskList = new List<Task>();

            string dbIdentifier = "";
            Stopwatch eventTimer = new Stopwatch();
            Int64 eventVersion;
            try
            {
                try
                {
                    dbIdentifier = _eventInfo.Rows[eventIndex][0].ToString();
                    eventTimer.Start();
                    LogMessage("\n\nStarting Upgrade of " + dbIdentifier);
                    using (SqlConnection con = SetupConnection(_metaDbconnectionString, GetEventConnectionString(_metaDbconnectionString, dbIdentifier)))
                    {
                        con.Open();
                        using (SqlCommand sqlCmd = new SqlCommand(GetSqlFromFile("Get_Event_DB_Version.sql"), con))
                        {
                            sqlCmd.CommandTimeout = 0;
                            eventVersion = Int64.Parse(sqlCmd.ExecuteScalar().ToString());
                        }
                    }
                    LogMessage("Required Version: " + eventVersion.ToString(), dbIdentifier);
                    LogMessage("Event Version: " + eventVersion.ToString(), dbIdentifier);
                    if (_version > eventVersion)
                    {
                        LogMessage("Event is being skipped due to incorrect version: " + eventVersion.ToString());
                        return;
                    }

                    #region drop all constraints on the db
                    RunSqlOnEvent(GetSqlFromFile("RemoveAllConstraints.sql"), "Drop All Constraints", dbIdentifier);
                    #endregion

                    #region update Programability section
                    taskList.Clear();
                    LogMessage(String.Format("update Programability section Started at: {0}", DateTime.Now), dbIdentifier);
                    DirectoryInfo diPro = new DirectoryInfo(@"Sql/Programmability\");
                    foreach (FileInfo f in diPro.GetFiles("*.sql"))
                    {
                        taskList.Add(taskFact.StartNew(() =>
                        {
                            try
                            {
                                RunSqlOnEvent(GetProSqlFromFile(f.Name), f.Name, dbIdentifier);

                            }
                            catch (Exception ex)
                            {
                                HandleException(ex, dbIdentifier, "update Programability section");
                                exceptions.Enqueue(ex);

                            }
                        }));
                    }
                    Task.WaitAll(taskList.ToArray());
                    if (!exceptions.IsEmpty)
                        throw exceptions.First();

                    LogMessage(String.Format("update Programability section Ended at: {0}", DateTime.Now), dbIdentifier);
                    #endregion

                    #region ReKey the AlignmentEntitys table
                    RunSqlOnMeta(GetSqlFromFileForEvent("Update_AlignmentEntitys_Table.sql", dbIdentifier), "Rekey AlignmentEntites");
                    #endregion

                    #region ReKey the TerritoryTypes table
                    RunSqlOnMeta(GetSqlFromFileForEvent("Update_TerritoryTypes_Table.sql", dbIdentifier), "Rekey TerritoryTypes");
                    #endregion

                    #region ReKey the OrgUnits table
                    RunSqlOnMeta(GetSqlFromFileForEvent("Update_OrgUnits_Table.sql", dbIdentifier), "Rekey OrgUnits");
                    #endregion

                    #region ReKey the OrgUnitLevels table
                    RunSqlOnMeta(GetSqlFromFileForEvent("Update_OrgUnitLevels_Table.sql", dbIdentifier), "Rekey OrgUnitLevels");
                    #endregion

                    #region ReKey the Positions table
                    RunSqlOnMeta(GetSqlFromFileForEvent("Update_Positions_Table.sql", dbIdentifier), "Rekey Positions");
                    #endregion

                    #region Update HiLo table for rekeyed objects
                    RunSqlOnMeta(GetSqlFromFileForEvent("Update_HiLoUniqueKey_Table.sql", dbIdentifier), "Update HiLoUniqueKey Table");
                    #endregion
                }

                catch (Exception ex)
                {
                    HandleException(ex, dbIdentifier, "Inner catchall");
                    exceptions.Enqueue(ex);

                }

                finally
                {
                    //trigger the next event if not last
                    if ((_eventsLeft.CurrentCount > 1) && (eventIndex < (_totalEvents-1)))
                    {
                        _factory.StartNew(() => { ProcessEvent(eventIndex + 1); });
                    }
                }

                #region add external indexes
                LogMessage(String.Format("add External indexes started at: {0}", DateTime.Now), dbIdentifier);
                foreach (string line in SplitSqlFromFile("AddExternalIndexes.sql"))
                {
                    taskList.Add(taskFact.StartNew(() =>
                    {
                        try
                        {
                            RunSqlOnEvent(line, line.Replace('\n', ' '), dbIdentifier);
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, dbIdentifier, "add external indexes");
                            exceptions.Enqueue(ex);

                        }
                    }));

                }
                Task.WaitAll(taskList.ToArray());
                if (!exceptions.IsEmpty)
                    throw exceptions.First();
                LogMessage(String.Format("add external indexes completed at: {0}", DateTime.Now, dbIdentifier));
                #endregion

                #region update all tables that reference the updated tables
                LogMessage(String.Format("Update reference tables Started at: {0}", DateTime.Now, dbIdentifier));
                taskList.Clear();
                DirectoryInfo di = new DirectoryInfo(@"Sql/CreateTables\");

                foreach (FileInfo f in di.GetFiles("*.sql"))
                {
                    taskList.Add(taskFact.StartNew(() =>
                    {
                        try
                        {
                            RunSqlOnEvent(GetCreateSqlFromFile(f.Name), f.Name, dbIdentifier);

                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, dbIdentifier, "update all tables that reference the updated tables");
                            exceptions.Enqueue(ex);

                        }
                    }));
                }
                Task.WaitAll(taskList.ToArray());
                if (!exceptions.IsEmpty)
                    throw exceptions.First();
                LogMessage(String.Format("Update reference tables Ended at: {0}", DateTime.Now), dbIdentifier);
                #endregion

                #region update foreign key data
                taskList.Clear();
                LogMessage(String.Format("update foreign key data Started at: {0}", DateTime.Now), dbIdentifier);
                DirectoryInfo diMigrate = new DirectoryInfo(@"Sql/MigrateForeignKeys\");
                foreach (FileInfo f in diMigrate.GetFiles("*.sql"))
                {
                    taskList.Add(taskFact.StartNew(() =>
                    {
                        try
                        {
                            RunSqlOnEvent(GetMigrateSqlFromFile(f.Name), f.Name, dbIdentifier);

                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, dbIdentifier, "update foreign key data");
                            exceptions.Enqueue(ex);

                        }
                    }));
                }
                Task.WaitAll(taskList.ToArray());
                if (!exceptions.IsEmpty)
                    throw exceptions.First();

                LogMessage(String.Format("update foreign key data Ended at: {0}", DateTime.Now), dbIdentifier);
                #endregion

                #region compress all unrelated tables
                RunSqlOnEvent(GetSqlFromFile("CompressUnrelatedTables.sql"), "Compress Unrelated Tables", dbIdentifier);
                #endregion

                #region update column nullability
                LogMessage(String.Format("update column nullability started at: {0}", DateTime.Now), dbIdentifier);
                taskList.Clear();
                foreach (string line in SplitSqlFromFile("UpdatedNullability.sql"))
                {
                    taskList.Add(taskFact.StartNew(() =>
                    {
                        try
                        {
                            RunSqlOnEvent(line, line.Replace('\n', ' '), dbIdentifier);
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, dbIdentifier, "update column nullability");
                            exceptions.Enqueue(ex);

                        }
                    }));

                }
                Task.WaitAll(taskList.ToArray());
                if (!exceptions.IsEmpty)
                    throw exceptions.First();
                LogMessage(String.Format("update column nullability completed at: {0}", DateTime.Now), dbIdentifier);
                #endregion

                #region add foreign keys
                RunSqlOnEvent(GetSqlFromFile("AddForeignKeys.sql"), "Add Foreign Keys", dbIdentifier);
                #endregion

                #region Delete columns
                RunSqlOnEvent(GetSqlFromFile("Drop_Old_Columns.sql"), "Drop Old Columns", dbIdentifier);
                #endregion

                #region add indexes
                LogMessage(String.Format("add indexes started at: {0}", DateTime.Now), dbIdentifier);
                taskList.Clear();
                foreach (string line in SplitSqlFromFile("AddAllIndexes.sql"))
                {
                    taskList.Add(taskFact.StartNew(() =>
                    {
                        try
                        {
                            RunSqlOnEvent(line, line.Replace('\n', ' '), dbIdentifier);
                        }
                        catch (Exception ex)
                        {
                            HandleException(ex, dbIdentifier, "add indexes");
                            exceptions.Enqueue(ex);

                        }
                    }));

                }
                Task.WaitAll(taskList.ToArray());
                if (!exceptions.IsEmpty)
                    throw exceptions.First();

                LogMessage(String.Format("add indexes completed at: {0}", DateTime.Now), dbIdentifier);
                #endregion

                #region Add OAESuperView
                RunSqlOnEvent(GetSqlFromFile("Create_OAESuperView.sql"), "Add OAESuperView", dbIdentifier);
                #endregion
            }
            catch (Exception ex)
            {
                HandleException(ex, dbIdentifier, "Outer catchall");

            }
            finally
            {
                eventTimer.Stop();
                LogMessage(String.Format("\n\nCompleted Upgrade of {0} duration: {1}:{2}:{3}", dbIdentifier, Math.Round(Math.Floor(eventTimer.Elapsed.TotalHours), 0), Math.Round(Math.Floor(eventTimer.Elapsed.TotalMinutes), 0), Math.Round(Math.Floor(eventTimer.Elapsed.TotalSeconds), 0)));
                _eventsLeft.Signal();
            }

        }

        private void HandleException(Exception ex, string dbIdentifier, string section)
        {
            LogMessage(String.Format("\n\n\n{0} Upgrade Failed with error at '{2}':\n{1}", dbIdentifier, ex, section));
        }


        private void RunSqlOnEvent(string sql, string section, string eventID)
        {
            Stopwatch runTime = new Stopwatch();
            LogMessage(String.Format("{0} Started", section), eventID);

            using (SqlConnection eventCon = SetupConnection(_metaDbconnectionString, GetEventConnectionString(_metaDbconnectionString, eventID)))
            {

                eventCon.Open();
                runTime.Start();
                using (SqlCommand sqlCmd = new SqlCommand(sql, eventCon))
                {
                    sqlCmd.CommandTimeout = 0;
                    sqlCmd.ExecuteNonQuery();
                }
                runTime.Stop();
            }
            LogMessage(String.Format("{0} Ended \nRun Time:{1} s", section, runTime.Elapsed.TotalSeconds), eventID);
            if (runTime.Elapsed.TotalSeconds > 30)
                LogLongOperation(section, runTime.Elapsed, eventID);
        }

        private void RunSqlOnMeta(string sql, string section)
        {
            Stopwatch runTime = new Stopwatch();
            LogMessage(String.Format("{0} Started", section));
            using (SqlConnection metaCon = SetupConnection(_metaDbconnectionString, ""))
            {
                metaCon.Open();
                runTime.Start();
                using (SqlCommand sqlCmd = new SqlCommand(sql, metaCon))
                {
                    sqlCmd.CommandTimeout = 0;
                    sqlCmd.ExecuteNonQuery();
                }
                runTime.Stop();
            }
            LogMessage(String.Format("{0} Ended \nRun Time:{1} s", section, runTime.Elapsed.TotalSeconds));
            if (runTime.Elapsed.TotalSeconds > 30)
                LogLongOperation(section, runTime.Elapsed);

        }

        private bool IsDbUpgraded(int version)
        {
            if (version >= _version)
            {
                return true;
            }
            return false;
        }

        private string GetEventConnectionString(string metaDbString, string title)
        {
            var connList = metaDbString.Split(';');
            var newConnString = "";

            foreach (var segment in connList)
            {
                if (segment.ToLower().Contains("database"))
                {
                    newConnString += "Database=" + title + ";";
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(segment))
                    {
                        newConnString += segment + ";";
                    }
                }
            }
            return newConnString;
        }

        private SqlConnection SetupConnection(string metaDbConnString, string eventDbConnString)
        {
            return new SqlConnection((String.IsNullOrEmpty(eventDbConnString) ? metaDbConnString : eventDbConnString));
        }

        private string GetSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/" + file);
            return f.OpenText().ReadToEnd();
        }
        private string GetSqlFromFileForEvent(string file, string dBIdentifier)
        {
            FileInfo f = new FileInfo("sql/" + file);
            return f.OpenText().ReadToEnd().Replace("{DBIdentifier}", dBIdentifier);
        }
        private string GetCreateSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/CreateTables/" + file);
            return f.OpenText().ReadToEnd();
        }
        private string GetMigrateSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/MigrateForeignKeys/" + file);
            return f.OpenText().ReadToEnd();
        }
        private string GetProSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/Programmability/" + file);
            return f.OpenText().ReadToEnd();
        }
        private string[] SplitSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/" + file);
            return f.OpenText().ReadToEnd().Split('\n');
        }
        private void LogMessage(string message)
        {
            lock (_log)
            {
                System.Console.WriteLine(String.Format("{0}: {1}", DateTime.Now.ToLongTimeString(), message));
                _log.WriteLine(String.Format("{0}: {1}", DateTime.Now.ToLongTimeString(), message));
                _log.Flush();
            }
        }

        private void LogMessage(string message, string dbIdentifier)
        {
            lock (_log)
            {
                System.Console.WriteLine(String.Format("{0}: [{1}] {2}", DateTime.Now.ToLongTimeString(), dbIdentifier, message));
                _log.WriteLine(String.Format("{0}: [{1}] {2}", DateTime.Now.ToLongTimeString(), dbIdentifier, message));
                _log.Flush();
            }
        }

        private void LogLongOperation(string operation, TimeSpan elapsed)
        {

            lock (_longLog)
            {
                _longLog.WriteLine(String.Format("{0}:{1}:{2} Operation {3}", Math.Round(Math.Floor(elapsed.TotalHours), 0), Math.Round(Math.Floor(elapsed.TotalMinutes), 0), Math.Round(Math.Floor(elapsed.TotalSeconds), 0), operation));
                _longLog.Flush();
            }
        }


        private void LogLongOperation(string operation, TimeSpan elapsed, string dbIdentifier)
        {

            lock (_longLog)
            {
                _longLog.WriteLine(String.Format("{0}:{1}:{2} [{3}] Operation {4}", Math.Round(Math.Floor(elapsed.TotalHours), 0), Math.Round(Math.Floor(elapsed.TotalMinutes), 0), Math.Round(Math.Floor(elapsed.TotalSeconds), 0), dbIdentifier, operation));
                _longLog.Flush();
            }
        }


        public void Dispose()
        {
            _log.Close();
            _longLog.Close();
        }
    }
}
