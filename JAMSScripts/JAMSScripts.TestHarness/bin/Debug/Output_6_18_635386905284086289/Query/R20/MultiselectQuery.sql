/********* This query gets selected entities list ********************/
EXECUTE [dbo].[MultiSelectQuery] 
   @minLat=42.6969776153564
  ,@maxLat=47.4596862792969
  ,@minLng=47.4596862792969
  ,@maxLng=-66.9500045776367
  ,@territoryTypeId=55
  ,@showAssigned=0
  ,@showUnassigned=0
  ,@TreeLeft=1
  ,@treeRight=10430
  ,@ShowGeos=0
  ,@ShowCustomers=0
