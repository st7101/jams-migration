
PRINT N'Starting rebuilding table [dbo].[OrgUnitOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitOverrides]
    (
      [OrgUnitOverride_Id] BIGINT NOT NULL ,
      [OrgUnitOverride_OrgUnitExternalId] NVARCHAR(255) NULL ,
      [OrgUnitOverride_OrgUnitId] BIGINT NULL ,
      [OrgUnitOverride_ExternalParentOrgUnitId] NVARCHAR(255) NULL ,
      [OrgUnitOverride_ParentOrgUnitId] BIGINT NULL ,
      [OrgUnitOverride_RefinementOperationType] NVARCHAR(100) NULL ,
      [OrgUnitOverride_Name] NVARCHAR(255) NULL ,
      [OrgUnitOverride_LegacyId] NVARCHAR(255) NULL ,
      [Request_Id] BIGINT NULL ,
      [Level_Id] NVARCHAR(100) NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitOverrides] PRIMARY KEY CLUSTERED
        ( [OrgUnitOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitOverrides] WITH (TABLOCK)
                ( [OrgUnitOverride_Id] ,
                  [OrgUnitOverride_OrgUnitExternalId] ,
                  [OrgUnitOverride_ExternalParentOrgUnitId] ,
                  [OrgUnitOverride_RefinementOperationType] ,
                  [OrgUnitOverride_Name] ,
                  [OrgUnitOverride_LegacyId] ,
                  [Request_Id] ,
                  [Level_Id]
                )
                SELECT  [OrgUnitOverride_Id] ,
                        [OrgUnitOverride_OrgUnitId] ,
                        [OrgUnitOverride_ParentOrgUnitId] ,
                        [OrgUnitOverride_RefinementOperationType] ,
                        [OrgUnitOverride_Name] ,
                        [OrgUnitOverride_LegacyId] ,
                        [Request_Id] ,
                        [Level_Id]
                FROM    [dbo].[OrgUnitOverrides]
                ORDER BY [OrgUnitOverride_Id] ASC;
    END
DROP TABLE [dbo].[OrgUnitOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitOverrides]', N'OrgUnitOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitOverrides]',
    N'PK_OrgUnitOverrides', N'OBJECT';

