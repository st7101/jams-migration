
PRINT N'Updating RequestOrgUnitAttributeCache ...';

UPDATE rouac
SET OrgUnit_Id=ou.OrgUnit_Id
FROM dbo.RequestOrgUnitAttributeCache rouac INNER JOIN 
dbo.OrgUnits ou ON rouac.OrgUnit_ExternalID=ou.OrgUnit_ExternalID
