PRINT N'Starting rebuilding table [dbo].[TerritoryShapeOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_TerritoryShapeOverrides]
    (
      [OrgUnit_Id] BIGINT NOT NULL ,
      [OrgUnit_ExternalId] NVARCHAR(255) NOT NULL ,
      [Position_Id] BIGINT NOT NULL ,
      [Position_ExternalId] NVARCHAR(255) NOT NULL ,
      [geom] [sys].[GEOMETRY] NULL ,
      [geom_low] [sys].[GEOMETRY] NULL ,
      [geom_medium] [sys].[GEOMETRY] NULL ,
      [MinLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STX, ( 0 )) )
        PERSISTED ,
      [MinLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STY, ( 0 )) )
        PERSISTED ,
      [MaxLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STX, ( 0 )) )
        PERSISTED ,
      [MaxLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STY, ( 0 )) )
        PERSISTED ,
      CONSTRAINT [tmp_ms_xx_constraint_pk_TerritoryShapeOverrides] PRIMARY KEY CLUSTERED
        ( [Position_Id] ASC,
		  [OrgUnit_Id] ASC )     );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[TerritoryShapeOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_TerritoryShapeOverrides] WITH ( TABLOCK )
                ( [OrgUnit_Id],
                  [OrgUnit_ExternalId] ,
                  [Position_Id],
                  [Position_ExternalId] ,
                  [geom],geom_low,geom_medium
                )
                SELECT  ROW_NUMBER() OVER ( ORDER BY OrgUnit_Id ) ,
						[OrgUnit_Id] ,
						ROW_NUMBER() OVER ( ORDER BY OrgUnit_Id ) ,
                        [Position_Id] ,
                        [geom],geom_low,geom_medium
                FROM    [dbo].[TerritoryShapeOverrides]
    END
DROP TABLE [dbo].[TerritoryShapeOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_TerritoryShapeOverrides]',
    N'TerritoryShapeOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_pk_TerritoryShapeOverrides]',
    N'PK_TerritoryShapeOverrides', N'OBJECT';
