PRINT N'Starting rebuilding table [dbo].[OrgUnitAttributeCache]...';

CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitAttributeCache]
    (
      [OrgUnitAttributeCache_Id] BIGINT NOT NULL ,
      [OrgUnitAttributeCache_Value] FLOAT(53) NULL ,
      [OrgUnit_ExternalId] NVARCHAR(100) NULL ,
	  [OrgUnit_Id] BIGINT NULL ,
      [AttributeType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitAttributeCache] PRIMARY KEY CLUSTERED
        ( [OrgUnitAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitAttributeCache] WITH (TABLOCK)
                ( [OrgUnitAttributeCache_Id] ,
                  [OrgUnitAttributeCache_Value] ,
                  [OrgUnit_ExternalId] ,
                  [AttributeType_Id]
                )
                SELECT  [OrgUnitAttributeCache_Id] ,
                        [OrgUnitAttributeCache_Value] ,
                        [OrgUnit_Id] ,
                        [AttributeType_Id]
                FROM    [dbo].[OrgUnitAttributeCache] ouac
				ORDER BY [OrgUnitAttributeCache_Id] ASC;
    END
DROP TABLE [dbo].[OrgUnitAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitAttributeCache]',
    N'OrgUnitAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitAttributeCache]',
    N'PK_OrgUnitAttributeCache', N'OBJECT';

