SELECT oae.AlignmentEntity_Id        AS Id,
	   ae.AlignmentEntity_Name      AS Name,
	   ae.AlignmentEntity_Latitude  AS Latitude,
	   ae.AlignmentEntity_Longitude AS Longitude,
	   ou.OrgUnit_Id AS OrgUnitId,
	   oae.OrgUnitAlignmentEntity_Weight AS AlignmentWeight,
	   ou.OrgUnit_Name AS OrgUnitName,
	   ou.OrgUnit_ColorId AS OrgUnitColorId
FROM   
		OrgUnitAlignmentEntitys AS oae
		inner join AlignmentEntitys as ae
		on ae.AlignmentEntity_Id = oae.AlignmentEntity_id
		inner join [OrgUnits] ou
		ON ou.OrgUnit_Id = oae.OrgUnit_id
		inner join [TerritoryTypes] tt
		ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id
		and tt.TerritoryType_Id = '204'
		and ae.AlignmentEntity_Longitude between -73.4377403259277 and -66.9500045776367
		and ae.AlignmentEntity_Latitude between 42.6969776153564 and 47.4596862792969
		and ((0 = 1 and ae.AlignmentEntityType_Id = 1)	or (0 = 0 and ae.AlignmentEntityType_Id <> 1))
		and ae.AlignmentEntity_Status = 'Normal'
		and (ae.AlignmentEntity_Latitude <> 0 and ae.AlignmentEntity_Longitude <> 0)
		and (ae.AlignmentEntity_Latitude Is NOT NULL and ae.AlignmentEntity_Longitude Is NOT NULL)