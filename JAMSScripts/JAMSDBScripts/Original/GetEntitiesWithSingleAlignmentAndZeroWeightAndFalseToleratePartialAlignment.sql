﻿WITH OrgUnitWithSingleAlignment
AS
(
	SELECT OUAE.AlignmentEntity_id AS aeId, TT.TerritoryType_Id AS ttId
		FROM OrgUnitAlignmentEntitys OUAE
		INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OUAE.OrgUnit_id
		INNER JOIN TerritoryTypes TT ON TT.OrgUnitLevel_id = OU.OrgUnitLevel_id
		INNER JOIN AlignmentEntitys AE ON OUAE.AlignmentEntity_id = AE.AlignmentEntity_Id
		INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
		AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND TT.TerritoryType_Id = AETTT.TerritoryType_id
		WHERE AETTT.ToleratePartialAlignment = 0
		GROUP BY OUAE.AlignmentEntity_id, TT.TerritoryType_Id
		HAVING COUNT(*) = 1
)
SELECT DISTINCT OUAE.AlignmentEntity_id FROM OrgUnitAlignmentEntitys OUAE 
	INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OUAE.OrgUnit_id
	INNER JOIN TerritoryTypes TT ON TT.OrgUnitLevel_id = OU.OrgUnitLevel_id
	INNER JOIN OrgUnitWithSingleAlignment
	ON OrgUnitWithSingleAlignment.aeId = OUAE.AlignmentEntity_id and OrgUnitWithSingleAlignment.ttId = TT.TerritoryType_Id WHERE
	OUAE.OrgUnitAlignmentEntity_Weight = 0
