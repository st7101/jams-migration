
PRINT N'Starting rebuilding table [dbo].[OrgUnitRelationshipStateEntrys]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitRelationshipStateEntrys]
    (
      [OrgUnitRelationshipStateEntry_Id] BIGINT NOT NULL ,
      [OrgUnitRelationshipState_Id] BIGINT NULL ,
      [OrgUnitRelationshipStateEntry_ExternalAffectedOrgUnitId] NVARCHAR(255)
        NULL ,
      [OrgUnitRelationshipStateEntry_AffectedOrgUnitId] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitRelationshipStateEntrys] PRIMARY KEY CLUSTERED
        ( [OrgUnitRelationshipStateEntry_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitRelationshipStateEntrys] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitRelationshipStateEntrys] WITH (TABLOCK)
                ( [OrgUnitRelationshipStateEntry_Id] ,
                  [OrgUnitRelationshipState_Id] ,
                  [OrgUnitRelationshipStateEntry_ExternalAffectedOrgUnitId]
                )
                SELECT  [OrgUnitRelationshipStateEntry_Id] ,
                        [OrgUnitRelationshipState_Id] ,
                        [OrgUnitRelationshipStateEntry_AffectedOrgUnitId]
                FROM    [dbo].[OrgUnitRelationshipStateEntrys]
                ORDER BY [OrgUnitRelationshipStateEntry_Id] ASC;
    END
DROP TABLE [dbo].[OrgUnitRelationshipStateEntrys];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitRelationshipStateEntrys]',
    N'OrgUnitRelationshipStateEntrys';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitRelationshipStateEntrys]',
    N'PK_OrgUnitRelationshipStateEntrys', N'OBJECT';



