/*Query to check All the entities which are present in GroupedEntitys table should not be present in OrgUnitAlignmentEntity
 for the territoryTypes having derived alignment.*/

select ge.AlignmentEntity_id, ge.TerritoryType_id
from GroupedEntities  ge
inner join AlignmentEntitys ae
	on ge.AlignmentEntity_id = ae.AlignmentEntity_Id
inner join OrgUnitAlignmentEntitys ouae
	on ouae.AlignmentEntity_id = ge.AlignmentEntity_id
inner join OrgUnits ou
	on ou.OrgUnit_Id = ouae.OrgUnit_id 
inner join TerritoryTypes tt
	on tt.OrgUnitLevel_id = ou.OrgUnitLevel_id
	and ge.TerritoryType_id = tt.TerritoryType_Id
inner join AlignmentEntityTypeTerritoryTypes aettt
	on aettt.AlignmentEntityType_id = ae.AlignmentEntityType_id
	and ge.TerritoryType_id = aettt.TerritoryType_id 
	and aettt.AlignmentEntityType_ParentId is not null