-- =============================================
-- Author:		Steve Tetrault
-- Create date: May 20, 2014
-- Description:	Get Internal ID's from Client ID's
-- =============================================
CREATE PROCEDURE [dbo].[GetInternalIDsFromClientIDs] 
	@ClientIDs StringArray READONLY,@Entity NVARCHAR(100),@DBIdentifier NVARCHAR(100)
AS
BEGIN

SET NOCOUNT ON
DECLARE @results TABLE (Entity_ClientID NVARCHAR(100) NOT NULL,	Entity_InternalID BIGINT NULL,Entity_IsNew BIT NULL)
DECLARE @EntityTypeID INT 
SELECT @EntityTypeID=EntityType_ID FROM dbo.EntityType WHERE EntityType_Name=@Entity
DECLARE @MaxID BIGINT 
SELECT @MAxID = ISNULL(MAX(EntityIDMapping_InternalID),1) FROM dbo.EntityIDMapping WHERE EntityIDMapping_TypeID=@EntityTypeID
DECLARE @effectiveDate DATETIME
SELECT @effectiveDate=e.EffectiveDate FROM dbo.Events e WHERE DbIdentifier = @DBIdentifier
INSERT INTO @results
        ( Entity_ClientID ,
          Entity_InternalID ,
          Entity_IsNew
        )
SELECT emap.EntityIDMapping_ClientID,emap.EntityIDMapping_InternalID,0 FROM dbo.EntityIDMapping emap INNER JOIN @ClientIDs client ON emap.EntityIDMapping_ClientID=client.String WHERE EntityIDMapping_TypeID=@EntityTypeID

INSERT INTO dbo.EntityIDMapping
        ( EntityIDMapping_TypeID ,
          EntityIDMapping_InternalID ,
          EntityIDMapping_ClientID ,
          EntityIDMapping_EffectiveDate
        )
OUTPUT Inserted.EntityIDMapping_ClientID,Inserted.EntityIDMapping_InternalID,1 INTO @results
SELECT @EntityTypeID,@MaxID+ROW_NUMBER() OVER (ORDER BY string ASC),string,@effectiveDate FROM @ClientIDs WHERE String NOT IN (SELECT EntityIDMapping_ClientID FROM dbo.EntityIDMapping WHERE EntityIDMapping_TypeID=@EntityTypeID)

SELECT * FROM  @results
END
