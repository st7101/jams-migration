
PRINT N'Starting rebuilding table [dbo].[PositionPersonnel]...';
CREATE TABLE [dbo].[tmp_ms_xx_PositionPersonnel] (
    [PositionPersonnel_Id] BIGINT             NOT NULL,
    [EffectiveDate]        DATETIMEOFFSET (7) NULL,
    [Position_ExternalId]          NVARCHAR (100)     NULL,
    [Position_Id]   BIGINT             NULL,
    [Personnel_Id]         NVARCHAR (100)     NULL,

    CONSTRAINT [tmp_ms_xx_constraint_PK_PositionPersonnel] PRIMARY KEY CLUSTERED ([PositionPersonnel_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[PositionPersonnel])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_PositionPersonnel] WITH (TABLOCK) ([PositionPersonnel_Id], [EffectiveDate], [Position_ExternalId], [Personnel_Id])
        SELECT   [PositionPersonnel_Id],
                 [EffectiveDate],
                 [Position_Id],
                 [Personnel_Id]
        FROM     [dbo].[PositionPersonnel]
        ORDER BY [PositionPersonnel_Id] ASC;
    END

DROP TABLE [dbo].[PositionPersonnel];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PositionPersonnel]', N'PositionPersonnel';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_PositionPersonnel]', N'PK_PositionPersonnel', N'OBJECT';


