IF (OBJECT_ID('[GetTerritoryShapeOverridesInBoundingBox]') IS NOT NULL)
  DROP PROCEDURE [GetTerritoryShapeOverridesInBoundingBox]
    DECLARE @SqlCommand NVARCHAR(MAX)= '
-- =============================================
-- Author:		Steve Tetrault
-- Create date: Jun. 17, 2014
-- Description:	Modified Logic for GetTerritoryShapeOverridesInBoundingBox SP
-- =============================================
CREATE PROCEDURE [dbo].[GetTerritoryShapeOverridesInBoundingBox]
	-- Add the parameters for the stored procedure here
    @positionId BIGINT ,
    @zoomLevel INT ,
    @ShapeWKB VARBINARY(MAX) ,
    @tolerance FLOAT ,
    @ShapeWKBType NVARCHAR(100) ,
    @territoryTypeId BIGINT ,
    @showAssigned BIT ,
    @showUnassigned BIT ,
    @TreeLeft INT ,
    @treeRight INT
AS
    BEGIN
        DECLARE @geo GEOMETRY

        IF ( @ShapeWKBType = ''GeometryCollection'' )
            SET @geo = geometry::STGeomCollFromWKB(@ShapeWKB, 0)
        IF ( @ShapeWKBType = ''MultiPolygon'' )
            SET @geo = geometry::STMPolyFromWKB(@ShapeWKB, 0)
        IF ( @ShapeWKBType = ''Polygon'' )
            SET @geo = geometry::STPolyFromWKB(@ShapeWKB, 0)
        IF ( @ShapeWKBType = ''Geometry'' )
            SET @geo = geometry::STGeomFromWKB(@ShapeWKB, 0)

        SET @geo = @geo.Reduce(@tolerance)

        DECLARE @sql NVARCHAR(MAX)= ''select shapes.OrgUnit_Id as OrgUnitId, org.OrgUnit_Name as OrgUnitName, org.OrgUnit_ColorId as OrgUnitColorId, ''
        IF ( @zoomLevel > 10 )
            BEGIN
                SET @sql = @sql
                    + ''Shape = shapes.geom.STIntersection(@geoIn).Reduce(@ToleranceIn) ''
            END
        ELSE
            IF ( @ZoomLevel >= 7 )
                BEGIN
                    SET @sql = @sql
                        + ''Shape = isnull(shapes.geom_medium,shapes.geom).STIntersection(@geoIn).Reduce(@ToleranceIn) ''
    
                END
            ELSE
                BEGIN
                    SET @sql = @sql
                        + '' isnull(shapes.geom_low,shapes.geom).STIntersection(@geoIn).Reduce(@ToleranceIn) ''

                END
        SET @sql = @sql + '' from TerritoryShapeOverrides as shapes
left outer join Orgunits as org
on shapes.OrgUnit_Id = org.OrgUnit_Id
left outer join OrgUnitOverrides as orgOverride
on shapes.OrgUnit_Id=orgOverride.OrgUnitOverride_OrgUnitId
inner join TerritoryTypes as t
on org.OrgUnitLevel_id in (t.OrgUnitLevel_id,orgOverride.Level_id) 
where Position_Id = '' + CAST(@positionId AS VARCHAR)
            + '' and t.TerritoryType_Id = '' + CAST(@territoryTypeId AS VARCHAR)
            + CHAR(13)
        IF ( @showAssigned = 1
             AND @showUnassigned = 1
           )
            BEGIN
                SET @sql = @sql + ''and (( org.OrgUnit_TreeLeft >= ''
                    + CAST(@treeLeft AS NVARCHAR)
                    + '' and org.OrgUnit_TreeRight <= ''
                    + CAST(@treeRight AS NVARCHAR)
                    + '') 
					OR orgOverride.OrgUnitOverride_RefinementOperationType=''''Add''''
					or org.OrgUnit_IsUnassigned = 1)''
            END
        ELSE
            IF ( @showAssigned = 1 )
                BEGIN
                    SET @sql = @sql + ''and (( org.OrgUnit_TreeLeft >= ''
                        + CAST(@treeLeft AS NVARCHAR)
                        + '' and org.OrgUnit_TreeRight <= ''
                        + CAST(@treeRight AS NVARCHAR)
                        + '') 
					OR orgOverride.OrgUnitOverride_RefinementOperationType=''''Add'''')
''
                END
            ELSE
                IF ( @showUnassigned = 1 )
                    BEGIN
                        SET @sql = @sql + ''and org.OrgUnit_IsUnassigned = 1
''
                    END

        EXEC sp_executesql @sql, N''@geoIn geometry, @toleranceIn float'',
            @geoIn = @geo, @toleranceIn = @tolerance
    END

'

EXEC (@SqlCommand)