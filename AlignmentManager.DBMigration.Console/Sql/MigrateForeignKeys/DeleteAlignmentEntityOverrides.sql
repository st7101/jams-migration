

PRINT N'Updating [dbo].[DeleteAlignmentEntityOverrides] ...';

UPDATE daeo
SET DeleteAlignmentEntityOverride_AlignmentEntityId=ae.AlignmentEntity_Id
FROM [dbo].[DeleteAlignmentEntityOverrides] daeo
INNER JOIN AlignmentEntitys ae ON ae.AlignmentEntity_ExternalID=daeo.DeleteAlignmentEntityOverride_ExternalAlignmentEntityId

