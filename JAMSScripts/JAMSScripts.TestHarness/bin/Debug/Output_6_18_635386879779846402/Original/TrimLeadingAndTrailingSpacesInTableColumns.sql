Declare c Cursor For 
(
SELECT COLUMN_NAME 
FROM INFORMATION_SCHEMA.COLUMNS
WHERE TABLE_NAME = 'Positions' and DATA_TYPE='nvarchar'
)
Open c

Declare @columnName nvarchar(128);
Declare @qry nvarchar(max);
Fetch next From c into @columnName;

While @@Fetch_Status=0 Begin
	
	-- UPDATE Roles
	-- SET Role_Id = LTRIM(RTRIM(Role_Id)) 
	-- WHERE Role_Id LIKE ' %' OR Role_Id LIKE '% '
	set @qry =	'UPDATE ['+ 'Positions' + 
				'] SET [' + @columnName + ']= LTRIM(RTRIM(['+ @columnName+'] ))'+
				' WHERE ['+ @columnName + '] LIKE '' %'' or [' + @columnName +'] LIKE ''% '' ';
   
   Exec sp_executesql @qry  
   Fetch next From c into @columnName
End

Close c
Deallocate c
