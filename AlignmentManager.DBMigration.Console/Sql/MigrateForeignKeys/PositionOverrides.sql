
PRINT N'Updating PositionOverrides ...';

UPDATE po
SET [PositionOverride_OrgUnitId]=ou.OrgUnit_Id,[PositionOverride_PositionId]=p.Position_Id
FROM dbo.PositionOverrides po INNER JOIN 
dbo.OrgUnits ou ON po.PositionOverride_ExternalOrgUnitId=ou.OrgUnit_ExternalID
INNER JOIN dbo.Positions p ON po.PositionOverride_PositionExternalId=p.Position_ExternalID

