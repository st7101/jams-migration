SELECT postoreturn.* 
                                                                FROM   positions postoreturn 
																INNER JOIN OrgUnits ou on postoreturn.OrgUnit_id = ou.OrgUnit_Id
                                                                       INNER JOIN(SELECT pospers.*, 
                                                                                         listreturned.positiondepth 
                                                                                  FROM   positionpersonnel pospers 
                                                                                         INNER JOIN (SELECT poslistone.positionpersonnel_id, 
                                                                                                            poslistone.positiondepth 
                                                                                                     FROM   (SELECT pospers.*, 
                                                                                                                    finallist.positiondepth 
                                                                                                                    positiondepth 
                                                                                                             FROM   positionpersonnel pospers 
                                                                                                                    INNER JOIN (SELECT 
                                                                                                                    sortedpositions.* 
                                                                                                                                FROM   (SELECT 
                                                                posdatelist.position_id, 
                                                                posdatelist.effectivedate, 
                                                                COUNT(o.orgunit_id) AS 
                                                                positiondepth 
                                                                FROM   (SELECT pos.*, 
                                                                ppos.effectivedate, 
                                                                o.orgunit_treeleft  AS 
                                                                treeleft, 
                                                                o.orgunit_treeright AS 
                                                                treeright 
                                                                FROM   personnel pers 
                                                                INNER JOIN positionpersonnel 
                                                                ppos 
                                                                ON pers.personnel_id = 
                                                                ppos.personnel_id 
                                                                INNER JOIN positions pos 
                                                                INNER JOIN 
                                                                orgunits o 
                                                                ON 
                                                                o.orgunit_id = pos.orgunit_id 
                                                                ON pos.position_id = 
                                                                ppos.position_id 
                                                                AND ppos.effectivedate <= '6/18/2014 12:04:47 PM' 
                                                                AND pers.personnel_id = 
                                                                '10000726') 
                                                                posdatelist 
                                                                INNER JOIN orgunits o 
                                                                ON ( o.orgunit_treeleft <= 
                                                                posdatelist.treeleft 
                                                                AND o.orgunit_treeright >= 
                                                                posdatelist.treeright ) 
                                                                GROUP  BY posdatelist.position_id, 
                                                                posdatelist.effectivedate) 
                                                                sortedpositions 
                                                                ) 
                                                                finallist 
                                                                ON pospers.position_id = finallist.position_id) poslistone 
                                                                JOIN (SELECT MAX(poslisttwo.effectivedate) AS returndate, 
                                                                poslisttwo.position_id 
                                                                FROM   (Select  pospers.*, 
                                                                finallist.positiondepth 
                                                                FROM   positionpersonnel pospers 
                                                                INNER JOIN (SELECT sortedpositions.* 
                                                                FROM   (SELECT posdatelist.position_id, 
                                                                posdatelist.effectivedate, 
                                                                COUNT(o.orgunit_id) AS 
                                                                positiondepth 
                                                                FROM   (SELECT pos.*, 
                                                                ppos.effectivedate, 
                                                                o.orgunit_treeleft  AS 
                                                                treeleft, 
                                                                o.orgunit_treeright AS 
                                                                treeright 
                                                                FROM   personnel pers 
                                                                INNER JOIN positionpersonnel 
                                                                ppos 
                                                                ON pers.personnel_id = 
                                                                ppos.personnel_id 
                                                                INNER JOIN positions pos 
                                                                INNER JOIN 
                                                                orgunits o 
                                                                ON 
                                                                o.orgunit_id = pos.orgunit_id 
                                                                ON pos.position_id = 
                                                                ppos.position_id 
                                                                AND ppos.effectivedate <= '6/18/2014 12:04:47 PM'
                                                                AND pers.personnel_id = 
                                                                '10000726') 
                                                                posdatelist 
                                                                INNER JOIN orgunits o 
                                                                ON ( o.orgunit_treeleft <= 
                                                                posdatelist.treeleft 
                                                                AND o.orgunit_treeright 
                                                                >= 
                                                                posdatelist.treeright 
                                                                ) 
                                                                GROUP  BY posdatelist.position_id, 
                                                                posdatelist.effectivedate) 
                                                                sortedpositions 
                                                                ) 
                                                                finallist                                                                
                                                                ON pospers.position_id = finallist.position_id and pospers.EffectiveDate<= '6/18/2014 12:04:47 PM' ) 
                                                                poslisttwo 
                                                                GROUP  BY poslisttwo.position_id) AS combinedpositionlist 
                                                                ON poslistone.effectivedate = combinedpositionlist.returndate 
                                                                AND poslistone.position_id = combinedpositionlist.position_id) 
                                                                listreturned 
                                                                ON listreturned.positionpersonnel_id = pospers.positionpersonnel_id 
                                                                AND pospers.personnel_id = '10000726') posperreturned 
                                                                ON postoreturn.position_id = posperreturned.position_id 
                                                                ORDER  BY positiondepth, ou.OrgUnit_Name