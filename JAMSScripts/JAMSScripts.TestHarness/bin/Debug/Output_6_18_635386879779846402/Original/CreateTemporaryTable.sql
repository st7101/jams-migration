SELECT  o.OrgUnit_id as OrgUnitId
		,[AlignmentEntity_Latitude] as ClusterLat
		,[AlignmentEntity_Longitude] as ClusterLong
		,Count([AlignmentEntity_Latitude]) as ClusterCount
FROM [AlignmentEntitys] as c
inner join 
OrgUnitAlignmentEntitys as o
on c.AlignmentEntity_Id = o.AlignmentEntity_id
inner join Orgunits as org
on o.OrgUnit_Id = org.OrgUnit_Id
inner join TerritoryTypes as t
on org.OrgUnitLevel_id = t.OrgUnitLevel_id
inner join AlignmentEntityTypes as at
on at.AlignmentEntityType_Id = c.AlignmentEntityType_id
where
t.TerritoryType_Id = '162' 
and c.AlignmentEntity_Latitude < 47.4596862792969 and c.AlignmentEntity_Latitude > 42.6969776153564 and c.AlignmentEntity_Longitude < -66.9500045776367 and c.AlignmentEntity_Longitude > -73.4377403259277
and ((0 = 1 and org.OrgUnit_TreeLeft >= 1 and org.OrgUnit_TreeRight <= 10430) or (0 = 1 and org.OrgUnit_IsUnassigned = 1))
and at.AlignmentEntityType_Name  = 'Geo' and AlignmentEntity_Latitude is not null
group by o.OrgUnit_id, [AlignmentEntity_Latitude] ,[AlignmentEntity_Longitude]
