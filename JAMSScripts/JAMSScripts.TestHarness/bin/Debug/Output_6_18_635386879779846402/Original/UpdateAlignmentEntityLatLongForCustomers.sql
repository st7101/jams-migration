update AlignmentEntitys
Set AlignmentEntity_Latitude = (Select Customer_Latitude from Customers Where Customer_Id = AlignmentEntity_Id),
AlignmentEntity_Longitude = (Select Customer_Longitude from Customers Where Customer_Id = AlignmentEntity_Id)
Where AlignmentEntity_Id in (Select Customer_Id From Customers)