﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.SqlServer.Types;

namespace JAMSScripts.TestHarness
{


    class TestParameters
    {
        #region Fields
        private List<string[]> _TERRITORYTYPEID_List;
        public string[] TERRITORYTYPEID
        {
            get
            {
                return (_TERRITORYTYPEID_List.Count != 0) ? _TERRITORYTYPEID_List[_rand.Next(0, _TERRITORYTYPEID_List.Count - 1)] : new string[] { "-1" };
            }

        }

        private Dictionary<string, string[]> _mappings;
        public string SEARCHSTRING
        {
            get { return "'" + alphabet[_rand.Next(0, 25)].ToString() + "'"; }

        }

        public int ISGEO
        {
            get { return _rand.Next(0, 1); }

        }
        private List<string> _AESTATUS_List;
        public string AESTATUS
        {
            get { return (_AESTATUS_List.Count != 0) ? "'" + _AESTATUS_List[_rand.Next(0, _AESTATUS_List.Count - 1)] + "'" : ""; }

        }
        private int _TREELEFT;

        public int TREELEFT
        {
            get { return _TREELEFT; }

        }
        private int _TREERIGHT;

        public int TREERIGHT
        {
            get { return _TREERIGHT; }

        }

        public int SHOWASSIGNED
        {
            get { return _rand.Next(0, 1); }

        }

        public int SHOWUNASSIGNED
        {
            get { return _rand.Next(0, 1); }

        }

        public string[] TERRTYPEID
        {
            get { return TERRITORYTYPEID; }

        }

        public int ZOOMLEVEL
        {
            get { return _rand.Next(7, 22); }

        }
        private int _GEOENTITYTYPEID;

        public int GEOENTITYTYPEID
        {
            get { return _GEOENTITYTYPEID; }

        }
        private SqlDecimal _MINLNG;

        public SqlDecimal MINLNG
        {
            get { return _MINLNG; }

        }
        private SqlDecimal _MAXLNG;

        public SqlDecimal MAXLNG
        {
            get { return _MAXLNG; }

        }
        private SqlDecimal _MINLAT;

        public SqlDecimal MINLAT
        {
            get { return _MINLAT; }

        }
        private SqlDecimal _MAXLAT;

        public SqlDecimal MAXLAT
        {
            get { return _MAXLAT; }

        }
        private string _BOUNDINGBOXWKT;

        public string BOUNDINGBOXWKT
        {
            get { return "'" + _BOUNDINGBOXWKT + "'"; }

        }

        private SqlGeometry _GEOM;

        public string SHAPEWKB
        {
            get
            {
                StringBuilder sb = new StringBuilder();
                var temp = _GEOM.STAsBinary();
                foreach (var b in temp.Value)
                {
                    sb.AppendFormat("{0:x2}", b);
                }
                return sb.ToString();
            }

        }

        public string SHAPEWKBTYPE
        {
            get { return _GEOM.STGeometryType().ToString(); }
        }
        private int _MAXRESULT;

        public int MAXRESULT
        {
            get { return 100; }

        }
        public SqlDateTime CURRENTDATE
        {
            get
            {
                return new SqlDateTime(DateTime.Now);
            }

        }
        private List<string> _PERSONNELID_List;

        public string PERSONNELID
        {
            get
            {
                return (_PERSONNELID_List.Count != 0) ? _PERSONNELID_List[_rand.Next(0, _PERSONNELID_List.Count - 1)] : "";
            }

        }

        public double REDUCETOLERANCE
        {
            get { return 0.5; }

        }
        private List<Int64> _REQUESTID_List;

        public Int64 REQUESTID
        {
            get
            {
                return (_REQUESTID_List.Count != 0) ? _REQUESTID_List[_rand.Next(0, _REQUESTID_List.Count - 1)] : -1;
            }

        }

        public string TABLENAME
        {
            get { return "'Positions'"; }

        }
        private List<string[]> _ATTRIBUTETYPEID_Text_List;
        private List<string[]> _ATTRIBUTETYPEID_Number_List;
        public string[] ATTRIBUTETYPEID_Text
        {
            get
            {
                return (_ATTRIBUTETYPEID_Text_List.Count != 0) ? _ATTRIBUTETYPEID_Text_List[_rand.Next(0, _ATTRIBUTETYPEID_Text_List.Count - 1)] : new string[1] { "-1" };
            }

        }
        public string[] ATTRIBUTETYPEID_Number
        {
            get
            {
                return (_ATTRIBUTETYPEID_Number_List.Count != 0) ? _ATTRIBUTETYPEID_Number_List[_rand.Next(0, _ATTRIBUTETYPEID_Number_List.Count - 1)] : new string[1] { "-1" };
            }

        }


        private List<Int64> _ALIGNMENTENTITYTYPEIDS_List;

        public string ALIGNMENTENTITYTYPEIDS
        {
            get { return GetRandomItems(_ALIGNMENTENTITYTYPEIDS_List); }

        }

        public string SORTSTATEMENT
        {
            get { return " Id ASC "; }

        }

        public Int64 TOTALNUMBEROFROWSSHOWN
        {
            get { return 20; }

        }

        public string SPANRESTRICTION
        {
            get { return ""; }

        }
        private List<string[]> _ORGUNITIDS_List;

        public string[] ORGUNITIDS
        {
            get { return (_ORGUNITIDS_List.Count != 0) ? _ORGUNITIDS_List[_rand.Next(0, _ORGUNITIDS_List.Count - 1)] : new string[] { "-1" }; }

        }

        public Int64 GEOTYPEID
        {
            get { return _GEOENTITYTYPEID; }

        }

        public string INDEPENDENTALIGNMENTENTITYTYPEIDS
        {
            get { return GetRandomItems(_ALIGNMENTENTITYTYPEIDS_List); }

        }

        public string GROUPEDALIGNMENTENTITYTYPEIDS
        {
            get { return GetRandomItems(_ALIGNMENTENTITYTYPEIDS_List); }

        }

        public Int32 PAGESIZE
        {
            get { return 20; }

        }

        public Int32 LASTSHOWNROW
        {
            get { return 5; }

        }

        public SqlDecimal TOPRIGHTLATITUDE
        {
            get { return _MAXLAT; }

        }

        public SqlDecimal BOTTOMLEFTLATITUDE
        {
            get { return _MINLAT; }

        }

        public SqlDecimal TOPRIGHTLONGITUDE
        {
            get { return _MAXLNG; }

        }
        public SqlDecimal BOTTOMLEFTLONGITUDE
        {
            get { return _MINLNG; }

        }

        public string STATUS
        {
            get { return (_AESTATUS_List.Count != 0) ? _AESTATUS_List[_rand.Next(0, _AESTATUS_List.Count - 1)] : ""; }

        }
        private List<string[]> _AEIDs_List;

        public string[] PARENTIDS
        {
            get { return GetMappedIds(_AEIDs_List); }

        }

        public SqlDecimal MINLATITUDE
        {
            get { return _MINLAT; }

        }

        public SqlDecimal MINLONGITUDE
        {
            get { return _MINLNG; }

        }

        public SqlDecimal MAXLATITUDE
        {
            get { return _MAXLAT; }

        }

        public SqlDecimal MAXLONGITUDE
        {
            get { return _MAXLNG; }

        }
        private List<string[]> _POSITIONID_List;

        public string[] POSITIONID
        {
            get { return (_POSITIONID_List.Count != 0) ? _POSITIONID_List[_rand.Next(0, _POSITIONID_List.Count - 1)]  :new string[] { "-1" }; }
        }
        private List<string> _ATTRIBUTETYPENAMES_List;

        public string NEWATTRIBUTETYPENAMES
        {
            get { return (_ATTRIBUTETYPENAMES_List.Count != 0) ? "'" + _ATTRIBUTETYPENAMES_List[_rand.Next(0, _ATTRIBUTETYPENAMES_List.Count - 1)] + "'" : ""; }

        }

        public int SHOWGEOS
        {
            get { return _rand.Next(0, 1); }

        }

        public int SHOWCUSTOMERS
        {
            get { return _rand.Next(0, 1); }

        }
        private List<string> _PERSONNELSTATUS_List;

        public string PERSONNELSTATUS
        {
            get { return (_PERSONNELSTATUS_List.Count != 0) ? "'" + _PERSONNELSTATUS_List[_rand.Next(0, _PERSONNELSTATUS_List.Count - 1)] + "'" : ""; }

        }

        public string[] ALIGNMENTENTITYIDS
        {
            get { return GetMappedIds(_AEIDs_List); }
        }
        public SqlDecimal LATITUDE
        {
            get { return _MINLAT; }

        }
        public SqlDecimal LONGITUDE
        {
            get { return _MINLNG; }

        }
        #endregion

        #region Variables
        string _eventCon;
        Random _rand;
        static char[] alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };
        #endregion

        public TestParameters(string eventConnectionString)
        {
            _eventCon = eventConnectionString;
            _rand = new Random();
            _mappings = new Dictionary<string, string[]>();
            InitializeParameters();
        }

        public string GetParameterValue(string name, bool legacy)
        {
 
            return GetParameterMapping(name)[legacy?0:1];
        }

        public string[] GetParameterMapping(string name)
        {
            name = name.TrimStart(':');
            string returnVal = "";
            string[] map = null;
            if (_mappings.ContainsKey(name))
            {
                map = _mappings[name];
                return map;
            }
            switch (name)
            {
                case "TERRITORYTYPEID":
                case "TERRTYPEID":
                    if (_mappings.ContainsKey("TERRITORYTYPEID"))
                        map = _mappings["TERRITORYTYPEID"];
                    else
                    {
                        map = TERRITORYTYPEID;
                        _mappings.Add("TERRITORYTYPEID", map);
                    }
                    break;
                case "SEARCHSTRING": returnVal = SEARCHSTRING.ToString(); break;
                case "ISGEO": returnVal = ISGEO.ToString(); break;
                case "AESTATUS":
                case "STATUS":
                    if (_mappings.ContainsKey("AESTATUS"))
                        map = _mappings["AESTATUS"];
                    else
                    {
                        map = new string[] { AESTATUS, AESTATUS };
                        _mappings.Add("AESTATUS", map);
                    }
                    break;

                case "TREELEFT": returnVal = TREELEFT.ToString(); break;
                case "TREERIGHT": returnVal = TREERIGHT.ToString(); break;
                case "SHOWASSIGNED": returnVal = SHOWASSIGNED.ToString(); break;
                case "SHOWUNASSIGNED": returnVal = SHOWUNASSIGNED.ToString(); break;
                case "ZOOMLEVEL": returnVal = ZOOMLEVEL.ToString(); break;
                case "GEOENTITYTYPEID": returnVal = GEOENTITYTYPEID.ToString(); break;
                case "MINLAT":
                case "MINLATITUDE":
                case "LATITUDE":
                case "BOTTOMLEFTLATITUDE":
                    if (_mappings.ContainsKey("MINLAT"))
                        map = _mappings["MINLAT"];
                    else
                    {
                        map = new string[] { MINLAT.ToString(), MINLAT.ToString() };
                        _mappings.Add("MINLAT", map);
                    }
                    break;
                case "MAXLAT":
                case "TOPRIGHTLATITUDE":
                case "MAXLONGITUDE":
                    if (_mappings.ContainsKey("MAXLAT"))
                        map = _mappings["MAXLAT"];
                    else
                    {
                        map = new string[] { MAXLAT.ToString(), MAXLAT.ToString() };
                        _mappings.Add("MAXLAT", map);
                    }
                    break;
                case "MINLNG":
                case "BOTTOMLEFTLONGITUDE":
                case "LONGITUDE":
                case "MINLONGITUDE":
                    if (_mappings.ContainsKey("MINLNG"))
                        map = _mappings["MINLNG"];
                    else
                    {
                        map = new string[] { MINLNG.ToString(), MINLNG.ToString() };
                        _mappings.Add("MINLNG", map);
                    }
                    break;
                case "MAXLNG":
                case "MAXLATITUDE":
                case "TOPRIGHTLONGITUDE":
                    if (_mappings.ContainsKey("MAXLNG"))
                        map = _mappings["MAXLNG"];

                    else
                    {
                        map = new string[] { MAXLNG.ToString(), MAXLNG.ToString() };
                        _mappings.Add("MAXLNG", map);
                    }
                    break;
                case "MAXRESULT": returnVal = MAXRESULT.ToString(); break;
                case "CURRENTDATE": returnVal = "'" + CURRENTDATE.ToString() + "'"; break;
                case "PERSONNELID": returnVal = PERSONNELID.ToString(); break;
                case "REDUCETOLERANCE": returnVal = REDUCETOLERANCE.ToString(); break;
                case "REQUESTID": returnVal = REQUESTID.ToString(); break;
                case "TABLENAME": returnVal = TABLENAME.ToString(); break;
                case "ALIGNMENTENTITYTYPEIDS":
                    if (_mappings.ContainsKey("ALIGNMENTENTITYTYPEIDS"))
                        map = _mappings["ALIGNMENTENTITYTYPEIDS"];
                    else
                    {
                        var temp = ALIGNMENTENTITYTYPEIDS;
                        map = new string[] { temp, "'" + temp + "'" };
                        _mappings.Add("ALIGNMENTENTITYTYPEIDS", map);
                    }
                    break;


                case "SORTSTATEMENT":
                    if (_mappings.ContainsKey("SORTSTATEMENT"))
                        map = _mappings["SORTSTATEMENT"];
                    else
                    {
                        var temp = SORTSTATEMENT;
                        map = new string[] { temp, "'" + temp + "'" };
                        _mappings.Add("SORTSTATEMENT", map);
                    }
                    break;
                case "TOTALNUMBEROFROWSSHOWN": returnVal = TOTALNUMBEROFROWSSHOWN.ToString(); break;
                case "SPANRESTRICTION":
                    if (_mappings.ContainsKey("SPANRESTRICTION"))
                        map = _mappings["SPANRESTRICTION"];
                    else
                    {
                        var temp = SPANRESTRICTION;
                        map = new string[] { temp, "'" + temp + "'" };
                        _mappings.Add("SPANRESTRICTION", map);
                    }
                    break;
                case "ORGUNITIDS":
                    if (_mappings.ContainsKey("ORGUNITIDS"))
                        map = _mappings["ORGUNITIDS"];
                    else
                        map = ORGUNITIDS;
                    _mappings.Add("ORGUNITIDS", map);
                   

                    break;
                case "GEOTYPEID": returnVal = GEOTYPEID.ToString(); break;
                case "INDEPENDENTALIGNMENTENTITYTYPEIDS":
                    if (_mappings.ContainsKey("INDEPENDENTALIGNMENTENTITYTYPEIDS"))
                        map = _mappings["INDEPENDENTALIGNMENTENTITYTYPEIDS"];
                    else
                    {
                        var temp = INDEPENDENTALIGNMENTENTITYTYPEIDS;
                        map = new string[] { temp, "'" + temp + "'" };
                        _mappings.Add("INDEPENDENTALIGNMENTENTITYTYPEIDS", map);
                    }
                    break;


                case "GROUPEDALIGNMENTENTITYTYPEIDS":
                    if (_mappings.ContainsKey("GROUPEDALIGNMENTENTITYTYPEIDS"))
                        map = _mappings["GROUPEDALIGNMENTENTITYTYPEIDS"];
                    else
                    {
                        var temp = GROUPEDALIGNMENTENTITYTYPEIDS;
                        map = new string[] { temp, "'" + temp + "'" };
                        _mappings.Add("GROUPEDALIGNMENTENTITYTYPEIDS", map);
                    }
                    break;
                case "PAGESIZE": returnVal = PAGESIZE.ToString(); break;
                case "LASTSHOWNROW": returnVal = LASTSHOWNROW.ToString(); break;

                case "PARENTIDS":
                    if (_mappings.ContainsKey("PARENTIDS"))
                        map = _mappings["PARENTIDS"];
                    else
                    {
                        map = PARENTIDS;
                        _mappings.Add("PARENTIDS", map);
                    }
                    break;
                case "POSITIONID":
                    if (_mappings.ContainsKey("POSITIONID"))
                        map = _mappings["POSITIONID"];
                    else
                    {
                        map = POSITIONID;
                        _mappings.Add("POSITIONID", map);
                    }
                    break;
                case "NEWATTRIBUTETYPENAMES": returnVal = NEWATTRIBUTETYPENAMES.ToString(); break;
                case "SHOWGEOS": returnVal = SHOWGEOS.ToString(); break;
                case "SHOWCUSTOMERS": returnVal = SHOWCUSTOMERS.ToString(); break;
                case "PERSONNELSTATUS": returnVal = PERSONNELSTATUS.ToString(); break;
                case "ALIGNMENTENTITYIDS":
                    if (_mappings.ContainsKey("ALIGNMENTENTITYIDS"))
                        map = _mappings["ALIGNMENTENTITYIDS"];
                    else
                    {
                        map = ALIGNMENTENTITYIDS;
                        _mappings.Add("AESTATUS", map);
                    }
                    break;
                case "SHAPEWKB": returnVal = "0x" + SHAPEWKB; break;
                case "SHAPEWKBTYPE": returnVal = "'" + SHAPEWKBTYPE + "'"; break;
                case "BOUNDINGBOXWKT": returnVal = BOUNDINGBOXWKT; break;

                case "ATTRIBUTETYPEID_Number":
                    if (_mappings.ContainsKey("ATTRIBUTETYPEID_Number"))
                        map = _mappings["ATTRIBUTETYPEID_Number"];
                    else
                    {
                        map = new string[] { ATTRIBUTETYPEID_Number[0] + "," + ATTRIBUTETYPEID_Number[1], ATTRIBUTETYPEID_Number[0] + "," + ATTRIBUTETYPEID_Number[1] };
                        _mappings.Add("ATTRIBUTETYPEID_Number", map);
                    }
                    break;


                case "ATTRIBUTETYPEID_Text":
                    if (_mappings.ContainsKey("ATTRIBUTETYPEID_Text"))
                        map = _mappings["ATTRIBUTETYPEID_Text"];
                    else
                    {
                        map = new string[] { ATTRIBUTETYPEID_Text[0] + "," + ATTRIBUTETYPEID_Text[1], ATTRIBUTETYPEID_Number[0] + "," + ATTRIBUTETYPEID_Number[1] };
                        _mappings.Add("ATTRIBUTETYPEID_Text", map);
                    }
                    break;
                default:
                    {
                        throw new ArgumentOutOfRangeException(String.Format("Variable not found: {0}", name));
                        break;
                    }

            }
            if (map == null)
            {
                map = new string[] { returnVal, returnVal };
                _mappings.Add(name, map);
            }

            return map;
        }


        private void InitializeParameters()
        {
            LoadTerritoryTypes();
            LoadAEStatus();
            LoadOUTree();
            LoadGeoAEType();
            LoadShapeInfo();
            LoadPersonnel();
            LoadRequests();
            LoadAttributes();
            LoadAETypes();
            LoadOUs();
            LoadAEs();
            LoadPositions();
            LoadAttributeNames();
            LoadPersonnelStatus();
        }

        private string GetRandomItems<T>(List<T> list)
        {
            int count = list.Count;
            int maxItems;
            string returnVal = "";
            if (count > 1)
            {
                maxItems = _rand.Next(2, count);
                for (int i = 0; i < maxItems; i++)
                {
                    returnVal = returnVal + ((i == 0) ? list[i].ToString() : "," + list[i].ToString());
                }
            }
            else
                returnVal = list[0].ToString();

            return returnVal;
        }

        private string[] GetMappedIds(List<string[]> list)
        {
            int count = list.Count;
            int maxItems;
            StringBuilder legacyID = new StringBuilder();
            StringBuilder newID = new StringBuilder();
            if (count > 1)
            {
                maxItems = _rand.Next(2, count);
                for (int i = 0; i < maxItems; i++)
                {
                    legacyID.Append((i == 0) ? list[i][0] : "," + list[i][0]);
                    newID.Append(((i == 0) ? list[i][1] : "," + list[i][1]));
                }
            }
            else
            {
                legacyID.Append(list[0][0] );
                newID.Append(list[0][1]);
            }
            return new string[] { legacyID.ToString(), newID.ToString() };
        }
        private void LoadTerritoryTypes()
        {
            _TERRITORYTYPEID_List = new List<string[]>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetTerritoryTypes, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _TERRITORYTYPEID_List.Add(new string[2] { "'" + rdr.GetString(0) + "'", rdr.GetInt64(1).ToString() });
                        }
                    }
                }
            }
        }

        private void LoadPersonnel()
        {
            _PERSONNELID_List = new List<string>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetPersonnelIDs, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _PERSONNELID_List.Add("'"+rdr.GetString(0)+"'");
                        }
                    }
                }
            }
        }

        private void LoadRequests()
        {
            _REQUESTID_List = new List<Int64>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetRequestIDs, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _REQUESTID_List.Add(rdr.GetInt64(0));
                        }
                    }
                }
            }
        }

        private void LoadAttributes()
        {
            _ATTRIBUTETYPEID_Number_List = new List<string[]>();
            _ATTRIBUTETYPEID_Text_List = new List<string[]>();

            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetAttributeIDs, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            if (rdr.GetString(1) == "Number")
                                _ATTRIBUTETYPEID_Number_List.Add(new string[2] { rdr.GetInt32(0).ToString(), rdr.GetString(2) });
                            else
                                _ATTRIBUTETYPEID_Text_List.Add(new string[2] { rdr.GetInt32(0).ToString(), rdr.GetString(2) });
                        }
                    }
                }
            }
        }
        private void LoadAETypes()
        {
            _ALIGNMENTENTITYTYPEIDS_List = new List<Int64>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetAETypeIDs, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _ALIGNMENTENTITYTYPEIDS_List.Add(rdr.GetInt64(0));
                        }
                    }
                }
            }
        }


        private void LoadAEStatus()
        {
            _AESTATUS_List = new List<string>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetAEStatus, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _AESTATUS_List.Add(rdr.GetString(0));
                        }
                    }
                }
            }
        }

        private void LoadPersonnelStatus()
        {
            _PERSONNELSTATUS_List = new List<string>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetPersonnelStatus, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _PERSONNELSTATUS_List.Add(rdr.GetString(0));
                        }
                    }
                }
            }
        }


        private void LoadAttributeNames()
        {
            _ATTRIBUTETYPENAMES_List = new List<string>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetAttributeNames, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _ATTRIBUTETYPENAMES_List.Add(rdr.GetString(0));
                        }
                    }
                }
            }
        }

        private void LoadAEs()
        {
            _AEIDs_List = new List<string[]>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetAEIDs, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _AEIDs_List.Add(new string[2] {   "'" + rdr.GetString(0) + "'", rdr.GetInt64(1).ToString() });
                        }
                    }
                }
            }
        }

        private void LoadOUs()
        {
            _ORGUNITIDS_List = new List<string[]>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetOUIDs, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _ORGUNITIDS_List.Add(new string[2] { "'" + rdr.GetString(0) + "'", rdr.GetInt64(1).ToString() });
                        }
                    }
                }
            }
        }

        private void LoadPositions()
        {
            _POSITIONID_List = new List<string[]>();
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetPositions, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _POSITIONID_List.Add(new string[2] { "'" + rdr.GetString(0) + "'", rdr.GetInt64(1).ToString() });
                        }
                    }
                }
            }
        }

        private void LoadOUTree()
        {
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetOUTree, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _TREELEFT = rdr.GetInt32(0);
                            _TREERIGHT = rdr.GetInt32(1);
                        }
                    }
                }
            }
        }

        private void LoadGeoAEType()
        {
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetGeoAEType, con))
                {
                    _GEOENTITYTYPEID = Int32.Parse(cmd.ExecuteScalar().ToString());
                }
            }
        }

        private void LoadShapeInfo()
        {
            using (SqlConnection con = new SqlConnection(_eventCon))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(SqlText.GetShapeInfo, con))
                {

                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            _MINLNG = new SqlDecimal(Decimal.Parse(rdr.GetValue(0).ToString()));
                            _MAXLNG = new SqlDecimal(Decimal.Parse(rdr.GetValue(1).ToString()));
                            _MINLAT = new SqlDecimal(Decimal.Parse(rdr.GetValue(2).ToString()));
                            _MAXLAT = new SqlDecimal(Decimal.Parse(rdr.GetValue(3).ToString()));
                            _BOUNDINGBOXWKT = rdr.GetString(4);
                            _GEOM = rdr.GetFieldValue<SqlGeometry>(5);
                        }
                    }
                }
            }
        }

        public void ClearMapping()
        {
            _mappings.Clear();
        }

    }


    class SqlText
    {
        public static string GetTerritoryTypes = "SELECT TerritoryType_ExternalId ,territoryType_id FROM TerritoryTypes";
        public static string GetAEStatus = "SELECT DISTINCT alignmententity_status FROM dbo.AlignmentEntitys";
        public static string GetOUTree = "SELECT TOP 1 OrgUnit_TreeLeft,OrgUnit_TreeRight FROM dbo.OrgUnits WHERE OrgUnit_ParentId IS NULL AND OrgUnit_IsUnassigned = 0";
        public static string GetGeoAEType = "SELECT AlignmentEntityType_Id FROM  dbo.AlignmentEntityTypes WHERE AlignmentEntityType_Name= 'Geo'";
        public static string GetShapeInfo = "SELECT TOP 1 MinLongitude,MaxLongitude,MinLatitude,MaxLatitude,geom.STAsText(),geom FROM TerritoryShapes ts INNER JOIN dbo.OrgUnits ou ON ou.OrgUnit_Id = ts.OrgUnit_Id WHERE OrgUnit_IsUnassigned=0 ORDER BY ou.OrgUnit_TreeLeft ";
        public static string GetPersonnelIDs = "SELECT TOP 50 p.Personnel_Id FROM dbo.Personnel p INNER JOIN dbo.PositionPersonnel pp ON pp.Personnel_id = p.Personnel_Id";
        public static string GetRequestIDs = "SELECT TOP 10 Request_Id FROM dbo.Requests";
        public static string GetAttributeIDs = "SELECT TOP 10 AttributeType_Id,AttributeType_DataType,AttributeType_Name FROM dbo.AttributeTypes WHERE AttributeType_DataType='Number' UNION ALL SELECT TOP 10 AttributeType_Id,AttributeType_DataType,AttributeType_Name FROM dbo.AttributeTypes WHERE AttributeType_DataType='Text'";
        public static string GetAETypeIDs = "SELECT AlignmentEntityType_Id FROM dbo.AlignmentEntityTypes";
        public static string GetAEIDs = "SELECT TOP 50 AlignmentEntity_ExternalId,AlignmentEntity_Id FROM dbo.AlignmentEntitys where AlignmentEntityType_Id=1";
        public static string GetOUIDs = "SELECT TOP 50 OrgUnit_ExternalId,OrgUnit_id FROM dbo.orgunits";
        public static string GetPositions = "SELECT TOP 50 Position_ExternalId,Position_Id  FROM dbo.Positions";
        public static string GetAttributeNames = "SELECT TOP 5 AttributeType_Name FROM dbo.AttributeTypes";
        public static string GetPersonnelStatus = "SELECT DISTINCT Personnel_Status FROM dbo.Personnel";
    }

}
