/*  Remove this query once beta feature for derived alignment is completed. */
EXECUTE  [dbo].[SearchQuery] 
   @showAssigned=0
  ,@showUnassigned=0
  ,@treeLeft= 1
  ,@treeRight= 10430
  ,@territoryTypeId=39
  ,@aeStatus='Normal'
  ,@MaxResult=100
  ,@GeoEntityTypeId=1
  ,@SearchString='u'
  ,@isGeo=0
  ,@alignmentEntityTypeIds=1,2,3
