IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[OrgUnits]') AND name = N'_idx_OrgUnits_ParenId')
DROP INDEX [_idx_OrgUnits_ParenId] ON [dbo].[OrgUnits] WITH ( ONLINE = OFF )

CREATE INDEX [_idx_OrgUnits_ParenId] ON [dbo].[OrgUnits] 
([OrgUnit_ParentId])