﻿delete ouae
from OrgUnitAlignmentEntitys ouae
join AlignmentEntitys ae on ouae.AlignmentEntity_Id = ae.AlignmentEntity_Id
where ae.AlignmentEntity_Status = 'SoftDelete';

delete ge
from GroupedEntities ge
join AlignmentEntitys ae on ge.AlignmentEntity_Id = ae.AlignmentEntity_Id
where ae.AlignmentEntity_Status = 'SoftDelete';

delete aenac
from AlignmentEntityNumberAttributeCache aenac
join AlignmentEntitys ae on aenac.AlignmentEntity_Id = ae.AlignmentEntity_Id
where ae.AlignmentEntity_Status = 'SoftDelete';

delete aetac
from AlignmentEntityTextAttributeCache aetac
join AlignmentEntitys ae on aetac.AlignmentEntity_Id = ae.AlignmentEntity_Id
where ae.AlignmentEntity_Status = 'SoftDelete';