﻿DECLARE @maxIdValue bigint;

IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedEntitiesWithAttributes') 
BEGIN
	PRINT 'Updating Numeric Attribute Cache';

	/* UPDATE THE NUMERIC ATTRIBUTE CACHE TABLE BY APPLYING THE CHANGES HAPPENED TO EACH ENTITY */
	UPDATE aenac
	SET aenac.AlignmentEntityNumberAttributeCache_Value = aenac.AlignmentEntityNumberAttributeCache_Value + temp.ChangeInValue
	FROM AlignmentEntityNumberAttributeCache aenac
	INNER JOIN TempUpdatedEntitiesWithAttributes temp ON 
	temp.AlignmentEntity_Id = aenac.AlignmentEntity_id AND temp.AttributeType_Id = aenac.AttributeType_id AND 
	(temp.TerritoryType_Id = aenac.TerritoryType_id OR (temp.TerritoryType_Id IS NULL AND aenac.TerritoryType_id IS NULL));

	SELECT * INTO NewEntitiesUpdatedWithNumberAttribute FROM 
	(SELECT AlignmentEntity_id, TerritoryType_id, AttributeType_id, ChangeInValue FROM TempUpdatedEntitiesWithAttributes temp
	 Where NOT EXISTS (Select 1 FROM AlignmentEntityNumberAttributeCache cache Where
	 temp.AlignmentEntity_Id = cache.AlignmentEntity_Id and temp.AttributeType_Id = cache.AttributeType_Id and
	 (temp.TerritoryType_Id = cache.TerritoryType_id OR (temp.TerritoryType_Id IS NULL AND cache.TerritoryType_id IS NULL)))) newNumeric

	 IF EXISTS (SELECT * FROM NewEntitiesUpdatedWithNumberAttribute)
	 BEGIN
	
		/* CREATE TEMPORARY TABLE FOR MISSING ENTRIES (BASICALLY ALL NEW ENTITIES WONT HAVE ANY ENTRY IN THE TABLE TO UPDATE)*/
		CREATE TABLE [TempAlignmentEntityNumberAttributeCache] 
		( AlignmentEntityNumberAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
		AlignmentEntityNumberAttributeCache_Value float NULL , 
		AlignmentEntity_id BIGINT NULL , 
		AttributeType_id bigint NULL , 
		TerritoryType_id BIGINT NULL , 
		CONSTRAINT PK_TempAlignmentEntityNumberAttributeCache_AlignmentEntityNumberAttributeCache_Id 
		PRIMARY KEY CLUSTERED (AlignmentEntityNumberAttributeCache_Id));

		/* INSERT ENTRIES FOR NEWLY ADDED CUSTOMERS INTO TEMP CACHE TABLE WITH AUTO GENERATED IDS */
		INSERT INTO TempAlignmentEntityNumberAttributeCache (AlignmentEntity_id, TerritoryType_id, AttributeType_id, AlignmentEntityNumberAttributeCache_Value)
	    SELECT AlignmentEntity_id, TerritoryType_id, AttributeType_id, ChangeInValue FROM NewEntitiesUpdatedWithNumberAttribute

		SELECT @maxIdValue = ISNULL(MAX(AlignmentEntityNumberAttributeCache_Id),0) FROM [AlignmentEntityNumberAttributeCache];

		 /* INSERT ENTRIES INTO ACTUAL CACHE TABLE FROM TEMP CACHE TABLE */
		INSERT INTO [AlignmentEntityNumberAttributeCache] WITH(TABLOCK) (AlignmentEntityNumberAttributeCache_Id, AlignmentEntityNumberAttributeCache_Value, AlignmentEntity_id, AttributeType_id, TerritoryType_id)
		SELECT AlignmentEntityNumberAttributeCache_Id + @maxIdValue, AlignmentEntityNumberAttributeCache_Value, AlignmentEntity_id, AttributeType_id, TerritoryType_id FROM [TempAlignmentEntityNumberAttributeCache]; 

		/* Update HiLo table */
		DELETE FROM HiLoUniqueKey
		WHERE ObjectType = 'AlignmentEntityNumberAttributeCache'

		INSERT INTO HiLoUniqueKey (NextHi, ObjectType)
		Values ((SELECT ISNULL(MAX(AlignmentEntityNumberAttributeCache_Id),0)+1 
					FROM [AlignmentEntityNumberAttributeCache]), 'AlignmentEntityNumberAttributeCache');

		DROP TABLE [TempAlignmentEntityNumberAttributeCache]
	END
END

/**** FIND OUT ALL ENTITIES THAT HAD ANY 'TEXT' ATTRIBUTES BEING UPDATED IN A REQUEST *********/
PRINT 'Updating Text Attribute Cache';

Select * INTO TempEntitiesUpdatedWithTextAttribute FROM (
SELECT DISTINCT eao.EditAttributeOverride_AlignmentEntityId as AlignmentEntity_Id, at.AttributeType_Id, eao.EditAttributeOverride_NewAttributeValue AS ChangedValue FROM EditAttributeOverrides eao
INNER JOIN AttributeTypes at ON at.AttributeType_Id = eao.AttributeType_Id
WHERE at.AttributeType_DataType = 'Text'
AND eao.Request_Id = :requestId
) a

/* UPDATE THE TEXT ATTRIBUTE CACHE TABLE BY APPLYING THE CHANGES HAPPENED TO EACH ENTITY */
UPDATE aetac
SET aetac.AlignmentEntityTextAttributeCache_Value = temp.ChangedValue
FROM AlignmentEntityTextAttributeCache aetac
INNER JOIN TempEntitiesUpdatedWithTextAttribute temp ON temp.AttributeType_Id = aetac.AttributeType_id and temp.AlignmentEntity_Id = aetac.AlignmentEntity_id


SELECT * INTO NewEntitiesUpdatedWithTextAttribute FROM (
	SELECT AlignmentEntity_id, AttributeType_id, ChangedValue FROM TempEntitiesUpdatedWithTextAttribute temp
	Where NOT EXISTS (Select 1 FROM [AlignmentEntityTextAttributeCache] cache Where
	temp.AlignmentEntity_Id = cache.AlignmentEntity_Id and temp.AttributeType_Id = cache.AttributeType_Id)
) new

IF EXISTS (SELECT * from NewEntitiesUpdatedWithTextAttribute)
BEGIN

	/* CREATE TEMPORARY TABLE */
	CREATE TABLE [TempAlignmentEntityTextAttributeCache] 
	( AlignmentEntityTextAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
	AlignmentEntityTextAttributeCache_Value nvarchar(2000) NULL ,
	AlignmentEntity_id BIGINT, 
	AttributeType_id bigint NULL , 
	CONSTRAINT PK_TempAlignmentEntityTextAttributeCache_AlignmentEntityTextAttributeCache_Id 
	PRIMARY KEY CLUSTERED (AlignmentEntityTextAttributeCache_Id))

	/* INSERT ENTRIES FOR NEWLY ADDED CUSTOMERS INTO TEMP CACHE TABLE WITH AUTO GENERATED IDS */
	INSERT INTO [TempAlignmentEntityTextAttributeCache] (AlignmentEntity_id, AttributeType_id, AlignmentEntityTextAttributeCache_Value)
	SELECT AlignmentEntity_id, AttributeType_id, ChangedValue FROM NewEntitiesUpdatedWithTextAttribute

	SELECT @maxIdValue = ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0) FROM [AlignmentEntityTextAttributeCache];

	INSERT INTO [AlignmentEntityTextAttributeCache] with(tablock) (AlignmentEntityTextAttributeCache_Id, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id)
	SELECT AlignmentEntityTextAttributeCache_Id + @maxIdValue, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id FROM [TempAlignmentEntityTextAttributeCache]; 

	/* Insert object in HiLo table */

	DELETE FROM HiLoUniqueKey WHERE ObjectType = 'AlignmentEntityTextAttributeCache';

	INSERT INTO HiLoUniqueKey
	(NextHi, ObjectType)
	VALUES
	((SELECT ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0)+1 
					FROM [AlignmentEntityTextAttributeCache]), 'AlignmentEntityTextAttributeCache');

	DROP TABLE [TempAlignmentEntityTextAttributeCache]
END

DROP TABLE [TempEntitiesUpdatedWithTextAttribute]
DROP TABLE [NewEntitiesUpdatedWithTextAttribute]

DROP TABLE [NewEntitiesUpdatedWithNumberAttribute]