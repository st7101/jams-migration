



PRINT N'Updating AlignmentEntityOverrides ...';

UPDATE aeo
SET AlignmentEntity_Id=ae.AlignmentEntity_Id,TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.AlignmentEntityOverrides aeo INNER JOIN 
dbo.AlignmentEntitys ae ON aeo.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON aeo.TerritoryType_ExternalID=tt.TerritoryType_ExternalID

PRINT N'Updating AlignmentEntitys ...';

UPDATE ae
SET AlignmentEntity_ParentId=aeParent.AlignmentEntity_Id
 FROM dbo.AlignmentEntitys ae 
INNER JOIN dbo.AlignmentEntitys aeParent ON ae.AlignmentEntity_ParentExternalId=aeparent.AlignmentEntity_ExternalID


PRINT N'Updating AlignmentEntityTextAttributeCache ...';

UPDATE aetac
SET AlignmentEntity_Id=ae.AlignmentEntity_Id
FROM dbo.AlignmentEntityTextAttributeCache aetac INNER JOIN 
dbo.AlignmentEntitys ae ON aetac.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID

PRINT N'Updating AlignmentEntityTypeTerritoryTypes ...';

UPDATE aettt
SET TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.AlignmentEntityTypeTerritoryTypes aettt INNER JOIN 
dbo.TerritoryTypes tt ON aettt.TerritoryType_ExternalID= tt.TerritoryType_ExternalID

PRINT N'Updating AlignmentStateEntrys ...';

UPDATE aes
SET [AlignmentEntityState_ParentId]=ae.[AlignmentEntity_Id]
FROM dbo.AlignmentEntityStates aes INNER JOIN 
dbo.AlignmentEntitys ae ON aes.[AlignmentEntityState_ParentExternalId]=ae.AlignmentEntity_ExternalId

PRINT N'Updating AlignmentEntityStates ...';

UPDATE ase
SET [AlignmentStateEntry_OrgUnitId]=ou.OrgUnit_Id
FROM dbo.AlignmentStateEntrys ase INNER JOIN 
dbo.OrgUnits ou ON ase.AlignmentStateEntry_ExternalOrgUnitId=ou.OrgUnit_ExternalID


PRINT N'Updating ApprovalProgress ...';

UPDATE ap
SET position_Id=p.Position_Id
FROM dbo.ApprovalProgress ap INNER JOIN 
dbo.Positions p ON ap.Position_ExternalID=p.Position_ExternalID


PRINT N'Updating ApprovalProgressTerritoryTypes ...';

UPDATE aptt
SET TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.ApprovalProgressTerritoryTypes aptt INNER JOIN 
dbo.TerritoryTypes tt ON aptt.TerritoryType_ExternalID=tt.TerritoryType_ExternalID


PRINT N'Updating AttributeTypeTerritoryTypes ...';

UPDATE attt
SET TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.AttributeTypeTerritoryTypes attt INNER JOIN 
dbo.TerritoryTypes tt ON attt.TerritoryType_ExternalID= tt.TerritoryType_ExternalID

PRINT N'Updating ClusterCaches ...';

UPDATE cc
SET [ClusterCache_TerritoryTypeId]=tt.TerritoryType_Id
FROM dbo.ClusterCaches cc INNER JOIN 
dbo.TerritoryTypes tt ON cc.[ClusterCache_ExternalTerritoryTypeId]= tt.TerritoryType_ExternalID

PRINT N'Updating ClusterCacheTrackers ...';

UPDATE cct
SET [ClusterCacheTracker_TerritoryTypeId]=tt.TerritoryType_Id
FROM dbo.ClusterCacheTrackers cct INNER JOIN 
dbo.TerritoryTypes tt ON cct.[ClusterCacheTracker_ExternalTerritoryTypeId]= tt.TerritoryType_ExternalID

PRINT N'Updating EditAttributeOverrides ...';

UPDATE aeo
SET EditAttributeOverride_AlignmentEntityId=ae.AlignmentEntity_Id
FROM dbo.EditAttributeOverrides aeo INNER JOIN 
dbo.AlignmentEntitys ae ON aeo.EditAttributeOverride_ExternalAlignmentEntityId= ae.AlignmentEntity_ExternalID


PRINT N'Updating GroupedEntities ...';

UPDATE ge
SET AlignmentEntity_Id=ae.AlignmentEntity_Id,TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.GroupedEntities ge INNER JOIN 
dbo.AlignmentEntitys ae ON ge.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON ge.TerritoryType_ExternalID=tt.TerritoryType_ExternalID


PRINT N'Updating IssueRepairCache ...';

UPDATE irc
SET AlignmentEntity_Id=ae.AlignmentEntity_Id,TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.IssueRepairCache irc INNER JOIN 
dbo.AlignmentEntitys ae ON irc.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON irc.TerritoryType_ExternalID=tt.TerritoryType_ExternalID


PRINT N'Updating OrgUnitAlignmentEntitys ...';
CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys]
    (
      [OrgUnitAlignmentEntity_Id] BIGINT NOT NULL ,
      [OrgUnitAlignmentEntity_Weight] DECIMAL(5, 2) NULL ,
      [AlignmentEntity_Id] BIGINT NOT NULL ,
      [OrgUnit_Id] BIGINT NOT NULL ,
	  [TerritoryType_Id] BIGINT NOT NULL,
      [OrgUnitAlignmentEntity_IsAutomatedAlignment] [BIT] NOT NULL
                                                          DEFAULT ( (0) )
    );
	ALTER TABLE [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys] ADD CONSTRAINT [PK_tmp_ms_xx_OrgUnitAlignmentEntitys] PRIMARY KEY CLUSTERED ( TerritoryType_id ASC,OrgUnit_id ASC, AlignmentEntity_Id ASC ) WITH (  DATA_COMPRESSION = PAGE ) ON PS_OUAE(territorytype_id) 

INSERT INTO [tmp_ms_xx_OrgUnitAlignmentEntitys] ([OrgUnitAlignmentEntity_Id], [OrgUnitAlignmentEntity_Weight], [AlignmentEntity_Id], [OrgUnit_Id],[TerritoryType_Id], [OrgUnitAlignmentEntity_IsAutomatedAlignment])
SELECT ouae.OrgUnitAlignmentEntity_Id,ouae.OrgUnitAlignmentEntity_Weight, ae.AlignmentEntity_Id,ou.OrgUnit_Id, tt.TerritoryType_Id,ouae.[OrgUnitAlignmentEntity_IsAutomatedAlignment]
FROM dbo.OrgUnitAlignmentEntitys ouae INNER JOIN 
dbo.AlignmentEntitys ae ON ouae.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.OrgUnits ou ON ouae.OrgUnit_ExternalID=ou.OrgUnit_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON ou.OrgUnitLevel_Externalid=tt.OrgUnitLevel_Externalid

DROP TABLE [dbo].[OrgUnitAlignmentEntitys];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys]',
    N'OrgUnitAlignmentEntitys';

EXECUTE sp_rename N'[dbo].[PK_tmp_ms_xx_OrgUnitAlignmentEntitys]',
    N'PK_OrgUnitAlignmentEntitys', N'OBJECT';


PRINT N'Updating OrgUnitAttributeCache ...';

UPDATE ouac
SET OrgUnit_Id=ou.OrgUnit_Id
FROM dbo.OrgUnitAttributeCache ouac INNER JOIN 
dbo.OrgUnits ou ON ouac.OrgUnit_ExternalID=ou.OrgUnit_ExternalID



PRINT N'Updating OrgUnitLevelPermissions ...';

UPDATE oulp
SET [OrgUnitLevel_Id]=oul.OrgUnitLevel_Id
FROM dbo.OrgUnitLevelPermissions oulp INNER JOIN 
dbo.OrgUnitLevels oul ON oulp.OrgUnitLevel_ExternalID=oul.OrgUnitLevel_ExternalID


PRINT N'Updating OrgUnitLevelRoles ...';

UPDATE oulr
SET [OrgUnitLevel_Id]=oul.OrgUnitLevel_Id
FROM dbo.OrgUnitLevelRoles oulr INNER JOIN 
dbo.OrgUnitLevels oul ON oulr.OrgUnitLevel_ExternalID=oul.OrgUnitLevel_ExternalID


PRINT N'Updating OrgUnitLevels ...';

UPDATE oul
SET OrgUnitLevel_ParentId=oulParent.OrgUnitLevel_Id
FROM dbo.OrgUnitLevels oul INNER JOIN 
dbo.OrgUnitLevels oulParent ON oulParent.OrgUnitLevel_ExternalID=oul.OrgUnitLevel_ParentExternalId


PRINT N'Updating OrgUnitOverrides ...';

UPDATE ouo
SET [OrgUnitOverride_OrgUnitId]=ou.OrgUnit_Id,[OrgUnitOverride_ParentOrgUnitId]=ouParent.OrgUnit_Id
FROM dbo.OrgUnitOverrides ouo 
LEFT OUTER JOIN dbo.OrgUnits ou ON ouo.OrgUnitOverride_ExternalOrgUnitId=ou.OrgUnit_ExternalID
LEFT OUTER JOIN dbo.OrgUnits ouParent ON ouo.OrgUnitOverride_ExternalParentOrgUnitId=ouParent.OrgUnit_ExternalID

PRINT N'Updating OrgUnitRelationshipOverrides ...';

UPDATE ouro
SET OrgUnitRelationshipOverride_DrivingOrgUnitId=ouD.OrgUnit_Id
FROM dbo.OrgUnitRelationshipOverrides ouro INNER JOIN 
dbo.OrgUnits ouD ON 
ouro.[OrgUnitRelationshipOverride_ExternalDrivingOrgUnitId]=ouD.OrgUnit_ExternalID
    
PRINT N'Updating OrgUnitRelationships ...';

UPDATE our
SET [Driving_OrgUnitId]=ouD.OrgUnit_Id,Affected_OrgUnitId=ouA.OrgUnit_Id
FROM dbo.OrgUnitRelationships our INNER JOIN 
dbo.OrgUnits ouD ON our.Driving_ExternalOrgUnitId=oud.OrgUnit_ExternalID
LEFT OUTER JOIN dbo.OrgUnits ouA ON our.Affected_ExternalOrgUnitId=ouA.OrgUnit_ExternalID

PRINT N'Updating OrgUnitRelationshipStateEntrys ...';

UPDATE ourse
SET [OrgUnitRelationshipStateEntry_AffectedOrgUnitId]=ouA.OrgUnit_Id
FROM dbo.OrgUnitRelationshipStateEntrys ourse INNER JOIN 
dbo.OrgUnits ouA ON ourse.[OrgUnitRelationshipStateEntry_ExternalAffectedOrgUnitId]=ouA.OrgUnit_ExternalID

PRINT N'Updating OrgUnits ...';

UPDATE ou
SET OrgUnit_ParentId=ouParent.OrgUnit_Id,OrgUnitLevel_Id=oul.OrgUnitLevel_Id
FROM dbo.OrgUnits ou INNER JOIN 
 dbo.OrgUnitLevels oul ON ou.OrgUnitLevel_ExternalID=oul.OrgUnitLevel_ExternalID
LEFT OUTER JOIN dbo.OrgUnits ouParent ON ou.OrgUnit_ParentExternalId=ouParent.OrgUnit_ExternalID

PRINT N'Updating PositionOverrides ...';

UPDATE po
SET [PositionOverride_OrgUnitId]=ou.OrgUnit_Id,[PositionOverride_PositionId]=p.Position_Id
FROM dbo.PositionOverrides po INNER JOIN 
dbo.OrgUnits ou ON po.PositionOverride_ExternalOrgUnitId=ou.OrgUnit_ExternalID
INNER JOIN dbo.Positions p ON po.PositionOverride_ExternalPositionId=p.Position_ExternalID


PRINT N'Updating PositionPersonnel ...';

UPDATE pp
SET Position_Id=p.Position_Id
FROM dbo.PositionPersonnel pp INNER JOIN 
dbo.Positions p ON pp.Position_ExternalID=p.Position_ExternalID

PRINT N'Updating PositionPersonnelOverrides ...';

UPDATE ppo
SET [PositionPersonnelOverride_PositionId]=p.Position_Id
FROM dbo.PositionPersonnelOverrides ppo INNER JOIN 
dbo.Positions p ON ppo.[PositionPersonnelOverride_ExternalPositionId]=p.Position_ExternalID

PRINT N'Updating Positions ...';

UPDATE p
SET OrgUnit_Id=ou.OrgUnit_Id
FROM dbo.Positions p INNER JOIN 
dbo.OrgUnits ou ON p.OrgUnit_ExternalID=ou.OrgUnit_ExternalID

PRINT N'Updating ProductPositionOverrides ...';

UPDATE ppo
SET ProductPositionOverride_PositionId=p.Position_Id
FROM dbo.ProductPositionOverrides ppo INNER JOIN 
dbo.Positions p ON ppo.ProductPositionOverride_ExternalPositionId=p.Position_ExternalID

PRINT N'Updating ProductPositions ...';

UPDATE pp
SET Position_Id=p.Position_Id
FROM dbo.ProductPositions pp INNER JOIN 
dbo.Positions p ON pp.Position_ExternalID=p.Position_ExternalID

PRINT N'Updating RequestAlignmentEntityAttributeCache ...';

UPDATE raeac
SET TerritoryType_Id=tt.TerritoryType_Id,AlignmentEntity_Id=ae.AlignmentEntity_Id
FROM dbo.RequestAlignmentEntityAttributeCache raeac INNER JOIN 
dbo.AlignmentEntitys ae ON raeac.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON raeac.TerritoryType_ExternalID=tt.TerritoryType_ExternalID

PRINT N'Updating RequestOrgUnitAttributeCache ...';

UPDATE rouac
SET OrgUnit_Id=ou.OrgUnit_Id
FROM dbo.RequestOrgUnitAttributeCache rouac INNER JOIN 
dbo.OrgUnits ou ON rouac.OrgUnit_ExternalID=ou.OrgUnit_ExternalID

PRINT N'Updating TerritoryTypes ...';

UPDATE tt
SET OrgUnitLevel_Id=ouo.OrgUnitLevel_Id,UnassignedOrgUnit_Id=o.OrgUnit_Id
FROM dbo.TerritoryTypes tt INNER JOIN
dbo.OrgUnitLevels ouo ON tt.OrgUnitLevel_ExternalID=ouo.OrgUnitLevel_ExternalID
LEFT OUTER JOIN dbo.OrgUnits o ON tt.UnassignedOrgUnit_ExternalID=o.OrgUnit_ExternalID

PRINT N'Updating TerritoryShapeOverrides ...';

UPDATE tso
SET Position_Id=p.Position_Id,OrgUnit_Id=ou.OrgUnit_Id
FROM dbo.TerritoryShapeOverrides tso INNER JOIN 
dbo.OrgUnits ou ON ou.OrgUnit_ExternalID = tso.OrgUnit_ExternalID
INNER JOIN dbo.Positions p ON p.Position_ExternalID = tso.Position_ExternalID

PRINT N'Updating TerritoryShape ...';

UPDATE ts
SET OrgUnit_Id=ou.OrgUnit_Id
FROM dbo.TerritoryShapes ts INNER JOIN 
dbo.OrgUnits ou ON ou.OrgUnit_ExternalID = ts.OrgUnit_ExternalID


PRINT N'Updating TerritoryTypePermissions ...';

UPDATE ttp
SET TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.TerritoryTypePermissions ttp INNER JOIN 
dbo.TerritoryTypes tt ON tt.TerritoryType_ExternalID = ttp.TerritoryType_ExternalID


PRINT N'Updating UpsertAlignmentEntityOverrides ...';

UPDATE uaeo
SET [UpsertAlignmentEntityOverride_AlignmentEntityId]=ae.AlignmentEntity_Id
FROM dbo.UpsertAlignmentEntityOverrides uaeo INNER JOIN 
dbo.AlignmentEntitys ae ON ae.AlignmentEntity_ExternalID=uaeo.[UpsertAlignmentEntityOverride_ExternalAlignmentEntityId]


PRINT N'Updating [dbo].[DeleteAlignmentEntityOverrides] ...';

UPDATE daeo
SET DeleteAlignmentEntityOverride_AlignmentEntityId=ae.AlignmentEntity_Id
FROM [dbo].[DeleteAlignmentEntityOverrides] daeo
INNER JOIN AlignmentEntitys ae ON ae.AlignmentEntity_ExternalID=daeo.DeleteAlignmentEntityOverride_ExternalAlignmentEntityId

