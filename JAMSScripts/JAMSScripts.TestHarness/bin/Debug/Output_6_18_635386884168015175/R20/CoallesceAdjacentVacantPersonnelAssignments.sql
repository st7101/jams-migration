with RankedPositionPersonnel as (
	select *, row_number() over (order by Position_Id, EffectiveDate asc) as RowNum  
	from PositionPersonnel
),
DuplicateVacants as (
	select later.PositionPersonnel_Id as ToDelete
	from RankedPositionPersonnel earlier
	join RankedPositionPersonnel later 
		on earlier.RowNum = later.RowNum - 1
	where earlier.Personnel_Id is null and later.Personnel_Id is null
)
Delete from PositionPersonnel 
where exists (
	select 1 from DuplicateVacants where PositionPersonnel_Id = ToDelete
);