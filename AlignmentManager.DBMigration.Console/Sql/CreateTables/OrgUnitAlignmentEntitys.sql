PRINT N'Starting rebuilding table [dbo].[OrgUnitAlignmentEntitys]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys]
    (
      [OrgUnitAlignmentEntity_Id] BIGINT NOT NULL ,
      [OrgUnitAlignmentEntity_Weight] DECIMAL(5, 2) NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
	  [AlignmentEntity_Id] BIGINT NOT NULL ,
      [OrgUnit_ExternalId] NVARCHAR(100) NULL ,
	  [OrgUnit_Id] BIGINT NOT NULL 
    );
ALTER TABLE [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys] ADD CONSTRAINT [PK_tmp_ms_xx_OrgUnitAlignmentEntitys] PRIMARY KEY CLUSTERED (AlignmentEntity_Id ASC,orgunit_id asc ) WITH (  DATA_COMPRESSION = PAGE )  

DECLARE @sqlStmt VARCHAR(MAX)

INSERT INTO [tmp_ms_xx_OrgUnitAlignmentEntitys] WITH (TABLOCK) ([OrgUnitAlignmentEntity_Id], 
	[OrgUnitAlignmentEntity_Weight], [AlignmentEntity_ExternalId], [OrgUnit_ExternalId], 
	[AlignmentEntity_Id], [OrgUnit_Id])
SELECT OrgUnitAlignmentEntity_Id, OrgUnitAlignmentEntity_Weight, 
	AlignmentEntity_Id, OrgUnit_Id, 
	ROW_NUMBER() OVER ( ORDER BY AlignmentEntity_Id ASC ), ROW_NUMBER() OVER ( ORDER BY OrgUnit_Id ASC )
FROM dbo.OrgUnitAlignmentEntitys


DROP TABLE [dbo].[OrgUnitAlignmentEntitys];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys]',
    N'OrgUnitAlignmentEntitys';

EXECUTE sp_rename N'[dbo].[PK_tmp_ms_xx_OrgUnitAlignmentEntitys]',
    N'PK_OrgUnitAlignmentEntitys', N'OBJECT';
