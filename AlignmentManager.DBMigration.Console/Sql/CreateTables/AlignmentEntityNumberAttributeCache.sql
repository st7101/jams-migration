
PRINT N'Starting rebuilding table [dbo].[AlignmentEntityNumberAttributeCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityNumberAttributeCache]
    (
      [AlignmentEntityNumberAttributeCache_Id] BIGINT NOT NULL ,
      [AlignmentEntityNumberAttributeCache_Value] FLOAT(53) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [AttributeType_Id] BIGINT NOT NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityNumberAttributeCache] PRIMARY KEY CLUSTERED
        ([AttributeType_Id], [AlignmentEntityNumberAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    ) ON PS_AENAC([AttributeType_Id]);
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AlignmentEntityNumberAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_AlignmentEntityNumberAttributeCache] WITH (TABLOCK)
                ( [AlignmentEntityNumberAttributeCache_Id] ,
                  [AlignmentEntityNumberAttributeCache_Value] ,
                  alignmententity_id,
                  [AttributeType_Id] ,
                  [TerritoryType_Id]
                )
                SELECT  [AlignmentEntityNumberAttributeCache_Id] ,
                        [AlignmentEntityNumberAttributeCache_Value] ,
                        ae.AlignmentEntity_Id,
                        [AttributeType_Id] ,
                        tt.[TerritoryType_Id]
                FROM    [dbo].[AlignmentEntityNumberAttributeCache] aenac
				LEFT OUTER JOIN dbo.AlignmentEntitys ae ON ae.AlignmentEntity_ExternalId = aenac.AlignmentEntity_Id
				LEFT OUTER JOIN dbo.TerritoryTypes tt ON tt.TerritoryType_ExternalId = aenac.TerritoryType_Id;
    END
DROP TABLE [dbo].[AlignmentEntityNumberAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityNumberAttributeCache]',
    N'AlignmentEntityNumberAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityNumberAttributeCache]',
    N'PK_AlignmentEntityNumberAttributeCache', N'OBJECT';
