/********* This query gets clusters in Assigned/Both display mode ********************/

DECLARE @ViewPresent bit;
SET @ViewPresent = 0;
SELECT @ViewPresent = 1 WHERE EXISTS (SELECT * FROM sys.views WHERE name = 'OAESuperView');

IF @ViewPresent = 1
BEGIN
	WITH AllEntities AS
	(
		SELECT ae.AlignmentEntity_id,
			   ae.AlignmentEntity_Latitude,
			   ae.AlignmentEntity_Longitude,
			   ou.OrgUnit_Id,
			   ou.OrgUnit_ColorId,
			   (Case WHEN ((0 = 1 and 
							ou.OrgUnit_TreeLeft>= 1 and
							ou.OrgUnit_TreeRight<= 10430)
						  or
						  (0 = 1 and ou.OrgUnit_IsUnassigned = 1)) then
						1
					else
						0
					end) as InSpan
		FROM [AlignmentEntitys] ae
			inner join [OAESuperView] oaesv WITH (NOEXPAND)	
			ON ae.AlignmentEntity_id = oaesv.AlignmentEntity_id
			inner join [OrgUnits] ou
			ON oaesv.OrgUnit_id = ou.OrgUnit_id
		WHERE
			oaesv.TerritoryType_Id = '115'
			and ae.AlignmentEntity_Latitude between 42.6969776153564 and 47.4596862792969
			and ae.AlignmentEntity_Longitude between -73.4377403259277 and -66.9500045776367
			and ((0 = 1 and ae.AlignmentEntityType_Id = 1) or (0 = 0 and ae.AlignmentEntityType_Id <> 1))
			and ae.AlignmentEntity_Status = 'Normal'
			and (ae.AlignmentEntity_Latitude <> 0 and ae.AlignmentEntity_Longitude <> 0)
			and (ae.AlignmentEntity_Latitude Is NOT NULL and ae.AlignmentEntity_Longitude Is NOT NULL)
	)

	SELECT	COUNT(1) AS ClusterCount,
			AVG(AlignmentEntity_Latitude) AS ClusterLat,
			AVG(AlignmentEntity_Longitude) AS ClusterLong,		
			Max(AlignmentEntity_Latitude)  AS MaxLat,
			Max(AlignmentEntity_Longitude) AS MaxLong,
			Min(AlignmentEntity_Latitude)  AS MinLat,
			Min(AlignmentEntity_Longitude) AS MinLong,
			(Case WHEN COUNT(1) = 1 then Min(AlignmentEntity_Id) 
			else (OrgUnitId + '|' + CAST(GroupByX AS varchar) + '|' + CAST(GroupByY AS varchar) + '|' + CAST(0 AS varchar)) end) AS EntityId,
			OrgUnitId AS OrgUnitsIdList,
			OrgUnitColorId AS OrgUnitsColorIdsList,
			Cast(GroupByX AS int) AS TileX,
			Cast(GroupByY AS int) AS TileY
	FROM
	(
		SELECT Min(ae.AlignmentEntity_Latitude) as AlignmentEntity_Latitude,
			   Min(ae.AlignmentEntity_Longitude) as AlignmentEntity_Longitude,
			   Min(dbo.ConvertLongitudeToTileX(AlignmentEntity_Longitude,13)) AS GroupByX, 
			   Min(dbo.ConvertLatitudeToTileY(AlignmentEntity_Latitude,13)) AS GroupByY,
			   ae.AlignmentEntity_Id,
			   (Case WHEN COUNT(ae.OrgUnit_id) = 1 then
					Min(ae.OrgUnit_id)
				WHEN @ViewPresent = 1 THEN
					STUFF((SELECT ',' + CAST(oaesv.OrgUnit_Id as VARCHAR(MAX))
					FROM  [OAESuperView] oaesv WITH (NOEXPAND)					
					WHERE  
					oaesv.TerritoryType_Id = '115' and 
					ae.AlignmentEntity_id = oaesv.AlignmentEntity_id
					ORDER BY oaesv.OrgUnit_id					   
					FOR XML PATH('')),1,1,'')					
				end) AS OrgUnitId,
			(Case WHEN COUNT(ae.OrgUnit_ColorId) = 1 then
				Min(Cast(ae.OrgUnit_ColorId as varchar(max))) 		
			else
				STUFF((SELECT ',' + CAST(ou.OrgUnit_ColorId as VARCHAR(MAX))
				FROM   [OrgUnitAlignmentEntitys] oae
				inner join [OrgUnits] ou
				ON oae.OrgUnit_id = ou.OrgUnit_id
				inner join [TerritoryTypes] tt
				ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id							
				WHERE  
				tt.TerritoryType_Id = '115' and 
				ae.AlignmentEntity_id = oae.AlignmentEntity_id
				ORDER BY ou.OrgUnit_Id					   
				FOR XML PATH('')),1,1,'') end) as OrgUnitColorId
		FROM AllEntities as ae
		GROUP BY ae.AlignmentEntity_id
		HAVING Max(InSpan)=1
	) as ClusterResults_Normal
	GROUP BY OrgUnitId,GroupByX,GroupByY,OrgUnitColorId
END
ELSE
BEGIN
WITH AllEntities AS
(
	SELECT ae.AlignmentEntity_id,
		   ae.AlignmentEntity_Latitude,
		   ae.AlignmentEntity_Longitude,
		   ou.OrgUnit_Id,
		   ou.OrgUnit_ColorId,
		   (Case WHEN ((0 = 1 and 
						ou.OrgUnit_TreeLeft>= 1 and
						ou.OrgUnit_TreeRight<= 10430)
					  or
					  (0 = 1 and ou.OrgUnit_IsUnassigned = 1)) then
					1
				else
					0
				end) as InSpan
	FROM [AlignmentEntitys] ae
		inner join OrgUnitAlignmentEntitys oae
		ON ae.AlignmentEntity_id = oae.AlignmentEntity_id
		inner join [OrgUnits] ou
		ON oae.OrgUnit_id = ou.OrgUnit_id
		inner join [TerritoryTypes] tt
        ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id
	WHERE
		tt.TerritoryType_Id = '115'
		and ae.AlignmentEntity_Latitude between 42.6969776153564 and 47.4596862792969
		and ae.AlignmentEntity_Longitude between -73.4377403259277 and -66.9500045776367
		and ((0 = 1 and ae.AlignmentEntityType_Id = 1) or (0 = 0 and ae.AlignmentEntityType_Id <> 1))
		and ae.AlignmentEntity_Status = 'Normal'
		and (ae.AlignmentEntity_Latitude <> 0 and ae.AlignmentEntity_Longitude <> 0)
		and (ae.AlignmentEntity_Latitude Is NOT NULL and ae.AlignmentEntity_Longitude Is NOT NULL)
)

SELECT	COUNT(1) AS ClusterCount,
		AVG(AlignmentEntity_Latitude) AS ClusterLat,
		AVG(AlignmentEntity_Longitude) AS ClusterLong,		
		Max(AlignmentEntity_Latitude)  AS MaxLat,
		Max(AlignmentEntity_Longitude) AS MaxLong,
		Min(AlignmentEntity_Latitude)  AS MinLat,
		Min(AlignmentEntity_Longitude) AS MinLong,
		(Case WHEN COUNT(1) = 1 then Min(AlignmentEntity_Id) 
		else (OrgUnitId + '|' + CAST(GroupByX AS varchar) + '|' + CAST(GroupByY AS varchar) + '|' + CAST(0 AS varchar)) end) AS EntityId,
		OrgUnitId AS OrgUnitsIdList,
		OrgUnitColorId AS OrgUnitsColorIdsList,
		Cast(GroupByX AS int) AS TileX,
		Cast(GroupByY AS int) AS TileY
FROM
(
	SELECT Min(ae.AlignmentEntity_Latitude) as AlignmentEntity_Latitude,
		   Min(ae.AlignmentEntity_Longitude) as AlignmentEntity_Longitude,
		   Min(dbo.ConvertLongitudeToTileX(AlignmentEntity_Longitude,13)) AS GroupByX, 
		   Min(dbo.ConvertLatitudeToTileY(AlignmentEntity_Latitude,13)) AS GroupByY,
		   ae.AlignmentEntity_Id,
		   (Case WHEN COUNT(ae.OrgUnit_id) = 1 then
				Min(ae.OrgUnit_id)		
			else
				STUFF((SELECT ',' + CAST(oae.OrgUnit_Id as VARCHAR(MAX))
				FROM   [OrgUnitAlignmentEntitys] oae
				inner join [OrgUnits] ou
				ON oae.OrgUnit_id = ou.OrgUnit_id
				inner join [TerritoryTypes] tt
				ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id							
				WHERE  
				tt.TerritoryType_Id = '115' and 
				ae.AlignmentEntity_id = oae.AlignmentEntity_id
				ORDER BY oae.OrgUnit_id					   
				FOR XML PATH('')),1,1,'') 			
			end) AS OrgUnitId,
			(Case WHEN COUNT(ae.OrgUnit_ColorId) = 1 then
				Min(Cast(ae.OrgUnit_ColorId as varchar(max))) 		
			else
				STUFF((SELECT ',' + CAST(ou.OrgUnit_ColorId as VARCHAR(MAX))
				FROM   [OrgUnitAlignmentEntitys] oae
				inner join [OrgUnits] ou
				ON oae.OrgUnit_id = ou.OrgUnit_id
				inner join [TerritoryTypes] tt
				ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id							
				WHERE  
				tt.TerritoryType_Id = '115' and 
				ae.AlignmentEntity_id = oae.AlignmentEntity_id
				ORDER BY ou.OrgUnit_Id					   
				FOR XML PATH('')),1,1,'') end) as OrgUnitColorId
	FROM AllEntities as ae
	GROUP BY ae.AlignmentEntity_id
	HAVING Max(InSpan)=1
) as ClusterResults_Normal
GROUP BY OrgUnitId,GroupByX,GroupByY,OrgUnitColorId
END
