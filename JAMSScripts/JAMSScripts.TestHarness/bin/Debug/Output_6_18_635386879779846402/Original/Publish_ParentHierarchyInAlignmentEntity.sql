-- Query to determine if any parent alignment entity also has a non null parent

with invalidParentEntity as 
( 
	select distinct ae1.AlignmentEntity_ParentId  
	from AlignmentEntitys ae1
	inner join AlignmentEntitys ae2 
	on ae1.AlignmentEntity_ParentId=ae2.AlignmentEntity_Id 
	where ae2.AlignmentEntity_ParentId is not null
)

select * from invalidParentEntity
