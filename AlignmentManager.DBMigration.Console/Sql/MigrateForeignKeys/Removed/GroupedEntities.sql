
PRINT N'Updating GroupedEntities ...';

UPDATE ge
SET AlignmentEntity_Id=ae.AlignmentEntity_Id,TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.GroupedEntities ge INNER JOIN 
dbo.AlignmentEntitys ae ON ge.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON ge.TerritoryType_ExternalID=tt.TerritoryType_ExternalID

