INSERT INTO [TempAlignmentEntityNumberAttributeCache]
	(AlignmentEntity_id, AttributeType_id, TerritoryType_id, AlignmentEntityNumberAttributeCache_Value) 
	(SELECT em.ParentAlignmentEntity_Id AS AlignmentEntity_id, 
			100 AS AttributeType_Id, 
			em.TerritoryType_id AS TerritoryType_id, 
			SUM(ISNULL(ae.[METRIC2], 0)) AS AlignmentEntityNumberAttributeCache_Value FROM
			AlignmentEntitys ae
			INNER JOIN ChildEntityPerTerritoryTypeMappings em ON em.AlignmentEntity_Id = ae.AlignmentEntity_Id
			GROUP BY em.ParentAlignmentEntity_Id, em.TerritoryType_id
		UNION
	SELECT ae.AlignmentEntity_Id AS AlignmentEntity_id, 
			100 AS AttributeType_Id,
			pae.TerritoryType_id AS TerritoryType_id,
			0 AS AlignmentEntityNumberAttributeCache_Value 
			FROM AlignmentEntitys ae
			INNER JOIN (SELECT DISTINCT aettt.TerritoryType_id AS TerritoryType_Id, 
								aettt.AlignmentEntityType_ParentId AS AlignmentEntityType_id 
			FROM AlignmentEntityTypeTerritoryTypes aettt
			WHERE aettt.AlignmentEntityType_ParentId IS NOT NULL) pae 
			ON ae.AlignmentEntityType_id = pae.AlignmentEntityType_id
			WHERE NOT EXISTS (SELECT 1 FROM ChildEntityPerTerritoryTypeMappings em WHERE em.ParentAlignmentEntity_Id = ae.AlignmentEntity_Id and em.TerritoryType_id = pae.TerritoryType_Id)
		UNION
	SELECT ae.AlignmentEntity_id AS AlignmentEntity_id, 
			100 AS AttributeType_id, 
			NULL AS TerritoryType_id, 
			ISNULL(ae.[METRIC2], 0) AS AlignmentEntityNumberAttributeCache_Value
			FROM [AlignmentEntitys] ae
			INNER JOIN IndependentEntitys ie ON ae.AlignmentEntity_Id = ie.AlignmentEntity_Id
	);