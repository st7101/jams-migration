/* Fetch the alignment entities along with their alignments to org units which are belonging to territory type 
that does not support alignmnt entity type of the entity*/

select ae.AlignmentEntity_Id, ou.OrgUnit_Id, tt.TerritoryType_Id 
from AlignmentEntitys ae
inner join OrgUnitAlignmentEntitys ouae on ouae.AlignmentEntity_id = ae.AlignmentEntity_Id
inner join OrgUnits ou on ouae.OrgUnit_id=ou.OrgUnit_Id 
inner join TerritoryTypes tt on ou.OrgUnitLevel_id=tt.OrgUnitLevel_id
Where Not Exists (Select 1 from alignmentEntityTypeTerritoryTypes aettt 
where aettt.AlignmentEntityType_id = ae.AlignmentEntityType_id and aettt.TerritoryType_id = tt.TerritoryType_Id)