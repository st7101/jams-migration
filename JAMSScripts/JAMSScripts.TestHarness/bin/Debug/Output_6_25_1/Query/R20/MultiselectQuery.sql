/********* This query gets selected entities list ********************/
EXECUTE [dbo].[MultiSelectQuery] 
   @minLat=42.6969776153564
  ,@maxLat=-66.9500045776367
  ,@minLng=-66.9500045776367
  ,@maxLng=47.4596862792969
  ,@territoryTypeId=51
  ,@showAssigned=0
  ,@showUnassigned=0
  ,@TreeLeft=1
  ,@treeRight=10430
  ,@ShowGeos=0
  ,@ShowCustomers=0
