
PRINT N'Starting rebuilding table [dbo].[AlignmentEntityTextAttributeCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityTextAttributeCache]
    (
      [AlignmentEntityTextAttributeCache_Id] BIGINT NOT NULL ,
      [AlignmentEntityTextAttributeCache_Value] NVARCHAR(2000) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [AttributeType_Id] BIGINT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityTextAttributeCache] PRIMARY KEY CLUSTERED
        ([AlignmentEntityTextAttributeCache_Id] ASC, [AttributeType_Id] )
        WITH (  DATA_COMPRESSION = PAGE )
    ) ON PS_AENAC([AttributeType_Id]);
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AlignmentEntityTextAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_AlignmentEntityTextAttributeCache] WITH (TABLOCK)
                ( [AlignmentEntityTextAttributeCache_Id] ,
                  [AlignmentEntityTextAttributeCache_Value] ,
                  [AlignmentEntity_Id] ,
                  [AttributeType_Id]
                )
                SELECT  [AlignmentEntityTextAttributeCache_Id] ,
                        [AlignmentEntityTextAttributeCache_Value] ,
                        ae.[AlignmentEntity_Id] ,
                        [AttributeType_Id]
                FROM    [dbo].[AlignmentEntityTextAttributeCache] aetac
				INNER JOIN dbo.AlignmentEntitys ae ON ae.AlignmentEntity_ExternalId = aetac.AlignmentEntity_id

    END
DROP TABLE [dbo].[AlignmentEntityTextAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityTextAttributeCache]',
    N'AlignmentEntityTextAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityTextAttributeCache]',
    N'PK_AlignmentEntityTextAttributeCache', N'OBJECT';
