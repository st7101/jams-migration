﻿WITH independentCustomer AS
(
	SELECT DISTINCT TOP(:totalNumberOfRowsShown) ae.AlignmentEntity_Id        AS Id,
		   ae.AlignmentEntity_Name      AS Name,
		   null AS ParentId
		FROM AlignmentEntitys ae
		inner join OrgUnitAlignmentEntitys oae
			ON ae.AlignmentEntity_Id = oae.AlignmentEntity_id
		inner join OrgUnits ou
			ON oae.OrgUnit_id = ou.OrgUnit_Id
			:spanRestriction
		inner join TerritoryTypes tt
			ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
			and tt.TerritoryType_Id = :terrTypeId
		WHERE ae.AlignmentEntity_Status = :aeStatus
			and ae.AlignmentEntityType_Id in (:independentAlignmentEntityTypeIds)
			and (ae.AlignmentEntity_Name like :searchString +'%' escape '\' 
			or ae.AlignmentEntity_Id like :searchString +'%' escape '\')
		ORDER BY :sortStatement
),
groupedCustomer AS
(
	SELECT DISTINCT TOP(:totalNumberOfRowsShown) ae.AlignmentEntity_Id        AS Id,
		   ae.AlignmentEntity_Name      AS Name,
		   ae.AlignmentEntity_ParentId AS ParentId
		FROM GroupedEntities ge
		inner join AlignmentEntitys ae 
			ON ae.AlignmentEntity_Id = ge.AlignmentEntity_id
		inner join OrgUnitAlignmentEntitys oae
			ON ae.AlignmentEntity_ParentId = oae.AlignmentEntity_id
		inner join OrgUnits ou 
			ON oae.OrgUnit_id = ou.OrgUnit_Id
			:spanRestriction
		inner join TerritoryTypes tt 
			ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
			and tt.TerritoryType_Id = :terrTypeId
		WHERE ae.AlignmentEntity_Status = :aeStatus
			and ae.AlignmentEntityType_Id in (:groupedAlignmentEntityTypeIds)
			and (ae.AlignmentEntity_Name like :searchString +'%' escape '\' 
			or ae.AlignmentEntity_Id like :searchString +'%' escape '\')
			and ge.TerritoryType_id = :terrTypeId
		ORDER BY :sortStatement
)
	

SELECT TOP (:pageSize) customers.Id, customers.Name 
FROM   (SELECT Id, Name,
			   ROW_NUMBER() OVER(ORDER BY :sortStatement) AS SortRow
		FROM   (SELECT  Id, Name
				FROM   independentCustomer
				UNION ALL
				SELECT  Id, Name
				FROM   groupedCustomer) AS combinedCustomers) AS customers
WHERE  customers.SortRow > :lastShownRow
ORDER  BY customers.SortRow ASC


OPTION (RECOMPILE)