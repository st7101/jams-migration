PRINT N'Starting rebuilding table [dbo].[EditAttributeOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_EditAttributeOverrides]
    (
      [EditAttributeOverride_Id] BIGINT NOT NULL ,
      [EditAttributeOverride_ExternalAlignmentEntityId] NVARCHAR(255) NULL ,
      [EditAttributeOverride_AlignmentEntityId] BIGINT NULL ,
      [EditAttributeOverride_OldAttributeValue] NVARCHAR(2000) NULL ,
      [EditAttributeOverride_NewAttributeValue] NVARCHAR(2000) NULL ,
      [Request_Id] BIGINT NULL ,
      [AttributeType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_EditAttributeOverrides] PRIMARY KEY CLUSTERED
        ( [EditAttributeOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[EditAttributeOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_EditAttributeOverrides] WITH (TABLOCK)
                ( [EditAttributeOverride_Id] ,
                  [EditAttributeOverride_ExternalAlignmentEntityId] ,
                  [EditAttributeOverride_OldAttributeValue] ,
                  [EditAttributeOverride_NewAttributeValue] ,
                  [Request_Id] ,
                  [AttributeType_Id]
                )
                SELECT  [EditAttributeOverride_Id] ,
                        [EditAttributeOverride_AlignmentEntityId] ,
                        [EditAttributeOverride_OldAttributeValue] ,
                        [EditAttributeOverride_NewAttributeValue] ,
                        [Request_Id] ,
                        [AttributeType_Id]
                FROM    [dbo].[EditAttributeOverrides]
                ORDER BY [EditAttributeOverride_Id] ASC;
    END
DROP TABLE [dbo].[EditAttributeOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EditAttributeOverrides]',
    N'EditAttributeOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_EditAttributeOverrides]',
    N'PK_EditAttributeOverrides', N'OBJECT';
