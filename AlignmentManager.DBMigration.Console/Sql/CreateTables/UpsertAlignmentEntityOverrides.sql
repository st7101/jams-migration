
PRINT N'Starting rebuilding table [dbo].[UpsertAlignmentEntityOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_UpsertAlignmentEntityOverrides]
    (
      [UpsertAlignmentEntityOverride_Id] BIGINT NOT NULL ,
      [UpsertAlignmentEntityOverride_AlignmentEntityExternalId] NVARCHAR(100)
        NULL ,
      [UpsertAlignmentEntityOverride_AlignmentEntityId] BIGINT NULL ,
      [UpsertAlignmentEntityOverride_UpsertOperationType] NVARCHAR(100) NULL ,
      [Request_Id] BIGINT NULL ,
      [AlignmentEntityType_Id] BIGINT NULL ,
      [CustomerType_Id] BIGINT NULL ,
      [NewAlignmentEntityState_Id] BIGINT NULL ,
      [OldAlignmentEntityState_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_UpsertAlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( [UpsertAlignmentEntityOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[UpsertAlignmentEntityOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_UpsertAlignmentEntityOverrides] WITH (TABLOCK)
                ( [UpsertAlignmentEntityOverride_Id] ,
                  [UpsertAlignmentEntityOverride_AlignmentEntityExternalId] ,
                  [UpsertAlignmentEntityOverride_UpsertOperationType] ,
                  [Request_Id] ,
                  [AlignmentEntityType_Id] ,
                  [CustomerType_Id] ,
                  [NewAlignmentEntityState_Id] ,
                  [OldAlignmentEntityState_Id]
                )
                SELECT  [UpsertAlignmentEntityOverride_Id] ,
                        [UpsertAlignmentEntityOverride_AlignmentEntityId] ,
                        [UpsertAlignmentEntityOverride_UpsertOperationType] ,
                        [Request_Id] ,
                        [AlignmentEntityType_Id] ,
                        [CustomerType_Id] ,
                        [NewAlignmentEntityState_Id] ,
                        [OldAlignmentEntityState_Id]
                FROM    [dbo].[UpsertAlignmentEntityOverrides]
                ORDER BY [UpsertAlignmentEntityOverride_Id] ASC;
    END
DROP TABLE [dbo].[UpsertAlignmentEntityOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_UpsertAlignmentEntityOverrides]',
    N'UpsertAlignmentEntityOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_UpsertAlignmentEntityOverrides]',
    N'PK_UpsertAlignmentEntityOverrides', N'OBJECT';
