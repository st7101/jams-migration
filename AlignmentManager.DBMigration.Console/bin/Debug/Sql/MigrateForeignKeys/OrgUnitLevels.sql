
PRINT N'Updating OrgUnitLevels ...';

UPDATE oul
SET OrgUnitLevel_ParentId=oulParent.OrgUnitLevel_Id
FROM dbo.OrgUnitLevels oul INNER JOIN 
dbo.OrgUnitLevels oulParent ON oulParent.OrgUnitLevel_ExternalID=oul.OrgUnitLevel_ParentExternalId

