IF (OBJECT_ID('[MultiSelectQuery]') IS NOT NULL)
  DROP PROCEDURE [MultiSelectQuery]
GO
-- =============================================
-- Author:		Steve Tetrault
-- Create date: Jun. 17, 2014
-- Description:	Modified Logic for MultiSelectQuery SP
-- =============================================
CREATE PROCEDURE [dbo].[MultiSelectQuery]
	-- Add the parameters for the stored procedure here
    @minLat FLOAT ,
    @maxLat FLOAT ,
    @minLng FLOAT ,
    @maxLng FLOAT ,
    @territoryTypeId BIGINT ,
    @showAssigned BIT ,
    @showUnassigned BIT ,
    @TreeLeft INT ,
    @treeRight INT ,
    @showGeos BIT ,
    @showCustomers BIT
AS
    BEGIN

        DECLARE @sql NVARCHAR(MAX)= '
select OAE.OrgUnit_id as OrgUnitId, COUNT(OAE.AlignmentEntity_id)as AlignmentEntityIdCount
from   OrgUnitAlignmentEntitys OAE
       inner join AlignmentEntitys AE1 
         on AE1.AlignmentEntity_id = OAE.AlignmentEntity_id' + CHAR(13)
        IF ( @showAssigned = 1
             AND @showUnassigned = 1
           )
            BEGIN
                SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and (( ou.OrgUnit_TreeLeft >= ' + CAST(@treeLeft AS NVARCHAR)
                    + ' and ou.OrgUnit_TreeRight <= '
                    + CAST(@treeRight AS NVARCHAR) + ') 
		or ou.OrgUnit_IsUnassigned = 1)
'
            END
        ELSE
            IF ( @showAssigned = 1 )
                BEGIN
                    SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
	and ( ou.OrgUnit_TreeLeft >= ' + CAST(@treeLeft AS NVARCHAR)
                        + ' and ou.OrgUnit_TreeRight <= '
                        + CAST(@treeRight AS NVARCHAR) + ') 
'
                END
            ELSE
                IF ( @showUnassigned = 1 )
                    BEGIN
                        SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and (ou.OrgUnit_IsUnassigned = 1)
'
                    END

       
        SET @sql = @sql
            + ' inner join AlignmentEntityTypeTerritoryTypes as aet
		 on aet.AlignmentEntityType_id = AE1.AlignmentEntityType_Id and aet.TerritoryType_id = ' + CAST(@territoryTypeId AS VARCHAR)
            + '
	where AE1.AlignmentEntity_Latitude is not null and AE1.AlignmentEntity_Latitude <> 0
	 and AE1.AlignmentEntity_Longitude <> 0 and AE1.AlignmentEntity_Longitude Is NOT NULL
	and AE1.AlignmentEntity_Longitude between ' + CAST(@minLng AS VARCHAR)
            + ' and ' + CAST(@maxLng AS VARCHAR) + '
	and AE1.AlignmentEntity_Latitude between ' + CAST(@minLat AS VARCHAR)
            + ' and ' + CAST(@maxLat AS VARCHAR) + '
'
        IF @showGeos = 1
            AND @showCustomers = 1
            SET @sql = @sql + ''     
        ELSE
            IF @showGeos = 1
                SET @Sql = @sql + 'and ae.AlignmentEntityType_Id = 1'
            ELSE
                IF @showCustomers = 1
                    SET @Sql = @sql + 'and ae.AlignmentEntityType_Id <> 1'

        SET @sql = @sql + ' group by oae.OrgUnit_id'
        EXEC (@sql)
    END




