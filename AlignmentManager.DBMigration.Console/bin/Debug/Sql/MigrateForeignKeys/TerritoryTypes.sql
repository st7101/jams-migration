
PRINT N'Updating TerritoryTypes ...';

UPDATE tt
SET OrgUnitLevel_Id=ouo.OrgUnitLevel_Id,UnassignedOrgUnit_Id=o.OrgUnit_Id
FROM dbo.TerritoryTypes tt INNER JOIN
dbo.OrgUnitLevels ouo ON tt.OrgUnitLevel_ExternalID=ouo.OrgUnitLevel_ExternalID
LEFT OUTER JOIN dbo.OrgUnits o ON tt.UnassignedOrgUnit_ExternalID=o.OrgUnit_ExternalID
