﻿Declare @minLat float
Declare @minLng float
Declare @maxLat float
Declare @maxLng float

set @minLat =:minlatitude
set @minLng =:minlongitude
set @maxLat =:maxlatitude
set @maxLng =:maxlongitude

select OAE.OrgUnit_id as OrgUnitId, COUNT(OAE.AlignmentEntity_id)as AlignmentEntityIdCount
from   OrgUnitAlignmentEntitys OAE
       inner join AlignmentEntitys AE1 
         on AE1.AlignmentEntity_id = OAE.AlignmentEntity_id
       inner join orgunits orgunit
         on OAE.OrgUnit_id = orgunit.OrgUnit_Id
       inner join OrgUnitLevels orgunitlevel
         on orgunit.OrgUnitLevel_id = orgunitlevel.OrgUnitLevel_Id
       inner join TerritoryTypes territorytype
         on orgunitlevel.OrgUnitLevel_Id = territorytype.OrgUnitLevel_id
	   inner join AlignmentEntityTypeTerritoryTypes as aet
		 on aet.AlignmentEntityType_id = AE1.AlignmentEntityType_Id and aet.TerritoryType_id = territorytype.TerritoryType_id
 where  AE1.AlignmentEntity_Latitude >= @minLat
        and AE1.AlignmentEntity_Longitude >= @minLng
        and AE1.AlignmentEntity_Latitude <= @maxLat
        and AE1.AlignmentEntity_Longitude <= @maxLng
        and ((:showAssigned = 1 and (orgunit.OrgUnit_TreeLeft >= :treeLeft or orgunit.OrgUnit_TreeLeft is null) 
							and (orgunit.OrgUnit_TreeRight <= :treeRight or orgunit.OrgUnit_TreeRight is null) 
           )

	   or (:showUnassigned = 1 and (orgunit.OrgUnit_IsUnassigned = 1 or orgunit.OrgUnit_IsUnassigned is null)))
     and territorytype.TerritoryType_Id = :territoryTypeId

     
     group by OAE.OrgUnit_id
