

PRINT N'Starting rebuilding table [dbo].[TerritoryTypePermissions]...';


CREATE TABLE [dbo].[tmp_ms_xx_TerritoryTypePermissions]
    (
      [TerritoryTypePermission_Id] BIGINT NOT NULL ,
      [TerritoryTypePermission_IsEnabled] BIT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      [Permission_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_TerritoryTypePermissions] PRIMARY KEY CLUSTERED
        ( [TerritoryTypePermission_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[TerritoryTypePermissions] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_TerritoryTypePermissions] WITH(TABLOCK)
                ( [TerritoryTypePermission_Id] ,
                  [TerritoryTypePermission_IsEnabled] ,
                  [TerritoryType_ExternalId] ,
                  [Permission_Id]
                )
                SELECT  [TerritoryTypePermission_Id] ,
                        [TerritoryTypePermission_IsEnabled] ,
                        [TerritoryType_Id] ,
                        [Permission_Id]
                FROM    [dbo].[TerritoryTypePermissions]
                ORDER BY [TerritoryTypePermission_Id] ASC
    END
DROP TABLE [dbo].[TerritoryTypePermissions];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_TerritoryTypePermissions]',
    N'TerritoryTypePermissions';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_TerritoryTypePermissions]',
    N'PK_TerritoryTypePermissions', N'OBJECT';
