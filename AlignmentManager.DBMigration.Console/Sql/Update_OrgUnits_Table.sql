PRINT N'Starting rebuilding table [dbo].[OrgUnits]...';

/*Base table structure */
DECLARE @CreateTable VARCHAR(MAX) ,
    @InsertTable VARCHAR(MAX)


SET @CreateTable = '
CREATE TABLE {DBIdentifier}.[dbo].[tmp_ms_xx_OrgUnits] (
    [OrgUnit_Id]       BIGINT       NOT NULL,
    [OrgUnit_ExternalId]              NVARCHAR (100) NOT NULL,
    [OrgUnit_Name]            NVARCHAR (255) NULL,
    [OrgUnit_LegacyId]        NVARCHAR (255) NULL,
    [OrgUnit_IsUnassigned]    BIT            NULL,
    [OrgUnit_TreeLeft]        INT            NULL,
    [OrgUnit_TreeRight]       INT            NULL,
    [OrgUnitLevel_ExternalId]         NVARCHAR (100) NULL,
    [OrgUnitLevel_Id]  BIGINT         NULL,
    [OrgUnit_ParentExternalId]        NVARCHAR (100) NULL,
    [OrgUnit_ParentId] BIGINT         NULL,
	[OrgUnit_ColorId] [tinyint] NOT NULL DEFAULT ((0)),
    CONSTRAINT [tmp_ms_xx_PK_OrgUnits] PRIMARY KEY CLUSTERED ([OrgUnit_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);'
EXEC ( @CreateTable )


/* creating the insert list*/
SET @InsertTable = '
IF EXISTS (SELECT TOP 1 1 FROM {DBIdentifier}.[dbo].[OrgUnits])
BEGIN
        INSERT INTO {DBIdentifier}.[dbo].[tmp_ms_xx_OrgUnits] WITH (TABLOCK) ([OrgUnit_Id], orgunit_Externalid, [OrgUnit_Name], [OrgUnit_LegacyId], [OrgUnit_IsUnassigned], [OrgUnit_TreeLeft], [OrgUnit_TreeRight], [OrgUnitLevel_ExternalId], [OrgUnit_ParentExternalId],OrgUnit_ColorId)
        SELECT ROW_NUMBER() OVER (ORDER BY OrgUnit_Id ASC),
		[OrgUnit_Id],
               [OrgUnit_Name],
               [OrgUnit_LegacyId],
               [OrgUnit_IsUnassigned],
               [OrgUnit_TreeLeft],
               [OrgUnit_TreeRight],
               [OrgUnitLevel_Id],
               [OrgUnit_ParentId],
			   [OrgUnit_ColorId]
        FROM   {DBIdentifier}.[dbo].[OrgUnits] ou 
END
'


EXEC (@insertTable)
DROP TABLE {DBIdentifier}.[dbo].OrgUnits;

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_OrgUnits]', N'OrgUnits';

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_PK_OrgUnits]', N'PK_OrgUnits', N'OBJECT';

