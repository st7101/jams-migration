SELECT DISTINCT TOP (:maxResult) 
					ae.AlignmentEntity_Id        AS Id,
					ae.AlignmentEntity_Name      AS Name,
					ae.AlignmentEntity_ParentId as ParentId
	FROM GroupedEntities ge
	inner join AlignmentEntitys ae 
		ON ae.AlignmentEntity_Id = ge.AlignmentEntity_id
	inner join OrgUnitAlignmentEntitys oae
		ON ae.AlignmentEntity_ParentId = oae.AlignmentEntity_id
	inner join OrgUnits ou 
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and ((:showAssigned = 1 and ou.OrgUnit_TreeLeft >= :treeLeft and ou.OrgUnit_TreeRight <= :treeRight) 
		or (:showUnassigned = 1 and ou.OrgUnit_IsUnassigned = 1))
	inner join TerritoryTypes tt 
		ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
		and tt.TerritoryType_Id = :terrTypeId
	WHERE ae.AlignmentEntity_Status = :aeStatus
		and ae.AlignmentEntityType_Id in (:alignmentEntityTypeIds)
		and ae.AlignmentEntityType_Id <> (:geoEntityTypeId)
		and (ae.AlignmentEntity_Name like :searchString +'%' escape '\' or ae.AlignmentEntity_Id like :searchString +'%' escape '\')
		and  ge.TerritoryType_id = :terrTypeId	
		OPTION (RECOMPILE)	