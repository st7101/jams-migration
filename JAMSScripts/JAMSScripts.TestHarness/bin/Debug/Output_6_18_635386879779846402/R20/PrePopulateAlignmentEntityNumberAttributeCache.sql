/* CREATE TEMPORARY TABLE */
CREATE TABLE [TempAlignmentEntityNumberAttributeCache] 
( AlignmentEntityNumberAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
AlignmentEntityNumberAttributeCache_Value float NULL , 
AlignmentEntity_id BIGINT NULL , 
AttributeType_id bigint NULL , 
TerritoryType_id BIGINT NULL);

SELECT * INTO ChildEntityPerTerritoryTypeMappings FROM
(
SELECT ae.AlignmentEntity_ParentId AS ParentAlignmentEntity_Id, ae.AlignmentEntity_Id as AlignmentEntity_Id, 
ge.TerritoryType_Id AS TerritoryType_id
FROM [AlignmentEntitys] ae
INNER JOIN [GroupedEntities] ge ON ae.AlignmentEntity_id = ge.AlignmentEntity_id -- join only child records to GE to sum them per territory type
) a

SELECT * INTO IndependentEntitys FROM
(
Select AlignmentEntity_Id
FROM [AlignmentEntitys] ae
WHERE EXISTS (Select 1 from [OrgUnitAlignmentEntitys] oae
WHERE ae.AlignmentEntity_Id = oae.AlignmentEntity_id )
and Exists 
(select distinct territorytype_id from AlignmentEntityTypeTerritoryTypes aet2
where aet2.AlignmentEntityType_id = ae.AlignmentEntityType_id
except
select aet1.TerritoryType_id from AlignmentEntityTypeTerritoryTypes aet1
where aet1.AlignmentEntityType_ParentId = ae.AlignmentEntityType_id
)
) b
