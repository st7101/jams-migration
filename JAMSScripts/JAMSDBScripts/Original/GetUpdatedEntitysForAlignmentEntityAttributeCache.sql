/****** Script for SelectTopNRows command from SSMS  ******/

IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedEntitys') 
DROP TABLE [TempUpdatedEntitys];

CREATE TABLE [dbo].[TempUpdatedEntitys](
	[AlignmentEntity_Id] [nvarchar](100) NOT NULL PRIMARY KEY
);


WITH CTE_UpdatedAlignmentEntities AS 
(
	Select AlignmentEntity_Id, AlignmentEntity_ParentId, AlignmentEntity_OldParentId 
	FROM AlignmentEntitys  
	WHERE AlignmentEntity_Status='Updated' OR AlignmentEntity_Status='SoftDelete'
),

AffectedAlignmentEntity as
(
	SELECT AlignmentEntity_Id FROM CTE_UpdatedAlignmentEntities

	UNION

	SELECT AlignmentEntity_ParentId FROM CTE_UpdatedAlignmentEntities 
	WHERE AlignmentEntity_ParentId is not null

	UNION

	SELECT AlignmentEntity_OldParentId FROM CTE_UpdatedAlignmentEntities 
	WHERE AlignmentEntity_OldParentId is not null
)

INSERT INTO [TempUpdatedEntitys] SELECT * from AffectedAlignmentEntity

