IF (OBJECT_ID('[GetNonSharedAlignmentEntitiesDetailsInCluster]') IS NOT NULL)
  DROP PROCEDURE [GetNonSharedAlignmentEntitiesDetailsInCluster]
DECLARE @sqlCommand NVARCHAR(MAX)='
-- =============================================
-- Author@		Steve Tetrault
-- Create date@ Jun. 17, 2014
-- Description@	Modified Logic for GetAlignmentEntityClustersOfTypeInBoundingBox SP
-- =============================================
CREATE PROCEDURE [dbo].[GetNonSharedAlignmentEntitiesDetailsInCluster]
	-- Add the parameters for the stored procedure here
    
	@minLat FLOAT, @maxLat FLOAT, @minLng FLOAT, @maxLng FLOAT,
    @territoryTypeId BIGINT ,
    @aeStatus NVARCHAR(255) = NULL ,
    @IsGeo BIT = 1 ,
    @OrgUnitId BIGINT
AS
    BEGIN

        DECLARE @sql NVARCHAR(MAX)= ''

	SELECT oae.AlignmentEntity_id as Id,
	   ae.AlignmentEntity_Name      AS Name,
	   ae.AlignmentEntity_Latitude  AS Latitude,
	   ae.AlignmentEntity_Longitude AS Longitude,
	   	   ou.OrgUnit_Id AS OrgUnitId,
	   oae.OrgUnitAlignmentEntity_Weight AS AlignmentWeight,
	   ou.OrgUnit_Name AS OrgUnitName,
	   ou.OrgUnit_ColorId AS OrgUnitColorId
	FROM  [OrgUnitAlignmentEntitys] oae
	inner join AlignmentEntitys ae
	ON oae.TerritoryType_Id = ''+CAST(@territoryTypeId AS VARCHAR)+'' AND ae.AlignmentEntity_Id = oae.AlignmentEntity_id
	inner join OrgUnits ou ON oae.OrgUnit_Id = ou.OrgUnit_Id
	
	where AlignmentEntity_Latitude is not null and ae.AlignmentEntity_Latitude <> 0
	 and ae.AlignmentEntity_Longitude <> 0 and ae.AlignmentEntity_Longitude Is NOT NULL
	and ae.AlignmentEntity_Longitude between ''+CAST(@minLng AS VARCHAR)+'' and ''+CAST(@maxLng AS VARCHAR)+''
	and ae.AlignmentEntity_Latitude between ''+CAST(@minLat AS VARCHAR)+'' and ''+CAST(@maxLat AS VARCHAR)+CHAR(13)
	IF (@isgeo =1)
	BEGIN
	SET @sql=@sql+''and (ae.AlignmentEntityType_Id = 1)''
	END ELSE BEGIN
	SET @sql=@sql+''and (ae.AlignmentEntityType_Id <> 1)''
	END
	IF ( @aeStatus IS NOT NULL
             AND @aeStatus != ''''
           )
    BEGIN
       SET @sql = @sql + '' and	ae.AlignmentEntity_Status = ''''''
                    + @aeStatus + ''''''''
    END
	SET @sql=@sql

        EXEC (@sql)
    END
'
EXEC(@sqlCommand)
