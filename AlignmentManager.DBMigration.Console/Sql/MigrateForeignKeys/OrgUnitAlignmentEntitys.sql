
PRINT N'Updating OrgUnitAlignmentEntitys ...';

UPDATE ouae
SET ouae.[OrgUnit_Id]=ou.OrgUnit_Id, ouae.[AlignmentEntity_Id]=ae.AlignmentEntity_Id
FROM dbo.OrgUnitAlignmentEntitys ouae INNER JOIN 
dbo.OrgUnits ou ON ouae.[OrgUnit_ExternalId]=ou.OrgUnit_ExternalID
INNER JOIN dbo.AlignmentEntitys ae ON ouae.[AlignmentEntity_ExternalId]=ae.[AlignmentEntity_ExternalId]

