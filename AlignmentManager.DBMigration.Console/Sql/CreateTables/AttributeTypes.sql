
CREATE TABLE dbo.Tmp_AttributeTypes
	(
	AttributeType_Id bigint NOT NULL,
	AttributeType_Name nvarchar(255) NULL,
	AttributeType_DisplayName nvarchar(255) NULL,
	AttributeType_DataType nvarchar(100) NOT NULL,
	AttributeType_Category nvarchar(100) NULL,
	AttributeType_Description nvarchar(255) NULL,
	AttributeType_Formula nvarchar(255) NULL,
	AttributeType_Entity nvarchar(255) NULL,
	AttributeType_Status nvarchar(100) NOT NULL,
	AttributeType_ValidationType nvarchar(100) NULL,
	AttributeType_MetricAvailable bit NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_AttributeTypes] PRIMARY KEY CLUSTERED (AttributeType_Id ASC) WITH (DATA_COMPRESSION = PAGE)

	)  ON [PRIMARY]
ALTER TABLE dbo.Tmp_AttributeTypes SET (LOCK_ESCALATION = TABLE)

IF EXISTS(SELECT * FROM dbo.AttributeTypes)
	 EXEC('INSERT INTO dbo.Tmp_AttributeTypes WITH (TABLOCK) (AttributeType_Id, AttributeType_Name, AttributeType_DisplayName, AttributeType_DataType, AttributeType_Category, AttributeType_Description, AttributeType_Formula, AttributeType_Entity, AttributeType_Status, AttributeType_ValidationType, AttributeType_MetricAvailable)
		SELECT AttributeType_Id, AttributeType_Name, AttributeType_DisplayName, AttributeType_DataType, AttributeType_Category, AttributeType_Description, AttributeType_Formula, AttributeType_Entity, AttributeType_Status, AttributeType_ValidationType, AttributeType_MetricAvailable FROM dbo.AttributeTypes WITH (HOLDLOCK TABLOCKX)')
DROP TABLE dbo.AttributeTypes

EXECUTE sp_rename N'dbo.Tmp_AttributeTypes', N'AttributeTypes', 'OBJECT' 
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AttributeTypes]', N'PK_AttributeTypes', N'OBJECT';
