SELECT TOP (100) ou.OrgUnit_Id as Id,
                 ou.OrgUnit_Name as Name,
				 ou.OrgUnit_LegacyId as LegacyId,
				 ou.OrgUnit_ColorId as ColorId               
FROM   OrgUnits ou
       inner join OrgUnitLevels ol
         on ou.OrgUnitLevel_id = ol.OrgUnitLevel_Id
       inner join TerritoryTypes tt
         on ol.OrgUnitLevel_Id = tt.OrgUnitLevel_id
         and tt.TerritoryType_Id = '117'
WHERE  ((0 = 1 and ou.OrgUnit_TreeLeft >= 1 and ou.OrgUnit_TreeRight <= 10430) 
		or (0 = 1 and ou.OrgUnit_IsUnassigned = 1))
       and (ou.OrgUnit_Name like 'm' +'%' escape '\'
             or (ou.OrgUnit_Id like 'm' +'%' escape '\'
                  or ou.OrgUnit_LegacyId like 'm' +'%' escape '\'))

OPTION (RECOMPILE)