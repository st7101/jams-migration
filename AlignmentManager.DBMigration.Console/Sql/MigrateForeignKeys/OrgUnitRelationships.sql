    
PRINT N'Updating OrgUnitRelationships ...';

UPDATE our
SET [Driving_OrgUnitId]=ouD.OrgUnit_Id,Affected_OrgUnitId=ouA.OrgUnit_Id
FROM dbo.OrgUnitRelationships our INNER JOIN 
dbo.OrgUnits ouD ON our.Driving_ExternalOrgUnitId=oud.OrgUnit_ExternalID
LEFT OUTER JOIN dbo.OrgUnits ouA ON our.Affected_ExternalOrgUnitId=ouA.OrgUnit_ExternalID

