SELECT DISTINCT TOP (100) 
					ae.AlignmentEntity_Id        AS Id,
					ae.AlignmentEntity_Name      AS Name,
					ae.AlignmentEntity_ParentId as ParentId
	FROM GroupedEntities ge
	inner join AlignmentEntitys ae 
		ON ae.AlignmentEntity_Id = ge.AlignmentEntity_id
	inner join OrgUnitAlignmentEntitys oae
		ON ae.AlignmentEntity_ParentId = oae.AlignmentEntity_id
	inner join OrgUnits ou 
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and ((0 = 1 and ou.OrgUnit_TreeLeft >= 1 and ou.OrgUnit_TreeRight <= 10430) 
		or (0 = 1 and ou.OrgUnit_IsUnassigned = 1))
	inner join TerritoryTypes tt 
		ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
		and tt.TerritoryType_Id = '124'
	WHERE ae.AlignmentEntity_Status = 'Normal'
		and ae.AlignmentEntityType_Id in (1,2,3)
		and ae.AlignmentEntityType_Id <> (1)
		and (ae.AlignmentEntity_Name like 'h' +'%' escape '\' or ae.AlignmentEntity_Id like 'h' +'%' escape '\')
		and  ge.TerritoryType_id = '124'	
		OPTION (RECOMPILE)	