﻿/*  Remove this query once beta feature for derived alignment is completed. */
EXECUTE  [dbo].[SearchQuery] 
   @showAssigned=:showAssigned
  ,@showUnassigned=:showUnassigned
  ,@treeLeft= :treeLeft
  ,@treeRight= :treeRight
  ,@territoryTypeId=:terrTypeId
  ,@aeStatus=:aeStatus
  ,@MaxResult=:maxResult
  ,@GeoEntityTypeId=:geoEntityTypeId
  ,@SearchString=:searchString
  ,@isGeo=0
  ,@alignmentEntityTypeIds=:alignmentEntityTypeIds
