
PRINT N'Updating OrgUnitRelationshipOverrides ...';

UPDATE ouro
SET OrgUnitRelationshipOverride_DrivingOrgUnitId=ouD.OrgUnit_Id
FROM dbo.OrgUnitRelationshipOverrides ouro INNER JOIN 
dbo.OrgUnits ouD ON 
ouro.[OrgUnitRelationshipOverride_ExternalDrivingOrgUnitId]=ouD.OrgUnit_ExternalID
