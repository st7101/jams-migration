
PRINT N'Updating TerritoryShapeOverrides ...';

UPDATE tso
SET Position_Id=p.Position_Id,OrgUnit_Id=ou.OrgUnit_Id
FROM dbo.TerritoryShapeOverrides tso INNER JOIN 
dbo.OrgUnits ou ON ou.OrgUnit_ExternalID = tso.OrgUnit_ExternalID
INNER JOIN dbo.Positions p ON p.Position_ExternalID = tso.Position_ExternalID
