
PRINT N'Starting rebuilding table [dbo].[AttributeTypeTerritoryTypes]...';


CREATE TABLE [dbo].[tmp_ms_xx_AttributeTypeTerritoryTypes]
    (
      [AttributeTypeTerritoryType_Id] BIGINT NOT NULL ,
      [AttributeTypeTerritoryType_Order] INT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      [AttributeType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AttributeTypeTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [AttributeTypeTerritoryType_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AttributeTypeTerritoryTypes] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_AttributeTypeTerritoryTypes] WITH (TABLOCK)
                ( [AttributeTypeTerritoryType_Id] ,
                  [AttributeTypeTerritoryType_Order] ,
                  [TerritoryType_ExternalId] ,
                  [AttributeType_Id]
                )
                SELECT  [AttributeTypeTerritoryType_Id] ,
                        [AttributeTypeTerritoryType_Order] ,
                        [TerritoryType_Id] ,
                        [AttributeType_Id]
                FROM    [dbo].[AttributeTypeTerritoryTypes]
                ORDER BY [AttributeTypeTerritoryType_Id] ASC;
    END
DROP TABLE [dbo].[AttributeTypeTerritoryTypes];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AttributeTypeTerritoryTypes]',
    N'AttributeTypeTerritoryTypes';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AttributeTypeTerritoryTypes]',
    N'PK_AttributeTypeTerritoryTypes', N'OBJECT';
