﻿SELECT DISTINCT OUAE.AlignmentEntity_id
	FROM OrgUnitAlignmentEntitys OUAE
	INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OUAE.OrgUnit_id
	INNER JOIN TerritoryTypes TT ON TT.OrgUnitLevel_id = OU.OrgUnitLevel_id
	INNER JOIN AlignmentEntitys AE ON OUAE.AlignmentEntity_id = AE.AlignmentEntity_Id
	INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
	AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND TT.TerritoryType_Id = AETTT.TerritoryType_id
	WHERE OUAE.OrgUnitAlignmentEntity_Weight > 100
	GROUP BY OUAE.AlignmentEntity_id, TT.TerritoryType_Id