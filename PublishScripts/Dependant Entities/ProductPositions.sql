INSERT INTO [dbo].[ProductPositions]
           ([ProductPosition_Id]
           ,[ProductPosition_Weight]
           ,[Position_Id]
           ,[Position_ExternalId]
           ,[Product_Id])
SELECT 
	ROW_NUMBER() OVER (ORDER BY ppTemp.PRODUCT_ID),
	ppTemp.PRODUCTPOSITION_WEIGHT,
	p.Position_Id,
	ppTemp.POSITION_ID,
	pptemp.PRODUCT_ID
FROM dbo.ProductPositions_Temp ppTemp
LEFT JOIN dbo.Positions p ON ppTemp.POSITION_ID=p.Position_ExternalId
