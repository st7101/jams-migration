IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedEntitys') 
DROP TABLE [TempUpdatedEntitys];

IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedOrgUnitIds') 
DROP TABLE [TempUpdatedOrgUnitIds];