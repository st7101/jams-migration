﻿-- get explicit alignments
select 
	alignments.AlignmentEntity_Id as AlignmentEntity_Id, 
	alignments.TerritoryType_Id as TerritoryType_Id,
	alignments.OrgUnit_Id as OrgUnit_Id, 
	alignments.OrgUnitAlignmentEntity_Weight as OrgUnitAlignmentEntity_Weight
		
from dbo.OrgUnitAlignmentEntitys alignments
join dbo.AlignmentEntitys entitys on alignments.AlignmentEntity_Id = entitys.AlignmentEntity_Id
where entitys.AlignmentEntity_Status = 'Normal'

union all

-- get derived alignments
select 
	alignments.AlignmentEntity_Id as AlignmentEntity_Id, 
	alignments.TerritoryType_Id as TerritoryType_Id, 
	alignments.OrgUnit_Id as OrgUnit_Id, 
	alignments.OrgUnitAlignmentEntity_Weight as OrgUnitAlignmentEntity_Weight
		
from dbo.GroupedEntities ge 
inner join dbo.AlignmentEntitys entitys on ge.alignmentEntity_Id = entitys.AlignmentEntity_Id 
INNER  JOIN dbo.AlignmentEntitys parents on entitys.AlignmentEntity_ParentId = parents.AlignmentEntity_Id
INNER JOIN dbo.OrgUnitAlignmentEntitys alignments ON  entitys.AlignmentEntity_ParentId =alignments.AlignmentEntity_Id
INNER JOIN  OrgUnits as OU ON OU.OrgUnit_Id = alignments.OrgUnit_Id
INNER JOIN OrgUnitLevels AS OUL
ON OU.OrgUnitLevel_id = OUL.OrgUnitLevel_id
INNER JOIN dbo.TerritoryTypes tt ON tt.OrgUnitLevel_Id = OUL.OrgUnitLevel_Id AND tt.TerritoryType_Id = ge.TerritoryType_Id 
WHERE entitys.AlignmentEntity_Status = 'Normal'

	and parents.AlignmentEntity_Status = 'Normal';