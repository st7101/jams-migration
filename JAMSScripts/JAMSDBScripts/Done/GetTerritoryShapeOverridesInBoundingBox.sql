
EXECUTE  [dbo].[GetTerritoryShapeOverridesInBoundingBox] 
   @positionId=:positionId
  ,@zoomLevel=:zoomLevel
  ,@ShapeWKB=:ShapeWKB
  ,@ShapeWKBType=:ShapeWKBType
  ,@tolerance=:reduceTolerance
  ,@territoryTypeId=:territoryTypeId
  ,@showAssigned=:showAssigned
  ,@showUnassigned=:showUnassigned
  ,@TreeLeft=:treeLeft
  ,@treeRight=:treeRight
