PRINT N'Starting rebuilding table [dbo].[OrgUnitLevelPermissions]...';

CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitLevelPermissions] (
    [OrgUnitLevelPermission_Id]        BIGINT         NOT NULL,
    [OrgUnitLevelPermission_IsEnabled] BIT            NULL,
    [OrgUnitLevel_ExternalId]                  NVARCHAR (100) NULL,
    [OrgUnitLevel_Id]           BIGINT         NULL,
    [Permission_Id]                    BIGINT         NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitLevelPermissions] PRIMARY KEY CLUSTERED ([OrgUnitLevelPermission_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[OrgUnitLevelPermissions])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_OrgUnitLevelPermissions] WITH (TABLOCK) ([OrgUnitLevelPermission_Id], [OrgUnitLevelPermission_IsEnabled], [OrgUnitLevel_ExternalId], [Permission_Id])
        SELECT   [OrgUnitLevelPermission_Id],
                 [OrgUnitLevelPermission_IsEnabled],
                 [OrgUnitLevel_Id],
                 [Permission_Id]
        FROM     [dbo].[OrgUnitLevelPermissions]
        ORDER BY [OrgUnitLevelPermission_Id] ASC;
    END

DROP TABLE [dbo].[OrgUnitLevelPermissions];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitLevelPermissions]', N'OrgUnitLevelPermissions';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitLevelPermissions]', N'PK_OrgUnitLevelPermissions', N'OBJECT';

