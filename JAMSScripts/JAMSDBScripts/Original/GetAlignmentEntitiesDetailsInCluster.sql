﻿SELECT oae.AlignmentEntity_Id        AS Id,
	   ae.AlignmentEntity_Name      AS Name,
	   ae.AlignmentEntity_Latitude  AS Latitude,
	   ae.AlignmentEntity_Longitude AS Longitude,
	   ou.OrgUnit_Id AS OrgUnitId,
	   oae.OrgUnitAlignmentEntity_Weight AS AlignmentWeight,
	   ou.OrgUnit_Name AS OrgUnitName,
	   ou.OrgUnit_ColorId AS OrgUnitColorId
FROM   
		OrgUnitAlignmentEntitys AS oae
		inner join AlignmentEntitys as ae
		on ae.AlignmentEntity_Id = oae.AlignmentEntity_id
		inner join [OrgUnits] ou
		ON ou.OrgUnit_Id = oae.OrgUnit_id
		inner join [TerritoryTypes] tt
		ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id
		and tt.TerritoryType_Id = :territoryTypeId
		and ae.AlignmentEntity_Longitude between :minLng and :maxLng
		and ae.AlignmentEntity_Latitude between :minLat and :maxLat
		and ((:isGeo = 1 and ae.AlignmentEntityType_Id = 1)	or (:isGeo = 0 and ae.AlignmentEntityType_Id <> 1))
		and ae.AlignmentEntity_Status = :aeStatus
		and (ae.AlignmentEntity_Latitude <> 0 and ae.AlignmentEntity_Longitude <> 0)
		and (ae.AlignmentEntity_Latitude Is NOT NULL and ae.AlignmentEntity_Longitude Is NOT NULL)