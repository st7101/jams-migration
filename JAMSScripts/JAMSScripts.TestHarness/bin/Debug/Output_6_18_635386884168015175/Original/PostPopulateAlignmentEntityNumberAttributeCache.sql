DROP TABLE [AlignmentEntityNumberAttributeCache];

/* POPULATE ORIGINAL TABLE */
CREATE TABLE [AlignmentEntityNumberAttributeCache] 
( AlignmentEntityNumberAttributeCache_Id bigint NOT NULL, 
AlignmentEntityNumberAttributeCache_Value float NULL , 
AlignmentEntity_id nvarchar(100) NULL , 
AttributeType_id bigint NULL , 
TerritoryType_id nvarchar(100) NULL , 
CONSTRAINT PK_AlignmentEntityNumberAttributeCache_AlignmentEntityNumberAttributeCache_Id 
PRIMARY KEY CLUSTERED (AlignmentEntityNumberAttributeCache_Id));

INSERT INTO [AlignmentEntityNumberAttributeCache] WITH(TABLOCK) (AlignmentEntityNumberAttributeCache_Id, AlignmentEntityNumberAttributeCache_Value, AlignmentEntity_id, AttributeType_id, TerritoryType_id)
SELECT AlignmentEntityNumberAttributeCache_Id, AlignmentEntityNumberAttributeCache_Value, AlignmentEntity_id, AttributeType_id, TerritoryType_id FROM [TempAlignmentEntityNumberAttributeCache]; 

 /* ADD INDEXES ON TABLE */
CREATE NONCLUSTERED INDEX [idx_SortByValue] ON [dbo].[AlignmentEntityNumberAttributeCache] 
(
	[AlignmentEntityNumberAttributeCache_Value] ASC,
	[AlignmentEntity_id] ASC,
	[AttributeType_id] ASC,
	[TerritoryType_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


CREATE NONCLUSTERED INDEX [idx_NumberAttributeCache_Details] ON [dbo].[AlignmentEntityNumberAttributeCache] 
(
	[AlignmentEntity_id] ASC,
	[TerritoryType_id] ASC
)
INCLUDE ( [AlignmentEntityNumberAttributeCache_Value],
[AttributeType_id]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

CREATE NONCLUSTERED INDEX [idx_AttributeTypeId_AEAttributeCacheId_AEId_TTId] ON [dbo].[AlignmentEntityNumberAttributeCache] 
(
	[AttributeType_id] ASC
)
INCLUDE ( [AlignmentEntityNumberAttributeCache_Id],
[AlignmentEntity_id],
[TerritoryType_id],
[AlignmentEntityNumberAttributeCache_Value])


/* Drop Temporary Table */
DROP TABLE [TempAlignmentEntityNumberAttributeCache]
DROP TABLE [ChildEntityPerTerritoryTypeMappings]
DROP TABLE [IndependentEntitys]

/* Update HiLo table */
DELETE FROM HiLoUniqueKey
WHERE ObjectType = 'AlignmentEntityNumberAttributeCache'

DELETE FROM HiLoUniqueKey
WHERE ObjectType = 'AlignmentEntityAttributeCache'

INSERT INTO HiLoUniqueKey (NextHi, ObjectType)
Values ((SELECT ISNULL(MAX(AlignmentEntityNumberAttributeCache_Id),0)+1 
				FROM [AlignmentEntityNumberAttributeCache]), 'AlignmentEntityNumberAttributeCache');