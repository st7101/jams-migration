﻿/* Populate Temporary table */
DECLARE @attributeTypeId VARCHAR(200) -- Attribute Type Id
DECLARE @attributeTypeName VARCHAR(200) -- Attribute Type Name
DECLARE @sql NVARCHAR(MAX)

DECLARE Attribute_Cursor CURSOR FOR
SELECT AttributeType_Id, AttributeType_Name FROM [AttributeTypes]
WHERE AttributeType_Entity = 'AlignmentEntity' AND
	(AttributeType_DataType = 'Text')

OPEN Attribute_Cursor
FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @sql = N'
	WITH [TempAlignmentEntityTextAttributeCache]
	AS
	(SELECT DISTINCT ae.AlignmentEntity_id AS AlignmentEntity_id,' + 
			@attributeTypeId + ' AS AttributeType_id, 
			ae.[' + @attributeTypeName + '] AS AlignmentEntityTextAttributeCache_Value
			FROM [AlignmentEntitys] ae
			WHERE EXISTS (Select 1 from [OrgUnitAlignmentEntitys] oae
				Where ae.AlignmentEntity_Id = oae.AlignmentEntity_id )
			AND ae.AlignmentEntity_Status =''Updated''
	)
	
	UPDATE  aetac WITH (TABLOCK)
	SET aetac.AlignmentEntityTextAttributeCache_Value = taetac.AlignmentEntityTextAttributeCache_Value
	FROM AlignmentEntityTextAttributeCache aetac
	inner join TempAlignmentEntityTextAttributeCache taetac
	ON aetac.AlignmentEntity_id = taetac.AlignmentEntity_id 
	AND aetac.AttributeType_id = taetac.AttributeType_id
	;'

	exec sp_executesql @sql;

	FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName
END


CLOSE Attribute_Cursor   
DEALLOCATE Attribute_Cursor