IF EXISTS (SELECT * FROM sys.objects o WHERE o.name = 'HiLoUniqueKey') 
DROP TABLE dbo.HiLoUniqueKey

CREATE TABLE HiLoUniqueKey
(
   ObjectType NVARCHAR(150) NOT NULL,
   NextHi BIGINT NOT NULL
) 

--Proceed with the actual script
DECLARE @name VARCHAR(200) -- table name  
DECLARE @idColumnName VARCHAR(200) -- bigint column name

DECLARE db_cursor CURSOR FOR  

SELECT DISTINCT t.name, c.name
from sys.tables t
inner join sys.schemas s on t.schema_id = s.schema_id
inner join sys.indexes i on i.object_id = t.object_id
inner join sys.index_columns ic on ic.object_id = t.object_id
        inner join sys.columns c on c.object_id = t.object_id and
                ic.column_id = c.column_id
where i.index_id > 0 and i.is_primary_key = 1 and TYPE_NAME(c.user_type_id) IN ('bigint','Int')
AND c.is_identity=0
OPEN db_cursor   
FETCH NEXT FROM db_cursor INTO @name, @idColumnName

WHILE @@FETCH_STATUS = 0   
BEGIN
	   Declare @sql nvarchar(max)
       --Add record to HiLoUniqueKey for this table
       set @sql = N'Insert into HiLoUniqueKey (ObjectType, NextHi) Select ''' + @name + ''', isnull(MAX(' + @idColumnName + '),0)+1 From ' + @name
       exec sp_executesql @sql
       
       FETCH NEXT FROM db_cursor INTO @name, @idColumnName
END   

CLOSE db_cursor   
DEALLOCATE db_cursor