INSERT INTO [dbo].[OrgUnitLevelRoles]
           ([OrgUnitLevelRole_Id]
           ,[OrgUnitLevelRole_MaxPosition]
           ,[OrgUnitLevelRole_MinPosition]
           ,[UsesEffort]
           ,[OrgUnitLevel_Id]
           ,[OrgUnitLevel_ExternalId]
		   ,[Role_Id])
SELECT 
	oulrT.OrgUnitLevelRole_Id,
	oulrT.ORGUNITLEVELROLE_MAXPOSITION,
	oulrT.ORGUNITLEVELROLE_MINPOSITION,
	oulrT.USESEFFORT,
	oul.OrgUnitLevel_Id,
	oulrT.ORGUNITLEVEL_ID,
	oulrT.ROLE_ID
FROM dbo.OrgUnitLevelRoles_Temp oulrT 
INNER JOIN dbo.OrgUnitLevels oul 
	ON oulrT.ORGUNITLEVEL_ID=oul.OrgUnitLevel_ExternalId

