
/* DROP TEMPORARY TABLES IF THEY EXIST*/
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'ChildEntityPerTerritoryTypeMappings') 
DROP TABLE [ChildEntityPerTerritoryTypeMappings];

IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'IndependentEntitys') 
DROP TABLE [IndependentEntitys];



SELECT * INTO ChildEntityPerTerritoryTypeMappings FROM
(
SELECT ae_child.AlignmentEntity_ParentId AS ParentAlignmentEntity_Id, ae_child.AlignmentEntity_Id as AlignmentEntity_Id, 
ge.TerritoryType_Id AS TerritoryType_id
FROM [AlignmentEntitys] ae_child
INNER JOIN [GroupedEntities] ge ON ae_child.AlignmentEntity_id = ge.AlignmentEntity_id -- join only child records to GE to sum them per territory type
INNER JOIN [TempUpdatedEntitys] tue ON tue.AlignmentEntity_id = ae_child.AlignmentEntity_ParentId 
WHERE ae_child.AlignmentEntity_Status in ('Normal','Updated')
) a

SELECT * INTO IndependentEntitys FROM
(
Select ae.AlignmentEntity_Id
FROM [AlignmentEntitys] ae
INNER JOIN [TempUpdatedEntitys] tue ON tue.AlignmentEntity_id = ae.AlignmentEntity_Id
WHERE EXISTS (Select 1 from [OrgUnitAlignmentEntitys] oae
WHERE ae.AlignmentEntity_Id = oae.AlignmentEntity_id )
and Exists 
(select distinct territorytype_id from AlignmentEntityTypeTerritoryTypes aet2
where aet2.AlignmentEntityType_id = ae.AlignmentEntityType_id
except
select aet1.TerritoryType_id from AlignmentEntityTypeTerritoryTypes aet1
where aet1.AlignmentEntityType_ParentId = ae.AlignmentEntityType_id
)
) b

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChildEntityPerTerritoryTypeMappings]') AND name = N'idx_AEID_ParentAEId_TType')
DROP INDEX [idx_AEID_ParentAEId_TType] ON [dbo].[ChildEntityPerTerritoryTypeMappings]
CREATE NONCLUSTERED INDEX [idx_AEID_ParentAEId_TType] ON [dbo].[ChildEntityPerTerritoryTypeMappings] 
(
	[AlignmentEntity_Id] ASC
)
INCLUDE ( [ParentAlignmentEntity_Id],
[TerritoryType_id]) WITH (DATA_COMPRESSION=PAGE)

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IndependentEntitys]') AND name = N'PK_IndependentEntitys')
ALTER TABLE [dbo].[IndependentEntitys] DROP CONSTRAINT [PK_IndependentEntitys]
ALTER TABLE [dbo].[IndependentEntitys] ADD  CONSTRAINT [PK_IndependentEntitys] PRIMARY KEY CLUSTERED 
(
	[AlignmentEntity_Id] ASC
) WITH (DATA_COMPRESSION=PAGE)

/* Populate Temporary table */
DECLARE @attributeTypeId VARCHAR(200) -- Attribute Type Id
DECLARE @attributeTypeName VARCHAR(200) -- Attribute Type Name
DECLARE @sql NVARCHAR(MAX)

DECLARE Attribute_Cursor CURSOR FOR
SELECT AttributeType_Id, AttributeType_Name FROM [AttributeTypes]
WHERE AttributeType_Entity = 'AlignmentEntity' AND
	(AttributeType_DataType = 'Number' OR AttributeType_DataType = 'Currency')

OPEN Attribute_Cursor
FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName

WHILE @@FETCH_STATUS = 0
BEGIN
	
	SET @sql = N'
	With TempAlignmentEntityNumberAttributeCache as
	(SELECT em.ParentAlignmentEntity_Id AS AlignmentEntity_id, ' + 
			@attributeTypeId + ' AS AttributeType_Id, 
			em.TerritoryType_id AS TerritoryType_id, 
			SUM(ISNULL(ae.[' + @attributeTypeName + '], 0)) AS AlignmentEntityNumberAttributeCache_Value FROM
			AlignmentEntitys ae
			INNER JOIN ChildEntityPerTerritoryTypeMappings em ON em.AlignmentEntity_Id = ae.AlignmentEntity_Id
			GROUP BY em.ParentAlignmentEntity_Id, em.TerritoryType_id
		UNION
	SELECT ae.AlignmentEntity_Id AS AlignmentEntity_id, ' + 
			@attributeTypeId + ' AS AttributeType_Id,
			pae.TerritoryType_id AS TerritoryType_id,
			0 AS AlignmentEntityNumberAttributeCache_Value 
			FROM AlignmentEntitys ae
			INNER JOIN TempUpdatedEntitys uae
			on uae.AlignmentEntity_Id = ae.AlignmentEntity_Id
			INNER JOIN (SELECT DISTINCT aettt.TerritoryType_id AS TerritoryType_Id, 
								aettt.AlignmentEntityType_ParentId AS AlignmentEntityType_id 
			FROM AlignmentEntityTypeTerritoryTypes aettt
			WHERE aettt.AlignmentEntityType_ParentId IS NOT NULL) pae 
			ON ae.AlignmentEntityType_id = pae.AlignmentEntityType_id
			WHERE NOT EXISTS (SELECT 1 FROM ChildEntityPerTerritoryTypeMappings em WHERE em.ParentAlignmentEntity_Id = ae.AlignmentEntity_Id and em.TerritoryType_id = pae.TerritoryType_Id)
			AND ae.AlignmentEntity_Status in (''Normal'',''Updated'')
			
		UNION
	SELECT ae.AlignmentEntity_id AS AlignmentEntity_id, ' + 
			@attributeTypeId + ' AS AttributeType_id, 
			NULL AS TerritoryType_id, 
			ISNULL(ae.[' + @attributeTypeName + '], 0) AS AlignmentEntityNumberAttributeCache_Value
			FROM [AlignmentEntitys] ae
			INNER JOIN IndependentEntitys ie ON ae.AlignmentEntity_Id = ie.AlignmentEntity_Id
			WHERE ae.AlignmentEntity_Status in (''Normal'',''Updated'')	
	UNION
	SELECT ae.AlignmentEntity_id AS AlignmentEntity_id, ' + 
			@attributeTypeId + ' AS AttributeType_id, 
			NULL AS TerritoryType_id, 
			0 AS AlignmentEntityNumberAttributeCache_Value
			FROM [AlignmentEntitys] ae
			INNER JOIN IndependentEntitys ie ON ae.AlignmentEntity_Id = ie.AlignmentEntity_Id
			WHERE ae.AlignmentEntity_Status = ''SoftDelete''
	)	
	UPDATE aenac WITH (TABLOCK)
		SET aenac.AlignmentEntityNumberAttributeCache_Value = taenac.AlignmentEntityNumberAttributeCache_Value 
	FROM AlignmentEntityNumberAttributeCache  aenac
	INNER join TempAlignmentEntityNumberAttributeCache taenac
		ON taenac.AlignmentEntity_id = aenac.AlignmentEntity_id
		AND taenac.AttributeType_id = aenac.AttributeType_id
		AND (taenac.TerritoryType_id = aenac.TerritoryType_id 
		OR (taenac.TerritoryType_id is null and aenac.TerritoryType_id is null))
	;'

	exec sp_executesql @sql;

	FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName
END


CLOSE Attribute_Cursor   
DEALLOCATE Attribute_Cursor

DROP TABLE [ChildEntityPerTerritoryTypeMappings]
DROP TABLE [IndependentEntitys]
DROP TABLE [TempUpdatedEntitys]
