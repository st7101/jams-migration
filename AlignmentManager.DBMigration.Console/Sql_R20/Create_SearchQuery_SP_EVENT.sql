-- =============================================
-- Author:		Steve Tetrault
-- Create date: Jun. 17, 2014
-- Description:	Modified Logic for advanced search, and geo search

-- =============================================
CREATE PROCEDURE [dbo].[SearchQuery]
	-- Add the parameters for the stored procedure here
    @showAssigned BIT ,
    @showUnassigned BIT ,
    @treeLeft INT ,
    @treeRight INT ,
    @territoryTypeId BIGINT ,
    @aeStatus NVARCHAR(255) = NULL ,
    @MaxResult INT ,
    @GeoEntityTypeId INT ,
    @SearchString NVARCHAR(100) = NULL ,
    @IsGeo BIT = 1 ,
    @alignmentEntityTypeIds NVARCHAR(MAX) = NULL
AS
    BEGIN

        DECLARE @sql NVARCHAR(MAX)= '
SELECT DISTINCT TOP ' + CAST(@maxResult AS NVARCHAR) + '
				 ae.AlignmentEntity_Id   AS Id,
                 ae.AlignmentEntity_Name   AS Name
    FROM AlignmentEntitys ae
	inner join OrgUnitAlignmentEntitys oae
		ON ae.AlignmentEntity_Id = oae.AlignmentEntity_id 
		and oae.TerritoryType_Id =' + CAST(@territoryTypeId AS NVARCHAR)
            + CHAR(13)
        IF ( @showAssigned = 1
             AND @showUnassigned = 1
           )
            BEGIN
                SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and (( ou.OrgUnit_TreeLeft >= ' + CAST(@treeLeft AS NVARCHAR)
                    + ' and ou.OrgUnit_TreeRight <= '
                    + CAST(@treeRight AS NVARCHAR) + ') 
		or ou.OrgUnit_IsUnassigned = 1)
'
            END
        ELSE
            IF ( @showAssigned = 1 )
                BEGIN
                    SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
	and ( ou.OrgUnit_TreeLeft >= ' + CAST(@treeLeft AS NVARCHAR)
                        + ' and ou.OrgUnit_TreeRight <= '
                        + CAST(@treeRight AS NVARCHAR) + ') 
'
                END
            ELSE
                IF ( @showUnassigned = 1 )
                    BEGIN
                        SET @sql = @sql + 'inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and (ou.OrgUnit_IsUnassigned = 1)
'
                    END
				
        IF ( @isgeo = 1 )
            BEGIN
                SET @sql = @sql + '
	WHERE ae.AlignmentEntityType_Id = ' + CAST(@GeoEntityTypeId AS NVARCHAR)
                    + CHAR(13)
            END
        ELSE
            BEGIN
                SET @sql = @sql + '
	WHERE ae.AlignmentEntityType_Id in (' + @alignmentEntityTypeIds + ')'
                    + CHAR(13)
            END

        IF ( @SearchString IS NOT NULL
             AND @SearchString != ''
           )
            BEGIN
                SET @sql = @sql + '	and (LEFT(ae.AlignmentEntity_Name,'
                    + CAST(LEN(@SearchString) AS NVARCHAR) + ') = '''
                    + @SearchString
                    + ''' or LEFT(ae.AlignmentEntity_ExternalId,'
                    + CAST(LEN(@SearchString) AS NVARCHAR) + ') = '''
                    + @SearchString + ''')' + CHAR(13)
            END
        IF ( @aeStatus IS NOT NULL
             AND @aeStatus != ''
           )
            BEGIN
                SET @sql = @sql + ' and	ae.AlignmentEntity_Status = '''
                    + @aeStatus + ''''
            END
        EXEC (@sql)
    END

