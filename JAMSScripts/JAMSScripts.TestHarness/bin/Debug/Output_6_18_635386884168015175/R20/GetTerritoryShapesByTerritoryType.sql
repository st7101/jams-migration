SELECT shapes.OrgUnit_Id AS OrgUnitId, org.OrgUnit_Name AS OrgUnitName, geom.Reduce(0.01).STAsBinary() AS Shape
FROM TerritoryShapes AS shapes
INNER JOIN Orgunits AS org
ON shapes.OrgUnit_Id = org.OrgUnit_Id
INNER JOIN TerritoryTypes AS t
ON org.OrgUnitLevel_id = t.OrgUnitLevel_id
WHERE t.TerritoryType_Id = 29 and org.OrgUnit_IsUnassigned = 0