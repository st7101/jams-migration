
PRINT N'Starting rebuilding table [dbo].[ApprovalProgressTerritoryTypes]...';


CREATE TABLE [dbo].[tmp_ms_xx_ApprovalProgressTerritoryTypes]
    (
      [ApprovalProgress_Id] BIGINT NOT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NOT NULL ,
      [TerritoryType_Id] BIGINT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_ApprovalProgressTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [ApprovalProgress_id], [TerritoryType_id] )
        WITH ( DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[ApprovalProgressTerritoryTypes] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_ApprovalProgressTerritoryTypes] WITH (TABLOCK)
                ( [ApprovalProgress_Id] ,
                  [TerritoryType_ExternalId],
				  [TerritoryType_Id]
                )
                SELECT  [ApprovalProgress_Id] ,
                        [TerritoryType_Id],
						ROW_NUMBER() OVER ( ORDER BY [TerritoryType_Id] )
                FROM    [dbo].[ApprovalProgressTerritoryTypes];
    END
DROP TABLE [dbo].[ApprovalProgressTerritoryTypes];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ApprovalProgressTerritoryTypes]',
    N'ApprovalProgressTerritoryTypes';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ApprovalProgressTerritoryTypes]',
    N'PK_ApprovalProgressTerritoryTypes', N'OBJECT';
