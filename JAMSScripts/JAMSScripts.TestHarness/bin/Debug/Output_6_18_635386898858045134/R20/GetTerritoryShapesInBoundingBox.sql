
EXECUTE  [dbo].[GetTerritoryShapesInBoundingBox] 
@zoomLevel=20
  ,@ShapeWKB=System.Data.SqlTypes.SqlBytes
  ,@ShapeWKBType=System.Data.SqlTypes.SqlBytesType
  ,@tolerance=0.5
  ,@territoryTypeId=4
  ,@showAssigned=0
  ,@showUnassigned=0
  ,@TreeLeft=1
  ,@treeRight=10430
