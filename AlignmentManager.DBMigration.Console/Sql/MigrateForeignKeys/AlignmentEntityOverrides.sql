PRINT N'Updating AlignmentEntityOverrides ...';

UPDATE aeo
SET AlignmentEntity_Id=ae.AlignmentEntity_Id,TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.AlignmentEntityOverrides aeo INNER JOIN 
dbo.AlignmentEntitys ae ON aeo.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON aeo.TerritoryType_ExternalID=tt.TerritoryType_ExternalID
