﻿/*Query to get count of records whose parent entity type is not Geo and Geo type entites having parent defined. 
:geoEntityTypeId belongs to geo type which will be passed from server side.*/

select AlignmentEntityType_id, TerritoryType_Id  from AlignmentEntityTypeTerritoryTypes
where AlignmentEntityType_ParentId <> :geoEntityTypeId
	or (AlignmentEntityType_id = :geoEntityTypeId and AlignmentEntityType_ParentId is not null)