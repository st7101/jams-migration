/********* This query gets selected entities list ********************/
EXECUTE [dbo].[MultiSelectQuery] 
   @minLat=:minLat
  ,@maxLat=:maxLat
  ,@minLng=:minlng
  ,@maxLng=:maxlng
  ,@territoryTypeId=:territoryTypeId
  ,@showAssigned=:showAssigned
  ,@showUnassigned=:showUnassigned
  ,@TreeLeft=:treeLeft
  ,@treeRight=:treeRight
  ,@ShowGeos=:showGeos
  ,@ShowCustomers=:showCustomers
