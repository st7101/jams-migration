
PRINT N'Updating AlignmentEntityTypeTerritoryTypes ...';

UPDATE aettt
SET TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.AlignmentEntityTypeTerritoryTypes aettt INNER JOIN 
dbo.TerritoryTypes tt ON aettt.TerritoryType_ExternalID= tt.TerritoryType_ExternalID
