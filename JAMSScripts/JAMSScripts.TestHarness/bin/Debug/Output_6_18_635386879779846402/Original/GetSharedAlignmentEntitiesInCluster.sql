/*This query gets the cluster details for shared entities. */

/********* GET SHARED ALIGNMENT ENTITIES FOR THE SELECTED TERRITORY TYPE ********************/
WITH SharedEntities AS
(
	SELECT oae.AlignmentEntity_id
	FROM  [OrgUnitAlignmentEntitys] oae
	inner join [OrgUnits] ou
	ON ou.OrgUnit_Id = oae.OrgUnit_id
	inner join [TerritoryTypes] tt
	ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id
	and tt.TerritoryType_Id = '132'
	inner join AlignmentEntitys ae
	ON ae.AlignmentEntity_Id = oae.AlignmentEntity_id
	and AlignmentEntity_Latitude is not null
	and ((0 = 1 and ae.AlignmentEntityType_Id = 1) or (0 = 0 and ae.AlignmentEntityType_Id <> 1))
	and ae.AlignmentEntity_Status = 'Normal'
	and ae.AlignmentEntity_Longitude between -73.4377403259277 and -66.9500045776367
	and ae.AlignmentEntity_Latitude between 42.6969776153564 and 47.4596862792969
	and (ae.AlignmentEntity_Latitude <> 0 and ae.AlignmentEntity_Longitude <> 0)
	and (ae.AlignmentEntity_Latitude Is NOT NULL and ae.AlignmentEntity_Longitude Is NOT NULL)
	GROUP BY oae.AlignmentEntity_id
	HAVING COUNT(oae.AlignmentEntity_id) >1
)

SELECT AlignmentEntity_Id        AS Id,
	   AlignmentEntity_Name      AS Name,
	   AlignmentEntity_Latitude  AS Latitude,
	   AlignmentEntity_Longitude AS Longitude
FROM   (
		SELECT [oae].AlignmentEntity_Id,
			   [oae].AlignmentEntity_Name,
			   [oae].AlignmentEntity_Latitude,
			   [oae].AlignmentEntity_Longitude,
			   Left(oae.orgUnitsList,Len(oae.orgUnitsList)-1) AS orgUnitIds
		FROM   
		(
			SELECT distinct [ae].AlignmentEntity_Id,
							[ae].AlignmentEntity_Name,
							[ae].AlignmentEntity_Latitude,
							[ae].AlignmentEntity_Longitude,
							(SELECT oae1.OrgUnit_id + ',' AS [text()]
								FROM   OrgUnitAlignmentEntitys oae1
									inner join [OrgUnits] ou
										ON ou.OrgUnit_Id = oae1.OrgUnit_id
									inner join [TerritoryTypes] tt
										ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id
											and tt.TerritoryType_Id = '132'
								WHERE  oae1.AlignmentEntity_id = oae2.AlignmentEntity_id
								ORDER  BY ou.OrgUnit_Id
								For XML PATH ('')
							) [orgUnitsList]
			FROM  OrgUnitAlignmentEntitys AS oae2
			inner join AlignmentEntitys as ae
			on ae.AlignmentEntity_Id = oae2.AlignmentEntity_id
			and ae.AlignmentEntity_Longitude between -73.4377403259277 and -66.9500045776367
			and ae.AlignmentEntity_Latitude between 42.6969776153564 and 47.4596862792969
			and ((0 = 1 and ae.AlignmentEntityType_Id = 1) or (0 = 0 and ae.AlignmentEntityType_Id <> 1))
			and ae.AlignmentEntity_Status = 'Normal'
			and (ae.AlignmentEntity_Latitude <> 0 and ae.AlignmentEntity_Longitude <> 0)
			and (ae.AlignmentEntity_Latitude Is NOT NULL and ae.AlignmentEntity_Longitude Is NOT NULL)
			inner join SharedEntities se
			on se.AlignmentEntity_Id = oae2.AlignmentEntity_id 
		) [oae]
	) AS EntitiesWithOrgunits   
WHERE  orgUnitIds = '21J50000'
