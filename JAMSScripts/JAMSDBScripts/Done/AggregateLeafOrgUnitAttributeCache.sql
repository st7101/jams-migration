IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempDerivedAlignmentsTerritoryTypes') 
DROP TABLE [TempDerivedAlignmentsTerritoryTypes]

SELECT * INTO [TempDerivedAlignmentsTerritoryTypes] FROM
(SELECT DISTINCT aettt.TerritoryType_id as TerritoryType_Id, 
					aettt.AlignmentEntityType_ParentId as AlignmentEntityType_id 
 FROM AlignmentEntityTypeTerritoryTypes aettt
 WHERE aettt.AlignmentEntityType_ParentId IS NOT NULL) pae
 
 /* Populate Temporary table */
DECLARE @attributeTypeId VARCHAR(200) -- Attribute Type Id
DECLARE @attributeTypeName VARCHAR(200) -- Attribute Type Name
DECLARE @sql NVARCHAR(MAX)

DECLARE Attribute_Cursor CURSOR FOR
SELECT AttributeType_Id, AttributeType_Name FROM [AttributeTypes]
WHERE AttributeType_Entity = 'AlignmentEntity' AND
	(AttributeType_DataType = 'Number' OR AttributeType_DataType = 'Currency')

OPEN Attribute_Cursor
FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @sql = N'
	WITH UpdatedOrgUnitAttributeCache AS
	(
		SELECT oae.OrgUnit_id, ' + @attributeTypeId + ' AS AttributeType_id, 
			SUM(isnull(ae.'+@attributeTypeName+',0) * oae.OrgUnitAlignmentEntity_Weight/100) AS OrgUnitAttributeCache_Value 
		FROM AlignmentEntitys ae
			INNER JOIN OrgUnitAlignmentEntitys oae ON oae.AlignmentEntity_id = ae.AlignmentEntity_Id
			INNER JOIN TempUpdatedOrgUnitIds tuoui on tuoui.OrgUnit_id = oae.OrgUnit_Id
			LEFT OUTER JOIN  TempDerivedAlignmentsTerritoryTypes da ON 
			 ae.AlignmentEntityType_id = da.AlignmentEntityType_id
			inner join TerritoryTypes tt 
			ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
	and tt.TerritoryType_Id =da.TerritoryType_Id
			WHERE 
			((ae.AlignmentEntityType_id = da.AlignmentEntityType_id) 
			AND (ae.AlignmentEntity_Status = ''Normal'' OR  ae.AlignmentEntity_Status = ''Updated'')
		GROUP BY oae.OrgUnit_id 
	)

	UPDATE ouae WITH (TABLOCK) 
	SET
		ouae.OrgUnitAttributeCache_Value = uouae.OrgUnitAttributeCache_Value
	FROM [OrgUnitAttributeCache] ouae
	INNER JOIN UpdatedOrgUnitAttributeCache uouae
	ON ouae.OrgUnit_id = uouae.OrgUnit_id 
	AND ouae.AttributeType_id = uouae.AttributeType_id
	;'
exec sp_executesql @sql;

	FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName
END


CLOSE Attribute_Cursor;  
DEALLOCATE Attribute_Cursor;

/* Update parents of updated leaf level org units */
SET @sql = N'
	WITH ParentsToBeUpdated AS
	(SELECT DISTINCT parents.* FROM OrgUnits parents, OrgUnits leaf 
							WHERE leaf.OrgUnit_Id in (select OrgUnit_Id from TempUpdatedOrgUnitIds)
							AND parents.OrgUnit_TreeLeft < leaf.OrgUnit_TreeLeft AND parents.OrgUnit_TreeRight > leaf.OrgUnit_TreeRight),

	UpdatedParentOrgUnitsCache AS
	(
	SELECT parent.orgunit_Id as OrgUnit_id, oc.AttributeType_id as AttributeType_id, SUM(oc.OrgUnitAttributeCache_Value) as OrgUnitAttributeCache_Value 
	FROM OrgUnitAttributeCache as oc
	INNER JOIN OrgUnits child ON child.OrgUnit_Id = oc.Orgunit_Id
	INNER JOIN OrgUnitLevels oul ON child.OrgUnitLevel_Id = oul.OrgUnitLevel_Id
	INNER JOIN ParentsToBeUpdated parent ON child.OrgUnit_TreeLeft >  parent.OrgUnit_TreeLeft AND child.OrgUnit_TreeRight < parent.OrgUnit_TreeRight
	WHERE oul.OrgUnitLevel_IsLeafLevel = 1
	GROUP BY parent.orgunit_Id, oc.AttributeType_id
	)

	UPDATE ouae WITH (TABLOCK) 
		SET
			ouae.OrgUnitAttributeCache_Value = upouc.OrgUnitAttributeCache_Value
		FROM [OrgUnitAttributeCache] ouae
		INNER JOIN UpdatedParentOrgUnitsCache upouc
		ON ouae.OrgUnit_id = upouc.OrgUnit_id 
		AND ouae.AttributeType_id = upouc.AttributeType_id';

exec sp_executesql @sql;

 /* Drop Temporary Table */
DROP TABLE [TempDerivedAlignmentsTerritoryTypes]
DROP TABLE [TempUpdatedOrgUnitIds]
