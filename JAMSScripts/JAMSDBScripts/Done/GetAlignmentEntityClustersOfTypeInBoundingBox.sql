/********* This query gets clusters in Assigned/Both display mode ********************/
EXEC dbo.GetAlignmentEntityClustersOfTypeInBoundingBox
	@showAssigned=:showAssigned,@showUnassigned=:showUnassigned , @isGeo=:isGeo,  @treeLeft=:treeLeft, @treeRight=:treeRight, @territoryTypeId=:territoryTypeId,
	@minLat=:minLat, @maxLat=:maxLat, @minLng=:minLng, @maxLng=:maxLng, @ZoomLevel=:zoomLevel,@aeStatus=:aeStatus;