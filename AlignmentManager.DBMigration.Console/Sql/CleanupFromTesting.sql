USE QA_AlignmentManager_0012e

DROP TYPE IntArray
DROP TYPE StringArray

USE [master]
ALTER DATABASE [QA_AlignmentManager_0012e_Event_31700] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
RESTORE DATABASE [QA_AlignmentManager_0012e_Event_31700] FROM  DISK = N'N:\QA_AlignmentManager_0012e_Event_30800.bak' WITH  FILE = 1,  MOVE N'QA_AlignmentManager_0012e_Event_31700_primary' TO N'h:\QA_AlignmentManager_0012e_Event_31700_2primary.mdf',  MOVE N'QA_AlignmentManager_0012e_Event_31700_log' TO N'l:\QA_AlignmentManager_0012e_Event_31700_log2.ldf',  NOUNLOAD,  REPLACE,  STATS = 5
ALTER DATABASE [QA_AlignmentManager_0012e_Event_31700] SET MULTI_USER

GO
USE [QA_AlignmentManager_0012e_Event_31700]
GO
ALTER INDEX [PK__Territor__676633B66F4A8121] ON [dbo].[TerritoryShapes] REBUILD PARTITION = ALL
GO
ALTER INDEX [pk_TerritoryShapeOverrides] ON [dbo].[TerritoryShapeOverrides] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
USE [master]
ALTER DATABASE [QA_AlignmentManager_0012e_Event_38500] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
RESTORE DATABASE [QA_AlignmentManager_0012e_Event_38500] FROM  DISK = N'N:\QA_AlignmentManager_0012e_Event_31700.bak' WITH  FILE = 1,  MOVE N'QA_AlignmentManager_0012e_Event_38500_primary' TO N'm:\QA_AlignmentManager_0012e_Event_38500_primary.mdf',  MOVE N'QA_AlignmentManager_0012e_Event_38500_log' TO N'j:\QA_AlignmentManager_0012e_Event_38500_log.ldf',  NOUNLOAD,  STATS = 5
ALTER DATABASE [QA_AlignmentManager_0012e_Event_38500] SET MULTI_USER

USE [QA_AlignmentManager_0012e_Event_38500]
GO
ALTER INDEX [PK__Territor__676633B6592635D8] ON [dbo].[TerritoryShapes] REBUILD PARTITION = ALL
GO
ALTER INDEX [pk_TerritoryShapeOverrides] ON [dbo].[TerritoryShapeOverrides] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
USE [master]
ALTER DATABASE [QA_AlignmentManager_0012e_Event_38600] SET SINGLE_USER WITH ROLLBACK IMMEDIATE
RESTORE DATABASE [QA_AlignmentManager_0012e_Event_38600] FROM  DISK = N'N:\QA_AlignmentManager_0012e_Event_38500.bak' WITH  FILE = 1,  MOVE N'QA_AlignmentManager_0012e_Event_38600_primary' TO N'F:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\DATA\QA_AlignmentManager_0012e_Event_38600_primary.mdf',  MOVE N'QA_AlignmentManager_0012e_Event_38600_log' TO N'n:\Program Files\Microsoft SQL Server\MSSQL12.MSSQLSERVER\MSSQL\Data\QA_AlignmentManager_0012e_Event_38600_log.ldf',  NOUNLOAD,  REPLACE,  STATS = 5
ALTER DATABASE [QA_AlignmentManager_0012e_Event_38600] SET MULTI_USER

USE [QA_AlignmentManager_0012e_Event_38600]
GO
ALTER INDEX [PK__Territor__676633B607220AB2] ON [dbo].[TerritoryShapes] REBUILD PARTITION = ALL
GO
ALTER INDEX [pk_TerritoryShapeOverrides] ON [dbo].[TerritoryShapeOverrides] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
GO
