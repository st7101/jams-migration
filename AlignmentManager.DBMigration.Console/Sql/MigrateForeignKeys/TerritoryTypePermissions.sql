
PRINT N'Updating TerritoryTypePermissions ...';

UPDATE ttp
SET TerritoryType_Id=tt.TerritoryType_Id
FROM dbo.TerritoryTypePermissions ttp INNER JOIN 
dbo.TerritoryTypes tt ON tt.TerritoryType_ExternalID = ttp.TerritoryType_ExternalID

