SELECT DISTINCT OUAE.AlignmentEntity_id
	FROM OrgUnitAlignmentEntitys OUAE
	INNER JOIN AlignmentEntitys AE ON OUAE.AlignmentEntity_id = AE.AlignmentEntity_Id
	INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
	AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND OUAE.TerritoryType_Id = AETTT.TerritoryType_id
	WHERE OUAE.OrgUnitAlignmentEntity_Weight > 100
	GROUP BY OUAE.AlignmentEntity_id, OUAE.TerritoryType_Id