
PRINT N'Starting rebuilding table [dbo].[PositionOverrides]...';
CREATE TABLE [dbo].[tmp_ms_xx_PositionOverrides] (
    [PositionOverride_Id]                      BIGINT         NOT NULL,
    [PositionOverride_RefinementOperationType] NVARCHAR (100) NULL,
    [PositionOverride_LegacyId]                NVARCHAR (255) NULL,
    [PositionOverride_ExternalOrgUnitId]               NVARCHAR (255) NULL,
    [PositionOverride_OrgUnitId]        BIGINT         NULL,
    [PositionOverride_PositionExternalId]              NVARCHAR (255) NULL,
    [PositionOverride_PositionId]       BIGINT         NULL,
    [PositionOverride_PositionEffort]          DECIMAL (5, 2) CONSTRAINT DF_tmp_ms_xx_PositionOverrides_PositionOverride_PositionEffort DEFAULT (100) NULL,
    [Request_Id]                               BIGINT         NULL,
    [Role_Id]                                  NVARCHAR (100) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_PositionOverrides] PRIMARY KEY CLUSTERED ([PositionOverride_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[PositionOverrides])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_PositionOverrides] WITH (TABLOCK) ([PositionOverride_Id], [PositionOverride_RefinementOperationType], [PositionOverride_LegacyId], [PositionOverride_ExternalOrgUnitId], [PositionOverride_PositionExternalId], [PositionOverride_PositionEffort], [Request_Id], [Role_Id])
        SELECT   [PositionOverride_Id],
                 [PositionOverride_RefinementOperationType],
                 [PositionOverride_LegacyId],
                 [PositionOverride_OrgUnitId],
                 [PositionOverride_PositionId],
                 [PositionOverride_PositionEffort],
                 [Request_Id],
                 [Role_Id]
        FROM     [dbo].[PositionOverrides]
        ORDER BY [PositionOverride_Id] ASC;
    END

DROP TABLE [dbo].[PositionOverrides];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PositionOverrides]', N'PositionOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_PositionOverrides]', N'PK_PositionOverrides', N'OBJECT';
EXECUTE sp_rename N'[dbo].[DF_tmp_ms_xx_PositionOverrides_PositionOverride_PositionEffort]', N'DF_PositionOverrides_PositionOverride_PositionEffort', N'OBJECT';

