update AlignmentEntitys
Set AlignmentEntity_Latitude = :latitude, AlignmentEntity_Longitude = :longitude
Where AlignmentEntityType_Id = :geoTypeId and AlignmentEntity_Id Not in 
(Select GeographyMapping_GeoId From GeographyMappings)