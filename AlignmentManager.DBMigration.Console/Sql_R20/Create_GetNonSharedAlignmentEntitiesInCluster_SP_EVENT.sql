
-- =============================================
-- Author@		Steve Tetrault
-- Create date@ Jun. 17, 2014
-- Description@	Modified Logic for GetAlignmentEntityClustersOfTypeInBoundingBox SP
-- =============================================
CREATE PROCEDURE [dbo].GetNonSharedAlignmentEntitiesInCluster
	-- Add the parameters for the stored procedure here
    
	@minLat FLOAT, @maxLat FLOAT, @minLng FLOAT, @maxLng FLOAT,
    @territoryTypeId BIGINT ,
    @aeStatus NVARCHAR(255) = NULL ,
    @IsGeo BIT = 1 ,
    @OrgUnitId BIGINT
AS
    BEGIN

        DECLARE @sql NVARCHAR(MAX)= '

	SELECT oae.AlignmentEntity_id as Id,
	  Min(ae.AlignmentEntity_Name)      AS Name,
	   Min(ae.AlignmentEntity_Latitude)  AS Latitude,
	   Min(ae.AlignmentEntity_Longitude) AS Longitude
	FROM  [OrgUnitAlignmentEntitys] oae
	inner join AlignmentEntitys ae
	ON oae.TerritoryType_Id = '+CAST(@territoryTypeId AS VARCHAR)+' AND ae.AlignmentEntity_Id = oae.AlignmentEntity_id
	where AlignmentEntity_Latitude is not null and ae.AlignmentEntity_Latitude <> 0
	 and ae.AlignmentEntity_Longitude <> 0 and ae.AlignmentEntity_Longitude Is NOT NULL
	and ae.AlignmentEntity_Longitude between '+CAST(@minLng AS VARCHAR)+' and '+CAST(@maxLng AS VARCHAR)+'
	and ae.AlignmentEntity_Latitude between '+CAST(@minLat AS VARCHAR)+' and '+CAST(@maxLat AS VARCHAR)+CHAR(13)
	IF (@isgeo =1)
	BEGIN
	SET @sql=@sql+'and (ae.AlignmentEntityType_Id = 1)'
	END ELSE BEGIN
	SET @sql=@sql+'and (ae.AlignmentEntityType_Id <> 1)'
	END
	IF ( @aeStatus IS NOT NULL
             AND @aeStatus != ''
           )
    BEGIN
       SET @sql = @sql + ' and	ae.AlignmentEntity_Status = '''
                    + @aeStatus + ''''
    END
	SET @sql=@sql+'

GROUP BY oae.AlignmentEntity_Id
HAVING COUNT(oae.AlignmentEntity_id) = 1 
	and Min(oae.OrgUnit_id) = '+CAST(@OrgUnitId AS VARCHAR)

        EXEC (@sql)
    END

