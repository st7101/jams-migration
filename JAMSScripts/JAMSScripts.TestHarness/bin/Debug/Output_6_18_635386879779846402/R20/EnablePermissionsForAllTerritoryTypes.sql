Delete from ApprovalRoundTerritoryTypePermissions
Delete from TerritoryTypePermissions

Declare @PkConstraint nvarchar(255)=''
Declare @FkConstraint nvarchar(255)=''
Declare @sql nvarchar(max)=''


--Delete Foreign Keys
SELECT @sql = @sql+N'Alter Table ApprovalRoundTerritoryTypePermissions
Drop CONSTRAINT '+ CONSTRAINT_NAME+CHAR(13) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'FOREIGN KEY' AND TABLE_NAME = 'ApprovalRoundTerritoryTypePermissions' 
EXEC (@sql)

--Delete PrimaryKey
SELECT @PkConstraint = (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_NAME = 'TerritoryTypePermissions')
Set @sql = N'Alter Table TerritoryTypePermissions
Drop CONSTRAINT ' + @PkConstraint
EXEC (@sql)


Alter Table TerritoryTypePermissions
Add TempTerritoryTypePermission_Id bigint not null identity(1,1)


Insert Into TerritoryTypePermissions
(TerritoryType_id, Permission_id, TerritoryTypePermission_IsEnabled, TerritoryTypePermission_Id)
(select tt.TerritoryType_id, p.Permission_Id, 1 as TerritoryTypePermission_IsEnabled, 0 as TerritoryTypePermission_Id from TerritoryTypes tt, Permissions p
Where p.Permission_PermissionLevel = 'TerritoryTypeLevel')


SET @sql = N'
Update TerritoryTypePermissions
Set TerritoryTypePermission_Id = TempTerritoryTypePermission_Id'
EXEC (@sql)

Alter Table TerritoryTypePermissions
drop column TempTerritoryTypePermission_Id

ALTER TABLE [dbo].[TerritoryTypePermissions] ADD  CONSTRAINT [PK_TerritoryTypePermissions] PRIMARY KEY CLUSTERED 
([TerritoryTypePermission_Id] ASC) WITH (DATA_COMPRESSION=PAGE)
Alter Table [ApprovalRoundTerritoryTypePermissions] WITH CHECK ADD  CONSTRAINT FK_ApprovalRoundTerritoryTypePermissions_TerritoryTypePermissions FOREIGN KEY([TerritoryTypePermission_id]) REFERENCES [dbo].[TerritoryTypePermissions] ([TerritoryTypePermission_id])
ALTER TABLE [dbo].[ApprovalRoundTerritoryTypePermissions] CHECK CONSTRAINT FK_ApprovalRoundTerritoryTypePermissions_TerritoryTypePermissions

Alter Table [ApprovalRoundTerritoryTypePermissions] WITH CHECK ADD  CONSTRAINT FK_ApprovalRoundTerritoryTypePermissions_ApprovalRounds FOREIGN KEY([ApprovalRound_id]) REFERENCES [dbo].[ApprovalRounds] ([ApprovalRound_id])
ALTER TABLE [dbo].[ApprovalRoundTerritoryTypePermissions] CHECK CONSTRAINT FK_ApprovalRoundTerritoryTypePermissions_ApprovalRounds
