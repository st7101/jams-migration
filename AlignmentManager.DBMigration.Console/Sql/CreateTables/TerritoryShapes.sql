
PRINT N'Starting rebuilding table [dbo].[TerritoryShapes]...';


CREATE TABLE [dbo].[tmp_ms_xx_TerritoryShapes]
    (
      [OrgUnit_Id] BIGINT NOT NULL ,
      [OrgUnit_ExternalId] NVARCHAR(255) NOT NULL ,
      [geom] [sys].[GEOMETRY] NULL ,
      [geom_low] [sys].[GEOMETRY] NULL ,
      [geom_medium] [sys].[GEOMETRY] NULL ,
      [MinLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STX, ( 0 )) )
        PERSISTED ,
      [MinLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STY, ( 0 )) )
        PERSISTED ,
      [MaxLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STX, ( 0 )) )
        PERSISTED ,
      [MaxLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STY, ( 0 )) )
        PERSISTED ,
      CONSTRAINT [tmp_ms_xx_constraint_pk_TerritoryShapes] PRIMARY KEY CLUSTERED
        ( [OrgUnit_Id] ASC )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[TerritoryShapes] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_TerritoryShapes] WITH ( TABLOCK )
                ( [OrgUnit_Id] ,
                  [OrgUnit_ExternalId] ,
                  [geom],geom_low,geom_medium
                )
                SELECT  ROW_NUMBER() OVER ( ORDER BY OrgUnit_Id ) ,
                        [OrgUnit_Id] ,
                        [geom],geom_Low,geom_medium
                FROM    [dbo].[TerritoryShapes]
                ORDER BY [OrgUnit_Id] ASC;
    END
DROP TABLE [dbo].[TerritoryShapes];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_TerritoryShapes]', N'TerritoryShapes';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_pk_TerritoryShapes]',
    N'PK_TerritoryShapes', N'OBJECT';
