
PRINT N'Starting rebuilding table [dbo].[ProductPositions]...';
CREATE TABLE [dbo].[tmp_ms_xx_ProductPositions] (
    [ProductPosition_Id]     BIGINT         NOT NULL,
    [ProductPosition_Weight] DECIMAL (5, 2) NOT NULL,
    [Position_ExternalId]            NVARCHAR (100) NOT NULL,
    [Position_Id]     BIGINT         NULL,
    [Product_Id]             NVARCHAR (100) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_ProductPositions] PRIMARY KEY CLUSTERED ([ProductPosition_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ProductPositions])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_ProductPositions] WITH (TABLOCK) ([ProductPosition_Id], [ProductPosition_Weight], [Position_ExternalId], [Product_Id])
        SELECT   [ProductPosition_Id],
                 [ProductPosition_Weight],
                 [Position_Id],
                 [Product_Id]
        FROM     [dbo].[ProductPositions]
        ORDER BY [ProductPosition_Id] ASC;
    END

DROP TABLE [dbo].[ProductPositions];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ProductPositions]', N'ProductPositions';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ProductPositions]', N'PK_ProductPositions', N'OBJECT';


