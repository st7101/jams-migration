/*
 * JAMS Create Database Schema
 * Version: 0.1
 * Created: July 23, 2014
 * Created By: Steve Tetrault
 * :DBName - the name of the Database to run the script on
 */


 --USE [:DBName]

 DECLARE @SQL VARCHAR(MAX)= ''

/*
 * Step 1: drop OAESuperView
 */
 
PRINT N'Dropping OAESuperView...';
IF EXISTS ( SELECT TOP 1
                    1
            FROM    sys.views
            WHERE   name = 'OAESuperView' )
    BEGIN
        DROP VIEW [dbo].[OAESuperView]
    END




/*
 * Step 2: drop Unique Constraints
 */
PRINT N'Dropping Unique Constraints...';
SELECT  @sql = @sql + 'ALTER TABLE ' + OBJECT_NAME(so.parent_obj)
        + ' DROP CONSTRAINT ' + QUOTENAME(so.name) + ';' + CHAR(13)
FROM    sysobjects so
WHERE   so.xtype = 'UQ' 

        EXEC (@sql)

/*
 * Step 3: drop default constraints
 */
SET @SQL = ''
PRINT N'Dropping Default Constraints...';
SELECT  @sql = @sql + 'ALTER TABLE ' + OBJECT_NAME(dc.parent_object_id)
        + ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + ';' + CHAR(13)
FROM    sys.default_constraints dc

        EXEC (@sql)

/*
 * Step 4: drop Indexes
 */
PRINT N'Dropping Indexes...';
SET @SQL = ''
SELECT  @sql = @sql + 'DROP INDEX ' + QUOTENAME(name) + ' ON '
        + QUOTENAME(OBJECT_SCHEMA_NAME([object_id])) + '.'
        + QUOTENAME(OBJECT_NAME([object_id])) + ';
'
FROM    sys.indexes
WHERE   index_id > 1
        AND OBJECTPROPERTY([object_id], 'IsMsShipped') = 0

ORDER BY [object_id] ,
        index_id DESC;

        --EXEC (@sql)


/*
 * Step 5: drop foreign keys
 */
PRINT N'Dropping Foreign Keys...';
SET @SQL = ''
SELECT  @SQL = @SQL + 'ALTER TABLE ' + FK.TABLE_NAME + ' DROP CONSTRAINT ['
        + RTRIM(C.CONSTRAINT_NAME) + '];' + CHAR(13)
FROM    INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C
        INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME
        INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME
        INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME
        INNER JOIN ( SELECT i1.TABLE_NAME ,
                            i2.COLUMN_NAME
                     FROM   INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1
                            INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2 ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME
                     WHERE  i1.CONSTRAINT_TYPE = 'PRIMARY KEY'
                   ) PT ON PT.TABLE_NAME = PK.TABLE_NAME
        EXEC (@sql)

/*
 * Step 6: drop all tables
 */
 SET @sql = ''
 PRINT N'Dropping All Tables...';
 SELECT  @SQL = @SQL + 'DROP TABLE ' + t.name + CHAR(13)
 FROM sys.tables t WHERE type='U' AND name NOT LIKE '%[_]%'
 EXEC (@sql)

/*
 * Step 7: Create partition objects for tables
 */
 
PRINT N'Creating partition functions'
GO
CREATE PARTITION FUNCTION [PF_AENAC] ([INT]) 
AS RANGE LEFT 
FOR VALUES (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499)
GO
CREATE PARTITION FUNCTION [PF_TT] ([BIGINT]) 
AS RANGE LEFT 
FOR VALUES (1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 103, 104, 105, 106, 107, 108, 109, 110, 111, 112, 113, 114, 115, 116, 117, 118, 119, 120, 121, 122, 123, 124, 125, 126, 127, 128, 129, 130, 131, 132, 133, 134, 135, 136, 137, 138, 139, 140, 141, 142, 143, 144, 145, 146, 147, 148, 149, 150, 151, 152, 153, 154, 155, 156, 157, 158, 159, 160, 161, 162, 163, 164, 165, 166, 167, 168, 169, 170, 171, 172, 173, 174, 175, 176, 177, 178, 179, 180, 181, 182, 183, 184, 185, 186, 187, 188, 189, 190, 191, 192, 193, 194, 195, 196, 197, 198, 199, 200, 201, 202, 203, 204, 205, 206, 207, 208, 209, 210, 211, 212, 213, 214, 215, 216, 217, 218, 219, 220, 221, 222, 223, 224, 225, 226, 227, 228, 229, 230, 231, 232, 233, 234, 235, 236, 237, 238, 239, 240, 241, 242, 243, 244, 245, 246, 247, 248, 249, 250, 251, 252, 253, 254, 255, 256, 257, 258, 259, 260, 261, 262, 263, 264, 265, 266, 267, 268, 269, 270, 271, 272, 273, 274, 275, 276, 277, 278, 279, 280, 281, 282, 283, 284, 285, 286, 287, 288, 289, 290, 291, 292, 293, 294, 295, 296, 297, 298, 299, 300, 301, 302, 303, 304, 305, 306, 307, 308, 309, 310, 311, 312, 313, 314, 315, 316, 317, 318, 319, 320, 321, 322, 323, 324, 325, 326, 327, 328, 329, 330, 331, 332, 333, 334, 335, 336, 337, 338, 339, 340, 341, 342, 343, 344, 345, 346, 347, 348, 349, 350, 351, 352, 353, 354, 355, 356, 357, 358, 359, 360, 361, 362, 363, 364, 365, 366, 367, 368, 369, 370, 371, 372, 373, 374, 375, 376, 377, 378, 379, 380, 381, 382, 383, 384, 385, 386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399, 400, 401, 402, 403, 404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417, 418, 419, 420, 421, 422, 423, 424, 425, 426, 427, 428, 429, 430, 431, 432, 433, 434, 435, 436, 437, 438, 439, 440, 441, 442, 443, 444, 445, 446, 447, 448, 449, 450, 451, 452, 453, 454, 455, 456, 457, 458, 459, 460, 461, 462, 463, 464, 465, 466, 467, 468, 469, 470, 471, 472, 473, 474, 475, 476, 477, 478, 479, 480, 481, 482, 483, 484, 485, 486, 487, 488, 489, 490, 491, 492, 493, 494, 495, 496, 497, 498, 499)
GO


PRINT N'Creating partition schemes'
GO
CREATE PARTITION SCHEME [PS_AENAC] 
AS PARTITION [PF_AENAC] 
TO ([PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY])
GO
CREATE PARTITION SCHEME [PS_TT] 
AS PARTITION [PF_TT] 
TO ([PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY], [PRIMARY])
GO

/*
 * Step 3: Update required functions
 */

PRINT N'Altering [dbo].[ConvertLongitudeToTileX]'
GO
IF EXISTS (SELECT TOP 1 1 FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA='dbo' AND specific_name = 'ConvertLongitudeToTileX' AND ROUTINE_TYPE = 'FUNCTION' )
BEGIN
DROP FUNCTION dbo.ConvertLongitudeToTileX
END
GO
CREATE FUNCTION [dbo].[ConvertLongitudeToTileX]
    (
      @longitude FLOAT ,
      @zoom INT
    )
RETURNS INT
    WITH SCHEMABINDING
AS
    BEGIN
	-- Declare the return variable here
        DECLARE @Result INT
        DECLARE @zoomFactor FLOAT
        SET @zoomFactor = POWER(2, @zoom)

	-- Add the T-SQL statements to compute the return value here
        SELECT  @Result = FLOOR(( RADIANS(@longitude) + PI() ) / ( 2 * PI() )
                                * @zoomFactor)

	-- Return the result of the function
        RETURN @Result

    END
GO
PRINT N'Altering [dbo].[ConvertLatitudeToTileY]'
GO

IF EXISTS (SELECT TOP 1 1 FROM INFORMATION_SCHEMA.ROUTINES
WHERE SPECIFIC_SCHEMA='dbo' AND specific_name = 'ConvertLatitudeToTileY' AND ROUTINE_TYPE = 'FUNCTION' )
BEGIN
DROP FUNCTION dbo.ConvertLatitudeToTileY
END
GO
CREATE FUNCTION [dbo].[ConvertLatitudeToTileY]
    (
      @latitude FLOAT ,
      @zoom INT
    )
RETURNS INT
    WITH SCHEMABINDING
AS
    BEGIN
        DECLARE @Result INT
        DECLARE @zoomFactor FLOAT
        SET @zoomFactor = POWER(2, @zoom)

        SELECT  @Result = FLOOR(( LOG(TAN(RADIANS(@latitude)) + ( 1
                                                              / COS(RADIANS(@latitude)) ))
                                  / -PI() + 1 ) / 2 * @zoomFactor)
	
        RETURN @Result

    END
GO

/*
 * Step 4: Create Final table schema
 */

PRINT N'Creating [dbo].[OrgUnitLevels]'
GO
CREATE TABLE [dbo].[OrgUnitLevels]
    (
      [OrgUnitLevel_Id] [BIGINT] NOT NULL ,
      [OrgUnitLevel_ExternalId] [NVARCHAR](100) NOT NULL ,
      [OrgUnitLevel_Name] [NVARCHAR](255) NULL ,
      [UsesOrgUnitLegacyId] [BIT] NULL ,
      [UsesPositionLegacyId] [BIT] NULL ,
      [OrgUnitLevel_IsLeafLevel] [BIT] NULL ,
      [OrgUnitLevel_ParentExternalId] [NVARCHAR](100) NULL ,
      [OrgUnitLevel_ParentId] [BIGINT] NULL ,
      [OrgUnitLevel_TreeLeft] [INT] NULL ,
      [OrgUnitLevel_TreeRight] [INT] NULL ,
      CONSTRAINT [PK_OrgUnitLevels] PRIMARY KEY CLUSTERED
        ( [OrgUnitLevel_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    ) 

GO
PRINT N'Creating [dbo].[AlignmentEntitys]'
GO
/*Base table structure */
DECLARE @CreateTable VARCHAR(MAX) ,
    @InsertTable VARCHAR(MAX)
DECLARE @GetIDs VARCHAR(MAX)



SET @CreateTable = '
CREATE TABLE [AlignmentEntitys] (
	[AlignmentEntity_Id] bigint not null,
	[AlignmentEntity_ExternalId] [nvarchar] (100) NOT NULL,
	[AlignmentEntity_Name] [nvarchar](255) NULL,
	[AlignmentEntity_Status] [nvarchar](255) NULL,
	[AlignmentEntity_Latitude] [float] NULL,
	[AlignmentEntity_Longitude] [float] NULL,
	[AlignmentEntity_ParentExternalId] [nvarchar](100) NULL,
	[AlignmentEntity_ParentId] [bigint] NULL,
	[AlignmentEntityType_Id] [bigint] NOT NULL,
	[TileX_20] AS ([dbo].[ConvertLongitudeToTileX] (AlignmentEntity_Longitude,20)) PERSISTED,
	[TileY_20] AS ([dbo].[ConvertLatitudeToTileY] (AlignmentEntity_Latitude,20)) PERSISTED,
 ' + CHAR(13)
 /*add metric columns*/
SELECT  @CreateTable = @CreateTable + QUOTENAME(clmns.name) + ' '
        + 'NUMERIC (' + CAST(clmns.PRECISION AS VARCHAR) + ','
        + CAST(clmns.SCALE AS VARCHAR) + ')' + ' ' + CASE clmns.is_nullable
                                                       WHEN 1 THEN 'NULL'
                                                       ELSE 'NOT NULL'
                                                     END + ', ' + CHAR(13)
FROM    sys.tables AS tbl
INNER JOIN sys.all_columns AS clmns ON clmns.object_Id = tbl.object_Id
LEFT OUTER JOIN sys.types AS baset ON ( baset.user_type_Id = clmns.system_type_Id
                                        AND baset.user_type_Id = baset.system_type_Id
                                      )
                                      OR ( ( baset.system_type_Id = clmns.system_type_Id )
                                           AND ( baset.user_type_Id = clmns.user_type_Id )
                                           AND ( baset.is_user_defined = 0 )
                                           AND ( baset.is_assembly_type = 1 )
                                         )
LEFT OUTER JOIN sys.types AS usrt ON usrt.user_type_Id = clmns.user_type_Id
LEFT OUTER JOIN sys.objects AS d ON d.object_Id = clmns.default_object_Id
LEFT OUTER JOIN sys.objects AS r ON r.object_Id = clmns.rule_object_Id
WHERE   ( tbl.name = 'AlignmentEntitys'
          AND SCHEMA_NAME(tbl.schema_Id) = 'dbo'
          AND baset.NAME = 'numeric'
        )
ORDER BY clmns.column_Id ASC

/*add attribute columns*/
SELECT  @CreateTable = @CreateTable + QUOTENAME(clmns.name) + ' '
        + 'NVARCHAR (' + CAST(clmns.max_length / 2 AS VARCHAR) + ')' + ' '
        + CASE clmns.is_nullable
            WHEN 1 THEN 'NULL'
            ELSE 'NOT NULL'
          END + ', ' + CHAR(13)
FROM    sys.tables AS tbl
INNER JOIN sys.all_columns AS clmns ON clmns.object_Id = tbl.object_Id
LEFT OUTER JOIN sys.types AS baset ON ( baset.user_type_Id = clmns.system_type_Id
                                        AND baset.user_type_Id = baset.system_type_Id
                                      )
                                      OR ( ( baset.system_type_Id = clmns.system_type_Id )
                                           AND ( baset.user_type_Id = clmns.user_type_Id )
                                           AND ( baset.is_user_defined = 0 )
                                           AND ( baset.is_assembly_type = 1 )
                                         )
LEFT OUTER JOIN sys.types AS usrt ON usrt.user_type_Id = clmns.user_type_Id
LEFT OUTER JOIN sys.objects AS d ON d.object_Id = clmns.default_object_Id
LEFT OUTER JOIN sys.objects AS r ON r.object_Id = clmns.rule_object_Id
WHERE   ( tbl.name = 'AlignmentEntitys'
          AND SCHEMA_NAME(tbl.schema_Id) = 'dbo'
          AND baset.NAME = 'nvarchar'
          AND clmns.max_length = 4000
        )
ORDER BY clmns.column_Id ASC
SET @CreateTable = @CreateTable
    + '
CONSTRAINT [PK_AlignmentEntitys] PRIMARY KEY CLUSTERED ([AlignmentEntity_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);'

EXEC (@CreateTable)
GO
PRINT N'Creating [dbo].[OrgUnitLevelRoles]'
GO
CREATE TABLE [dbo].[OrgUnitLevelRoles]
    (
      [OrgUnitLevelRole_Id] [BIGINT] NOT NULL ,
      [OrgUnitLevelRole_MaxPosition] [INT] NULL ,
      [OrgUnitLevelRole_MinPosition] [INT] NULL ,
      [UsesEffort] [BIT]
        NOT NULL
        CONSTRAINT [DF_OrgUnitLevelRoles_UsesEffort] DEFAULT ( (0) ) ,
      [OrgUnitLevel_Id] [BIGINT] NULL ,
      [OrgUnitLevel_ExternalId] [NVARCHAR](100) NOT NULL ,
      [Role_Id] [NVARCHAR](100) NOT NULL ,
      CONSTRAINT [PK_OrgUnitLevelRoles] PRIMARY KEY CLUSTERED
        ( [OrgUnitLevelRole_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    ) 

GO
PRINT N'Creating [dbo].[PositionOverrides]'
GO
CREATE TABLE [dbo].[PositionOverrides]
    (
      [PositionOverride_Id] [BIGINT] NOT NULL ,
      [PositionOverride_RefinementOperationType] [NVARCHAR](100) NULL ,
      [PositionOverride_LegacyId] [NVARCHAR](255) NULL ,
      [PositionOverride_ExternalOrgUnitId] [NVARCHAR](255) NULL ,
      [PositionOverride_OrgUnitId] [BIGINT] NULL ,
      [PositionOverride_ExternalPositionId] [NVARCHAR](255) NULL ,
      [PositionOverride_PositionId] [BIGINT] NULL ,
      [PositionOverride_PositionEffort] [DECIMAL](5, 2)
        NOT NULL
        CONSTRAINT [DF_PositionOverrides_PositionOverride_PositionEffort]
        DEFAULT ( (100) ) ,
      [Request_Id] [BIGINT] NOT NULL ,
      [Role_Id] [NVARCHAR](100) NULL ,
      CONSTRAINT [PK_PositionOverrides] PRIMARY KEY CLUSTERED
        ( [PositionOverride_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    ) 

GO
PRINT N'Creating [dbo].[TerritoryTypes]'
GO
CREATE TABLE [dbo].[TerritoryTypes]
    (
      [TerritoryType_ExternalId] [NVARCHAR](100) NOT NULL ,
      [TerritoryType_Id] [BIGINT] NOT NULL ,
      [TerritoryType_Name] [NVARCHAR](255) NULL ,
      [TerritoryType_AreAllAttributesVisible] [BIT] NULL ,
      [OrgUnitLevel_ExternalId] [NVARCHAR](100) NULL ,
      [OrgUnitLevel_Id] [BIGINT] NULL ,
      [UnassignedOrgUnit_Id] [BIGINT] NULL ,
      [UnassignedOrgUnit_ExternalId] [NVARCHAR](100) NULL ,
      [TerritoryType_SingleAssignmentWeightEnabled] [BIT]
        NOT NULL
        CONSTRAINT [DF_TerritoryTypes_TerritoryType_SingleAssignmentWeightEnabled]
        DEFAULT ( (0) ) ,
      [TerritoryType_ZeroWeightToleranceEnabled] [BIT]
        NOT NULL
        CONSTRAINT [DF_TerritoryTypes_ZeroWeightToleranceEnabled]
        DEFAULT ( (0) ) ,
      [TerritoryType_WeightSumNotEqualTo100Enabled] [BIT]
        NOT NULL
        CONSTRAINT [DF_TerritoryTypes_WeightSumNotEqualTo100Enabled]
        DEFAULT ( (0) ) ,
      CONSTRAINT [PK_TerritoryTypes] PRIMARY KEY CLUSTERED
        ( [TerritoryType_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    ) 
GO
PRINT N'Creating [dbo].[OrgUnits]'
GO
CREATE TABLE [dbo].[OrgUnits]
    (
      [OrgUnit_Id] [BIGINT] NOT NULL ,
      [OrgUnit_ExternalId] [NVARCHAR](100) NOT NULL ,
      [OrgUnit_Name] [NVARCHAR](255) NULL ,
      [OrgUnit_LegacyId] [NVARCHAR](255) NULL ,
      [OrgUnit_IsUnassigned] [BIT] NULL ,
      [OrgUnit_TreeLeft] [INT] NULL ,
      [OrgUnit_TreeRight] [INT] NULL ,
      [OrgUnitLevel_Id] [BIGINT] NULL ,
      [OrgUnitLevel_ExternalId] [NVARCHAR](100) NULL ,
      [OrgUnit_ParentExternalId] [NVARCHAR](100) NULL ,
      [OrgUnit_ParentId] [BIGINT] NULL ,
      [OrgUnit_ColorId] [TINYINT]
        NOT NULL
        CONSTRAINT [DF__tmp_ms_xx__OrgUnits] DEFAULT ( (0) ) ,
      CONSTRAINT [PK_OrgUnits] PRIMARY KEY CLUSTERED ( [OrgUnit_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    )
    
 

GO
PRINT N'Creating [dbo].[Positions]'
GO
CREATE TABLE [dbo].[Positions]
    (
      [Position_ExternalId] [NVARCHAR](100) NOT NULL ,
      [Position_Id] [BIGINT] NOT NULL ,
      [Position_LegacyId] [NVARCHAR](255) NULL ,
      [Position_Effort] [DECIMAL](5, 2)
        NOT NULL
        CONSTRAINT [DF_tmp_ms_xx_Positions_Position_Effort] DEFAULT ( (100) ) ,
      [OrgUnit_Id] [BIGINT] NULL ,
      [OrgUnit_ExternalId] [NVARCHAR](100) NULL ,
      [Role_Id] [NVARCHAR](100) NULL ,
      CONSTRAINT [PK_Positions] PRIMARY KEY CLUSTERED ( [Position_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    )
    
 

GO
PRINT N'Creating [dbo].[AlignmentEntityTypeTerritoryTypes]'
GO
CREATE TABLE [dbo].[AlignmentEntityTypeTerritoryTypes]
    (
      [AlignmentEntityTypeTerritoryType_Id] [BIGINT] NOT NULL ,
      [ToleratePartialAlignment] [BIT]
        NOT NULL
        CONSTRAINT [DF_AlignmentEntityTypeTerritoryTypes_ToleratePartialAlignment]
        DEFAULT ( (0) ) ,
      [AlignmentEntityType_Id] [BIGINT] NOT NULL ,
      [AlignmentEntityType_ParentId] [BIGINT] NULL ,
      [TerritoryType_ExternalId] [NVARCHAR](100) NULL ,
      [TerritoryType_Id] [BIGINT] NOT NULL ,
      CONSTRAINT [PK_AlignmentEntityTypeTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [AlignmentEntityTypeTerritoryType_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    )
    
 

GO
PRINT N'Creating [dbo].[AlignmentEntityNumberAttributeCache]'
GO
CREATE TABLE [dbo].[AlignmentEntityNumberAttributeCache]
    (
      [AlignmentEntityNumberAttributeCache_Id] [BIGINT] NOT NULL ,
      [AlignmentEntityNumberAttributeCache_Value] [FLOAT] NULL ,
      [AlignmentEntity_ExternalId] [NVARCHAR](100) NULL ,
      [AlignmentEntity_Id] [BIGINT] NULL ,
      [AttributeType_Id] [INT] NOT NULL ,
      [TerritoryType_ExternalId] [NVARCHAR](100) NULL ,
      [TerritoryType_Id] [BIGINT] NULL ,
      CONSTRAINT [PK_AlignmentEntityNumberAttributeCache] PRIMARY KEY CLUSTERED
        ( [AttributeType_Id], [AlignmentEntityNumberAttributeCache_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    )
ON  PS_AENAC([AttributeType_Id]);

GO
PRINT N'Creating [dbo].[AttributeTypes]'
GO
CREATE TABLE [dbo].[AttributeTypes]
    (
      [AttributeType_Id] [INT] NOT NULL ,
      [AttributeType_Name] [NVARCHAR](255) NULL ,
      [AttributeType_DisplayName] [NVARCHAR](255) NULL ,
      [AttributeType_DataType] [NVARCHAR](100) NOT NULL ,
      [AttributeType_Category] [NVARCHAR](100) NULL ,
      [AttributeType_Description] [NVARCHAR](255) NULL ,
      [AttributeType_Formula] [NVARCHAR](255) NULL ,
      [AttributeType_Entity] [NVARCHAR](255) NULL ,
      [AttributeType_Status] [NVARCHAR](100) NOT NULL ,
      [AttributeType_ValidationType] [NVARCHAR](100) NULL ,
      [AttributeType_MetricAvailable] [BIT] NULL ,
      CONSTRAINT [PK_AttributeTypes] PRIMARY KEY CLUSTERED
        ( AttributeType_Id ASC ) WITH ( DATA_COMPRESSION = PAGE )
    )
    
 

GO
PRINT N'Creating [dbo].[AlignmentStates]'
GO
CREATE TABLE [dbo].[AlignmentStates]
    (
      [AlignmentState_Id] [BIGINT] NOT NULL ,
      [AlignmentState_IsGrouped] [BIT] NULL ,
      CONSTRAINT [PK_AlignmentStates] PRIMARY KEY CLUSTERED
        ( [AlignmentState_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    )
    
 


GO
PRINT N'Creating [dbo].[AlignmentEntityOverrides]'
GO
CREATE TABLE [dbo].[AlignmentEntityOverrides]
    (
      [AlignmentEntityOverride_Id] [BIGINT] NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [AlignmentEntity_ExternalId] [NVARCHAR](100) NULL ,
      [AlignmentEntity_Id] [BIGINT] NOT NULL ,
      [TerritoryType_ExternalId] [NVARCHAR](100) NULL ,
      [TerritoryType_Id] [BIGINT] NULL ,
      [NewAlignmentState_Id] [BIGINT] NULL ,
      [OldAlignmentState_Id] [BIGINT] NULL ,
      CONSTRAINT [PK_AlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( [AlignmentEntityOverride_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    )
    
 

GO
PRINT N'Creating [dbo].[Requests]'
GO
CREATE TABLE [dbo].[Requests]
    (
      [Request_Id] [BIGINT] NOT NULL ,
      [Request_CreateDate] [DATETIMEOFFSET] NULL ,
      [Request_IsSuccessful] [BIT] NULL ,
      [ApprovalProgressTracker_id] [BIGINT] NULL ,
      CONSTRAINT [PK_Requests] PRIMARY KEY CLUSTERED ( [Request_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    )
    
 

GO
PRINT N'Creating [dbo].[AlignmentEntityTypes]'
GO
CREATE TABLE [dbo].[AlignmentEntityTypes]
    (
      [AlignmentEntityType_Id] [BIGINT] NOT NULL ,
      [AlignmentEntityType_Name] [NVARCHAR](255) NULL ,
      CONSTRAINT [PK_AlignmentEntityTypes] PRIMARY KEY CLUSTERED
        ( [AlignmentEntityType_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    )
    
 

GO
PRINT N'Creating [dbo].[AlignmentEntityTextAttributeCache]'
GO
CREATE TABLE [dbo].[AlignmentEntityTextAttributeCache]
    (
      [AlignmentEntityTextAttributeCache_Id] [BIGINT] NOT NULL ,
      [AlignmentEntityTextAttributeCache_Value] [NVARCHAR](2000) NULL ,
      [AlignmentEntity_Id] [BIGINT] NULL ,
      [AlignmentEntity_ExternalId] [NVARCHAR](100) NULL ,
      [AttributeType_Id] [INT] NOT NULL ,
      CONSTRAINT [PK_AlignmentEntityTextAttributeCache] PRIMARY KEY CLUSTERED
        ( [AttributeType_Id], [AlignmentEntityTextAttributeCache_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    )
ON  PS_AENAC([AttributeType_Id]);

    
 

GO
PRINT N'Creating [dbo].[AlignmentStateEntrys]'
GO
CREATE TABLE [dbo].[AlignmentStateEntrys]
    (
      [AlignmentStateEntry_Id] [BIGINT] NOT NULL ,
      [AlignmentStateEntry_OrgUnitId] [BIGINT] NULL ,
      [AlignmentStateEntry_ExternalOrgUnitId] [NVARCHAR](100) NULL ,
      [AlignmentStateEntry_Weight] [DECIMAL](5, 2) NOT NULL ,
      [AlignmentState_Id] [BIGINT] NOT NULL ,
      CONSTRAINT [PK_AlignmentStateEntrys] PRIMARY KEY CLUSTERED
        ( [AlignmentStateEntry_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    )
    
 

GO
PRINT N'Creating [dbo].[ApprovalProgress]'
GO
CREATE TABLE [dbo].[ApprovalProgress]
    (
      [ApprovalProgress_Id] [BIGINT] NOT NULL ,
      [ApprovalProgress_ActionTaken] [NVARCHAR](100) NULL ,
      [Position_ExternalId] [NVARCHAR](100) NULL ,
      [Position_Id] [BIGINT] NULL ,
      CONSTRAINT [PK_ApprovalProgress] PRIMARY KEY CLUSTERED
        ( [ApprovalProgress_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    )
    
 GO
PRINT N'Creating [dbo].[ApprovalProgressLogicalExpressions]'
GO
CREATE TABLE [dbo].[ApprovalProgressLogicalExpressions]
    (
      [ApprovalProgressLogicalExpression_Id] [BIGINT] NOT NULL ,
      [LogicalExpressionType] [NVARCHAR](255) NOT NULL ,
      [Operation_id] [BIGINT] NULL ,
      [ApprovalProgress_id] [BIGINT] NULL ,
      [MultipleApprovalProgressExpression_id] [BIGINT] NULL ,
      CONSTRAINT [PK_ApprovalProgressLogicalExpressions] PRIMARY KEY CLUSTERED
        ( [ApprovalProgressLogicalExpression_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    )
    
 GO
PRINT N'Creating [dbo].[PermissionOperations]'
GO
CREATE TABLE [dbo].[PermissionOperations]
    (
      [PermissionOperation_Id] [BIGINT] NOT NULL ,
      [PermissionOperation_Operator] [NVARCHAR](100) NULL ,
      CONSTRAINT [PK_PermissionOperations] PRIMARY KEY CLUSTERED
        ( [PermissionOperation_Id] ASC ) WITH ( DATA_COMPRESSION = PAGE )
    )
    
 GO
PRINT N'Creating [dbo].[ApprovalProgressTerritoryTypes]'
GO
CREATE TABLE [dbo].[ApprovalProgressTerritoryTypes]
    (
      [ApprovalProgressTerritoryTypes_Id] [BIGINT] NOT NULL
                                                   IDENTITY(1, 1) ,
      [ApprovalProgress_Id] [BIGINT] NOT NULL ,
      [TerritoryType_Id] [BIGINT] NOT NULL ,
      [TerritoryType_ExternalId] [NVARCHAR](100) NULL ,
      CONSTRAINT [PK_ApprovalProgressTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [ApprovalProgressTerritoryTypes_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    )
    
 GO
PRINT N'Creating [dbo].[ApprovalRounds]'
GO
CREATE TABLE [dbo].[ApprovalRounds]
    (
      [ApprovalRound_Id] [BIGINT] NOT NULL ,
      [ApprovalRound_ApprovalByAny] [BIT] NULL ,
      [ApprovalRound_RoundNumber] [INT] NOT NULL,
	        CONSTRAINT [PK_ApprovalRounds] PRIMARY KEY CLUSTERED
        ( [ApprovalRound_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[ApprovalRoundOrgUnitLevelPermissions]'
GO
CREATE TABLE [dbo].[ApprovalRoundOrgUnitLevelPermissions]
    (
      [OrgUnitLevelPermission_id] [BIGINT] NOT NULL ,
      [ApprovalRound_id] [BIGINT] NOT NULL,
	        CONSTRAINT [PK_ApprovalRoundOrgUnitLevelPermissions] PRIMARY KEY CLUSTERED
        ( 	[OrgUnitLevelPermission_id] ASC,[ApprovalRound_id] ASC)
        WITH ( DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[OrgUnitLevelPermissions]'
GO
CREATE TABLE [dbo].[OrgUnitLevelPermissions]
    (
      [OrgUnitLevelPermission_Id] [BIGINT] NOT NULL ,
      [OrgUnitLevelPermission_IsEnabled] [BIT] NULL ,
      [OrgUnitLevel_Id] [BIGINT] NOT NULL ,
      [OrgUnitLevel_ExternalId] [NVARCHAR](100) NULL ,
      [Permission_Id] [BIGINT] NOT NULL,
	      CONSTRAINT [PK_OrgUnitLevelPermissions] PRIMARY KEY CLUSTERED ([OrgUnitLevelPermission_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ApprovalRoundProgressTrackers]'
GO
CREATE TABLE [dbo].[ApprovalRoundProgressTrackers]
    (
      [ApprovalRoundProgressTracker_Id] [BIGINT] NOT NULL ,
      [ApprovalRoundProgressTracker_RoundNumber] [INT] NULL ,
      [ApprovalRoundProgress_id] [BIGINT] NOT NULL ,
      [ApprovalProgressTracker_id] [BIGINT] NOT NULL,
	      CONSTRAINT [PK_ApprovalRoundProgressTrackers] PRIMARY KEY CLUSTERED ([ApprovalRoundProgressTracker_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ApprovalProgressTrackers]'
GO
CREATE TABLE [dbo].[ApprovalProgressTrackers]
    (
      [ApprovalProgressTracker_Id] [BIGINT] NOT NULL,
	  	      CONSTRAINT [PK_ApprovalProgressTrackerss] PRIMARY KEY CLUSTERED ([ApprovalProgressTracker_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ApprovalRoundRoles]'
GO
CREATE TABLE [dbo].[ApprovalRoundRoles]
    (
      [ApprovalRound_id] [BIGINT] NOT NULL ,
      [Role_id] [NVARCHAR](100) NOT NULL,
	  	      CONSTRAINT [PK_ApprovalRoundRoles] PRIMARY KEY CLUSTERED ([ApprovalRound_id] ASC,[Role_id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[Roles]'
GO
CREATE TABLE [dbo].[Roles]
    (
      [Role_Id] [NVARCHAR](100) NOT NULL ,
      [Role_Name] [NVARCHAR](255) NULL ,
      [Team_id] [NVARCHAR](100) NULL,
	  	      CONSTRAINT [PK_Roles] PRIMARY KEY CLUSTERED ([Role_id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ApprovalRoundTerritoryTypePermissions]'
GO
CREATE TABLE [dbo].[ApprovalRoundTerritoryTypePermissions]
    (
      [TerritoryTypePermission_id] [BIGINT] NOT NULL ,
      [ApprovalRound_id] [BIGINT] NOT NULL,
	  CONSTRAINT [PK_ApprovalRoundTerritoryTypePermissions] PRIMARY KEY CLUSTERED ([ApprovalRound_id] ASC,[TerritoryTypePermission_id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[TerritoryTypePermissions]'
GO
CREATE TABLE [dbo].[TerritoryTypePermissions]
    (
      [TerritoryTypePermission_Id] [BIGINT] NOT NULL ,
      [TerritoryTypePermission_IsEnabled] [BIT] NULL ,
      [TerritoryType_Id] [BIGINT] NOT NULL ,
      [TerritoryType_ExternalId] [NVARCHAR](100) NULL ,
      [Permission_Id] [BIGINT] NOT NULL,
	        CONSTRAINT [PK_TerritoryTypePermissions] PRIMARY KEY CLUSTERED
        ( [TerritoryTypePermission_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[AttributeTypeTerritoryTypes]'
GO
CREATE TABLE [dbo].[AttributeTypeTerritoryTypes]
    (
      [AttributeTypeTerritoryType_Id] [BIGINT] NOT NULL ,
      [AttributeTypeTerritoryType_Order] [INT] NULL ,
      [TerritoryType_Id] [BIGINT] NOT NULL ,
      [TerritoryType_ExternalId] [NVARCHAR](100) NULL ,
      [AttributeType_Id] [INT] NOT NULL,
       CONSTRAINT [PK_AttributeTypeTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [AttributeTypeTerritoryType_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[ClusterCaches]'
GO
CREATE TABLE [dbo].[ClusterCaches]
    (
      [ClusterCache_Id] [BIGINT] NOT NULL
                                 IDENTITY(1, 1) ,
      [ClusterCache_ClusterCount] [INT] NULL ,
      [ClusterCache_EntityId] [NVARCHAR](255) NULL ,
      [ClusterCache_SharedOrgUnits] [NVARCHAR](255) NULL ,
      [ClusterCache_ClusteredOrgUnits] [NVARCHAR](255) NULL ,
      [ClusterCache_ZoomLevel] [INT] NULL ,
      [ClusterCache_TileX] [INT] NULL ,
      [ClusterCache_TileY] [INT] NULL ,
      [ClusterCache_ClusterLat] [FLOAT] NULL ,
      [ClusterCache_ClusterLong] [FLOAT] NULL ,
      [ClusterCache_MinLong] [FLOAT] NULL ,
      [ClusterCache_MinLat] [FLOAT] NULL ,
      [ClusterCache_MaxLat] [FLOAT] NULL ,
      [ClusterCache_MaxLong] [FLOAT] NULL ,
      [ClusterCache_TerritoryTypeId] [BIGINT] NULL ,
      [ClusterCache_ExternalTerritoryTypeId] [NVARCHAR](100) NULL ,
      [ClusterCache_IsGeoCluster] [BIT] NULL ,
      [ClusterCache_IsUnassignedCluster] [BIT] NULL,
	        CONSTRAINT [PK_ClusterCaches] PRIMARY KEY CLUSTERED
        ( [ClusterCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[ClusterCacheTrackers]'
GO
CREATE TABLE [dbo].[ClusterCacheTrackers]
    (
      [ClusterCacheTracker_Id] [BIGINT] NOT NULL
                                        IDENTITY(1, 1) ,
      [ClusterCacheTracker_ZoomLevel] [INT] NULL ,
      [ClusterCacheTracker_TerritoryTypeId] [BIGINT] NULL ,
      [ClusterCacheTracker_ExternalTerritoryTypeId] [NVARCHAR](100) NULL ,
      [ClusterCacheTracker_IsGeoCluster] [BIT] NULL,
	        CONSTRAINT [PK_ClusterCacheTrackers] PRIMARY KEY CLUSTERED
        ( [ClusterCacheTracker_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[Customers]'
GO
CREATE TABLE [dbo].[Customers]
    (
      [Customer_Id] [BIGINT] NOT NULL ,
      [Customer_ExternalId] [NVARCHAR](100) NOT NULL ,
      [Customer_Name] [NVARCHAR](255) NULL ,
      [Customer_Address1] [NVARCHAR](255) NULL ,
      [Customer_Address2] [NVARCHAR](255) NULL ,
      [Customer_City] [NVARCHAR](255) NULL ,
      [Customer_State] [NVARCHAR](255) NULL ,
      [Customer_Country] [NVARCHAR](255) NULL ,
      [Customer_Latitude] [FLOAT] NULL ,
      [Customer_Longitude] [FLOAT] NULL ,
      [Customer_Quality] [INT] NULL ,
      [Customer_PostalCode] [NVARCHAR](255) NULL ,
      [Customer_Status] [NVARCHAR](255) NULL ,
      [CustomerType_Id] [BIGINT] NOT NULL,
	      CONSTRAINT [PK_Customers] PRIMARY KEY CLUSTERED ([Customer_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[CustomerTypes]'
GO
CREATE TABLE [dbo].[CustomerTypes]
    (
      [CustomerType_Id] [BIGINT] NOT NULL ,
      [CustomerType_Name] [NVARCHAR](255) NULL,
	      CONSTRAINT [PK_CustomerTypes] PRIMARY KEY CLUSTERED ([CustomerType_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[DeleteAlignmentEntityOverrides]'
GO
CREATE TABLE [dbo].[DeleteAlignmentEntityOverrides]
    (
      [DeleteAlignmentEntityOverride_Id] [BIGINT] NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [DeleteAlignmentEntityOverride_ExternalAlignmentEntityId]
        [NVARCHAR](100) NOT NULL ,
      [DeleteAlignmentEntityOverride_AlignmentEntityId] [BIGINT] NOT NULL ,
      [AlignmentEntityType_Id] [BIGINT] NOT NULL,
	        CONSTRAINT [PK_DeleteAlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( DeleteAlignmentEntityOverride_Id ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    )
    
 GO
PRINT N'Creating [dbo].[DeletePersonnelOverrides]'
GO
CREATE TABLE [dbo].[DeletePersonnelOverrides]
    (
      [DeletePersonnelOverride_Id] [BIGINT] NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [DeletePersonnelOverride_PersonnelId] [NVARCHAR](100) NOT NULL ,
      [DeletePersonnelOverride_PersonnelName] [NVARCHAR](255) NULL,
	        CONSTRAINT [PK_DeletePersonnelOverrides] PRIMARY KEY CLUSTERED
        ( DeletePersonnelOverride_Id ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    )
    
 GO
PRINT N'Creating [dbo].[EditAttributeOverrides]'
GO
CREATE TABLE [dbo].[EditAttributeOverrides]
    (
      [EditAttributeOverride_Id] [BIGINT] NOT NULL ,
      [EditAttributeOverride_ExternalAlignmentEntityId] [NVARCHAR](255) NULL ,
      [EditAttributeOverride_AlignmentEntityId] [BIGINT] NOT NULL ,
      [EditAttributeOverride_OldAttributeValue] [NVARCHAR](2000) NULL ,
      [EditAttributeOverride_NewAttributeValue] [NVARCHAR](2000) NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [AttributeType_Id] [INT] NOT NULL,
	        CONSTRAINT [PK_EditAttributeOverrides] PRIMARY KEY CLUSTERED
        ( [EditAttributeOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[Geos]'
GO
CREATE TABLE [dbo].[Geos]
    (
      [Geo_Id] [BIGINT] NOT NULL ,
      [Geo_ExternalId] [NVARCHAR](100) NOT NULL ,
      [Geo_Name] [NVARCHAR](255) NULL ,
      [Geo_Status] [NVARCHAR](255) NULL,
	      CONSTRAINT [tmp_ms_xx_PK_Geos] PRIMARY KEY CLUSTERED ([Geo_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[GroupedEntities]'
GO
CREATE TABLE [dbo].[GroupedEntities]
    (
      [TerritoryType_Id] [BIGINT] NOT NULL ,
      [TerritoryType_ExternalId] [NVARCHAR](100) NULL ,
      [AlignmentEntity_Id] [BIGINT] NOT NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL,

      CONSTRAINT [tmp_ms_xx_PK_GroupedEntities] PRIMARY KEY CLUSTERED
        ( [TerritoryType_Id]	ASC ,[AlignmentEntity_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE ) ON PS_TT(territorytype_id) )

    
 GO
PRINT N'Creating [dbo].[IssueRepairCache]'
GO
CREATE TABLE [dbo].[IssueRepairCache]
    (
      [IssueRepairCache_Id] [BIGINT] NOT NULL ,
      [AlignmentEntity_ExternalId] [NVARCHAR](100) NULL ,
      [AlignmentEntity_Id] [BIGINT] NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] [BIGINT] NULL,
	        CONSTRAINT [PK_IssueRepairCache] PRIMARY KEY CLUSTERED
        ( [IssueRepairCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[OrderedOverrides]'
GO
CREATE TABLE [dbo].[OrderedOverrides]
    (
      [OrderedOverride_Id] [BIGINT] NOT NULL ,
      [OrderedOverride_OverrideId] [BIGINT] NOT NULL ,
      [OrderedOverride_OverrideType] [NVARCHAR](255) NULL ,
      [OrderedOverride_CreateDate] [DATETIMEOFFSET] NULL ,
      [Request_id] [BIGINT] NOT NULL ,
      [OrderedOverride_ParentId] [BIGINT] NULL,
	        CONSTRAINT [PK_OrderedOverrides] PRIMARY KEY CLUSTERED
        ( [OrderedOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[OrgUnitAlignmentEntitys]'
GO
CREATE TABLE [dbo].[OrgUnitAlignmentEntitys]
    (
      [OrgUnitAlignmentEntity_Id] [BIGINT] NOT NULL ,
      [OrgUnitAlignmentEntity_Weight] [DECIMAL](5, 2) NULL ,
      [AlignmentEntity_Id] [BIGINT] NOT NULL ,
      [AlignmentEntity_ExternalId] [NVARCHAR](100) NULL ,
      [OrgUnit_Id] [BIGINT] NOT NULL ,
      [OrgUnit_ExternalId] [NVARCHAR](100) NULL
    )
    
 GO
 ALTER TABLE [dbo].[OrgUnitAlignmentEntitys] ADD CONSTRAINT [PK_OrgUnitAlignmentEntitys] PRIMARY KEY CLUSTERED (AlignmentEntity_Id ASC,orgunit_id asc ) WITH (  DATA_COMPRESSION = PAGE )  
GO
PRINT N'Creating [dbo].[OrgUnitAttributeCache]'
GO
CREATE TABLE [dbo].[OrgUnitAttributeCache]
    (
      [OrgUnitAttributeCache_Id] [BIGINT] NOT NULL ,
      [OrgUnitAttributeCache_Value] [FLOAT] NULL ,
      [OrgUnit_Id] [BIGINT] NULL ,
      [OrgUnit_ExternalId] [NVARCHAR](100) NULL ,
      [AttributeType_Id] [INT] NOT NULL,
	        CONSTRAINT [PK_OrgUnitAttributeCache] PRIMARY KEY CLUSTERED
        ( [OrgUnitAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[Permissions]'
GO
CREATE TABLE [dbo].[Permissions]
    (
      [Permission_Id] [BIGINT] NOT NULL ,
      [Discriminator] [NVARCHAR](255) NOT NULL ,
      [Permission_Description] [NVARCHAR](255) NULL ,
      [Permission_PermissionType] [NVARCHAR](100) NULL ,
      [Permission_PermissionLevel] [NVARCHAR](100) NULL,
	        CONSTRAINT [PK_Permissions] PRIMARY KEY CLUSTERED
        ( [Permission_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[OrgUnitOverrides]'
GO
CREATE TABLE [dbo].[OrgUnitOverrides]
    (
      [OrgUnitOverride_Id] [BIGINT] NOT NULL ,
      [OrgUnitOverride_ExternalOrgUnitId] [NVARCHAR](255) NULL ,
      [OrgUnitOverride_OrgUnitId] [BIGINT] NOT NULL ,
      [OrgUnitOverride_ExternalParentOrgUnitId] [NVARCHAR](255) NULL ,
      [OrgUnitOverride_ParentOrgUnitId] [BIGINT] NULL ,
      [OrgUnitOverride_RefinementOperationType] [NVARCHAR](100) NULL ,
      [OrgUnitOverride_Name] [NVARCHAR](255) NULL ,
      [OrgUnitOverride_LegacyId] [NVARCHAR](255) NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [Level_Id] [NVARCHAR](100) NULL,
	        CONSTRAINT [PK_OrgUnitOverrides] PRIMARY KEY CLUSTERED
        ( [OrgUnitOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[OrgUnitRelationshipStates]'
GO
CREATE TABLE [dbo].[OrgUnitRelationshipStates]
    (
      [OrgUnitRelationshipState_Id] [INT] NOT NULL,
	  CONSTRAINT PK_OrgUnitRelationshipStates PRIMARY KEY CLUSTERED ([OrgUnitRelationshipState_Id])
	  WITH (DATA_COMPRESSION=PAGE)
    )
    
 GO
PRINT N'Creating [dbo].[OrgUnitRelationshipOverrides]'
GO
CREATE TABLE [dbo].[OrgUnitRelationshipOverrides]
    (
      [OrgUnitRelationshipOverride_Id] [BIGINT] NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [OrgUnitRelationshipOverride_ExternalDrivingOrgUnitId] [NVARCHAR](255)
        NULL ,
      [OrgUnitRelationshipOverride_DrivingOrgUnitId] [BIGINT] NOT NULL ,
      [NewOrgUnitRelationshipState_Id] [INT] NOT NULL ,
      [OldOrgUnitRelationshipState_Id] [INT] NULL,
	       CONSTRAINT [PK_OrgUnitRelationshipOverrides] PRIMARY KEY CLUSTERED
        ( [OrgUnitRelationshipOverride_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[OrgUnitRelationships]'
GO
CREATE TABLE [dbo].[OrgUnitRelationships]
    (
      [OrgUnitRelationship_Id] [BIGINT] NOT NULL ,
      [Driving_ExternalOrgUnitId] NVARCHAR(100) NOT NULL ,
      [Driving_OrgUnitId] BIGINT NULL ,
      [Affected_ExternalOrgUnitId] NVARCHAR(100) NOT NULL ,
      [Affected_OrgUnitId] [BIGINT] NULL,
	        CONSTRAINT [PK_OrgUnitRelationships] PRIMARY KEY CLUSTERED
        ( [OrgUnitRelationship_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[OrgUnitRelationshipStateEntrys]'
GO
CREATE TABLE [dbo].[OrgUnitRelationshipStateEntrys]
    (
      [OrgUnitRelationshipStateEntry_Id] [BIGINT] NOT NULL ,
      [OrgUnitRelationshipState_Id] [INT] NOT NULL ,
      [OrgUnitRelationshipStateEntry_AffectedOrgUnitId] [BIGINT] NOT NULL ,
      [OrgUnitRelationshipStateEntry_ExternalAffectedOrgUnitId] NVARCHAR(100)
        NOT NULL,
		      CONSTRAINT [PK_OrgUnitRelationshipStateEntrys] PRIMARY KEY CLUSTERED
        ( [OrgUnitRelationshipStateEntry_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[Personnel]'
GO
CREATE TABLE [dbo].[Personnel]
    (
      [Personnel_Id] [NVARCHAR](100) NOT NULL ,
      [Personnel_Name] [NVARCHAR](255) NULL ,
      [Personnel_Login] [NVARCHAR](255) NULL ,
      [Personnel_Address1] [NVARCHAR](255) NULL ,
      [Personnel_Address2] [NVARCHAR](255) NULL ,
      [Personnel_City] [NVARCHAR](255) NULL ,
      [Personnel_State] [NVARCHAR](255) NULL ,
      [Personnel_Country] [NVARCHAR](255) NULL ,
      [Personnel_PostalCode] [NVARCHAR](255) NULL ,
      [Personnel_Latitude] [FLOAT] NULL ,
      [Personnel_Longitude] [FLOAT] NULL ,
      [Personnel_Quality] [INT] NULL ,
      [Personnel_Status] [NVARCHAR](255) NULL,
	        CONSTRAINT [PK_Personnel] PRIMARY KEY CLUSTERED
        ( [Personnel_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[PositionPersonnel]'
GO
CREATE TABLE [dbo].[PositionPersonnel]
    (
      [PositionPersonnel_Id] [BIGINT] NOT NULL ,
      [EffectiveDate] [DATETIMEOFFSET] NULL ,
      [Position_Id] [BIGINT] NOT NULL ,
      [Position_ExternalId] [NVARCHAR](100) NOT NULL ,
      [Personnel_Id] [NVARCHAR](100) NOT NULL,
	      CONSTRAINT [PK_PositionPersonnel] PRIMARY KEY CLUSTERED ([PositionPersonnel_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[PositionPersonnelOverrides]'
GO
CREATE TABLE [dbo].[PositionPersonnelOverrides]
    (
      [PositionPersonnelOverride_Id] [BIGINT] NOT NULL ,
      [EffectiveDate] [DATETIMEOFFSET] NULL ,
      [PositionPersonnelOverride_RefinementOperationType] [NVARCHAR](100) NULL ,
      [PositionPersonnelOverride_ExternalPositionId] [NVARCHAR](255) NULL ,
      [PositionPersonnelOverride_PositionId] [BIGINT] NOT NULL ,
      [Personnel_Id] [NVARCHAR](100) NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL,
	      CONSTRAINT [PK_PositionPersonnelOverrides] PRIMARY KEY CLUSTERED ([PositionPersonnelOverride_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ProductBaskets]'
GO
CREATE TABLE [dbo].[ProductBaskets]
    (
      [ProductBasket_Id] [BIGINT] NOT NULL,
	      CONSTRAINT [PK_ProductBaskets] PRIMARY KEY CLUSTERED ([ProductBasket_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ProductBasketEntrys]'
GO
CREATE TABLE [dbo].[ProductBasketEntrys]
    (
      [ProductBasketEntry_Id] [BIGINT] NOT NULL ,
      [ProductBasketEntry_Weight] [DECIMAL](5, 2) NOT NULL ,
      [Product_id] [NVARCHAR](100) NOT NULL ,
      [ProductBasket_id] [BIGINT] NULL,
	      CONSTRAINT [PK_ProductBasketEntrys] PRIMARY KEY CLUSTERED ([ProductBasketEntry_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[Products]'
GO
CREATE TABLE [dbo].[Products]
    (
      [Product_Id] [NVARCHAR](100) NOT NULL ,
      [Product_Name] [NVARCHAR](255) NULL,
	  	      CONSTRAINT [PK_Products] PRIMARY KEY CLUSTERED ([Product_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ProductPositionOverrides]'
GO
CREATE TABLE [dbo].[ProductPositionOverrides]
    (
      [ProductPositionOverride_Id] [BIGINT] NOT NULL ,
      [ProductPositionOverride_ExternalPositionId] [NVARCHAR](255) NOT NULL ,
      [ProductPositionOverride_PositionId] [BIGINT] NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [OldProductBasket_Id] [BIGINT] NULL ,
      [NewProductBasket_Id] [BIGINT] NOT NULL,
	      CONSTRAINT [PK_ProductPositionOverrides] PRIMARY KEY CLUSTERED ([ProductPositionOverride_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ProductPositions]'
GO
CREATE TABLE [dbo].[ProductPositions]
    (
      [ProductPosition_Id] [BIGINT] NOT NULL ,
      [ProductPosition_Weight] [DECIMAL](5, 2) NOT NULL ,
      [Position_Id] [BIGINT] NOT NULL ,
      [Position_ExternalId] [NVARCHAR](100) NOT NULL ,
      [Product_Id] [NVARCHAR](100) NOT NULL,
	     CONSTRAINT [PK_ProductPositions] PRIMARY KEY CLUSTERED ([ProductPosition_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[RequestAlignmentEntityAttributeCache]'
GO
CREATE TABLE [dbo].[RequestAlignmentEntityAttributeCache]
    (
      [RequestAlignmentEntityAttributeCache_Id] [BIGINT] NOT NULL ,
      [RequestAlignmentEntityAttributeCache_Value] [FLOAT] NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [AttributeType_Id] [INT] NULL ,
      [TerritoryType_Id] [BIGINT] NULL ,
      [TerritoryType_ExternalId] [NVARCHAR](100) NOT NULL ,
      [AlignmentEntity_Id] [BIGINT] NOT NULL ,
      [Alignment_ExternalId] [NVARCHAR](100) NOT NULL,
	        CONSTRAINT [PK_RequestAlignmentEntityAttributeCache] PRIMARY KEY CLUSTERED
        ( [RequestAlignmentEntityAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[RequestOrgUnitAttributeCache]'
GO
CREATE TABLE [dbo].[RequestOrgUnitAttributeCache]
    (
      [Id] [BIGINT] NOT NULL ,
      [AttributeType_Value] [FLOAT] NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [OrgUnit_Id] [BIGINT] NOT NULL ,
      [OrgUnit_ExternalId] [NVARCHAR](100) NOT NULL ,
      [AttributeType_Id] [INT] NOT NULL,
	       CONSTRAINT [PK_RequestOrgUnitAttributeCache] PRIMARY KEY CLUSTERED
        ( [Id] ASC ) WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[RoleProducts]'
GO
CREATE TABLE [dbo].[RoleProducts]
    (
      [RoleProduct_Id] [BIGINT] NOT NULL ,
      [RoleProduct_DefaultOn] [BIT] NULL ,
      [RoleProduct_DefaultWeight] [DECIMAL](5, 2) NULL ,
      [Role_id] [NVARCHAR](100) NOT NULL ,
      [Product_id] [NVARCHAR](100) NOT NULL,
	       CONSTRAINT [PK_RoleProducts] PRIMARY KEY CLUSTERED
        ( [RoleProduct_Id] ASC ) WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[Teams]'
GO
CREATE TABLE [dbo].[Teams]
    (
      [Team_Id] [NVARCHAR](100) NOT NULL ,
      [Team_Name] [NVARCHAR](255) NULL,
	  	       CONSTRAINT [PK_Teams] PRIMARY KEY CLUSTERED
        ( [Team_Id] ASC ) WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[TerritoryShapes]'
GO
CREATE TABLE [dbo].[TerritoryShapes]
    (
      [TerritoryShape_Id] [BIGINT] NOT NULL ,
      [OrgUnit_Id] [BIGINT] NOT NULL ,
      [OrgUnit_ExternalId] [NVARCHAR](100) NOT NULL ,
      [geom] [sys].[GEOMETRY] NULL ,
      [MinLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STX, ( 0 )) )
        PERSISTED ,
      [MinLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STY, ( 0 )) )
        PERSISTED ,
      [MaxLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STX, ( 0 )) )
        PERSISTED ,
      [MaxLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STY, ( 0 )) )
        PERSISTED ,
      [geom_low] [sys].[geometry] NULL ,
      [geom_medium] [sys].[geometry] NULL,
	       CONSTRAINT [pk_TerritoryShapes] PRIMARY KEY CLUSTERED
        ( [TerritoryShape_Id] ASC )

    ) 
GO
PRINT N'Creating [dbo].[UpsertAlignmentEntityOverrides]'
GO
CREATE TABLE [dbo].[UpsertAlignmentEntityOverrides]
    (
      [UpsertAlignmentEntityOverride_Id] [BIGINT] NOT NULL ,
      [UpsertAlignmentEntityOverride_ExternalAlignmentEntityId]
        [NVARCHAR](100) NULL ,
      [UpsertAlignmentEntityOverride_AlignmentEntityId] [BIGINT] NULL ,
      [UpsertAlignmentEntityOverride_UpsertOperationType] [NVARCHAR](100) NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [AlignmentEntityType_Id] [BIGINT] NULL ,
      [CustomerType_Id] [BIGINT] NULL ,
      [NewAlignmentEntityState_Id] [BIGINT] NULL ,
      [OldAlignmentEntityState_Id] [BIGINT] NULL,
	        CONSTRAINT [PK_UpsertAlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( [UpsertAlignmentEntityOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )

    )
    
 GO
PRINT N'Creating [dbo].[AlignmentEntityStates]'
GO
CREATE TABLE [dbo].[AlignmentEntityStates]
    (
      [AlignmentEntityState_Id] [BIGINT] NOT NULL ,
      [AlignmentEntityState_Name] [NVARCHAR](255) NULL ,
      [AlignmentEntityState_ParentId] [BIGINT] NULL ,
      [AlignmentEntityState_Address1] [NVARCHAR](255) NULL ,
      [AlignmentEntityState_Address2] [NVARCHAR](255) NULL ,
      [AlignmentEntityState_City] [NVARCHAR](255) NULL ,
      [AlignmentEntityState_State] [NVARCHAR](255) NULL ,
      [AlignmentEntityState_Country] [NVARCHAR](255) NULL ,
      [AlignmentEntityState_Latitude] [FLOAT] NULL ,
      [AlignmentEntityState_Longitude] [FLOAT] NULL ,
      [AlignmentEntityState_PostalCode] [NVARCHAR](255) NULL ,
      [AlignmentEntityState_Quality] [INT] NULL,
	     CONSTRAINT [PK_AlignmentEntityStates] PRIMARY KEY CLUSTERED ([AlignmentEntityState_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[PersonnelStates]'
GO
CREATE TABLE [dbo].[PersonnelStates]
    (
      [PersonnelState_Id] [BIGINT] NOT NULL ,
      [PersonnelState_Name] [NVARCHAR](255) NULL ,
      [PersonnelState_Login] [NVARCHAR](255) NULL ,
      [PersonnelState_Address1] [NVARCHAR](255) NULL ,
      [PersonnelState_Address2] [NVARCHAR](255) NULL ,
      [PersonnelState_City] [NVARCHAR](255) NULL ,
      [PersonnelState_State] [NVARCHAR](255) NULL ,
      [PersonnelState_Country] [NVARCHAR](255) NULL ,
      [PersonnelState_PostalCode] [NVARCHAR](255) NULL ,
      [PersonnelState_Latitude] [FLOAT] NULL ,
      [PersonnelState_Longitude] [FLOAT] NULL ,
      [PersonnelState_Quality] [INT] NULL,
	     CONSTRAINT [PK_PersonnelStates] PRIMARY KEY CLUSTERED ([PersonnelState_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[UpsertPersonnelOverrides]'
GO
CREATE TABLE [dbo].[UpsertPersonnelOverrides]
    (
      [UpsertPersonnelOverride_Id] [BIGINT] NOT NULL ,
      [Request_Id] [BIGINT] NOT NULL ,
      [UpsertPersonnelOverride_PersonnelId] [NVARCHAR](100) NOT NULL ,
      [NewPersonnelState_Id] [BIGINT] NOT NULL ,
      [OldPersonnelState_Id] [BIGINT] NULL ,
      [UpsertPersonnelOverride_UpsertOperationType] [NVARCHAR](255) NOT NULL,
	     CONSTRAINT [PK_UpsertPersonnelOverrides] PRIMARY KEY CLUSTERED ([UpsertPersonnelOverride_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[Validations]'
GO
CREATE TABLE [dbo].[Validations]
    (
      [Validation_Id] [BIGINT] NOT NULL ,
      [Validation_Name] [NVARCHAR](255) NOT NULL ,
      [Validation_IsElastic] [BIT] NOT NULL ,
      [Validation_AppliesTo] [NVARCHAR](255) NULL ,
      [AttributeType_id] [INT] NULL ,
      [Folder_id] [BIGINT] NULL,
	  	     CONSTRAINT [PK_Validations] PRIMARY KEY CLUSTERED ([Validation_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ValidationRanges]'
GO
CREATE TABLE [dbo].[ValidationRanges]
    (
      [ValidationRange_Id] [BIGINT] NOT NULL ,
      [ValidationRange_Type] [NVARCHAR](100) NOT NULL ,
      [ValidationRange_Operator] [NVARCHAR](100) NOT NULL ,
      [ValidationRange_Value] [FLOAT] NOT NULL ,
      [ValidationRange_Message] [NVARCHAR](255) NOT NULL ,
      [ValidationRange_EntityCount] [INT] NULL ,
      [Validation_id] [BIGINT] NOT NULL,
	  	     CONSTRAINT [PK_ValidationRanges] PRIMARY KEY CLUSTERED ([ValidationRange_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[Folders]'
GO
CREATE TABLE [dbo].[Folders]
    (
      [Folder_Id] [BIGINT] NOT NULL ,
      [Folder_Name] [NVARCHAR](255) NULL ,
      [Folder_FolderType] [NVARCHAR](100) NULL,
	  	     CONSTRAINT [PK_Folders] PRIMARY KEY CLUSTERED ([Folder_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[Accounts]'
GO
CREATE TABLE [dbo].[Accounts]
    (
      [CUSTOMER_ID] [NVARCHAR](2000) NOT NULL,
	  	     CONSTRAINT [PK_Accounts] PRIMARY KEY CLUSTERED ([Customer_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[GeographyMappings]'
GO
CREATE TABLE [dbo].[GeographyMappings]
    (
      [GeographyMapping_GeoId] [NVARCHAR](100) NOT NULL ,
      [GeographyMapping_ShapeId] [NVARCHAR](255) NULL ,
      [GeographyMapping_ShapeIndex] [NVARCHAR](255) NULL ,
      [GeographyMapping_Latitude] [FLOAT] NULL ,
      [GeographyMapping_Longitude] [FLOAT] NULL ,
      [GeographyMapping_Type] [NVARCHAR](100) NULL,
	  	     CONSTRAINT [PK_GeographyMappings] PRIMARY KEY CLUSTERED ([GeographyMapping_GeoId] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[LiveFeedSummaries]'
GO
CREATE TABLE [dbo].[LiveFeedSummaries]
    (
      [LiveFeedSummary_Id] [BIGINT] NOT NULL ,
      [LiveFeed_Id] [BIGINT] NOT NULL ,
      [LiveFeedSummary_StartTime] [DATETIMEOFFSET](0) NULL ,
      [LiveFeedSummary_EndTime] [DATETIMEOFFSET](0) NULL ,
      [LiveFeedSummary_CountOfNewCustomers] [INT] NOT NULL ,
      [LiveFeedSummary_CountOfEditedCustomers] [INT] NOT NULL ,
      [LiveFeedSummary_CountOfDeletedCustomers] [INT] NOT NULL ,
      [LiveFeedSummary_CountOfNewGeos] [INT] NOT NULL ,
      [LiveFeedSummary_CountOfEditedGeos] [INT] NOT NULL ,
      [LiveFeedSummary_CountOfDeletedGeos] [INT] NOT NULL ,
      [LiveFeedSummary_CountOfNewPersonnel] [INT] NOT NULL ,
      [LiveFeedSummary_CountOfEditedPersonnel] [INT] NOT NULL ,
      [LiveFeedSummary_CountOfDeletedPersonnel] [INT] NOT NULL,
	  	     CONSTRAINT [PK_LiveFeedSummaries] PRIMARY KEY CLUSTERED ([LiveFeedSummary_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[HiLoUniqueKey]'
GO
CREATE TABLE [dbo].[HiLoUniqueKey]
    (
      [ObjectType] [NVARCHAR](150) NOT NULL ,
      [NextHi] [BIGINT] NOT NULL,
	  	     CONSTRAINT [PK_HiLoUniqueKey] PRIMARY KEY CLUSTERED (ObjectType ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[SchemaInfo]'
GO
CREATE TABLE [dbo].[SchemaInfo]
    (
      [Version] [BIGINT] NOT NULL,
	  	     CONSTRAINT [PK_SchemaInfo] PRIMARY KEY CLUSTERED ([Version] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[TerritoryShapeOverrides]'
GO
CREATE TABLE [dbo].[TerritoryShapeOverrides]
    (
      [TerritoryShapeOverride_Id] [BIGINT] NOT NULL ,
      [OrgUnit_ExternalId] [NVARCHAR](100) NOT NULL ,
      [OrgUnit_Id] [BIGINT] NOT NULL ,
      [Position_ExternalId] [NVARCHAR](100) NOT NULL ,
      [Position_Id] [BIGINT] NOT NULL ,
      [geom] [sys].[GEOMETRY] NOT NULL ,
      [MinLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STX, ( 0 )) )
        PERSISTED ,
      [MinLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STY, ( 0 )) )
        PERSISTED ,
      [MaxLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STX, ( 0 )) )
        PERSISTED ,
      [MaxLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STY, ( 0 )) )
        PERSISTED ,
      [geom_low] [sys].[geometry] NULL ,
      [geom_medium] [sys].[geometry] NULL,
	  	     CONSTRAINT [PK_TerritoryShapeOverrides] PRIMARY KEY CLUSTERED ([TerritoryShapeOverride_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    ) 
GO
PRINT N'Creating [dbo].[LiveFeeds]'
GO
CREATE TABLE [dbo].[LiveFeeds]
    (
      [LiveFeed_Id] [BIGINT] NOT NULL ,
      [LiveFeed_RefreshInterval] [NVARCHAR](100) NOT NULL ,
      [LiveFeed_LastRefresh] [DATETIMEOFFSET] NULL ,
      [LiveFeed_StartDate] [DATETIMEOFFSET] NOT NULL ,
      [LiveFeed_FeedState] [NVARCHAR](100) NOT NULL ,
      [LiveFeed_ETlScenarioIdentifier] [NVARCHAR](255) NOT NULL ,
      [LiveFeed_ETlScenarioName] [NVARCHAR](255) NULL ,
      [LiveFeed_QuartzJob] [NVARCHAR](255) NULL,
	  	     CONSTRAINT [PK_LiveFeeds] PRIMARY KEY CLUSTERED ([LiveFeed_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
PRINT N'Creating [dbo].[ShapeFileGeoMap]'
GO
CREATE TABLE [dbo].[ShapeFileGeoMap]
    (
      [ShapeFileGeoMap_Id] [BIGINT] NOT NULL ,
      [Shape_Id] [NVARCHAR](255) NOT NULL ,
      [Geo_Id] [NVARCHAR](255) NOT NULL,
	  	     CONSTRAINT [PK_ShapeFileGeoMap] PRIMARY KEY CLUSTERED ([ShapeFileGeoMap_Id] ASC) WITH (DATA_COMPRESSION = PAGE)

    )
    
 GO
