Delete from ApprovalRoundOrgUnitLevelPermissions
Delete from OrgUnitLevelPermissions

Declare @PkConstraint nvarchar(255)=''
Declare @FkConstraint nvarchar(255)=''
Declare @sql nvarchar(max)=''

--Delete Foreign Keys
SELECT @sql = @sql+N'Alter Table ApprovalRoundOrgUnitLevelPermissions
Drop CONSTRAINT '+ CONSTRAINT_NAME+CHAR(13) FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'FOREIGN KEY' AND TABLE_NAME = 'ApprovalRoundOrgUnitLevelPermissions' 
EXEC (@sql)

--Delete PrimaryKey
SELECT @PkConstraint = (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_NAME = 'OrgUnitLevelPermissions')
Set @sql = N'Alter Table OrgUnitLevelPermissions
Drop CONSTRAINT ' + @PkConstraint
EXEC (@sql)


Alter Table OrgUnitLevelPermissions
Add TempOrgUnitLevelPermission_Id bigint not null identity(1,1)

Insert Into OrgUnitLevelPermissions
(OrgUnitLevel_id, Permission_id, OrgUnitLevelPermission_IsEnabled, OrgUnitLevelPermission_Id)
(select oul.OrgUnitLevel_Id, p.Permission_Id, 1 as OrgUnitLevelPermission_IsEnabled, 0 as OrgUnitLevelPermission_Id from OrgUnitLevels oul, Permissions p
Where p.Permission_PermissionLevel = 'OrgUnitLevel')

SET @sql = N'
Update OrgUnitLevelPermissions
Set OrgUnitLevelPermission_Id = TempOrgUnitLevelPermission_Id'
EXEC (@sql)

Alter Table OrgUnitLevelPermissions
drop column TempOrgUnitLevelPermission_Id


--Add Primary Key again
ALTER TABLE [dbo].[OrgUnitLevelPermissions] ADD  CONSTRAINT [PK_OrgUnitLevelPermissions] PRIMARY KEY CLUSTERED 
([OrgUnitLevelPermission_Id] ASC)WITH (DATA_COMPRESSION=PAGE);

--Add Foeign Keys
Alter Table ApprovalRoundOrgUnitLevelPermissions WITH CHECK ADD  CONSTRAINT FK_ApprovalRoundOrgUnitLevelPermissions_ApprovalRounds FOREIGN KEY([ApprovalRound_id]) REFERENCES [dbo].[ApprovalRounds] ([ApprovalRound_id])
Alter Table ApprovalRoundOrgUnitLevelPermissions WITH CHECK ADD  CONSTRAINT FK_ApprovalRoundOrgUnitLevelPermissions_OrgUnitLevelPermissions FOREIGN KEY([OrgUnitLevelPermission_id]) REFERENCES [dbo].OrgUnitLevelPermissions ([OrgUnitLevelPermission_id])
ALTER TABLE [dbo].ApprovalRoundOrgUnitLevelPermissions CHECK CONSTRAINT FK_ApprovalRoundOrgUnitLevelPermissions_ApprovalRounds
ALTER TABLE [dbo].ApprovalRoundOrgUnitLevelPermissions CHECK CONSTRAINT FK_ApprovalRoundOrgUnitLevelPermissions_OrgUnitLevelPermissions




