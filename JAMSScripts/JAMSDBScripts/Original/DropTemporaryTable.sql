﻿DECLARE @id varchar(255)
DECLARE @sql NVARCHAR(MAX)

DECLARE tableCursor CURSOR FOR 
SELECT table_name FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_TYPE='BASE TABLE' and table_name like 'TEMPORARYTABLE_%'

OPEN tableCursor 
FETCH next FROM tableCursor INTO @id 

WHILE @@fetch_status=0
BEGIN

	SET @sql = N'drop table ' + @id
	exec sp_executesql @sql;
	FETCH next FROM tableCursor INTO @id

END

CLOSE tableCursor
DEALLOCATE tableCursor 
