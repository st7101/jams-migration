﻿DECLARE @isFirstTextAttribute bit;
SET @isFirstTextAttribute = 0;

/* DROP EXISTING TempAlignmentEntityTextAttributeCache TABLE */
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempAlignmentEntityTextAttributeCache') 
DROP TABLE [TempAlignmentEntityTextAttributeCache]

/* CREATE TEMPORARY TABLE */
CREATE TABLE [TempAlignmentEntityTextAttributeCache] 
( AlignmentEntityTextAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
AlignmentEntityTextAttributeCache_Value nvarchar(2000) NULL ,
AlignmentEntity_id BIGINT NULL , 
AttributeType_id bigint NULL , 
CONSTRAINT PK_TempAlignmentEntityTextAttributeCache_AlignmentEntityTextAttributeCache_Id 
PRIMARY KEY CLUSTERED (AlignmentEntityTextAttributeCache_Id))

/* POPULATE ORIGINAL TABLE */
IF NOT EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'AlignmentEntityTextAttributeCache') 
BEGIN
	SET @isFirstTextAttribute = 1;

	CREATE TABLE [AlignmentEntityTextAttributeCache] 
	( AlignmentEntityTextAttributeCache_Id bigint NOT NULL, 
	AlignmentEntityTextAttributeCache_Value nvarchar(2000) NULL ,
	AlignmentEntity_id BIGINT NULL , 
	AttributeType_id bigint NULL , 
	CONSTRAINT PK_AlignmentEntityTextAttributeCache_AlignmentEntityTextAttributeCache_Id 
	PRIMARY KEY CLUSTERED (AlignmentEntityTextAttributeCache_Id) WITH(DATA_COMPRESSION=PAGE));
END

/* INSERT NEW ATTRIBUTE VALUES IN CACHE TABLE FOR ALL INDEPENDENT ENTITYS */
INSERT INTO [TempAlignmentEntityTextAttributeCache] with(tablock)
	(AlignmentEntity_id, AttributeType_id, AlignmentEntityTextAttributeCache_Value) 
	SELECT ae.AlignmentEntity_id AS AlignmentEntity_id, 
			-1 AS AttributeType_id, 
			ae.[{0}] AS AlignmentEntityTextAttributeCache_Value
			FROM [AlignmentEntitys] ae
			WHERE EXISTS (Select 1 from [OrgUnitAlignmentEntitys] oae 
						Where ae.AlignmentEntity_Id = oae.AlignmentEntity_id);


/* FIND OUT MAXIMUM VALUE OF ID IN CACHE TABLE */
DECLARE @maxIdInCache bigint;

Select @maxIdInCache = ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0) FROM [AlignmentEntityTextAttributeCache];

/* INSERT CACHE ENTRIES FROM TEMPORARY TABLE TO ACTUAL CACHE TABLE */

INSERT INTO [AlignmentEntityTextAttributeCache] with(tablock) 
(AlignmentEntityTextAttributeCache_Id, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id)
SELECT AlignmentEntityTextAttributeCache_Id + @maxIdInCache, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id 
FROM [TempAlignmentEntityTextAttributeCache]; 

IF @isFirstTextAttribute = 1
BEGIN
CREATE NONCLUSTERED INDEX [IDX_ALIGNMENTENTITYTEXTATTRIBUTECACHE_AttributeTypeId] ON [dbo].[AlignmentEntityTextAttributeCache] ([AttributeType_Id] ASC) WITH ( ONLINE = OFF, DATA_COMPRESSION = PAGE) ON PS_AENAC(AttributeType_Id);
CREATE NONCLUSTERED INDEX [IDX_ALIGNMENTENTITYTEXTATTRIBUTECACHE_AlignmentEntityTextAttributeCacheValue_AlignmentEntityId_AttributeTypeId] ON [dbo].[AlignmentEntityTextAttributeCache] ([AlignmentEntityTextAttributeCache_Value] ASC,[AlignmentEntity_Id] ASC,[AttributeType_Id] ASC) WITH ( ONLINE = OFF, DATA_COMPRESSION = PAGE);
CREATE NONCLUSTERED INDEX [IDX_ALIGNMENTENTITYTEXTATTRIBUTECACHE_AlignmentEntityId_INC_AlignmentEntityTextAttributeCacheValue_AttributeTypeId] ON [dbo].[AlignmentEntityTextAttributeCache] ([AlignmentEntity_Id] ASC)  INCLUDE ([AlignmentEntityTextAttributeCache_Value],[AttributeType_Id])WITH ( ONLINE = OFF, DATA_COMPRESSION = PAGE);

END

/* DROP TEMPORARY TABLE */
DROP TABLE [TempAlignmentEntityTextAttributeCache]

/* INSERT/UPDATE OBJECT IN HiLo TABLE */

DELETE FROM HiLoUniqueKey WHERE ObjectType = 'AlignmentEntityTextAttributeCache';

INSERT INTO HiLoUniqueKey
(NextHi, ObjectType)
VALUES
((SELECT ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0)+1 
				FROM [AlignmentEntityTextAttributeCache]), 'AlignmentEntityTextAttributeCache');
