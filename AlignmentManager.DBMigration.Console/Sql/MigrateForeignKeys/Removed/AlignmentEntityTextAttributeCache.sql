
PRINT N'Updating AlignmentEntityTextAttributeCache ...';

UPDATE aetac
SET AlignmentEntity_Id=ae.AlignmentEntity_Id
FROM dbo.AlignmentEntityTextAttributeCache aetac INNER JOIN 
dbo.AlignmentEntitys ae ON aetac.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
