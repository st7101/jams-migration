
PRINT N'Starting rebuilding table [dbo].[AlignmentEntityNumberAttributeCache]...';

CREATE PARTITION FUNCTION PF_AENAC(INT) AS RANGE LEFT FOR VALUES (100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599)
CREATE PARTITION SCHEME PS_AENAC AS PARTITION PF_AENAC ALL TO ([PRIMARY])
CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityNumberAttributeCache]
    (
      [AlignmentEntityNumberAttributeCache_Id] BIGINT NOT NULL ,
      [AlignmentEntityNumberAttributeCache_Value] FLOAT(53) NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [AttributeType_Id] INT NOT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityNumberAttributeCache] PRIMARY KEY CLUSTERED
        ([AttributeType_Id], [AlignmentEntityNumberAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    ) ON PS_AENAC([AttributeType_Id]);
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AlignmentEntityNumberAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_AlignmentEntityNumberAttributeCache]
                ( [AlignmentEntityNumberAttributeCache_Id] ,
                  [AlignmentEntityNumberAttributeCache_Value] ,
                  [AlignmentEntity_ExternalId] , alignmententity_id,
                  [AttributeType_Id] ,
                  [TerritoryType_ExternalId], [TerritoryType_Id]
                )
                SELECT  [AlignmentEntityNumberAttributeCache_Id] ,
                        [AlignmentEntityNumberAttributeCache_Value] ,
                        aenac.[AlignmentEntity_Id] ,ae.AlignmentEntity_Id,
                        [AttributeType_Id] ,
                        aenac.[TerritoryType_Id], tt.[TerritoryType_Id]
                FROM    [dbo].[AlignmentEntityNumberAttributeCache] aenac
				LEFT OUTER JOIN dbo.AlignmentEntitys ae ON ae.AlignmentEntity_ExternalId = aenac.AlignmentEntity_Id
				LEFT OUTER JOIN dbo.TerritoryTypes tt ON tt.TerritoryType_ExternalId = aenac.TerritoryType_Id
				ORDER BY AttributeType_Id, [AlignmentEntityNumberAttributeCache_Id] ASC;
    END
DROP TABLE [dbo].[AlignmentEntityNumberAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityNumberAttributeCache]',
    N'AlignmentEntityNumberAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityNumberAttributeCache]',
    N'PK_AlignmentEntityNumberAttributeCache', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[AlignmentEntityOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityOverrides]
    (
      [AlignmentEntityOverride_Id] BIGINT NOT NULL ,
      [Request_Id] BIGINT NOT NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      [NewAlignmentState_Id] BIGINT NULL ,
      [OldAlignmentState_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( [AlignmentEntityOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AlignmentEntityOverrides] )
    BEGIN 
        INSERT  INTO [dbo].[tmp_ms_xx_AlignmentEntityOverrides]
                ( [AlignmentEntityOverride_Id] ,
                  [Request_Id] ,
                  [AlignmentEntity_ExternalId] ,
                  [TerritoryType_ExternalId] ,
                  [NewAlignmentState_Id] ,
                  [OldAlignmentState_Id]
                )
                SELECT  [AlignmentEntityOverride_Id] ,
                        [Request_Id] ,
                        [AlignmentEntity_Id] ,
                        [TerritoryType_Id] ,
                        [NewAlignmentState_Id] ,
                        [OldAlignmentState_Id]
                FROM    [dbo].[AlignmentEntityOverrides]
                ORDER BY [AlignmentEntityOverride_Id] ASC;
    END
DROP TABLE [dbo].[AlignmentEntityOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityOverrides]',
    N'AlignmentEntityOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityOverrides]',
    N'PK_AlignmentEntityOverrides', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[AlignmentStateEntrys]...';

CREATE TABLE [dbo].[tmp_ms_xx_AlignmentStateEntrys] (
    [AlignmentStateEntry_Id]               BIGINT         NOT NULL,
    [AlignmentStateEntry_ExternalOrgUnitId]        NVARCHAR (255) NULL,
    [AlignmentStateEntry_OrgUnitId] BIGINT         NULL,
    [AlignmentStateEntry_Weight]           DECIMAL (5, 2) NOT NULL,
    [AlignmentState_Id]                    BIGINT         NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentStateEntrys] PRIMARY KEY CLUSTERED ([AlignmentStateEntry_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[AlignmentStateEntrys])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_AlignmentStateEntrys] ([AlignmentStateEntry_Id], [AlignmentStateEntry_ExternalOrgUnitId], [AlignmentStateEntry_Weight], [AlignmentState_Id])
        SELECT   [AlignmentStateEntry_Id],
                 [AlignmentStateEntry_OrgUnitId],
                 [AlignmentStateEntry_Weight],
                 [AlignmentState_Id]
        FROM     [dbo].[AlignmentStateEntrys]
        ORDER BY [AlignmentStateEntry_Id] ASC;
    END

DROP TABLE [dbo].[AlignmentStateEntrys];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentStateEntrys]', N'AlignmentStateEntrys';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentStateEntrys]', N'PK_AlignmentStateEntrys', N'OBJECT';






PRINT N'Starting rebuilding table [dbo].[AlignmentEntityTextAttributeCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityTextAttributeCache]
    (
      [AlignmentEntityTextAttributeCache_Id] BIGINT NOT NULL ,
      [AlignmentEntityTextAttributeCache_Value] NVARCHAR(2000) NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [AttributeType_Id] INT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityTextAttributeCache] PRIMARY KEY CLUSTERED
        ( [AlignmentEntityTextAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AlignmentEntityTextAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_AlignmentEntityTextAttributeCache]
                ( [AlignmentEntityTextAttributeCache_Id] ,
                  [AlignmentEntityTextAttributeCache_Value] ,
                  [AlignmentEntity_ExternalId] ,
                  [AttributeType_Id]
                )
                SELECT  [AlignmentEntityTextAttributeCache_Id] ,
                        [AlignmentEntityTextAttributeCache_Value] ,
                        [AlignmentEntity_Id] ,
                        CAST([AttributeType_Id] AS INT)
                FROM    [dbo].[AlignmentEntityTextAttributeCache]
                ORDER BY [AlignmentEntityTextAttributeCache_Id] ASC;
    END
DROP TABLE [dbo].[AlignmentEntityTextAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityTextAttributeCache]',
    N'AlignmentEntityTextAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityTextAttributeCache]',
    N'PK_AlignmentEntityTextAttributeCache', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[AlignmentEntityTypeTerritoryTypes_Internal]...';


CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityTypeTerritoryTypes]
    (
      [AlignmentEntityTypeTerritoryType_Id] BIGINT NOT NULL ,
      [ToleratePartialAlignment] BIT
        CONSTRAINT DF_tmp_ms_xx_AlignmentEntityTypeTerritoryTypes_ToleratePartialAlignment
        DEFAULT ( (0) )
        NOT NULL ,
      [AlignmentEntityType_Id] BIGINT NOT NULL ,
      [AlignmentEntityType_ParentId] BIGINT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityTypeTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [AlignmentEntityTypeTerritoryType_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AlignmentEntityTypeTerritoryTypes] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_AlignmentEntityTypeTerritoryTypes]
                ( [AlignmentEntityTypeTerritoryType_Id] ,
                  [ToleratePartialAlignment] ,
                  [AlignmentEntityType_Id] ,
                  [AlignmentEntityType_ParentId] ,
                  [TerritoryType_ExternalId]
                )
                SELECT  [AlignmentEntityTypeTerritoryType_Id] ,
                        [ToleratePartialAlignment] ,
                        [AlignmentEntityType_Id] ,
                        [AlignmentEntityType_ParentId] ,
                        [TerritoryType_Id]
                FROM    [dbo].[AlignmentEntityTypeTerritoryTypes]
                ORDER BY [AlignmentEntityTypeTerritoryType_Id] ASC;
    END
DROP TABLE [dbo].[AlignmentEntityTypeTerritoryTypes];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityTypeTerritoryTypes]',
    N'AlignmentEntityTypeTerritoryTypes';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityTypeTerritoryTypes]',
    N'PK_AlignmentEntityTypeTerritoryTypes', N'OBJECT';
EXECUTE sp_rename N'[dbo].[DF_tmp_ms_xx_AlignmentEntityTypeTerritoryTypes_ToleratePartialAlignment]',
    N'DF_AlignmentEntityTypeTerritoryTypes_ToleratePartialAlignment',
    N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[ApprovalProgressTerritoryTypes]...';


CREATE TABLE [dbo].[tmp_ms_xx_ApprovalProgressTerritoryTypes]
    (
      [ApprovalProgressTerritoryTypes_Id] BIGINT IDENTITY(1, 1)
                                                 NOT NULL ,
      [ApprovalProgress_Id] BIGINT NOT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NOT NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_ApprovalProgressTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [ApprovalProgressTerritoryTypes_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[ApprovalProgressTerritoryTypes] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_ApprovalProgressTerritoryTypes]
                ( [ApprovalProgress_Id] ,
                  [TerritoryType_ExternalId]
                )
                SELECT  [ApprovalProgress_Id] ,
                        [TerritoryType_Id]
                FROM    [dbo].[ApprovalProgressTerritoryTypes];
    END
DROP TABLE [dbo].[ApprovalProgressTerritoryTypes];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ApprovalProgressTerritoryTypes]',
    N'ApprovalProgressTerritoryTypes';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ApprovalProgressTerritoryTypes]',
    N'PK_ApprovalProgressTerritoryTypes', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[AttributeTypeTerritoryTypes]...';


CREATE TABLE [dbo].[tmp_ms_xx_AttributeTypeTerritoryTypes]
    (
      [AttributeTypeTerritoryType_Id] BIGINT NOT NULL ,
      [AttributeTypeTerritoryType_Order] INT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      [AttributeType_Id] INT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AttributeTypeTerritoryTypes] PRIMARY KEY CLUSTERED
        ( [AttributeTypeTerritoryType_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AttributeTypeTerritoryTypes] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_AttributeTypeTerritoryTypes]
                ( [AttributeTypeTerritoryType_Id] ,
                  [AttributeTypeTerritoryType_Order] ,
                  [TerritoryType_ExternalId] ,
                  [AttributeType_Id]
                )
                SELECT  [AttributeTypeTerritoryType_Id] ,
                        [AttributeTypeTerritoryType_Order] ,
                        [TerritoryType_Id] ,
                        CAST([AttributeType_Id] AS INT)
                FROM    [dbo].[AttributeTypeTerritoryTypes]
                ORDER BY [AttributeTypeTerritoryType_Id] ASC;
    END
DROP TABLE [dbo].[AttributeTypeTerritoryTypes];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AttributeTypeTerritoryTypes]',
    N'AttributeTypeTerritoryTypes';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AttributeTypeTerritoryTypes]',
    N'PK_AttributeTypeTerritoryTypes', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[ClusterCaches]...';


CREATE TABLE [dbo].[tmp_ms_xx_ClusterCaches]
    (
      [ClusterCache_Id] BIGINT IDENTITY(1, 1)
                               NOT NULL ,
      [ClusterCache_ClusterCount] INT NULL ,
      [ClusterCache_EntityId] NVARCHAR(255) NULL ,
      [ClusterCache_SharedOrgUnits] NVARCHAR(255) NULL ,
      [ClusterCache_ClusteredOrgUnits] NVARCHAR(255) NULL ,
      [ClusterCache_ZoomLevel] INT NULL ,
      [ClusterCache_TileX] INT NULL ,
      [ClusterCache_TileY] INT NULL ,
      [ClusterCache_ClusterLat] FLOAT(53) NULL ,
      [ClusterCache_ClusterLong] FLOAT(53) NULL ,
      [ClusterCache_MinLong] FLOAT(53) NULL ,
      [ClusterCache_MinLat] FLOAT(53) NULL ,
      [ClusterCache_MaxLat] FLOAT(53) NULL ,
      [ClusterCache_MaxLong] FLOAT(53) NULL ,
      [ClusterCache_ExternalTerritoryTypeId] NVARCHAR(255) NULL ,
      [ClusterCache_TerritoryTypeId] BIGINT NULL ,
      [ClusterCache_IsGeoCluster] BIT NULL ,
      [ClusterCache_IsUnassignedCluster] BIT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_ClusterCaches] PRIMARY KEY CLUSTERED
        ( [ClusterCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[ClusterCaches] )
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ClusterCaches] ON;
        INSERT  INTO [dbo].[tmp_ms_xx_ClusterCaches]
                ( [ClusterCache_Id] ,
                  [ClusterCache_ClusterCount] ,
                  [ClusterCache_EntityId] ,
                  [ClusterCache_SharedOrgUnits] ,
                  [ClusterCache_ClusteredOrgUnits] ,
                  [ClusterCache_ZoomLevel] ,
                  [ClusterCache_TileX] ,
                  [ClusterCache_TileY] ,
                  [ClusterCache_ClusterLat] ,
                  [ClusterCache_ClusterLong] ,
                  [ClusterCache_MinLong] ,
                  [ClusterCache_MinLat] ,
                  [ClusterCache_MaxLat] ,
                  [ClusterCache_MaxLong] ,
                  [ClusterCache_ExternalTerritoryTypeId] ,
                  [ClusterCache_IsGeoCluster] ,
                  [ClusterCache_IsUnassignedCluster]
                )
                SELECT  [ClusterCache_Id] ,
                        [ClusterCache_ClusterCount] ,
                        [ClusterCache_EntityId] ,
                        [ClusterCache_SharedOrgUnits] ,
                        [ClusterCache_ClusteredOrgUnits] ,
                        [ClusterCache_ZoomLevel] ,
                        [ClusterCache_TileX] ,
                        [ClusterCache_TileY] ,
                        [ClusterCache_ClusterLat] ,
                        [ClusterCache_ClusterLong] ,
                        [ClusterCache_MinLong] ,
                        [ClusterCache_MinLat] ,
                        [ClusterCache_MaxLat] ,
                        [ClusterCache_MaxLong] ,
                        [ClusterCache_TerritoryTypeId] ,
                        [ClusterCache_IsGeoCluster] ,
                        [ClusterCache_IsUnassignedCluster]
                FROM    [dbo].[ClusterCaches]
                ORDER BY [ClusterCache_Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ClusterCaches] OFF;
    END
DROP TABLE [dbo].[ClusterCaches];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ClusterCaches]', N'ClusterCaches';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ClusterCaches]',
    N'PK_ClusterCaches', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[ClusterCacheTrackers]...';


CREATE TABLE [dbo].[tmp_ms_xx_ClusterCacheTrackers]
    (
      [ClusterCacheTracker_Id] BIGINT IDENTITY(1, 1)
                                      NOT NULL ,
      [ClusterCacheTracker_ZoomLevel] INT NULL ,
      [ClusterCacheTracker_ExternalTerritoryTypeId] NVARCHAR(255) NULL ,
      [ClusterCacheTracker_TerritoryTypeId] BIGINT NULL ,
      [ClusterCacheTracker_IsGeoCluster] BIT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_ClusterCacheTrackers] PRIMARY KEY CLUSTERED
        ( [ClusterCacheTracker_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );

IF EXISTS (SELECT TOP 1 1 FROM sys.tables WHERE name ='ClusterCacheTrackers')
BEGIN
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[ClusterCacheTrackers] )
    BEGIN
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ClusterCacheTrackers] ON;
        INSERT  INTO [dbo].[tmp_ms_xx_ClusterCacheTrackers]
                ( [ClusterCacheTracker_Id] ,
                  [ClusterCacheTracker_ZoomLevel] ,
                  [ClusterCacheTracker_ExternalTerritoryTypeId] ,
                  [ClusterCacheTracker_IsGeoCluster]
                )
                SELECT  [ClusterCacheTracker_Id] ,
                        [ClusterCacheTracker_ZoomLevel] ,
                        [ClusterCacheTracker_TerritoryTypeId] ,
                        [ClusterCacheTracker_IsGeoCluster]
                FROM    [dbo].[ClusterCacheTrackers]
                ORDER BY [ClusterCacheTracker_Id] ASC;
        SET IDENTITY_INSERT [dbo].[tmp_ms_xx_ClusterCacheTrackers] OFF;
    END
DROP TABLE [dbo].[ClusterCacheTrackers];
END
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ClusterCacheTrackers]',
    N'ClusterCacheTrackers';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ClusterCacheTrackers]',
    N'PK_ClusterCacheTrackers', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[EditAttributeOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_EditAttributeOverrides]
    (
      [EditAttributeOverride_Id] BIGINT NOT NULL ,
      [EditAttributeOverride_ExternalAlignmentEntityId] NVARCHAR(255) NULL ,
      [EditAttributeOverride_AlignmentEntityId] BIGINT NULL ,
      [EditAttributeOverride_OldAttributeValue] NVARCHAR(2000) NULL ,
      [EditAttributeOverride_NewAttributeValue] NVARCHAR(2000) NULL ,
      [Request_Id] BIGINT NOT NULL ,
      [AttributeType_Id] INT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_EditAttributeOverrides] PRIMARY KEY CLUSTERED
        ( [EditAttributeOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[EditAttributeOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_EditAttributeOverrides]
                ( [EditAttributeOverride_Id] ,
                  [EditAttributeOverride_ExternalAlignmentEntityId] ,
                  [EditAttributeOverride_OldAttributeValue] ,
                  [EditAttributeOverride_NewAttributeValue] ,
                  [Request_Id] ,
                  [AttributeType_Id]
                )
                SELECT  [EditAttributeOverride_Id] ,
                        [EditAttributeOverride_AlignmentEntityId] ,
                        [EditAttributeOverride_OldAttributeValue] ,
                        [EditAttributeOverride_NewAttributeValue] ,
                        [Request_Id] ,
                        CAST([AttributeType_Id] AS INT)
                FROM    [dbo].[EditAttributeOverrides]
                ORDER BY [EditAttributeOverride_Id] ASC;
    END
DROP TABLE [dbo].[EditAttributeOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_EditAttributeOverrides]',
    N'EditAttributeOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_EditAttributeOverrides]',
    N'PK_EditAttributeOverrides', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[GroupedEntities]...';


CREATE TABLE [dbo].[tmp_ms_xx_GroupedEntities]
    (
      [GroupedEntities_Id] BIGINT NOT NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NOT NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NOT NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_PK_GroupedEntities] PRIMARY KEY CLUSTERED
        ( [GroupedEntities_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[GroupedEntities] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_GroupedEntities]
                ( GroupedEntities_Id ,
                  [AlignmentEntity_ExternalId] ,
                  [TerritoryType_ExternalId]
                )
                SELECT  ROW_NUMBER() OVER ( ORDER BY AlignmentEntity_Id ASC, [TerritoryType_Id] ASC ) ,
                        [AlignmentEntity_Id] ,
                        [TerritoryType_Id]
                FROM    [dbo].[GroupedEntities];
    END
DROP TABLE [dbo].[GroupedEntities];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_GroupedEntities]', N'GroupedEntities';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PK_GroupedEntities]',
    N'PK_GroupedEntities', N'OBJECT';




PRINT N'Starting rebuilding table [dbo].[IssueRepairCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_IssueRepairCache]
    (
      [IssueRepairCache_Id] BIGINT NOT NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_IssueRepairCache] PRIMARY KEY CLUSTERED
        ( [IssueRepairCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[IssueRepairCache] )
    BEGIN
 
        INSERT  INTO [dbo].[tmp_ms_xx_IssueRepairCache]
                ( [IssueRepairCache_Id] ,
                  [AlignmentEntity_ExternalId] ,
                  [TerritoryType_ExternalId]
                )
                SELECT  ROW_NUMBER() OVER ( ORDER BY issuerepaircache_id ASC ) ,
                        [AlignmentEntity_Id] ,
                        [TerritoryType_Id]
                FROM    [dbo].[IssueRepairCache]
                ORDER BY [IssueRepairCache_Id] ASC;
    END
DROP TABLE [dbo].[IssueRepairCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_IssueRepairCache]', N'IssueRepairCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_IssueRepairCache]',
    N'PK_IssueRepairCache', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[OrgUnitAlignmentEntitys]...';

CREATE PARTITION FUNCTION PF_OUAE(BIGINT) AS RANGE LEFT FOR VALUES (100,101,102,103,104,105,106,107,108,109,110,111,112,113,114,115,116,117,118,119,120,121,122,123,124,125,126,127,128,129,130,131,132,133,134,135,136,137,138,139,140,141,142,143,144,145,146,147,148,149,150,151,152,153,154,155,156,157,158,159,160,161,162,163,164,165,166,167,168,169,170,171,172,173,174,175,176,177,178,179,180,181,182,183,184,185,186,187,188,189,190,191,192,193,194,195,196,197,198,199,200,201,202,203,204,205,206,207,208,209,210,211,212,213,214,215,216,217,218,219,220,221,222,223,224,225,226,227,228,229,230,231,232,233,234,235,236,237,238,239,240,241,242,243,244,245,246,247,248,249,250,251,252,253,254,255,256,257,258,259,260,261,262,263,264,265,266,267,268,269,270,271,272,273,274,275,276,277,278,279,280,281,282,283,284,285,286,287,288,289,290,291,292,293,294,295,296,297,298,299,300,301,302,303,304,305,306,307,308,309,310,311,312,313,314,315,316,317,318,319,320,321,322,323,324,325,326,327,328,329,330,331,332,333,334,335,336,337,338,339,340,341,342,343,344,345,346,347,348,349,350,351,352,353,354,355,356,357,358,359,360,361,362,363,364,365,366,367,368,369,370,371,372,373,374,375,376,377,378,379,380,381,382,383,384,385,386,387,388,389,390,391,392,393,394,395,396,397,398,399,400,401,402,403,404,405,406,407,408,409,410,411,412,413,414,415,416,417,418,419,420,421,422,423,424,425,426,427,428,429,430,431,432,433,434,435,436,437,438,439,440,441,442,443,444,445,446,447,448,449,450,451,452,453,454,455,456,457,458,459,460,461,462,463,464,465,466,467,468,469,470,471,472,473,474,475,476,477,478,479,480,481,482,483,484,485,486,487,488,489,490,491,492,493,494,495,496,497,498,499,500,501,502,503,504,505,506,507,508,509,510,511,512,513,514,515,516,517,518,519,520,521,522,523,524,525,526,527,528,529,530,531,532,533,534,535,536,537,538,539,540,541,542,543,544,545,546,547,548,549,550,551,552,553,554,555,556,557,558,559,560,561,562,563,564,565,566,567,568,569,570,571,572,573,574,575,576,577,578,579,580,581,582,583,584,585,586,587,588,589,590,591,592,593,594,595,596,597,598,599)
CREATE PARTITION SCHEME PS_OUAE AS PARTITION PF_OUAE ALL TO ([PRIMARY])

CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys]
    (
      [OrgUnitAlignmentEntity_Id] BIGINT NOT NULL ,
      [OrgUnitAlignmentEntity_Weight] DECIMAL(5, 2) NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [OrgUnit_ExternalId] NVARCHAR(100) NULL ,
      [OrgUnit_Id] BIGINT NULL ,
	  [TerritoryType_Id] BIGINT NULL,
	  TerritoryType_ExternalId NVARCHAR(100) NULL,
      [OrgUnitAlignmentEntity_IsAutomatedAlignment] [BIT] NOT NULL
                                                          DEFAULT ( (0) )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitAlignmentEntitys] )
    BEGIN
        DECLARE @sqlStmt VARCHAR(MAX)
        IF EXISTS ( SELECT TOP 1
                            1
                    FROM    sys.columns
                    WHERE   OBJECT_NAME(object_id) = 'OrgUnitAlignmentEntitys'
                            AND name = 'OrgUnitAlignmentEntity_IsAutomatedAlignment' )
            BEGIN

                SET @sqlStmt = '
        INSERT INTO [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys] ([OrgUnitAlignmentEntity_Id], [OrgUnitAlignmentEntity_Weight], [AlignmentEntity_ExternalId], [OrgUnit_ExternalId], [OrgUnitAlignmentEntity_IsAutomatedAlignment])
        SELECT   [OrgUnitAlignmentEntity_Id],
                 [OrgUnitAlignmentEntity_Weight],
                 [AlignmentEntity_Id],OrgUnit_Id,
                
				 [OrgUnitAlignmentEntity_IsAutomatedAlignment]
        FROM     [dbo].[OrgUnitAlignmentEntitys] ouae' 
            END
        ELSE
            BEGIN
                SET @sqlStmt = '
	        INSERT INTO [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys] ([OrgUnitAlignmentEntity_Id], [OrgUnitAlignmentEntity_Weight], [AlignmentEntity_ExternalId], [OrgUnit_ExternalId])
        SELECT   [OrgUnitAlignmentEntity_Id],
                 [OrgUnitAlignmentEntity_Weight],
                 [AlignmentEntity_Id],
                 [OrgUnit_Id]
				 
        FROM     [dbo].[OrgUnitAlignmentEntitys]'
            END
        EXEC (@sqlStmt)
    END
DROP TABLE [dbo].[OrgUnitAlignmentEntitys];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys]',
    N'OrgUnitAlignmentEntitys';


PRINT N'Starting rebuilding table [dbo].[OrgUnitAttributeCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitAttributeCache]
    (
      [OrgUnitAttributeCache_Id] BIGINT NOT NULL ,
      [OrgUnitAttributeCache_Value] FLOAT(53) NULL ,
      [OrgUnit_ExternalId] NVARCHAR(100) NULL ,
      [OrgUnit_Id] BIGINT NULL ,
      [AttributeType_Id] INT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitAttributeCache] PRIMARY KEY CLUSTERED
        ( [OrgUnitAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitAttributeCache]
                ( [OrgUnitAttributeCache_Id] ,
                  [OrgUnitAttributeCache_Value] ,
                  [OrgUnit_ExternalId] ,
                  [AttributeType_Id]
                )
                SELECT  [OrgUnitAttributeCache_Id] ,
                        [OrgUnitAttributeCache_Value] ,
                        [OrgUnit_Id] ,
                        CAST([AttributeType_Id] AS INT)
                FROM    [dbo].[OrgUnitAttributeCache]
                ORDER BY [OrgUnitAttributeCache_Id] ASC;
    END
DROP TABLE [dbo].[OrgUnitAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitAttributeCache]',
    N'OrgUnitAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitAttributeCache]',
    N'PK_OrgUnitAttributeCache', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[OrgUnitOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitOverrides]
    (
      [OrgUnitOverride_Id] BIGINT NOT NULL ,
      [OrgUnitOverride_ExternalOrgUnitId] NVARCHAR(255) NULL ,
      [OrgUnitOverride_OrgUnitId] BIGINT NULL ,
      [OrgUnitOverride_ExternalParentOrgUnitId] NVARCHAR(255) NULL ,
      [OrgUnitOverride_ParentOrgUnitId] BIGINT NULL ,
      [OrgUnitOverride_RefinementOperationType] NVARCHAR(100) NULL ,
      [OrgUnitOverride_Name] NVARCHAR(255) NULL ,
      [OrgUnitOverride_LegacyId] NVARCHAR(255) NULL ,
      [Request_Id] BIGINT NULL ,
      [Level_Id] NVARCHAR(100) NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitOverrides] PRIMARY KEY CLUSTERED
        ( [OrgUnitOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitOverrides]
                ( [OrgUnitOverride_Id] ,
                  [OrgUnitOverride_ExternalOrgUnitId] ,
                  [OrgUnitOverride_ExternalParentOrgUnitId] ,
                  [OrgUnitOverride_RefinementOperationType] ,
                  [OrgUnitOverride_Name] ,
                  [OrgUnitOverride_LegacyId] ,
                  [Request_Id] ,
                  [Level_Id]
                )
                SELECT  [OrgUnitOverride_Id] ,
                        [OrgUnitOverride_OrgUnitId] ,
                        [OrgUnitOverride_ParentOrgUnitId] ,
                        [OrgUnitOverride_RefinementOperationType] ,
                        [OrgUnitOverride_Name] ,
                        [OrgUnitOverride_LegacyId] ,
                        [Request_Id] ,
                        [Level_Id]
                FROM    [dbo].[OrgUnitOverrides]
                ORDER BY [OrgUnitOverride_Id] ASC;
    END
DROP TABLE [dbo].[OrgUnitOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitOverrides]', N'OrgUnitOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitOverrides]',
    N'PK_OrgUnitOverrides', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[OrgUnitRelationshipOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitRelationshipOverrides]
    (
      [OrgUnitRelationshipOverride_Id] BIGINT NOT NULL ,
      [Request_Id] BIGINT NOT NULL ,
      [OrgUnitRelationshipOverride_ExternalDrivingOrgUnitId] NVARCHAR(255)
        NULL ,
      [OrgUnitRelationshipOverride_DrivingOrgUnitId] BIGINT NULL ,
      [NewOrgUnitRelationshipState_Id] INT NOT NULL ,
      [OldOrgUnitRelationshipState_Id] INT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitRelationshipOverrides] PRIMARY KEY CLUSTERED
        ( [OrgUnitRelationshipOverride_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitRelationshipOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitRelationshipOverrides]
                ( [OrgUnitRelationshipOverride_Id] ,
                  [Request_Id] ,
                  [OrgUnitRelationshipOverride_ExternalDrivingOrgUnitId] ,
                  [NewOrgUnitRelationshipState_Id] ,
                  [OldOrgUnitRelationshipState_Id]
                )
                SELECT  [OrgUnitRelationshipOverride_Id] ,
                        [Request_Id] ,
                        [OrgUnitRelationshipOverride_DrivingOrgUnitId] ,
                        [NewOrgUnitRelationshipState_Id] ,
                        [OldOrgUnitRelationshipState_Id]
                FROM    [dbo].[OrgUnitRelationshipOverrides]
                ORDER BY [OrgUnitRelationshipOverride_Id] ASC;
    END
DROP TABLE [dbo].[OrgUnitRelationshipOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitRelationshipOverrides]',
    N'OrgUnitRelationshipOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitRelationshipOverrides]',
    N'PK_OrgUnitRelationshipOverrides', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[OrgUnitRelationships]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitRelationships]
    (
      [OrgUnitRelationship_Id] BIGINT NOT NULL ,
      [Driving_ExternalOrgUnitId] NVARCHAR(100) NOT NULL ,
      [Driving_OrgUnitId] BIGINT NULL ,
      [Affected_ExternalOrgUnitId] NVARCHAR(100) NOT NULL ,
      [Affected_OrgUnitId] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitRelationships] PRIMARY KEY CLUSTERED
        ( [OrgUnitRelationship_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitRelationships] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitRelationships]
                ( [OrgUnitRelationship_Id] ,
                  [Driving_ExternalOrgUnitId] ,
                  [Affected_ExternalOrgUnitId]
                )
                SELECT  ROW_NUMBER() OVER ( ORDER BY Driving_OrgUnitId ASC ) ,
                        [Driving_OrgUnitId] ,
                        [Affected_OrgUnitId]
                FROM    [dbo].[OrgUnitRelationships];
    END
DROP TABLE [dbo].[OrgUnitRelationships];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitRelationships]',
    N'OrgUnitRelationships';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitRelationships]',
    N'PK_OrgUnitRelationships', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[OrgUnitRelationshipStateEntrys]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitRelationshipStateEntrys]
    (
      [OrgUnitRelationshipStateEntry_Id] BIGINT NOT NULL ,
      [OrgUnitRelationshipState_Id] INT NOT NULL ,
      [OrgUnitRelationshipStateEntry_ExternalAffectedOrgUnitId] NVARCHAR(255)
        NULL ,
      [OrgUnitRelationshipStateEntry_AffectedOrgUnitId] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitRelationshipStateEntrys] PRIMARY KEY CLUSTERED
        ( [OrgUnitRelationshipStateEntry_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitRelationshipStateEntrys] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitRelationshipStateEntrys]
                ( [OrgUnitRelationshipStateEntry_Id] ,
                  [OrgUnitRelationshipState_Id] ,
                  [OrgUnitRelationshipStateEntry_ExternalAffectedOrgUnitId]
                )
                SELECT  [OrgUnitRelationshipStateEntry_Id] ,
                        [OrgUnitRelationshipState_Id] ,
                        [OrgUnitRelationshipStateEntry_AffectedOrgUnitId]
                FROM    [dbo].[OrgUnitRelationshipStateEntrys]
                ORDER BY [OrgUnitRelationshipStateEntry_Id] ASC;
    END
DROP TABLE [dbo].[OrgUnitRelationshipStateEntrys];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitRelationshipStateEntrys]',
    N'OrgUnitRelationshipStateEntrys';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitRelationshipStateEntrys]',
    N'PK_OrgUnitRelationshipStateEntrys', N'OBJECT';




PRINT N'Starting rebuilding table [dbo].[RequestAlignmentEntityAttributeCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_RequestAlignmentEntityAttributeCache]
    (
      [RequestAlignmentEntityAttributeCache_Id] BIGINT NOT NULL ,
      [RequestAlignmentEntityAttributeCache_Value] FLOAT(53) NOT NULL ,
      [Request_Id] BIGINT NOT NULL ,
      [AttributeType_Id] INT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_RequestAlignmentEntityAttributeCache] PRIMARY KEY CLUSTERED
        ( [RequestAlignmentEntityAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[RequestAlignmentEntityAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_RequestAlignmentEntityAttributeCache]
                ( [RequestAlignmentEntityAttributeCache_Id] ,
                  [RequestAlignmentEntityAttributeCache_Value] ,
                  [Request_Id] ,
                  [AttributeType_Id] ,
                  [TerritoryType_ExternalId] ,
                  [AlignmentEntity_ExternalId]
                )
                SELECT  [RequestAlignmentEntityAttributeCache_Id] ,
                        [RequestAlignmentEntityAttributeCache_Value] ,
                        [Request_Id] ,
                        [AttributeType_Id] ,
                        [TerritoryType_Id] ,
                        [AlignmentEntity_Id]
                FROM    [dbo].[RequestAlignmentEntityAttributeCache]
                ORDER BY [RequestAlignmentEntityAttributeCache_Id] ASC;
    END
DROP TABLE [dbo].[RequestAlignmentEntityAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_RequestAlignmentEntityAttributeCache]',
    N'RequestAlignmentEntityAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_RequestAlignmentEntityAttributeCache]',
    N'PK_RequestAlignmentEntityAttributeCache', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[RequestOrgUnitAttributeCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_RequestOrgUnitAttributeCache]
    (
      [Id] BIGINT NOT NULL ,
      [AttributeType_Value] FLOAT(53) NOT NULL ,
      [Request_Id] BIGINT NOT NULL ,
      [OrgUnit_ExternalId] NVARCHAR(100) NULL ,
      [OrgUnit_Id] BIGINT NULL ,
      [AttributeType_Id] INT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_RequestOrgUnitAttributeCache] PRIMARY KEY CLUSTERED
        ( [Id] ASC ) WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[RequestOrgUnitAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_RequestOrgUnitAttributeCache]
                ( [Id] ,
                  [AttributeType_Value] ,
                  [Request_Id] ,
                  [OrgUnit_ExternalId] ,
                  [AttributeType_Id]
                )
                SELECT  [Id] ,
                        [AttributeType_Value] ,
                        [Request_Id] ,
                        [OrgUnit_Id] ,
                        CAST([AttributeType_Id] AS INT)
                FROM    [dbo].[RequestOrgUnitAttributeCache]
                ORDER BY [Id] ASC;
    END
DrOP TABLE [dbo].[RequestOrgUnitAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_RequestOrgUnitAttributeCache]',
    N'RequestOrgUnitAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_RequestOrgUnitAttributeCache]',
    N'PK_RequestOrgUnitAttributeCache', N'OBJECT';

PRINT 'Fixing geometry indexes due to upgrade'
ALTER INDEX [PK__Territor__676633B66F4A8121] ON [dbo].[TerritoryShapes] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)

ALTER INDEX [pk_TerritoryShapeOverrides] ON [dbo].[TerritoryShapeOverrides] REBUILD PARTITION = ALL WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, SORT_IN_TEMPDB = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)





PRINT N'Starting rebuilding table [dbo].[TerritoryShapeOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_TerritoryShapeOverrides]
    (
      [TerritoryShapeOverride_Id] BIGINT NOT NULL ,
      [OrgUnit_ExternalId] NVARCHAR(100) NOT NULL ,
      [OrgUnit_Id] BIGINT NULL ,
      [Position_ExternalId] NVARCHAR(100) NOT NULL ,
	  [Position_Id] BIGINT NULL,
      [geom] [sys].[GEOMETRY] NOT NULL ,
	  [MinLongitude] AS (CONVERT([float],[geom].[STEnvelope]().STPointN((1)).STX,(0))) PERSISTED,
		[MinLatitude] AS (CONVERT([float],[geom].[STEnvelope]().STPointN((1)).STY,(0))) PERSISTED,
		[MaxLongitude] AS (CONVERT([float],[geom].[STEnvelope]().STPointN((3)).STX,(0))) PERSISTED,
		[MaxLatitude] AS (CONVERT([float],[geom].[STEnvelope]().STPointN((3)).STY,(0))) PERSISTED,
		[geom_low] [sys].[geometry] NULL,
		[geom_medium] [sys].[geometry] NULL,
      CONSTRAINT [tmp_ms_xx_constraint_pk_TerritoryShapeOverrides] PRIMARY KEY CLUSTERED
        ( [TerritoryShapeOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[TerritoryShapeOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_TerritoryShapeOverrides]
                ( territoryshapeoverride_id ,
                  [OrgUnit_ExternalId] ,
                  [Position_ExternalId] ,
                  [geom],Geom_low,geom_medium
                )
                SELECT  ROW_NUMBER() OVER ( ORDER BY OrgUnit_Id ASC ) ,
                        [OrgUnit_Id] ,
                        [Position_Id] ,
                        [geom], geom_low,geom_medium
                FROM    [dbo].[TerritoryShapeOverrides];
    END
DROP TABLE [dbo].[TerritoryShapeOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_TerritoryShapeOverrides]',
    N'TerritoryShapeOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_pk_TerritoryShapeOverrides]',
    N'PK_TerritoryShapeOverrides', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[TerritoryShapes]...';


CREATE TABLE [dbo].[tmp_ms_xx_TerritoryShapes]
    (
      [TerritoryShape_Id] BIGINT NOT NULL ,
      [OrgUnit_ExternalId] NVARCHAR(255) NOT NULL ,
      [OrgUnit_Id] BIGINT NULL ,
      [geom] [sys].[GEOMETRY] NULL ,
      [MinLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STX, ( 0 )) )
        PERSISTED ,
      [MinLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 1 )).STY, ( 0 )) )
        PERSISTED ,
      [MaxLongitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STX, ( 0 )) )
        PERSISTED ,
      [MaxLatitude] AS ( CONVERT([FLOAT], [geom].[STEnvelope]().STPointN(( 3 )).STY, ( 0 )) )
        PERSISTED ,
		[geom_low] [sys].[geometry] NULL,
		[geom_medium] [sys].[geometry] NULL,
      CONSTRAINT [tmp_ms_xx_constraint_pk_TerritoryShapes] PRIMARY KEY CLUSTERED
        ( [TerritoryShape_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[TerritoryShapes] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_TerritoryShapes]
                ( TerritoryShape_Id ,
                  [OrgUnit_ExternalId] ,
                  [geom],Geom_low,geom_medium
                )
                SELECT  ROW_NUMBER() OVER ( ORDER BY OrgUnit_Id ) ,
                        [OrgUnit_Id] ,
                        [geom],Geom_low,geom_medium
                FROM    [dbo].[TerritoryShapes]
                ORDER BY [OrgUnit_Id] ASC;
    END
DROP TABLE [dbo].[TerritoryShapes];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_TerritoryShapes]', N'TerritoryShapes';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_pk_TerritoryShapes]',
    N'PK_TerritoryShapes', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[TerritoryTypePermissions]...';


CREATE TABLE [dbo].[tmp_ms_xx_TerritoryTypePermissions]
    (
      [TerritoryTypePermission_Id] BIGINT NOT NULL ,
      [TerritoryTypePermission_IsEnabled] BIT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      [Permission_Id] BIGINT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_TerritoryTypePermissions] PRIMARY KEY CLUSTERED
        ( [TerritoryTypePermission_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[TerritoryTypePermissions] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_TerritoryTypePermissions]
                ( [TerritoryTypePermission_Id] ,
                  [TerritoryTypePermission_IsEnabled] ,
                  [TerritoryType_ExternalId] ,
                  [Permission_Id]
                )
                SELECT  [TerritoryTypePermission_Id] ,
                        [TerritoryTypePermission_IsEnabled] ,
                        [TerritoryType_Id] ,
                        [Permission_Id]
                FROM    [dbo].[TerritoryTypePermissions]
                ORDER BY [TerritoryTypePermission_Id] ASC
    END
DROP TABLE [dbo].[TerritoryTypePermissions];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_TerritoryTypePermissions]',
    N'TerritoryTypePermissions';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_TerritoryTypePermissions]',
    N'PK_TerritoryTypePermissions', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[UpsertAlignmentEntityOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_UpsertAlignmentEntityOverrides]
    (
      [UpsertAlignmentEntityOverride_Id] BIGINT NOT NULL ,
      [UpsertAlignmentEntityOverride_ExternalAlignmentEntityId] NVARCHAR(100)
        NULL ,
      [UpsertAlignmentEntityOverride_AlignmentEntityId] BIGINT NULL ,
      [UpsertAlignmentEntityOverride_UpsertOperationType] NVARCHAR(100) NULL ,
      [Request_Id] BIGINT NOT NULL ,
      [AlignmentEntityType_Id] BIGINT NULL ,
      [CustomerType_Id] BIGINT NULL ,
      [NewAlignmentEntityState_Id] BIGINT NULL ,
      [OldAlignmentEntityState_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_UpsertAlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( [UpsertAlignmentEntityOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[UpsertAlignmentEntityOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_UpsertAlignmentEntityOverrides]
                ( [UpsertAlignmentEntityOverride_Id] ,
                  [UpsertAlignmentEntityOverride_ExternalAlignmentEntityId] ,
                  [UpsertAlignmentEntityOverride_UpsertOperationType] ,
                  [Request_Id] ,
                  [AlignmentEntityType_Id] ,
                  [CustomerType_Id] ,
                  [NewAlignmentEntityState_Id] ,
                  [OldAlignmentEntityState_Id]
                )
                SELECT  [UpsertAlignmentEntityOverride_Id] ,
                        [UpsertAlignmentEntityOverride_AlignmentEntityId] ,
                        [UpsertAlignmentEntityOverride_UpsertOperationType] ,
                        [Request_Id] ,
                        [AlignmentEntityType_Id] ,
                        [CustomerType_Id] ,
                        [NewAlignmentEntityState_Id] ,
                        [OldAlignmentEntityState_Id]
                FROM    [dbo].[UpsertAlignmentEntityOverrides]
                ORDER BY [UpsertAlignmentEntityOverride_Id] ASC;
    END
DROP TABLE [dbo].[UpsertAlignmentEntityOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_UpsertAlignmentEntityOverrides]',
    N'UpsertAlignmentEntityOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_UpsertAlignmentEntityOverrides]',
    N'PK_UpsertAlignmentEntityOverrides', N'OBJECT';



PRINT 'Updating DeleteAlignmentEntityOverrides'

CREATE TABLE dbo.tmp_ms_xx_DeleteAlignmentEntityOverrides
    (
      DeleteAlignmentEntityOverride_Id BIGINT NOT NULL ,
      Request_Id BIGINT NOT NULL ,
      DeleteAlignmentEntityOverride_ExternalAlignmentEntityId NVARCHAR(100)
        NOT NULL ,
      DeleteAlignmentEntityOverride_AlignmentEntityId BIGINT NULL ,
      AlignmentEntityType_Id BIGINT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_DeleteAlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( DeleteAlignmentEntityOverride_Id ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    )
ON  [PRIMARY]
IF EXISTS ( SELECT TOP 1
                    1
            FROM    dbo.DeleteAlignmentEntityOverrides )
    BEGIN
        INSERT  INTO dbo.tmp_ms_xx_DeleteAlignmentEntityOverrides
                ( DeleteAlignmentEntityOverride_Id ,
                  Request_Id ,
                  DeleteAlignmentEntityOverride_ExternalAlignmentEntityId ,
                  AlignmentEntityType_Id
                )
                SELECT  DeleteAlignmentEntityOverride_Id ,
                        Request_Id ,
                        DeleteAlignmentEntityOverride_AlignmentEntityId ,
                        AlignmentEntityType_Id
                FROM    dbo.DeleteAlignmentEntityOverrides 
    END
DROP TABLE dbo.DeleteAlignmentEntityOverrides
EXECUTE sp_rename N'dbo.tmp_ms_xx_DeleteAlignmentEntityOverrides',
    N'DeleteAlignmentEntityOverrides'
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_DeleteAlignmentEntityOverrides]',
    N'[PK_DeleteAlignmentEntityOverrides', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[ApprovalProgress]...';


CREATE TABLE [dbo].[tmp_ms_xx_ApprovalProgress] (
    [ApprovalProgress_Id]          BIGINT         NOT NULL,
    [ApprovalProgress_ActionTaken] NVARCHAR (100) NULL,
    [Position_ExternalId]                  NVARCHAR (100) NULL,
    [Position_Id]           BIGINT         NULL,
    CONSTRAINT [tmp_ms_xx_PK_ApprovalProgress] PRIMARY KEY CLUSTERED ([ApprovalProgress_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ApprovalProgress])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_ApprovalProgress] ([ApprovalProgress_Id], [ApprovalProgress_ActionTaken], [Position_ExternalId])
        SELECT   [ApprovalProgress_Id],
                 [ApprovalProgress_ActionTaken],
                 [Position_Id]
        FROM     [dbo].[ApprovalProgress]
        ORDER BY [ApprovalProgress_Id] ASC;
    END

DROP TABLE [dbo].[ApprovalProgress];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ApprovalProgress]', N'ApprovalProgress';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PK_ApprovalProgress]', N'PK_ApprovalProgress', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[PositionOverrides]...';
CREATE TABLE [dbo].[tmp_ms_xx_PositionOverrides] (
    [PositionOverride_Id]                      BIGINT         NOT NULL,
    [PositionOverride_RefinementOperationType] NVARCHAR (100) NULL,
    [PositionOverride_LegacyId]                NVARCHAR (255) NULL,
    [PositionOverride_ExternalOrgUnitId]               NVARCHAR (255) NULL,
    [PositionOverride_OrgUnitId]        BIGINT         NULL,
    [PositionOverride_ExternalPositionId]              NVARCHAR (255) NULL,
    [PositionOverride_PositionId]       BIGINT         NULL,
    [PositionOverride_PositionEffort]          DECIMAL (5, 2) CONSTRAINT DF_tmp_ms_xx_PositionOverrides_PositionOverride_PositionEffort DEFAULT (100) NOT NULL,
    [Request_Id]                               BIGINT         NOT NULL,
    [Role_Id]                                  NVARCHAR (100) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_PositionOverrides] PRIMARY KEY CLUSTERED ([PositionOverride_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[PositionOverrides])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_PositionOverrides] ([PositionOverride_Id], [PositionOverride_RefinementOperationType], [PositionOverride_LegacyId], [PositionOverride_ExternalOrgUnitId], [PositionOverride_ExternalPositionId], [PositionOverride_PositionEffort], [Request_Id], [Role_Id])
        SELECT   [PositionOverride_Id],
                 [PositionOverride_RefinementOperationType],
                 [PositionOverride_LegacyId],
                 [PositionOverride_OrgUnitId],
                 [PositionOverride_PositionId],
                 [PositionOverride_PositionEffort],
                 [Request_Id],
                 [Role_Id]
        FROM     [dbo].[PositionOverrides]
        ORDER BY [PositionOverride_Id] ASC;
    END

DROP TABLE [dbo].[PositionOverrides];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PositionOverrides]', N'PositionOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_PositionOverrides]', N'PK_PositionOverrides', N'OBJECT';
EXECUTE sp_rename N'[dbo].[DF_tmp_ms_xx_PositionOverrides_PositionOverride_PositionEffort]', N'DF_PositionOverrides_PositionOverride_PositionEffort', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[PositionPersonnel]...';
CREATE TABLE [dbo].[tmp_ms_xx_PositionPersonnel] (
    [PositionPersonnel_Id] BIGINT             NOT NULL,
    [EffectiveDate]        DATETIMEOFFSET (7) NULL,
    [Position_ExternalId]          NVARCHAR (100)     NULL,
    [Position_Id]   BIGINT             NULL,
    [Personnel_Id]         NVARCHAR (100)     NULL,

    CONSTRAINT [tmp_ms_xx_constraint_PK_PositionPersonnel] PRIMARY KEY CLUSTERED ([PositionPersonnel_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[PositionPersonnel])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_PositionPersonnel] ([PositionPersonnel_Id], [EffectiveDate], [Position_ExternalId], [Personnel_Id])
        SELECT   [PositionPersonnel_Id],
                 [EffectiveDate],
                 [Position_Id],
                 [Personnel_Id]
        FROM     [dbo].[PositionPersonnel]
        ORDER BY [PositionPersonnel_Id] ASC;
    END

DROP TABLE [dbo].[PositionPersonnel];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PositionPersonnel]', N'PositionPersonnel';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_PositionPersonnel]', N'PK_PositionPersonnel', N'OBJECT';



PRINT N'Starting rebuilding table [dbo].[PositionPersonnelOverrides]...';
CREATE TABLE [dbo].[tmp_ms_xx_PositionPersonnelOverrides] (
    [PositionPersonnelOverride_Id]                      BIGINT             NOT NULL,
    [EffectiveDate]                                     DATETIMEOFFSET (7) NULL,
    [PositionPersonnelOverride_RefinementOperationType] NVARCHAR (100)     NULL,
    [PositionPersonnelOverride_ExternalPositionId]              NVARCHAR (255)     NULL,
    [PositionPersonnelOverride_PositionId]       BIGINT             NULL,
    [Personnel_Id]                                      NVARCHAR (100)     NULL,
    [Request_Id]                                        BIGINT             NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_PositionPersonnelOverrides] PRIMARY KEY CLUSTERED ([PositionPersonnelOverride_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[PositionPersonnelOverrides])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_PositionPersonnelOverrides] ([PositionPersonnelOverride_Id], [EffectiveDate], [PositionPersonnelOverride_RefinementOperationType], [PositionPersonnelOverride_ExternalPositionId], [Personnel_Id], [Request_Id])
        SELECT   [PositionPersonnelOverride_Id],
                 [EffectiveDate],
                 [PositionPersonnelOverride_RefinementOperationType],
                 [PositionPersonnelOverride_PositionId],
                 [Personnel_Id],
                 [Request_Id]
        FROM     [dbo].[PositionPersonnelOverrides]
        ORDER BY [PositionPersonnelOverride_Id] ASC;
    END

DROP TABLE [dbo].[PositionPersonnelOverrides];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PositionPersonnelOverrides]', N'PositionPersonnelOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_PositionPersonnelOverrides]', N'PK_PositionPersonnelOverrides', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[ProductPositionOverrides]...';
CREATE TABLE [dbo].[tmp_ms_xx_ProductPositionOverrides] (
    [ProductPositionOverride_Id]                BIGINT         NOT NULL,
    [ProductPositionOverride_ExternalPositionId]        NVARCHAR (255) NOT NULL,
    [ProductPositionOverride_PositionId] BIGINT         NULL,
    [Request_Id]                                BIGINT         NOT NULL,
    [OldProductBasket_Id]                       BIGINT         NULL,
    [NewProductBasket_Id]                       BIGINT         NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_ProductPositionOverrides] PRIMARY KEY CLUSTERED ([ProductPositionOverride_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ProductPositionOverrides])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_ProductPositionOverrides] ([ProductPositionOverride_Id], [ProductPositionOverride_ExternalPositionId], [Request_Id], [OldProductBasket_Id], [NewProductBasket_Id])
        SELECT   [ProductPositionOverride_Id],
                 [ProductPositionOverride_PositionId],
                 [Request_Id],
                 [OldProductBasket_Id],
                 [NewProductBasket_Id]
        FROM     [dbo].[ProductPositionOverrides]
        ORDER BY [ProductPositionOverride_Id] ASC;
    END

DROP TABLE [dbo].[ProductPositionOverrides];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ProductPositionOverrides]', N'ProductPositionOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ProductPositionOverrides]', N'PK_ProductPositionOverrides', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[ProductPositions]...';
CREATE TABLE [dbo].[tmp_ms_xx_ProductPositions] (
    [ProductPosition_Id]     BIGINT         NOT NULL,
    [ProductPosition_Weight] DECIMAL (5, 2) NOT NULL,
    [Position_ExternalId]            NVARCHAR (100) NOT NULL,
    [Position_Id]     BIGINT         NULL,
    [Product_Id]             NVARCHAR (100) NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_ProductPositions] PRIMARY KEY CLUSTERED ([ProductPosition_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ProductPositions])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_ProductPositions] ([ProductPosition_Id], [ProductPosition_Weight], [Position_ExternalId], [Product_Id])
        SELECT   [ProductPosition_Id],
                 [ProductPosition_Weight],
                 [Position_Id],
                 [Product_Id]
        FROM     [dbo].[ProductPositions]
        ORDER BY [ProductPosition_Id] ASC;
    END

DROP TABLE [dbo].[ProductPositions];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ProductPositions]', N'ProductPositions';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ProductPositions]', N'PK_ProductPositions', N'OBJECT';




PRINT N'Starting rebuilding table [dbo].[OrgUnitLevelPermissions]...';









CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitLevelPermissions] (
    [OrgUnitLevelPermission_Id]        BIGINT         NOT NULL,
    [OrgUnitLevelPermission_IsEnabled] BIT            NULL,
    [OrgUnitLevel_ExternalId]                  NVARCHAR (100) NULL,
    [OrgUnitLevel_Id]           BIGINT         NULL,
    [Permission_Id]                    BIGINT         NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitLevelPermissions] PRIMARY KEY CLUSTERED ([OrgUnitLevelPermission_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[OrgUnitLevelPermissions])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_OrgUnitLevelPermissions] ([OrgUnitLevelPermission_Id], [OrgUnitLevelPermission_IsEnabled], [OrgUnitLevel_ExternalId], [Permission_Id])
        SELECT   [OrgUnitLevelPermission_Id],
                 [OrgUnitLevelPermission_IsEnabled],
                 [OrgUnitLevel_Id],
                 [Permission_Id]
        FROM     [dbo].[OrgUnitLevelPermissions]
        ORDER BY [OrgUnitLevelPermission_Id] ASC;
    END

DROP TABLE [dbo].[OrgUnitLevelPermissions];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitLevelPermissions]', N'OrgUnitLevelPermissions';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitLevelPermissions]', N'PK_OrgUnitLevelPermissions', N'OBJECT';

PRINT N'Starting rebuilding table [dbo].[OrgUnitLevelRoles]...';

CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitLevelRoles] (
    [OrgUnitLevelRole_Id]          BIGINT         NOT NULL,
    [OrgUnitLevelRole_MaxPosition] INT            NULL,
    [OrgUnitLevelRole_MinPosition] INT            NULL,
    [UsesEffort]                   BIT            CONSTRAINT DF_tmp_ms_xx_OrgUnitLevelRoles_UsesEffort DEFAULT ((0)) NOT NULL,
    [OrgUnitLevel_ExternalId]              NVARCHAR (100) NULL,
    [OrgUnitLevel_Id]       BIGINT         NULL,
    [Role_Id]                      NVARCHAR (100) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitLevelRoles] PRIMARY KEY CLUSTERED ([OrgUnitLevelRole_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[OrgUnitLevelRoles])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_OrgUnitLevelRoles] ([OrgUnitLevelRole_Id], [OrgUnitLevelRole_MaxPosition], [OrgUnitLevelRole_MinPosition], [UsesEffort], [OrgUnitLevel_ExternalId], [Role_Id])
        SELECT   [OrgUnitLevelRole_Id],
                 [OrgUnitLevelRole_MaxPosition],
                 [OrgUnitLevelRole_MinPosition],
                 [UsesEffort],
                 [OrgUnitLevel_Id],
                 [Role_Id]
        FROM     [dbo].[OrgUnitLevelRoles]
        ORDER BY [OrgUnitLevelRole_Id] ASC;
    END

DROP TABLE [dbo].[OrgUnitLevelRoles];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitLevelRoles]', N'OrgUnitLevelRoles';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitLevelRoles]', N'PK_OrgUnitLevelRoles', N'OBJECT';
EXECUTE sp_rename N'[dbo].[DF_tmp_ms_xx_OrgUnitLevelRoles_UsesEffort]', N'DF_OrgUnitLevelRoles_UsesEffort', N'OBJECT';


PRINT N'Starting rebuilding table [dbo].[Customers]...';

CREATE TABLE [dbo].[tmp_ms_xx_Customers] (
    [Customer_Id]         BIGINT NOT NULL,
    [Customer_ExternalId]  NVARCHAR(100)   NOT NULL,
    [Customer_Name]       NVARCHAR (255) NULL,
    [Customer_Address1]   NVARCHAR (255) NULL,
    [Customer_Address2]   NVARCHAR (255) NULL,
    [Customer_City]       NVARCHAR (255) NULL,
    [Customer_State]      NVARCHAR (255) NULL,
    [Customer_Country]    NVARCHAR (255) NULL,
    [Customer_Latitude]   FLOAT (53)     NULL,
    [Customer_Longitude]  FLOAT (53)     NULL,
    [Customer_Quality]    INT            NULL,
    [Customer_PostalCode] NVARCHAR (255) NULL,
    [Customer_Status]     NVARCHAR (255) NULL,
    [CustomerType_Id]     BIGINT         NOT NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_Customers] PRIMARY KEY CLUSTERED ([Customer_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Customers])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Customers] ([Customer_Id], [Customer_ExternalId],[Customer_Name], [Customer_Address1], [Customer_Address2], [Customer_City], [Customer_State], [Customer_Country], [Customer_Latitude], [Customer_Longitude], [Customer_Quality], [Customer_PostalCode], [Customer_Status], [CustomerType_Id])
        SELECT ae.AlignmentEntity_Id, [Customer_Id],
               [Customer_Name],
               [Customer_Address1],
               [Customer_Address2],
               [Customer_City],
               [Customer_State],
               [Customer_Country],
               [Customer_Latitude],
               [Customer_Longitude],
               [Customer_Quality],
               [Customer_PostalCode],
               [Customer_Status],
               [CustomerType_Id]
        FROM   [dbo].[Customers] c
		INNER JOIN dbo.AlignmentEntitys ae ON c.Customer_Id=AlignmentEntity_ExternalId;
    END

DROP TABLE [dbo].[Customers];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Customers]', N'Customers';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_Customers]', N'PK_Customers', N'OBJECT';



PRINT N'Starting rebuilding table [dbo].[Geos]...';

CREATE TABLE [dbo].[tmp_ms_xx_Geos] (
    [Geo_Id]        BIGINT NOT NULL,
    [Geo_ExternalId] NVARCHAR(100) NOT NULL,
    [Geo_Name]      NVARCHAR (255) NULL,
    [Geo_Status]    NVARCHAR (255) NULL,
    CONSTRAINT [tmp_ms_xx_PK_Geos] PRIMARY KEY CLUSTERED ([Geo_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[Geos])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_Geos] (Geo_Id, [Geo_ExternalId], [Geo_Name], [Geo_Status])
        SELECT ae.AlignmentEntity_Id,
		g.[Geo_Id],
               [Geo_Name],
               [Geo_Status]
        FROM   [dbo].[Geos] g
		INNER JOiN dbo.AlignmentEntitys ae ON g.Geo_Id=ae.AlignmentEntity_ExternalId
    END

DROP TABLE [dbo].[Geos];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_Geos]', N'Geos';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PK_Geos]', N'PK_Geos', N'OBJECT';



PRINT N'Starting rebuilding table [dbo].[AlignmentEntityStates]...';

CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityStates] (
    [AlignmentEntityState_Id]              BIGINT         NOT NULL,
    [AlignmentEntityState_Name]            NVARCHAR (255) NULL,
    [AlignmentEntityState_ParentExternalId]        NVARCHAR (255) NULL,
    [AlignmentEntityState_ParentId] BIGINT         NULL,
    [AlignmentEntityState_Address1]        NVARCHAR (255) NULL,
    [AlignmentEntityState_Address2]        NVARCHAR (255) NULL,
    [AlignmentEntityState_City]            NVARCHAR (255) NULL,
    [AlignmentEntityState_State]           NVARCHAR (255) NULL,
    [AlignmentEntityState_Country]         NVARCHAR (255) NULL,
    [AlignmentEntityState_Latitude]        FLOAT (53)     NULL,
    [AlignmentEntityState_Longitude]       FLOAT (53)     NULL,
    [AlignmentEntityState_PostalCode]      NVARCHAR (255) NULL,
    [AlignmentEntityState_Quality]         INT            NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityStates] PRIMARY KEY CLUSTERED ([AlignmentEntityState_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[AlignmentEntityStates])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_AlignmentEntityStates] ([AlignmentEntityState_Id], [AlignmentEntityState_Name], [AlignmentEntityState_ParentId], [AlignmentEntityState_Address1], [AlignmentEntityState_Address2], [AlignmentEntityState_City], [AlignmentEntityState_State], [AlignmentEntityState_Country], [AlignmentEntityState_Latitude], [AlignmentEntityState_Longitude], [AlignmentEntityState_PostalCode], [AlignmentEntityState_Quality])
        SELECT   [AlignmentEntityState_Id],
                 [AlignmentEntityState_Name],
                 [AlignmentEntityState_ParentId],
                 [AlignmentEntityState_Address1],
                 [AlignmentEntityState_Address2],
                 [AlignmentEntityState_City],
                 [AlignmentEntityState_State],
                 [AlignmentEntityState_Country],
                 [AlignmentEntityState_Latitude],
                 [AlignmentEntityState_Longitude],
                 [AlignmentEntityState_PostalCode],
                 [AlignmentEntityState_Quality]
        FROM     [dbo].[AlignmentEntityStates]
        ORDER BY [AlignmentEntityState_Id] ASC;
    END

DROP TABLE [dbo].[AlignmentEntityStates];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityStates]', N'AlignmentEntityStates';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityStates]', N'PK_AlignmentEntityStates', N'OBJECT';


CREATE TABLE dbo.Tmp_AttributeTypes
	(
	AttributeType_Id int NOT NULL,
	AttributeType_Name nvarchar(255) NULL,
	AttributeType_DisplayName nvarchar(255) NULL,
	AttributeType_DataType nvarchar(100) NOT NULL,
	AttributeType_Category nvarchar(100) NULL,
	AttributeType_Description nvarchar(255) NULL,
	AttributeType_Formula nvarchar(255) NULL,
	AttributeType_Entity nvarchar(255) NULL,
	AttributeType_Status nvarchar(100) NOT NULL,
	AttributeType_ValidationType nvarchar(100) NULL,
	AttributeType_MetricAvailable bit NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_AttributeTypes] PRIMARY KEY CLUSTERED (AttributeType_Id ASC) WITH (DATA_COMPRESSION = PAGE)

	)  ON [PRIMARY]
ALTER TABLE dbo.Tmp_AttributeTypes SET (LOCK_ESCALATION = TABLE)

IF EXISTS(SELECT * FROM dbo.AttributeTypes)
	 EXEC('INSERT INTO dbo.Tmp_AttributeTypes (AttributeType_Id, AttributeType_Name, AttributeType_DisplayName, AttributeType_DataType, AttributeType_Category, AttributeType_Description, AttributeType_Formula, AttributeType_Entity, AttributeType_Status, AttributeType_ValidationType, AttributeType_MetricAvailable)
		SELECT CONVERT(int, AttributeType_Id), AttributeType_Name, AttributeType_DisplayName, AttributeType_DataType, AttributeType_Category, AttributeType_Description, AttributeType_Formula, AttributeType_Entity, AttributeType_Status, AttributeType_ValidationType, AttributeType_MetricAvailable FROM dbo.AttributeTypes WITH (HOLDLOCK TABLOCKX)')
DROP TABLE dbo.AttributeTypes

EXECUTE sp_rename N'dbo.Tmp_AttributeTypes', N'AttributeTypes', 'OBJECT' 
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AttributeTypes]', N'PK_AttributeTypes', N'OBJECT';
