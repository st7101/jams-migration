INSERT INTO [dbo].[OrgUnitLevels]
           ([OrgUnitLevel_Id]
           ,[OrgUnitLevel_ExternalId]
           ,[OrgUnitLevel_Name]
           ,[UsesOrgUnitLegacyId]
           ,[UsesPositionLegacyId]
           ,[OrgUnitLevel_IsLeafLevel]
           ,[OrgUnitLevel_ParentExternalId]
)
SELECT 
	ROW_NUMBER() OVER (ORDER BY ORGUNITLEVEL_ID),
	ouTemp.ORGUNITLEVEL_ID,
	ouTemp.ORGUNITLEVEL_NAME,
	ouTemp.USESORGUNITLEGACYID,
	ouTemp.USESPOSITIONLEGACYID,
	ouTemp.ORGUNITLEVEL_ISLEAFLEVEL,
	ouTemp.ORGUNITLEVEL_PARENTID
FROM dbo.OrgUnitLevels_Temp ouTemp
GO

UPDATE oul
SET OrgUnitLevel_ParentId =oulParent.OrgUnitLevel_Id
FROM dbo.[OrgUnitLevels] oul
INNER JOIN dbo.[OrgUnitLevels] oulParent ON oul.[OrgUnitLevel_ParentExternalId] =oulParent.OrgUnitLevel_ExternalId
WHERE oul.OrgUnitLevel_ParentExternalId IS NOT NULL



GO


