
PRINT N'Updating AlignmentEntityStates ...';

UPDATE ase
SET [AlignmentStateEntry_OrgUnitId]=ou.OrgUnit_Id
FROM dbo.AlignmentStateEntrys ase INNER JOIN 
dbo.OrgUnits ou ON ase.AlignmentStateEntry_ExternalOrgUnitId=ou.OrgUnit_ExternalID
