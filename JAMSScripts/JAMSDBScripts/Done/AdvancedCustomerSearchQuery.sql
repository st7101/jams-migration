
EXECUTE  [dbo].[AdvancedCustomerSearch] 
   @territoryTypeId=:terrTypeId
  ,@aeStatus=:aeStatus
  ,@PageSize=:pageSize
  ,@GroupedEntityTypeIds=:groupedAlignmentEntityTypeIds
  ,@IndependantEntityTypeIds=:independentAlignmentEntityTypeIds
  ,@SearchString=:searchString
  ,@totalNumberOfRowsShown=:totalNumberOfRowsShown
  ,@spanRestriction=:spanRestriction
  ,@treeLeft=:treeLeft
  ,@treeRight = :treeRight
  ,@sortStatement=:sortStatement
  ,@lastShownRow=:lastShownRow



