--ALTER TABLE [OrgUnitOverrides] DROP COLUMN [OrgUnitOverride_OrgUnitExternalId]
--ALTER TABLE [PositionOverrides] DROP COLUMN [PositionOverride_PositionExternalId]
--ALTER TABLE [UpsertAlignmentEntityOverrides] DROP COLUMN [UpsertAlignmentEntityOverride_AlignmentEntityExternalId]
ALTER TABLE [AlignmentEntitys] DROP COLUMN [AlignmentEntity_ParentExternalId] 
ALTER TABLE [AlignmentEntityOverrides] DROP COLUMN [AlignmentEntity_externalid],[TerritoryType_externalid]
ALTER TABLE [AlignmentEntityStates] DROP COLUMN [AlignmentEntityState_ParentExternalId]
ALTER TABLE [AlignmentEntityTypeTerritoryTypes] DROP COLUMN [TerritoryType_externalid]
ALTER TABLE [AlignmentStateEntrys] DROP COLUMN [AlignmentStateEntry_ExternalOrgUnitId]
ALTER TABLE [ApprovalProgress] DROP COLUMN [Position_externalid]
ALTER TABLE [ApprovalProgressTerritoryTypes] DROP COLUMN [TerritoryType_externalid]
ALTER TABLE [AttributeTypeTerritoryTypes] DROP COLUMN [TerritoryType_externalid]
ALTER TABLE [Customers] DROP COLUMN [Customer_ExternalId]
ALTER TABLE [EditAttributeOverrides] DROP COLUMN [EditAttributeOverride_ExternalAlignmentEntityId]
ALTER TABLE [Geos] DROP COLUMN [Geo_ExternalId]
ALTER TABLE dbo.OrgUnitLevelRoles DROP COLUMN OrgUnitLevel_ExternalId
ALTER TABLE dbo.OrgUnitLevels DROP COLUMN [OrgUnitLevel_ParentExternalId] 
ALTER TABLE dbo.OrgUnitLevelPermissions DROP COLUMN OrgUnitLevel_ExternalId
ALTER TABLE orgunits DROP COLUMN OrgUnitLevel_ExternalId, [OrgUnit_ParentExternalId] 
ALTER TABLE OrgUnitAlignmentEntitys DROP COLUMN [AlignmentEntity_externalid],[OrgUnit_ExternalId]
ALTER TABLE OrgUnitAttributeCache DROP COLUMN [OrgUnit_ExternalId]
ALTER TABLE [IssueRepairCache] DROP COLUMN [AlignmentEntity_externalid],[TerritoryType_externalid]
ALTER TABLE [OrgUnitOverrides] DROP COLUMN [OrgUnitOverride_ExternalParentOrgUnitId]
ALTER TABLE [OrgUnitRelationshipOverrides] DROP COLUMN [OrgUnitRelationshipOverride_ExternalDrivingOrgUnitId]
ALTER TABLE [OrgUnitRelationships] DROP COLUMN [Driving_ExternalOrgUnitId],[Affected_ExternalOrgUnitId]
ALTER TABLE [OrgUnitRelationshipStateEntrys] DROP COLUMN [OrgUnitRelationshipStateEntry_ExternalAffectedOrgUnitId]
ALTER TABLE [PositionOverrides] DROP COLUMN [PositionOverride_ExternalOrgUnitId]
ALTER TABLE [PositionPersonnel] DROP COLUMN [Position_externalid]
ALTER TABLE [PositionPersonnelOverrides] DROP COLUMN [PositionPersonnelOverride_ExternalPositionId]
ALTER TABLE [Positions] DROP COLUMN [OrgUnit_ExternalId]
ALTER TABLE [ProductPositionOverrides] DROP COLUMN [ProductPositionOverride_ExternalPositionId]
ALTER TABLE [ProductPositions] DROP COLUMN [Position_Externalid]
ALTER TABLE [RequestAlignmentEntityAttributeCache] DROP COLUMN [TerritoryType_ExternalId],[AlignmentEntity_externalid]
ALTER TABLE [RequestOrgUnitAttributeCache] DROP COLUMN [OrgUnit_Externalid]
ALTER TABLE [TerritoryShapeOverrides] DROP COLUMN [OrgUnit_ExternalId],[Position_ExternalId]
ALTER TABLE [TerritoryShapes] DROP COLUMN [OrgUnit_ExternalId]
ALTER TABLE [TerritoryTypePermissions] DROP COLUMN [TerritoryType_externalid]
ALTER TABLE [TerritoryTypes] DROP COLUMN [UnassignedOrgUnit_ExternalId], [OrgUnitLevel_ExternalId]
ALTER TABLE DeleteAlignmentEntityOverrides DROP COLUMN DeleteAlignmentEntityOverride_ExternalAlignmentEntityId