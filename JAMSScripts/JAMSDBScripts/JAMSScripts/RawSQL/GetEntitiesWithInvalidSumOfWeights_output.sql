SELECT DISTINCT ResultTable.AlignmentEntity_Id
FROM (SELECT Temp.AlignmentEntity_Id AS AlignmentEntity_Id, Temp.TerritoryType_Id AS TerritoryType_Id 
	  FROM (SELECT OAE.AlignmentEntity_Id, OAE.OrgUnitAlignmentEntity_Weight, OU.OrgUnit_Id, TT.TerritoryType_Id
		    FROM OrgUnitAlignmentEntitys OAE
			INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OAE.OrgUnit_Id
		    INNER JOIN TerritoryTypes TT ON OU.OrgUnitLevel_Id = TT.OrgUnitLevel_Id
			INNER JOIN AlignmentEntitys AE ON OAE.AlignmentEntity_id = AE.AlignmentEntity_Id
			INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
			AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND TT.TerritoryType_Id = AETTT.TerritoryType_id
			WHERE AETTT.ToleratePartialAlignment = 0) AS Temp
	  GROUP BY Temp.TerritoryType_Id, Temp.AlignmentEntity_Id
	  HAVING SUM(Temp.OrgUnitAlignmentEntity_Weight) <> 100
	  EXCEPT (SELECT Temp2.AlignmentEntity_Id AS AlignmentEntity_Id, Temp2.TerritoryType_Id AS TerritoryType_Id
	          FROM (SELECT OAE.AlignmentEntity_Id, OAE.OrgUnitAlignmentEntity_Weight, OU.OrgUnit_Id, TT.TerritoryType_Id
				    FROM OrgUnitAlignmentEntitys OAE
				    INNER JOIN OrgUnits OU ON OU.OrgUnit_Id = OAE.OrgUnit_Id
				    INNER JOIN TerritoryTypes TT ON OU.OrgUnitLevel_Id = TT.OrgUnitLevel_Id
				    INNER JOIN AlignmentEntitys AE ON OAE.AlignmentEntity_id = AE.AlignmentEntity_Id
				    INNER JOIN AlignmentEntityTypeTerritoryTypes AETTT ON 
				    AE.AlignmentEntityType_id = AETTT.AlignmentEntityType_id AND TT.TerritoryType_Id = AETTT.TerritoryType_id
				    WHERE AETTT.ToleratePartialAlignment = 0) AS Temp2 
		      WHERE Temp2.OrgUnitAlignmentEntity_Weight = 0 OR Temp2.OrgUnitAlignmentEntity_Weight IS NULL)
	 ) As ResultTable