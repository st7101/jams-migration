
PRINT N'Updating UpsertAlignmentEntityOverrides ...';

UPDATE uaeo
SET [UpsertAlignmentEntityOverride_AlignmentEntityId]=ae.AlignmentEntity_Id
FROM dbo.UpsertAlignmentEntityOverrides uaeo INNER JOIN 
dbo.AlignmentEntitys ae ON ae.AlignmentEntity_ExternalID=uaeo.[UpsertAlignmentEntityOverride_AlignmentEntityExternalId]

