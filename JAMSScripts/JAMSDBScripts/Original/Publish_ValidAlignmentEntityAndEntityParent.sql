﻿-- Query to find all the rows in which alignment entity and parent in the same TT are allowable
with invalidAlignmentEntitys as 
( 
	select distinct ae1.AlignmentEntity_Id
	from AlignmentEntitys ae1
	inner join AlignmentEntitys ae2 
		on ae1.AlignmentEntity_ParentId=ae2.AlignmentEntity_Id 
	
	except
		(
			select distinct ae1.AlignmentEntity_Id
			from AlignmentEntitys ae1
			inner join AlignmentEntitys ae2 
				on ae1.AlignmentEntity_ParentId=ae2.AlignmentEntity_Id
			inner join AlignmentEntityTypeTerritoryTypes aettt1 
				on aettt1.AlignmentEntityType_id = ae1.AlignmentEntityType_id 
			inner join AlignmentEntityTypeTerritoryTypes aettt2
				on aettt2.AlignmentEntityType_id = ae2.AlignmentEntityType_id 
			where  
				aettt1.AlignmentEntityType_ParentId is null 
				or (
						aettt1.AlignmentEntityType_ParentId = aettt2.AlignmentEntityType_id 
						and aettt1.TerritoryType_id = aettt2.TerritoryType_id
					)	
		)
)

select * from invalidAlignmentEntitys