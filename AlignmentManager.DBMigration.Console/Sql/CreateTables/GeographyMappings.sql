

PRINT N'Starting rebuilding table [dbo].[GeographyMappings]...';

CREATE TABLE [dbo].[tmp_ms_xx_GeographyMappings](
	[GeographyMapping_GeoId] bigint NOT NULL,
	[GeographyMapping_ExternalGeoId] [nvarchar](100) NOT NULL,
	[GeographyMapping_ShapeId] [nvarchar](255) NULL,
	[GeographyMapping_ShapeIndex] [nvarchar](255) NULL,
	[GeographyMapping_Latitude] [float] NULL,
	[GeographyMapping_Longitude] [float] NULL,
	[GeographyMapping_Type] [nvarchar](100) NULL,
CONSTRAINT [tmp_ms_xx_PK_GeographyMappings] PRIMARY KEY CLUSTERED 
(
	[GeographyMapping_GeoId] ASC
)WITH (DATA_COMPRESSION=PAGE)
)


IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].GeographyMappings)
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_GeographyMappings] WITH (TABLOCK) (
	[GeographyMapping_GeoId] ,
	[GeographyMapping_ExternalGeoId] ,
	[GeographyMapping_ShapeId] ,
	[GeographyMapping_ShapeIndex],
	[GeographyMapping_Latitude] ,
	[GeographyMapping_Longitude],
	[GeographyMapping_Type] )
        SELECT 	ae.AlignmentEntity_Id ,
	[GeographyMapping_GeoId] ,
	[GeographyMapping_ShapeId] ,
	[GeographyMapping_ShapeIndex],
	[GeographyMapping_Latitude] ,
	[GeographyMapping_Longitude],
	[GeographyMapping_Type] 
        FROM   [dbo].GeographyMappings gm
		INNER JOiN dbo.AlignmentEntitys ae ON gm.GeographyMapping_GeoId=ae.AlignmentEntity_ExternalId
    END

DROP TABLE [dbo].GeographyMappings;

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_GeographyMappings]', N'GeographyMappings';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PK_GeographyMappings]', N'PK_GeographyMappings', N'OBJECT';

