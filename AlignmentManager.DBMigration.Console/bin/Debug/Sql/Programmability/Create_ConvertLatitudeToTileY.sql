IF (OBJECT_ID('[ConvertLatitudeToTileY]') IS NOT NULL)
  DROP FUNCTION [ConvertLatitudeToTileY]

  DECLARE @sqlCommand NVARCHAR(MAX)='
CREATE FUNCTION [dbo].[ConvertLatitudeToTileY](@latitude float, @zoom int)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN
	DECLARE @Result int
	declare @zoomFactor float
    set @zoomFactor = Power(2, @zoom)

	SELECT @Result = Floor((LOG(TAN(RADIANS(@latitude)) + (1/COS(RADIANS(@latitude))))/-PI()+1)/2*@zoomFactor)
	
	RETURN @Result

END
'
EXEC (@sqlCommand)