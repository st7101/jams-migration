SELECT ou.OrgUnit_Id 
FROM OrgUnits AS ou
LEFT JOIN OrgUnitAlignmentEntitys OUAE ON ou.Orgunit_Id = OUAE.OrgUnit_Id
LEFT JOIN TerritoryTypes TT ON TT.OrgUnitLevel_Id = ou.OrgUnitLevel_Id
LEFT JOIN OrgUnitLevels OUL ON ou.OrgUnitLevel_Id = OUL.OrgUnitLevel_Id
WHERE (OUL.OrgUnitLevel_IsLeafLevel = 1) AND ou.OrgUnit_IsUnassigned = 0
GROUP BY ou.OrgUnit_Id
HAVING COUNT(OUAE.OrgUnit_Id) = 0