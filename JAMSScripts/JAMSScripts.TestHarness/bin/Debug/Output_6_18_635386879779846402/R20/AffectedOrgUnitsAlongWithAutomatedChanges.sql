With MirroredOverrides As
(Select oo.OrderedOverride_OverrideId as OverrideId from OrderedOverrides oo where oo.Request_Id = 101
and oo.OrderedOverride_ParentId is not null and oo.OrderedOverride_OverrideType like '%.AlignmentEntityOverride%')

Select distinct OrgUnit.OrgUnit_Id as OrgUnitId,  
Case When Exists (Select 1 from MirroredOverrides where OverrideId = aeo.AlignmentEntityOverride_Id) Then 1 Else 0 End as IsAutomatedBusinessRuleChange
from OrgUnits as OrgUnit, 
AlignmentEntityOverrides as aeo
left join AlignmentStateEntrys ose on ose.AlignmentState_id = aeo.OldAlignmentState_id
left join AlignmentStateEntrys nse on nse.AlignmentState_id = aeo.NewAlignmentState_id
Where (OrgUnit.OrgUnit_Id = ose.AlignmentStateEntry_OrgUnitId or OrgUnit.OrgUnit_Id = nse.AlignmentStateEntry_OrgUnitId) 
and ose.AlignmentStateEntry_OrgUnitId != nse.AlignmentStateEntry_OrgUnitId 
and aeo.Request_Id = 101