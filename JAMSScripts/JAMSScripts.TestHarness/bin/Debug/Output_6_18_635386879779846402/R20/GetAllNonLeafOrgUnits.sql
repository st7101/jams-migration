SELECT OrgUnit_Id
FROM OrgUnits as OU
INNER JOIN OrgUnitLevels AS OUL
ON OU.OrgUnitLevel_id = OUL.OrgUnitLevel_id
WHERE (OUL.OrgUnitLevel_IsLeafLevel = 0)