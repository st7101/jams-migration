select entity.AlignmentEntity_ParentId, count(entity.AlignmentEntity_Id) 
from AlignmentEntitys entity With (INDEX([ParentId]))
inner join GroupedEntities groupedEntity on entity.AlignmentEntity_Id = groupedEntity.AlignmentEntity_Id 
where entity.AlignmentEntity_Status = Normal 
and entity.AlignmentEntity_ParentId in (2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22) 
and groupedEntity.TerritoryType_Id = 16 
group by entity.AlignmentEntity_ParentId