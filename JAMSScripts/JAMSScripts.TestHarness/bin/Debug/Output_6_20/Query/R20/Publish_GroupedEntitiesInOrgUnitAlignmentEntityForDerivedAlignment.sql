/*Query to check All the entities which are present in GroupedEntitys table should not be present in OrgUnitAlignmentEntity
 for the territoryTypes having derived alignment.*/

select ouae.AlignmentEntity_id, ouae.TerritoryType_id
from OrgUnitAlignmentEntitys ouae
inner join AlignmentEntitys ae
	on ouae.AlignmentEntity_id = ae.AlignmentEntity_Id
inner join AlignmentEntityTypeTerritoryTypes aettt
	on aettt.AlignmentEntityType_id = ae.AlignmentEntityType_id
	and ouae.TerritoryType_id = aettt.TerritoryType_id 
	and aettt.AlignmentEntityType_ParentId is not null