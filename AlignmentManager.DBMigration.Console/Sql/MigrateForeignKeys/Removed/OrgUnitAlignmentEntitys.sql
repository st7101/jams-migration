PRINT N'Updating OrgUnitAlignmentEntitys ...';
CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys]
    (
      [OrgUnitAlignmentEntity_Id] BIGINT NOT NULL ,
      [OrgUnitAlignmentEntity_Weight] DECIMAL(5, 2) NULL ,
      [AlignmentEntity_Id] BIGINT NOT NULL ,
      [OrgUnit_Id] BIGINT NOT NULL ,
	  [TerritoryType_Id] BIGINT NOT NULL,
      [OrgUnitAlignmentEntity_IsAutomatedAlignment] [BIT] NOT NULL
                                                          DEFAULT ( (0) )
    );
	ALTER TABLE [dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys] ADD CONSTRAINT [PK_tmp_ms_xx_OrgUnitAlignmentEntitys] PRIMARY KEY CLUSTERED ( TerritoryType_id ASC,OrgUnit_id ASC, AlignmentEntity_Id ASC ) WITH (  DATA_COMPRESSION = PAGE ) ON PS_OUAE(territorytype_id) 

INSERT INTO [tmp_ms_xx_OrgUnitAlignmentEntitys] ([OrgUnitAlignmentEntity_Id], [OrgUnitAlignmentEntity_Weight], [AlignmentEntity_Id], [OrgUnit_Id],[TerritoryType_Id], [OrgUnitAlignmentEntity_IsAutomatedAlignment])
SELECT ouae.OrgUnitAlignmentEntity_Id,ouae.OrgUnitAlignmentEntity_Weight, ae.AlignmentEntity_Id,ou.OrgUnit_Id, tt.TerritoryType_Id,ouae.[OrgUnitAlignmentEntity_IsAutomatedAlignment]
FROM dbo.OrgUnitAlignmentEntitys ouae INNER JOIN 
dbo.AlignmentEntitys ae ON ouae.AlignmentEntity_ExternalID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.OrgUnits ou ON ouae.OrgUnit_ExternalID=ou.OrgUnit_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON ou.OrgUnitLevel_Externalid=tt.OrgUnitLevel_Externalid

DROP TABLE [dbo].[OrgUnitAlignmentEntitys];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitAlignmentEntitys]',
    N'OrgUnitAlignmentEntitys';

EXECUTE sp_rename N'[dbo].[PK_tmp_ms_xx_OrgUnitAlignmentEntitys]',
    N'PK_OrgUnitAlignmentEntitys', N'OBJECT';
