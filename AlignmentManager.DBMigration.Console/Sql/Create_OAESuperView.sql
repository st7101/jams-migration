DECLARE @sqlCommand VARCHAR(MAX) ='
CREATE VIEW [dbo].[OAESuperView]
WITH SCHEMABINDING 
AS
SELECT     oae.OrgUnit_id, oae.AlignmentEntity_id, tt.TerritoryType_Id, oae.OrgUnitAlignmentEntity_Id
FROM         dbo.OrgUnitAlignmentEntitys AS oae INNER JOIN
                      dbo.OrgUnits AS ou ON oae.OrgUnit_id = ou.OrgUnit_Id INNER JOIN
                      dbo.TerritoryTypes AS tt ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id '
EXEC(@sqlCommand)
SET @sqlCommand ='CREATE UNIQUE CLUSTERED INDEX [OAE_One] ON [dbo].[OAESuperView]
(
	[TerritoryType_Id] ASC,
	[AlignmentEntity_id] ASC,
	[OrgUnit_id] ASC,
	[OrgUnitAlignmentEntity_Id] ASC
)WITH ( DATA_COMPRESSION = PAGE) ON [PRIMARY]'
EXEC (@sqlCommand)
