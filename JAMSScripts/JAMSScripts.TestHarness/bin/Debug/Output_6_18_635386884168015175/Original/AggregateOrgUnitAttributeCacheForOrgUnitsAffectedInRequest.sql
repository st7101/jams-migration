
/***** DROP EXISTING TEMPORARY TABLE IF THERE IS ANY NEEDED FOR ORG UNIT AGGREGATION *******/
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempDerivedAlignmentsTerritoryTypes') 
DROP TABLE [TempDerivedAlignmentsTerritoryTypes];

IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedOrgUnitAttributesDeltas') 
DROP TABLE [TempUpdatedOrgUnitAttributesDeltas];

/******** TEMPORARY TABLE THAT STORES THE TERRITORY TYPES THAT HAS GEO ASSOCIATION ENABLED AND THE CORRESPONDING PARENT TYPE IN THE TERRITORYTYPE ****************/
SELECT * INTO [TempDerivedAlignmentsTerritoryTypes] FROM
(SELECT DISTINCT aettt.TerritoryType_id as TerritoryType_Id, 
					aettt.AlignmentEntityType_ParentId as AlignmentEntityType_id 
 FROM AlignmentEntityTypeTerritoryTypes aettt
 WHERE aettt.AlignmentEntityType_ParentId IS NOT NULL) pae;

/* TEMPORARY TABLE TO STORE THE CHANGES IN EACH ATTRIBUTE VALUE FOR ORG UNITS ALIGNED TO UPDATED ENTITIES */
SELECT * INTO TempUpdatedOrgUnitAttributesDeltas FROM (
SELECT oae.OrgUnit_id, temp.AttributeType_Id, SUM(temp.ChangeInValue * oae.OrgUnitAlignmentEntity_Weight/100) as ChangeInValue 
FROM TempUpdatedEntitiesWithAttributes temp 
INNER JOIN OrgUnitAlignmentEntitys oae on temp.AlignmentEntity_Id = oae.AlignmentEntity_id
INNER JOIN AlignmentEntitys ae on ae.AlignmentEntity_Id = temp.AlignmentEntity_Id
INNER JOIN OrgUnits o on o.OrgUnit_Id = oae.OrgUnit_id
INNER JOIN TerritoryTypes tt on tt.OrgUnitLevel_id = o.OrgUnitLevel_id
LEFT OUTER JOIN  TempDerivedAlignmentsTerritoryTypes da ON da.TerritoryType_Id = tt.TerritoryType_Id AND ae.AlignmentEntityType_id = da.AlignmentEntityType_id
WHERE 
((ae.AlignmentEntityType_id = da.AlignmentEntityType_id AND tt.TerritoryType_Id = temp.TerritoryType_id) 
OR 
(da.AlignmentEntityType_id IS NULL AND temp.TerritoryType_id IS NULL))
GROUP BY oae.OrgUnit_id, temp.AttributeType_Id 
) tOrg

/* UPDATE ORG UNIT ATTRIBUTE CACHE WITH UPDATED VALUES FOR LEAF ORG UNITS */
UPDATE ouae WITH (TABLOCK) 
	SET ouae.OrgUnitAttributeCache_Value = ouae.OrgUnitAttributeCache_Value + uouae.ChangeInValue
	FROM [OrgUnitAttributeCache] ouae
	INNER JOIN TempUpdatedOrgUnitAttributesDeltas uouae
	ON ouae.OrgUnit_id = uouae.OrgUnit_id AND ouae.AttributeType_id = uouae.AttributeType_id;


/* FIND PARENTS OF THE ORGUNITS THAT WERE UPDATED ABOVE */
WITH ParentsToBeUpdated AS
(SELECT DISTINCT parents.* FROM OrgUnits parents, OrgUnits leaf 
						WHERE leaf.OrgUnit_Id in (select Distinct OrgUnit_Id from TempUpdatedOrgUnitAttributesDeltas)
						AND parents.OrgUnit_TreeLeft < leaf.OrgUnit_TreeLeft AND parents.OrgUnit_TreeRight > leaf.OrgUnit_TreeRight),

/* FIND THE VALUES TO BE CHANGED FOR EACH ATTRIBUTE TYPE FOR PARENTS OF THE ORGUNITS THAT WERE UPDATED ABOVE */						
UpdatedParentOrgUnitsCache AS
(
SELECT distinct parent.orgunit_Id as OrgUnit_id, oc.AttributeType_id as AttributeType_id, SUM(oc.ChangeInValue) as ChangeInValue 
FROM TempUpdatedOrgUnitAttributesDeltas as oc
INNER JOIN OrgUnits child ON child.OrgUnit_Id = oc.Orgunit_Id
INNER JOIN OrgUnitLevels oul ON child.OrgUnitLevel_Id = oul.OrgUnitLevel_Id
INNER JOIN ParentsToBeUpdated parent ON child.OrgUnit_TreeLeft >  parent.OrgUnit_TreeLeft AND child.OrgUnit_TreeRight < parent.OrgUnit_TreeRight
WHERE oul.OrgUnitLevel_IsLeafLevel = 1
GROUP BY parent.orgunit_Id, oc.AttributeType_id
)

/* UPDATE THE ORG UNIT ATTRIBUTE CACHE FOR THE PARENT ORG UNITS IN THE HIERARCHY OF THE LEAF ORG UNITS UPDATED IN THE REQUEST */
UPDATE ouae WITH (TABLOCK) 
SET ouae.OrgUnitAttributeCache_Value = ouae.OrgUnitAttributeCache_Value + uouae.ChangeInValue
FROM [OrgUnitAttributeCache] ouae
INNER JOIN UpdatedParentOrgUnitsCache uouae
ON ouae.OrgUnit_id = uouae.OrgUnit_id AND ouae.AttributeType_id = uouae.AttributeType_id;


/* DROP ALL TEMPORARY TABLE THAT WERE CREATED */
DROP TABLE [TempUpdatedEntitiesWithAttributes]
DROP TABLE [TempUpdatedOrgUnitAttributesDeltas]
DROP TABLE [TempDerivedAlignmentsTerritoryTypes]