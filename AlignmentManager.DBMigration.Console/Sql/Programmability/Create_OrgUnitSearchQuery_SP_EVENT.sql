IF (OBJECT_ID('[OrgUnitSearchQuery]') IS NOT NULL)
  DROP PROCEDURE OrgUnitSearchQuery

DECLARE @SqlCommand NVARCHAR(MAX)= '
-- =============================================
-- Author:		Steve Tetrault
-- Create date: Jun. 11, 2014
-- Description:	Modified Logic for OrgUnitSearchQuery SP
-- =============================================
CREATE PROCEDURE [dbo].[OrgUnitSearchQuery]
	-- Add the parameters for the stored procedure here
    @showAssigned BIT ,
    @showUnassigned BIT ,
    @treeLeft INT ,
    @treeRight INT ,
    @territoryTypeId BIGINT ,
    @MaxResult INT ,
    @SearchString NVARCHAR(100) = NULL
AS
    BEGIN

        DECLARE @sql NVARCHAR(MAX)= ''
SELECT DISTINCT TOP '' + CAST(@maxResult AS NVARCHAR) + ''
				ou.OrgUnit_Id as Id,
                 ou.OrgUnit_Name as Name,
				 ou.OrgUnit_LegacyId as LegacyId,
				 ou.OrgUnit_ColorId as ColorId
		FROM   OrgUnits ou
       inner join TerritoryTypes tt
         on ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
		and tt.TerritoryType_Id ='' + CAST(@territoryTypeId AS NVARCHAR)
            + CHAR(13)
        IF ( @showAssigned = 1
             AND @showUnassigned = 1
           )
            BEGIN
                SET @sql = @sql + ''
		
		WHERE (( ou.OrgUnit_TreeLeft >= '' + CAST(@treeLeft AS NVARCHAR)
                    + '' and ou.OrgUnit_TreeRight <= ''
                    + CAST(@treeRight AS NVARCHAR) + '') 
		or ou.OrgUnit_IsUnassigned = 1)
''
            END
        ELSE
            IF ( @showAssigned = 1 )
                BEGIN
                    SET @sql = @sql + ''
					WHERE ( ou.OrgUnit_TreeLeft >= ''
                        + CAST(@treeLeft AS NVARCHAR)
                        + '' and ou.OrgUnit_TreeRight <= ''
                        + CAST(@treeRight AS NVARCHAR) + '') 
''
                END
            ELSE
                IF ( @showUnassigned = 1 )
                    BEGIN
                        SET @sql = @sql + ''WHERE (ou.OrgUnit_IsUnassigned = 1)
''
                    END
			

        IF ( @SearchString IS NOT NULL
             AND @SearchString != ''''
           )
            BEGIN
                SET @sql = @sql + ''	and (LEFT(ou.OrgUnit_Name,''
                    + CAST(LEN(@SearchString) AS NVARCHAR) + '') = ''''''
                    + @SearchString + CHAR(13)
                    + '''''' or LEFT(ou.OrgUnit_ExternalId,''
                    + CAST(LEN(@SearchString) AS NVARCHAR) + '') = ''''''
                    + @SearchString + '''''' or LEFT(ou.OrgUnit_LegacyId,''
                    + CAST(LEN(@SearchString) AS NVARCHAR) + '') = ''''''
                    + @SearchString + '''''')'' + CHAR(13)
            END

        EXEC (@sql)
    END
'
EXEC (@sqlCommand)


