IF (OBJECT_ID('[ConvertLongitudeToTileX]') IS NOT NULL)
  DROP PROCEDURE [ConvertLongitudeToTileX]
GO


CREATE FUNCTION [dbo].[ConvertLongitudeToTileX](@longitude float, @zoom int)
RETURNS INT
WITH SCHEMABINDING
AS
BEGIN
	-- Declare the return variable here
	DECLARE @Result int
	declare @zoomFactor float
    set @zoomFactor = Power(2, @zoom)

	-- Add the T-SQL statements to compute the return value here
	SELECT @Result = Floor((RADIANS(@longitude) + PI())/(2*PI())*@zoomFactor)

	-- Return the result of the function
	RETURN @Result

END
