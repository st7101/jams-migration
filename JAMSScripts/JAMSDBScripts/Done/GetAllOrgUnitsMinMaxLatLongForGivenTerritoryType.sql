﻿SELECT 
OUAE.OrgUnit_Id as OrgUnitId,
Max(AE.AlignmentEntity_Latitude) as MaxLatitude, 
Min(AE.AlignmentEntity_Latitude) as MinLatitude,
Max(AE.AlignmentEntity_Longitude) as MaxLongitude, 
Min(AE.AlignmentEntity_Longitude) as MinLongitude 
FROM OrgUnitAlignmentEntitys as OUAE
inner join AlignmentEntitys as AE on OUAE.AlignmentEntity_id = AE.AlignmentEntity_id
INNER JOIN OrgUnits as OU ON OU.OrgUnit_Id = OUAE.OrgUnit_Id
INNER JOIN OrgUnitLevels AS OUL
ON OU.OrgUnitLevel_id = OUL.OrgUnitLevel_id
where (AE.AlignmentEntity_Longitude != 0) AND (AE.AlignmentEntity_Latitude != 0) AND (oul.TerritoryType_Id = :territoryTypeId)
group by OUAE.OrgUnit_Id