Delete from ApprovalRoundTerritoryTypePermissions
Delete from TerritoryTypePermissions

Declare @PkConstraint nvarchar(255)
Declare @FkConstraint nvarchar(255)
Declare @sql nvarchar(max)

--Delete Foreign Key
SELECT @FkConstraint = (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'FOREIGN KEY' AND TABLE_NAME = 'ApprovalRoundTerritoryTypePermissions' AND CONSTRAINT_NAME like '%Terr%')
Set @sql = N'Alter Table ApprovalRoundTerritoryTypePermissions
Drop CONSTRAINT ' + @FkConstraint
EXEC (@sql)

--Delete PrimaryKey
SELECT @PkConstraint = (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_NAME = 'TerritoryTypePermissions')
Set @sql = N'Alter Table TerritoryTypePermissions
Drop CONSTRAINT ' + @PkConstraint
EXEC (@sql)


Alter Table TerritoryTypePermissions
Add TempTerritoryTypePermission_Id bigint not null identity(1,1)


Insert Into TerritoryTypePermissions
(TerritoryType_id, Permission_id, TerritoryTypePermission_IsEnabled, TerritoryTypePermission_Id)
(select tt.TerritoryType_id, p.Permission_Id, 1 as TerritoryTypePermission_IsEnabled, 0 as TerritoryTypePermission_Id from TerritoryTypes tt, Permissions p
Where p.Permission_PermissionLevel = 'TerritoryTypeLevel')


SET @sql = N'
Update TerritoryTypePermissions
Set TerritoryTypePermission_Id = TempTerritoryTypePermission_Id'
EXEC (@sql)

Alter Table TerritoryTypePermissions
drop column TempTerritoryTypePermission_Id


ALTER TABLE [dbo].[TerritoryTypePermissions] ADD PRIMARY KEY CLUSTERED 
(
	[TerritoryTypePermission_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

ALTER TABLE [dbo].[ApprovalRoundTerritoryTypePermissions]  WITH CHECK ADD FOREIGN KEY([TerritoryTypePermission_id])
REFERENCES [dbo].[TerritoryTypePermissions] ([TerritoryTypePermission_Id])