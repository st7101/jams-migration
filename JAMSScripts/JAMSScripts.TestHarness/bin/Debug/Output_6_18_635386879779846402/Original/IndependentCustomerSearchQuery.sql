SELECT DISTINCT TOP (100) 
				ae.AlignmentEntity_Id        AS Id,
				ae.AlignmentEntity_Name      AS Name,
				null as ParentId
	FROM AlignmentEntitys ae
	inner join OrgUnitAlignmentEntitys oae
		ON ae.AlignmentEntity_Id = oae.AlignmentEntity_id
	inner join OrgUnits ou
		ON oae.OrgUnit_id = ou.OrgUnit_Id
		and ((0 = 1 and ou.OrgUnit_TreeLeft >= 1 and ou.OrgUnit_TreeRight <= 10430) 
		or (0 = 1 and ou.OrgUnit_IsUnassigned = 1))
	inner join TerritoryTypes tt
		ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
		and tt.TerritoryType_Id = '158'
	WHERE ae.AlignmentEntity_Status = 'Normal'
		and ae.AlignmentEntityType_Id in (1,2,3)
		and ae.AlignmentEntityType_Id <> (1)
		and (ae.AlignmentEntity_Name like 'r' +'%' escape '\' or ae.AlignmentEntity_Id like 'r' +'%' escape '\')
		OPTION (RECOMPILE)	