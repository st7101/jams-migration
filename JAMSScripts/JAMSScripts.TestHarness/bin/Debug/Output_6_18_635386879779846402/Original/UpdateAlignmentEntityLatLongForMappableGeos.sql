Update AlignmentEntitys
Set AlignmentEntity_Latitude = (Select GeographyMapping_Latitude from GeographyMappings Where GeographyMapping_GeoId = AlignmentEntity_Id),
AlignmentEntity_Longitude = (Select GeographyMapping_Longitude from GeographyMappings Where GeographyMapping_GeoId = AlignmentEntity_Id)
Where AlignmentEntityType_Id = 1