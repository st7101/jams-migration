/********* This query gets clusters in Assigned/Both display mode ********************/
EXEC dbo.GetAlignmentEntityClustersOfTypeInBoundingBox
	@showAssigned=1,@showUnassigned=1 , @isGeo=1,  @treeLeft=1, @treeRight=10430, @territoryTypeId=10,
	@minLat=42.6969776153564, @maxLat=47.4596862792969, @minLng=-73.4377403259277, @maxLng=-66.9500045776367, @ZoomLevel=9,@aeStatus='Normal';