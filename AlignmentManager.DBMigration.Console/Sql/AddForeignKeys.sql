PRINT N'Creating Foreign Key on [dbo].[AlignmentEntitys]....';



ALTER TABLE [dbo].[AlignmentEntitys] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntitys_AlignmentEntitys] FOREIGN KEY ([AlignmentEntity_ParentId]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);



PRINT N'Creating Foreign Key on [dbo].[AlignmentEntitys]....';



ALTER TABLE [dbo].[AlignmentEntitys] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntitys_AlignmentEntityTypes] FOREIGN KEY ([AlignmentEntityType_Id]) 
REFERENCES [dbo].[AlignmentEntityTypes] ([AlignmentEntityType_Id]);



PRINT N'Creating FK_AlignmentEntityNumberAttributeCache_AlignmentEntitys...';



ALTER TABLE [dbo].[AlignmentEntityNumberAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityNumberAttributeCache_AlignmentEntitys] FOREIGN KEY ([AlignmentEntity_Id]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);



PRINT N'Creating FK_AlignmentEntityNumberAttributeCache_AttributeTypes...';



ALTER TABLE [dbo].[AlignmentEntityNumberAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityNumberAttributeCache_AttributeTypes] FOREIGN KEY ([AttributeType_Id]) 
REFERENCES [dbo].[AttributeTypes] ([AttributeType_Id]);



PRINT N'Creating FK_AlignmentEntityNumberAttributeCache_TerritoryTypes...';



ALTER TABLE [dbo].[AlignmentEntityNumberAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityNumberAttributeCache_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);





PRINT N'Creating Foreign Key on [dbo].[AlignmentEntityOverrides]....';



ALTER TABLE [dbo].[AlignmentEntityOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityOverrides_NewAlignmentState] FOREIGN KEY ([NewAlignmentState_Id]) 
REFERENCES [dbo].[AlignmentStates] ([AlignmentState_Id]);



PRINT N'Creating Foreign Key on [dbo].[AlignmentEntityOverrides]....';



ALTER TABLE [dbo].[AlignmentEntityOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityOverrides_OldAlignmentState] FOREIGN KEY ([OldAlignmentState_Id]) 
REFERENCES [dbo].[AlignmentStates] ([AlignmentState_Id]);



PRINT N'Creating Foreign Key on [dbo].[AlignmentEntityOverrides]....';



ALTER TABLE [dbo].[AlignmentEntityOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityOverrides_Request] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating Foreign Key on [dbo].[AlignmentEntityOverrides]....';



ALTER TABLE [dbo].[AlignmentEntityOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityOverrides_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);



PRINT N'Creating FK_AlignmentEntityTextAttributeCache_AlignmentEntitys...';



ALTER TABLE [dbo].[AlignmentEntityTextAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityTextAttributeCache_AlignmentEntitys] FOREIGN KEY ([AlignmentEntity_Id]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);



PRINT N'Creating FK_AlignmentEntityTextAttributeCache_AttributeTypes...';



ALTER TABLE [dbo].[AlignmentEntityTextAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityTextAttributeCache_AttributeTypes] FOREIGN KEY ([AttributeType_Id]) 
REFERENCES [dbo].[AttributeTypes] ([AttributeType_Id]);



PRINT N'Creating Foreign Key on [dbo].[AlignmentEntityTypeTerritoryTypes]....';



ALTER TABLE [dbo].[AlignmentEntityTypeTerritoryTypes] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityTypeTerritoryTypes_AlignmentEntityTypes] FOREIGN KEY ([AlignmentEntityType_Id]) 
REFERENCES [dbo].[AlignmentEntityTypes] ([AlignmentEntityType_Id]);



PRINT N'Creating Foreign Key on [dbo].[AlignmentEntityTypeTerritoryTypes]....';



ALTER TABLE [dbo].[AlignmentEntityTypeTerritoryTypes] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentEntityTypeTerritoryTypes_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);



PRINT N'Creating FK_AlignmentStateEntrys_OrgUnits...';



ALTER TABLE [dbo].[AlignmentStateEntrys] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentStateEntrys_OrgUnits] FOREIGN KEY ([AlignmentStateEntry_OrgUnitId]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating Foreign Key on [dbo].[AlignmentStateEntrys]....';



ALTER TABLE [dbo].[AlignmentStateEntrys] WITH NOCHECK
ADD CONSTRAINT [FK_AlignmentStateEntrys_AlignmentStates] FOREIGN KEY ([AlignmentState_Id]) 
REFERENCES [dbo].[AlignmentStates] ([AlignmentState_Id]);



PRINT N'Creating Foreign Key on [dbo].[ApprovalProgress]....';



ALTER TABLE [dbo].[ApprovalProgress] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalProgress_Positions] FOREIGN KEY ([Position_Id]) 
REFERENCES [dbo].[Positions] ([Position_Id]);



PRINT N'Creating Foreign Key on [dbo].[ApprovalProgressLogicalExpressions]....';



ALTER TABLE [dbo].[ApprovalProgressLogicalExpressions] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalProgressLogicalExpressions_ApprovalProgress] FOREIGN KEY ([ApprovalProgress_Id]) 
REFERENCES [dbo].[ApprovalProgress] ([ApprovalProgress_Id]);



PRINT N'Creating Foreign Key on [dbo].[ApprovalProgressLogicalExpressions]....';



ALTER TABLE [dbo].[ApprovalProgressLogicalExpressions] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalProgressLogicalExpressions_ApprovalProgressLogicalExpressions] FOREIGN KEY ([MultipleApprovalProgressExpression_Id]) 
REFERENCES [dbo].[ApprovalProgressLogicalExpressions] ([ApprovalProgressLogicalExpression_Id]);



PRINT N'Creating Foreign Key on [dbo].[ApprovalProgressLogicalExpressions]....';



ALTER TABLE [dbo].[ApprovalProgressLogicalExpressions] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalProgressLogicalExpressions_MultipleApprovalProgressLogicalExpressions] FOREIGN KEY ([Operation_Id]) 
REFERENCES [dbo].[PermissionOperations] ([PermissionOperation_Id]);



PRINT N'Creating FK_ApprovalProgressTerritoryTypes_ApprovalProgress...';



ALTER TABLE [dbo].[ApprovalProgressTerritoryTypes] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalProgressTerritoryTypes_ApprovalProgress] FOREIGN KEY ([ApprovalProgress_Id]) 
REFERENCES [dbo].[ApprovalProgress] ([ApprovalProgress_Id]);



PRINT N'Creating FK_ApprovalProgressTerritoryTypes_TerritoryTypes...';



ALTER TABLE [dbo].[ApprovalProgressTerritoryTypes] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalProgressTerritoryTypes_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);



PRINT N'Creating Foreign Key on [dbo].[ApprovalRoundOrgUnitLevelPermissions]....';



ALTER TABLE [dbo].[ApprovalRoundOrgUnitLevelPermissions] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalRoundOrgUnitLevelPermissions_ApprovalRounds] FOREIGN KEY ([ApprovalRound_Id]) 
REFERENCES [dbo].[ApprovalRounds] ([ApprovalRound_Id]);



PRINT N'Creating Foreign Key on [dbo].[ApprovalRoundOrgUnitLevelPermissions]....';



ALTER TABLE [dbo].[ApprovalRoundOrgUnitLevelPermissions] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalRoundOrgUnitLevelPermissions_OrgUnitLevelPermissions] FOREIGN KEY ([OrgUnitLevelPermission_Id]) 
REFERENCES [dbo].[OrgUnitLevelPermissions] ([OrgUnitLevelPermission_Id]);



PRINT N'Creating Foreign Key on [dbo].[ApprovalRoundProgressTrackers]....';



ALTER TABLE [dbo].[ApprovalRoundProgressTrackers] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalRoundProgressTrackers_ApprovalProgressTrackers] FOREIGN KEY ([ApprovalProgressTracker_Id]) 
REFERENCES [dbo].[ApprovalProgressTrackers] ([ApprovalProgressTracker_Id]);



PRINT N'Creating Foreign Key on [dbo].[ApprovalRoundProgressTrackers]....';



ALTER TABLE [dbo].[ApprovalRoundProgressTrackers] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalRoundProgressTrackers_ApprovalProgressLogicalExpressions] FOREIGN KEY ([ApprovalRoundProgress_Id]) 
REFERENCES [dbo].[ApprovalProgressLogicalExpressions] ([ApprovalProgressLogicalExpression_Id]);



PRINT N'Creating FK__ApprovalR__Appro__30C33EC3...';



ALTER TABLE [dbo].[ApprovalRoundRoles] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalRoundRoles_ApprovalRounds] FOREIGN KEY ([ApprovalRound_Id]) 
REFERENCES [dbo].[ApprovalRounds] ([ApprovalRound_Id]);



PRINT N'Creating FK__ApprovalR__Role___31B762FC...';



ALTER TABLE [dbo].[ApprovalRoundRoles] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalRoundRoles_Roles] FOREIGN KEY ([Role_Id]) 
REFERENCES [dbo].[Roles] ([Role_Id]);



PRINT N'Creating FK__ApprovalR__Appro__32AB8735...';



ALTER TABLE [dbo].[ApprovalRoundTerritoryTypePermissions] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalRoundTerritoryTypePermissions_ApprovalRounds] FOREIGN KEY ([ApprovalRound_Id]) 
REFERENCES [dbo].[ApprovalRounds] ([ApprovalRound_Id]);



PRINT N'Creating FK__ApprovalR__Terri__339FAB6E...';



ALTER TABLE [dbo].[ApprovalRoundTerritoryTypePermissions] WITH NOCHECK
ADD CONSTRAINT [FK_ApprovalRoundTerritoryTypePermissions_TerritoryTypePermissions] FOREIGN KEY ([TerritoryTypePermission_Id]) 
REFERENCES [dbo].[TerritoryTypePermissions] ([TerritoryTypePermission_Id]);



PRINT N'Creating Foreign Key on [dbo].[AttributeTypeTerritoryTypes]....';



ALTER TABLE [dbo].[AttributeTypeTerritoryTypes] WITH NOCHECK
ADD CONSTRAINT [FK_AttributeTypeTerritoryTypes_AttributeTypes] FOREIGN KEY ([AttributeType_Id]) 
REFERENCES [dbo].[AttributeTypes] ([AttributeType_Id]);



PRINT N'Creating Foreign Key on [dbo].[AttributeTypeTerritoryTypes]....';



ALTER TABLE [dbo].[AttributeTypeTerritoryTypes] WITH NOCHECK
ADD CONSTRAINT [FK_AttributeTypeTerritoryTypes_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);



PRINT N'Creating FK_Customers_AlignmentEntitys...';



ALTER TABLE [dbo].[Customers] WITH NOCHECK
ADD CONSTRAINT [FK_Customers_AlignmentEntitys] FOREIGN KEY ([Customer_Id]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);

PRINT N'Creating FK_Geos_AlignmentEntitys...';



ALTER TABLE [dbo].[Geos] WITH NOCHECK
ADD CONSTRAINT [FK_Geos_AlignmentEntitys] FOREIGN KEY ([Geo_Id]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);


PRINT N'Creating Foreign Key on [dbo].[Customers]....';

ALTER TABLE [dbo].[Customers] WITH NOCHECK
ADD CONSTRAINT [FK_Customers_CustomerTypes] FOREIGN KEY ([CustomerType_Id]) 
REFERENCES [dbo].[CustomerTypes] ([CustomerType_Id]);






PRINT N'Creating Foreign Key on [dbo].[EditAttributeOverrides]....';



ALTER TABLE [dbo].[EditAttributeOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_EditAttributeOverrides_AttributeTypes] FOREIGN KEY ([AttributeType_Id]) 
REFERENCES [dbo].[AttributeTypes] ([AttributeType_Id]);



PRINT N'Creating Foreign Key on [dbo].[EditAttributeOverrides]....';



ALTER TABLE [dbo].[EditAttributeOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_EditAttributeOverrides_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);




PRINT N'Creating FK__GroupedEn__Align__3A4CA8FD...';



ALTER TABLE [dbo].[GroupedEntities] WITH NOCHECK
ADD CONSTRAINT [FK_GroupedEntities_AlignmentEntitys] FOREIGN KEY ([AlignmentEntity_Id]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);



PRINT N'Creating FK__GroupedEn__Terri__3B40CD36...';



ALTER TABLE [dbo].[GroupedEntities] WITH NOCHECK
ADD CONSTRAINT [FK_GroupedEntities_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);



PRINT N'Creating Foreign Key on [dbo].[IssueRepairCache]....';



ALTER TABLE [dbo].[IssueRepairCache] WITH NOCHECK
ADD CONSTRAINT [FK_IssueRepairCache_AlignmentEntitys] FOREIGN KEY ([AlignmentEntity_Id]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);



PRINT N'Creating Foreign Key on [dbo].[IssueRepairCache]....';



ALTER TABLE [dbo].[IssueRepairCache] WITH NOCHECK
ADD CONSTRAINT [FK_IssueRepairCache_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);



PRINT N'Creating FK_OrderedOverrides_ParentOrderedOverride...';



ALTER TABLE [dbo].[OrderedOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_OrderedOverrides_ParentOrderedOverride] FOREIGN KEY ([OrderedOverride_ParentId]) 
REFERENCES [dbo].[OrderedOverrides] ([OrderedOverride_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrderedOverrides]....';



ALTER TABLE [dbo].[OrderedOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_OrderedOverrides_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnitAlignmentEntitys]....';



ALTER TABLE [dbo].[OrgUnitAlignmentEntitys] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitAlignmentEntitys_AlignmentEntitys] FOREIGN KEY ([AlignmentEntity_Id]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnitAlignmentEntitys]....';



ALTER TABLE [dbo].[OrgUnitAlignmentEntitys] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitAlignmentEntitys_OrgUnits] FOREIGN KEY ([OrgUnit_Id]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating FK_OrgUnitAttributeCache_AttributeTypes...';



ALTER TABLE [dbo].[OrgUnitAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitAttributeCache_AttributeTypes] FOREIGN KEY ([AttributeType_Id]) 
REFERENCES [dbo].[AttributeTypes] ([AttributeType_Id]);



PRINT N'Creating FK_OrgUnitAttributeCache_OrgUnits...';



ALTER TABLE [dbo].[OrgUnitAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitAttributeCache_OrgUnits] FOREIGN KEY ([OrgUnit_Id]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnitLevelPermissions]....';



ALTER TABLE [dbo].[OrgUnitLevelPermissions] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitLevelPermissions_OrgUnitLevels] FOREIGN KEY ([OrgUnitLevel_Id]) 
REFERENCES [dbo].[OrgUnitLevels] ([OrgUnitLevel_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnitLevelPermissions]....';



ALTER TABLE [dbo].[OrgUnitLevelPermissions] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitLevelPermissions_Permissions] FOREIGN KEY ([Permission_Id]) 
REFERENCES [dbo].[Permissions] ([Permission_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnitLevelRoles]....';



ALTER TABLE [dbo].[OrgUnitLevelRoles] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitLevelRoles_OrgUnitLevels] FOREIGN KEY ([OrgUnitLevel_Id]) 
REFERENCES [dbo].[OrgUnitLevels] ([OrgUnitLevel_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnitLevelRoles]....';



ALTER TABLE [dbo].[OrgUnitLevelRoles] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitLevelRoles_Roles] FOREIGN KEY ([Role_Id]) 
REFERENCES [dbo].[Roles] ([Role_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnitLevels]....';



ALTER TABLE [dbo].[OrgUnitLevels] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitLevels_ParentOrgUnitLevel] FOREIGN KEY ([OrgUnitLevel_ParentId]) 
REFERENCES [dbo].[OrgUnitLevels] ([OrgUnitLevel_Id]);








PRINT N'Creating Foreign Key on [dbo].[OrgUnitOverrides]....';



ALTER TABLE [dbo].[OrgUnitOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitOverrides_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating FK_OrgUnitRelationshipOverrides_OrgUnitRelationshipStates...';



ALTER TABLE [dbo].[OrgUnitRelationshipOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitRelationshipOverrides_NewOrgUnitRelationshipState] FOREIGN KEY ([NewOrgUnitRelationshipState_Id]) 
REFERENCES [dbo].[OrgUnitRelationshipStates] ([OrgUnitRelationshipState_Id]);



PRINT N'Creating FK_OrgUnitRelationshipOverrides_OrgUnitRelationshipStates1...';



ALTER TABLE [dbo].[OrgUnitRelationshipOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitRelationshipOverrides_OldOrgUnitRelationshipStates] FOREIGN KEY ([OldOrgUnitRelationshipState_Id]) 
REFERENCES [dbo].[OrgUnitRelationshipStates] ([OrgUnitRelationshipState_Id]);





PRINT N'Creating FK_OrgUnitRelationshipOverrides_Requests...';



ALTER TABLE [dbo].[OrgUnitRelationshipOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitRelationshipOverrides_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating FK_OrgUnit_OrgUnitRelationships1...';



ALTER TABLE [dbo].[OrgUnitRelationships] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitRelationships_DrivingOrgUnit] FOREIGN KEY ([Driving_OrgUnitId]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating FK_OrgUnit_OrgUnitRelationships2...';



ALTER TABLE [dbo].[OrgUnitRelationships] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitRelationships_AffectedOrgUnit] FOREIGN KEY ([Affected_OrgUnitId]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating FK_OrgUnitRelationshipStateEntrys_OrgUnitRelationshipStates...';



ALTER TABLE [dbo].[OrgUnitRelationshipStateEntrys] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitRelationshipStateEntrys_OrgUnitRelationshipStates] FOREIGN KEY ([OrgUnitRelationshipState_Id]) 
REFERENCES [dbo].[OrgUnitRelationshipStates] ([OrgUnitRelationshipState_Id]);



PRINT N'Creating FK_OrgUnitRelationshipStateEntrys_OrgUnits...';



ALTER TABLE [dbo].[OrgUnitRelationshipStateEntrys] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnitRelationshipStateEntrys_OrgUnits] FOREIGN KEY ([OrgUnitRelationshipStateEntry_AffectedOrgUnitId]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnits]....';



ALTER TABLE [dbo].[OrgUnits] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnits_ParentOrgUnit] FOREIGN KEY ([OrgUnit_ParentId]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating Foreign Key on [dbo].[OrgUnits]....';



ALTER TABLE [dbo].[OrgUnits] WITH NOCHECK
ADD CONSTRAINT [FK_OrgUnits_OrgUnitLevels] FOREIGN KEY ([OrgUnitLevel_Id]) 
REFERENCES [dbo].[OrgUnitLevels] ([OrgUnitLevel_Id]);



PRINT N'Creating FK_PositionOverrides_OrgUnits...';



ALTER TABLE [dbo].[PositionOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_PositionOverrides_OrgUnits] FOREIGN KEY ([PositionOverride_OrgUnitId]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating FK_PositionOverrides_Positions...';



ALTER TABLE [dbo].[PositionOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_PositionOverrides_Positions] FOREIGN KEY ([PositionOverride_PositionId]) 
REFERENCES [dbo].[Positions] ([Position_Id]);



PRINT N'Creating Foreign Key on [dbo].[PositionOverrides]....';



ALTER TABLE [dbo].[PositionOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_PositionOverrides_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);






PRINT N'Creating Foreign Key on [dbo].[PositionPersonnel]....';



ALTER TABLE [dbo].[PositionPersonnel] WITH NOCHECK
ADD CONSTRAINT [FK_PositionPersonnel_Personnel] FOREIGN KEY ([Personnel_Id]) 
REFERENCES [dbo].[Personnel] ([Personnel_Id]);



PRINT N'Creating Foreign Key on [dbo].[PositionPersonnel]....';



ALTER TABLE [dbo].[PositionPersonnel] WITH NOCHECK
ADD CONSTRAINT [FK_PositionPersonnel_Positions] FOREIGN KEY ([Position_Id]) 
REFERENCES [dbo].[Positions] ([Position_Id]);








PRINT N'Creating Foreign Key on [dbo].[PositionPersonnelOverrides]....';



ALTER TABLE [dbo].[PositionPersonnelOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_PositionPersonnelOverrides_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating Foreign Key on [dbo].[Positions]....';



ALTER TABLE [dbo].[Positions] WITH NOCHECK
ADD CONSTRAINT [FK_Positions_OrgUnits] FOREIGN KEY ([OrgUnit_Id]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating Foreign Key on [dbo].[Positions]....';



ALTER TABLE [dbo].[Positions] WITH NOCHECK
ADD CONSTRAINT [FK_Positions_Roles] FOREIGN KEY ([Role_Id]) 
REFERENCES [dbo].[Roles] ([Role_Id]);



PRINT N'Creating Foreign Key on [dbo].[ProductBasketEntrys]....';



ALTER TABLE [dbo].[ProductBasketEntrys] WITH NOCHECK
ADD CONSTRAINT [FK_ProductBasketEntrys_Products] FOREIGN KEY ([Product_Id]) 
REFERENCES [dbo].[Products] ([Product_Id]);



PRINT N'Creating Foreign Key on [dbo].[ProductBasketEntrys]....';



ALTER TABLE [dbo].[ProductBasketEntrys] WITH NOCHECK
ADD CONSTRAINT [FK_ProductBasketEntrys_ProductBaskets] FOREIGN KEY ([ProductBasket_Id]) 
REFERENCES [dbo].[ProductBaskets] ([ProductBasket_Id]);





PRINT N'Creating Foreign Key on [dbo].[ProductPositionOverrides]....';



ALTER TABLE [dbo].[ProductPositionOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_ProductPositionOverrides_NewProductBasket] FOREIGN KEY ([NewProductBasket_Id]) 
REFERENCES [dbo].[ProductBaskets] ([ProductBasket_Id]);



PRINT N'Creating Foreign Key on [dbo].[ProductPositionOverrides]....';



ALTER TABLE [dbo].[ProductPositionOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_ProductPositionOverrides_OldProductBasket] FOREIGN KEY ([OldProductBasket_Id]) 
REFERENCES [dbo].[ProductBaskets] ([ProductBasket_Id]);



PRINT N'Creating Foreign Key on [dbo].[ProductPositionOverrides]....';



ALTER TABLE [dbo].[ProductPositionOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_ProductPositionOverrides_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating Foreign Key on [dbo].[ProductPositions]....';



ALTER TABLE [dbo].[ProductPositions] WITH NOCHECK
ADD CONSTRAINT [FK_ProductPositions_Positions] FOREIGN KEY ([Position_Id]) 
REFERENCES [dbo].[Positions] ([Position_Id]);



PRINT N'Creating Foreign Key on [dbo].[ProductPositions]....';



ALTER TABLE [dbo].[ProductPositions] WITH NOCHECK
ADD CONSTRAINT [FK_ProductPositions_Products] FOREIGN KEY ([Product_Id]) 
REFERENCES [dbo].[Products] ([Product_Id]);



PRINT N'Creating Foreign Key on [dbo].[RequestAlignmentEntityAttributeCache]....';



ALTER TABLE [dbo].[RequestAlignmentEntityAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_RequestAlignmentEntityAttributeCache_AlignmentEntitys] FOREIGN KEY ([AlignmentEntity_Id]) 
REFERENCES [dbo].[AlignmentEntitys] ([AlignmentEntity_Id]);



PRINT N'Creating Foreign Key on [dbo].[RequestAlignmentEntityAttributeCache]....';



ALTER TABLE [dbo].[RequestAlignmentEntityAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_RequestAlignmentEntityAttributeCache_AttributeTypes] FOREIGN KEY ([AttributeType_Id]) 
REFERENCES [dbo].[AttributeTypes] ([AttributeType_Id]);



PRINT N'Creating Foreign Key on [dbo].[RequestAlignmentEntityAttributeCache]....';



ALTER TABLE [dbo].[RequestAlignmentEntityAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_RequestAlignmentEntityAttributeCache_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating Foreign Key on [dbo].[RequestAlignmentEntityAttributeCache]....';



ALTER TABLE [dbo].[RequestAlignmentEntityAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_RequestAlignmentEntityAttributeCache_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);



PRINT N'Creating FK_RequestOrgUnitAttributeCache_OrgUnits...';



ALTER TABLE [dbo].[RequestOrgUnitAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_RequestOrgUnitAttributeCache_OrgUnits] FOREIGN KEY ([OrgUnit_Id]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating Foreign Key on [dbo].[RequestOrgUnitAttributeCache]....';



ALTER TABLE [dbo].[RequestOrgUnitAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_RequestOrgUnitAttributeCache_AttributeTypes] FOREIGN KEY ([AttributeType_Id]) 
REFERENCES [dbo].[AttributeTypes] ([AttributeType_Id]);



PRINT N'Creating Foreign Key on [dbo].[RequestOrgUnitAttributeCache]....';



ALTER TABLE [dbo].[RequestOrgUnitAttributeCache] WITH NOCHECK
ADD CONSTRAINT [FK_RequestOrgUnitAttributeCache_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating Foreign Key on [dbo].[Requests]....';



ALTER TABLE [dbo].[Requests] WITH NOCHECK
ADD CONSTRAINT [FK_Requests_ApprovalProgressTrackers] FOREIGN KEY ([ApprovalProgressTracker_Id]) 
REFERENCES [dbo].[ApprovalProgressTrackers] ([ApprovalProgressTracker_Id]);



PRINT N'Creating Foreign Key on [dbo].[RoleProducts]....';



ALTER TABLE [dbo].[RoleProducts] WITH NOCHECK
ADD CONSTRAINT [FK_RoleProducts_Products] FOREIGN KEY ([Product_Id]) 
REFERENCES [dbo].[Products] ([Product_Id]);



PRINT N'Creating Foreign Key on [dbo].[RoleProducts]....';



ALTER TABLE [dbo].[RoleProducts] WITH NOCHECK
ADD CONSTRAINT [FK_RoleProducts_Roles] FOREIGN KEY ([Role_Id]) 
REFERENCES [dbo].[Roles] ([Role_Id]);



PRINT N'Creating Foreign Key on [dbo].[Roles]....';



ALTER TABLE [dbo].[Roles] WITH NOCHECK
ADD CONSTRAINT [FK_Roles_Teams] FOREIGN KEY ([Team_Id]) 
REFERENCES [dbo].[Teams] ([Team_Id]);









PRINT N'Creating FK_TerritoryShapes_OrgUnits...';



ALTER TABLE [dbo].[TerritoryShapes] WITH NOCHECK
ADD CONSTRAINT [FK_TerritoryShapes_OrgUnits] FOREIGN KEY ([OrgUnit_Id]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);



PRINT N'Creating Foreign Key on [dbo].[TerritoryTypePermissions]....';



ALTER TABLE [dbo].[TerritoryTypePermissions] WITH NOCHECK
ADD CONSTRAINT [FK_TerritoryTypePermissions_Permissions] FOREIGN KEY ([Permission_Id]) 
REFERENCES [dbo].[Permissions] ([Permission_Id]);



PRINT N'Creating Foreign Key on [dbo].[TerritoryTypePermissions]....';



ALTER TABLE [dbo].[TerritoryTypePermissions] WITH NOCHECK
ADD CONSTRAINT [FK_TerritoryTypePermissions_TerritoryTypes] FOREIGN KEY ([TerritoryType_Id]) 
REFERENCES [dbo].[TerritoryTypes] ([TerritoryType_Id]);



PRINT N'Creating Foreign Key on [dbo].[TerritoryTypes]....';



ALTER TABLE [dbo].[TerritoryTypes] WITH NOCHECK
ADD CONSTRAINT [FK_TerritoryTypes_OrgUnitLevels] FOREIGN KEY ([OrgUnitLevel_Id]) 
REFERENCES [dbo].[OrgUnitLevels] ([OrgUnitLevel_Id]);



PRINT N'Creating Foreign Key on [dbo].[TerritoryTypes]....';



ALTER TABLE [dbo].[TerritoryTypes] WITH NOCHECK
ADD CONSTRAINT [FK_TerritoryTypes_OrgUnits] FOREIGN KEY ([UnassignedOrgUnit_Id]) 
REFERENCES [dbo].[OrgUnits] ([OrgUnit_Id]);




PRINT N'Creating Foreign Key on [dbo].[UpsertAlignmentEntityOverrides]....';



ALTER TABLE [dbo].[UpsertAlignmentEntityOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_UpsertAlignmentEntityOverrides_NewAlignmentEntityState] FOREIGN KEY ([NewAlignmentEntityState_Id]) 
REFERENCES [dbo].[AlignmentEntityStates] ([AlignmentEntityState_Id]);



PRINT N'Creating Foreign Key on [dbo].[UpsertAlignmentEntityOverrides]....';



ALTER TABLE [dbo].[UpsertAlignmentEntityOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_UpsertAlignmentEntityOverrides_OldAlignmentEntityState] FOREIGN KEY ([OldAlignmentEntityState_Id]) 
REFERENCES [dbo].[AlignmentEntityStates] ([AlignmentEntityState_Id]);



PRINT N'Creating Foreign Key on [dbo].[UpsertAlignmentEntityOverrides]....';



ALTER TABLE [dbo].[UpsertAlignmentEntityOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_UpsertAlignmentEntityOverrides_Requests] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating FK_NewPersonnelState_UpsertPersonnelOverrides...';



ALTER TABLE [dbo].[UpsertPersonnelOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_UpsertPersonnelOverrides_NewPersonnelState] FOREIGN KEY ([NewPersonnelState_Id]) 
REFERENCES [dbo].[PersonnelStates] ([PersonnelState_Id]);



PRINT N'Creating FK_OldPersonnelState_UpsertPersonnelOverrides...';



ALTER TABLE [dbo].[UpsertPersonnelOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_UpsertPersonnelOverrides_OldPersonnelState] FOREIGN KEY ([OldPersonnelState_Id]) 
REFERENCES [dbo].[PersonnelStates] ([PersonnelState_Id]);



PRINT N'Creating FK_Request_UpsertPersonnelOverrides...';



ALTER TABLE [dbo].[UpsertPersonnelOverrides] WITH NOCHECK
ADD CONSTRAINT [FK_UpsertPersonnelOverrides_Request] FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Creating Foreign Key on [dbo].[ValidationRanges]....';



ALTER TABLE [dbo].[ValidationRanges] WITH NOCHECK
ADD CONSTRAINT [FK_ValidationRanges_Validations] FOREIGN KEY ([Validation_Id]) 
REFERENCES [dbo].[Validations] ([Validation_Id]);



PRINT N'Creating Foreign Key on [dbo].[Validations]....';



ALTER TABLE [dbo].[Validations] WITH NOCHECK
ADD CONSTRAINT [FK_Validations_AttributeTypes] FOREIGN KEY ([AttributeType_Id]) 
REFERENCES [dbo].[AttributeTypes] ([AttributeType_Id]);



PRINT N'Creating Foreign Key on [dbo].[Validations]....';



ALTER TABLE [dbo].[Validations] WITH NOCHECK
ADD CONSTRAINT [FK_Validations_Folders] FOREIGN KEY ([Folder_Id]) 
REFERENCES [dbo].[Folders] ([Folder_Id]);






PRINT N'Creating FK_Request_DeleteAlignmentEntityOverrides...';



ALTER TABLE [dbo].[DeleteAlignmentEntityOverrides] WITH NOCHECK
ADD CONSTRAINT FK_DeleteAlignmentEntityOverrides_Request FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);





PRINT N'Creating FK_Request_DeletePersonnelOverrides...';



ALTER TABLE [dbo].DeletePersonnelOverrides WITH NOCHECK
ADD CONSTRAINT FK_DeletePersonnelOverrides_Request FOREIGN KEY ([Request_Id]) 
REFERENCES [dbo].[Requests] ([Request_Id]);



PRINT N'Checking existing data against newly created constraints';
ALTER TABLE [dbo].[AlignmentEntityNumberAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityNumberAttributeCache_AlignmentEntitys];
ALTER TABLE [dbo].[AlignmentEntityNumberAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityNumberAttributeCache_AttributeTypes];
ALTER TABLE [dbo].[AlignmentEntityNumberAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityNumberAttributeCache_TerritoryTypes];
ALTER TABLE [dbo].[AlignmentEntityOverrides] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityOverrides_NewAlignmentState];
ALTER TABLE [dbo].[AlignmentEntityOverrides] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityOverrides_OldAlignmentState];
ALTER TABLE [dbo].[AlignmentEntityOverrides] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityOverrides_Request];
ALTER TABLE [dbo].[AlignmentEntitys] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntitys_AlignmentEntitys];
ALTER TABLE [dbo].[AlignmentEntitys] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntitys_AlignmentEntityTypes];
ALTER TABLE [dbo].[AlignmentEntityTextAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityTextAttributeCache_AlignmentEntitys];
ALTER TABLE [dbo].[AlignmentEntityTextAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityTextAttributeCache_AttributeTypes];
ALTER TABLE [dbo].[AlignmentEntityTypeTerritoryTypes] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityTypeTerritoryTypes_AlignmentEntityTypes];
ALTER TABLE [dbo].[AlignmentEntityTypeTerritoryTypes] WITH CHECK CHECK CONSTRAINT [FK_AlignmentEntityTypeTerritoryTypes_TerritoryTypes];
ALTER TABLE [dbo].[AlignmentStateEntrys] WITH CHECK CHECK CONSTRAINT [FK_AlignmentStateEntrys_OrgUnits];
ALTER TABLE [dbo].[AlignmentStateEntrys] WITH CHECK CHECK CONSTRAINT [FK_AlignmentStateEntrys_AlignmentStates];
ALTER TABLE [dbo].[ApprovalProgress] WITH CHECK CHECK CONSTRAINT [FK_ApprovalProgress_Positions];
ALTER TABLE [dbo].[ApprovalProgressLogicalExpressions] WITH CHECK CHECK CONSTRAINT [FK_ApprovalProgressLogicalExpressions_ApprovalProgress];
ALTER TABLE [dbo].[ApprovalProgressLogicalExpressions] WITH CHECK CHECK CONSTRAINT [FK_ApprovalProgressLogicalExpressions_ApprovalProgressLogicalExpressions];
ALTER TABLE [dbo].[ApprovalProgressLogicalExpressions] WITH CHECK CHECK CONSTRAINT [FK_ApprovalProgressLogicalExpressions_MultipleApprovalProgressLogicalExpressions];
ALTER TABLE [dbo].[ApprovalProgressTerritoryTypes] WITH CHECK CHECK CONSTRAINT [FK_ApprovalProgressTerritoryTypes_ApprovalProgress];
ALTER TABLE [dbo].[ApprovalProgressTerritoryTypes] WITH CHECK CHECK CONSTRAINT [FK_ApprovalProgressTerritoryTypes_TerritoryTypes];
ALTER TABLE [dbo].[ApprovalRoundOrgUnitLevelPermissions] WITH CHECK CHECK CONSTRAINT [FK_ApprovalRoundOrgUnitLevelPermissions_ApprovalRounds];
ALTER TABLE [dbo].[ApprovalRoundOrgUnitLevelPermissions] WITH CHECK CHECK CONSTRAINT [FK_ApprovalRoundOrgUnitLevelPermissions_OrgUnitLevelPermissions];
ALTER TABLE [dbo].[ApprovalRoundProgressTrackers] WITH CHECK CHECK CONSTRAINT [FK_ApprovalRoundProgressTrackers_ApprovalProgressTrackers];
ALTER TABLE [dbo].[ApprovalRoundProgressTrackers] WITH CHECK CHECK CONSTRAINT [FK_ApprovalRoundProgressTrackers_ApprovalProgressLogicalExpressions];
ALTER TABLE [dbo].[ApprovalRoundRoles] WITH CHECK CHECK CONSTRAINT [FK_ApprovalRoundRoles_ApprovalRounds];
ALTER TABLE [dbo].[ApprovalRoundRoles] WITH CHECK CHECK CONSTRAINT [FK_ApprovalRoundRoles_Roles];
ALTER TABLE [dbo].[ApprovalRoundTerritoryTypePermissions] WITH CHECK CHECK CONSTRAINT [FK_ApprovalRoundTerritoryTypePermissions_ApprovalRounds];
ALTER TABLE [dbo].[ApprovalRoundTerritoryTypePermissions] WITH CHECK CHECK CONSTRAINT [FK_ApprovalRoundTerritoryTypePermissions_TerritoryTypePermissions];
ALTER TABLE [dbo].[AttributeTypeTerritoryTypes] WITH CHECK CHECK CONSTRAINT [FK_AttributeTypeTerritoryTypes_AttributeTypes];
ALTER TABLE [dbo].[AttributeTypeTerritoryTypes] WITH CHECK CHECK CONSTRAINT [FK_AttributeTypeTerritoryTypes_TerritoryTypes];
ALTER TABLE [dbo].[Customers] WITH CHECK CHECK CONSTRAINT [FK_Customers_AlignmentEntitys];
ALTER TABLE [dbo].[Customers] WITH CHECK CHECK CONSTRAINT [FK_Customers_CustomerTypes];
ALTER TABLE [dbo].[EditAttributeOverrides] WITH CHECK CHECK CONSTRAINT [FK_EditAttributeOverrides_AttributeTypes];
ALTER TABLE [dbo].[EditAttributeOverrides] WITH CHECK CHECK CONSTRAINT [FK_EditAttributeOverrides_Requests];
ALTER TABLE [dbo].Geos WITH CHECK CHECK CONSTRAINT [FK_Geos_AlignmentEntitys];
ALTER TABLE [dbo].[GroupedEntities] WITH CHECK CHECK CONSTRAINT [FK_GroupedEntities_AlignmentEntitys];
ALTER TABLE [dbo].[GroupedEntities] WITH CHECK CHECK CONSTRAINT [FK_GroupedEntities_TerritoryTypes];
ALTER TABLE [dbo].[IssueRepairCache] WITH CHECK CHECK CONSTRAINT [FK_IssueRepairCache_AlignmentEntitys];
ALTER TABLE [dbo].[IssueRepairCache] WITH CHECK CHECK CONSTRAINT [FK_IssueRepairCache_TerritoryTypes];
ALTER TABLE [dbo].[OrderedOverrides] WITH CHECK CHECK CONSTRAINT [FK_OrderedOverrides_ParentOrderedOverride];
ALTER TABLE [dbo].[OrderedOverrides] WITH CHECK CHECK CONSTRAINT [FK_OrderedOverrides_Requests];
ALTER TABLE [dbo].[OrgUnitAlignmentEntitys] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitAlignmentEntitys_AlignmentEntitys];
ALTER TABLE [dbo].[OrgUnitAlignmentEntitys] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitAlignmentEntitys_OrgUnits];
ALTER TABLE [dbo].[OrgUnitAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitAttributeCache_AttributeTypes];
ALTER TABLE [dbo].[OrgUnitAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitAttributeCache_OrgUnits];
ALTER TABLE [dbo].[OrgUnitLevelPermissions] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitLevelPermissions_OrgUnitLevels];
ALTER TABLE [dbo].[OrgUnitLevelPermissions] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitLevelPermissions_Permissions];
ALTER TABLE [dbo].[OrgUnitLevelRoles] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitLevelRoles_OrgUnitLevels];
ALTER TABLE [dbo].[OrgUnitLevelRoles] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitLevelRoles_Roles];
ALTER TABLE [dbo].[OrgUnitLevels] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitLevels_ParentOrgUnitLevel];
ALTER TABLE [dbo].[OrgUnitOverrides] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitOverrides_Requests];
ALTER TABLE [dbo].[OrgUnitRelationshipOverrides] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitRelationshipOverrides_NewOrgUnitRelationshipState];
ALTER TABLE [dbo].[OrgUnitRelationshipOverrides] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitRelationshipOverrides_OldOrgUnitRelationshipStates];
ALTER TABLE [dbo].[OrgUnitRelationshipOverrides] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitRelationshipOverrides_Requests];
ALTER TABLE [dbo].[OrgUnitRelationships] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitRelationships_DrivingOrgUnit];
ALTER TABLE [dbo].[OrgUnitRelationships] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitRelationships_AffectedOrgUnit];
ALTER TABLE [dbo].[OrgUnitRelationshipStateEntrys] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitRelationshipStateEntrys_OrgUnitRelationshipStates];
ALTER TABLE [dbo].[OrgUnitRelationshipStateEntrys] WITH CHECK CHECK CONSTRAINT [FK_OrgUnitRelationshipStateEntrys_OrgUnits];
ALTER TABLE [dbo].[OrgUnits] WITH CHECK CHECK CONSTRAINT [FK_OrgUnits_ParentOrgUnit];
ALTER TABLE [dbo].[OrgUnits] WITH CHECK CHECK CONSTRAINT [FK_OrgUnits_OrgUnitLevels];
ALTER TABLE [dbo].[PositionOverrides] WITH CHECK CHECK CONSTRAINT [FK_PositionOverrides_Requests];
ALTER TABLE [dbo].[PositionPersonnel] WITH CHECK CHECK CONSTRAINT [FK_PositionPersonnel_Personnel];
ALTER TABLE [dbo].[PositionPersonnel] WITH CHECK CHECK CONSTRAINT [FK_PositionPersonnel_Positions];
ALTER TABLE [dbo].[PositionPersonnelOverrides] WITH CHECK CHECK CONSTRAINT [FK_PositionPersonnelOverrides_Requests];
ALTER TABLE [dbo].[Positions] WITH CHECK CHECK CONSTRAINT [FK_Positions_OrgUnits];
ALTER TABLE [dbo].[Positions] WITH CHECK CHECK CONSTRAINT [FK_Positions_Roles];
ALTER TABLE [dbo].[ProductBasketEntrys] WITH CHECK CHECK CONSTRAINT [FK_ProductBasketEntrys_Products];
ALTER TABLE [dbo].[ProductBasketEntrys] WITH CHECK CHECK CONSTRAINT [FK_ProductBasketEntrys_ProductBaskets];
ALTER TABLE [dbo].[ProductPositionOverrides] WITH CHECK CHECK CONSTRAINT [FK_ProductPositionOverrides_NewProductBasket];
ALTER TABLE [dbo].[ProductPositionOverrides] WITH CHECK CHECK CONSTRAINT [FK_ProductPositionOverrides_OldProductBasket];
ALTER TABLE [dbo].[ProductPositionOverrides] WITH CHECK CHECK CONSTRAINT [FK_ProductPositionOverrides_Requests];
ALTER TABLE [dbo].[ProductPositions] WITH CHECK CHECK CONSTRAINT [FK_ProductPositions_Positions];
ALTER TABLE [dbo].[ProductPositions] WITH CHECK CHECK CONSTRAINT [FK_ProductPositions_Products];
ALTER TABLE [dbo].[RequestAlignmentEntityAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_RequestAlignmentEntityAttributeCache_AlignmentEntitys];
ALTER TABLE [dbo].[RequestAlignmentEntityAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_RequestAlignmentEntityAttributeCache_AttributeTypes];
ALTER TABLE [dbo].[RequestAlignmentEntityAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_RequestAlignmentEntityAttributeCache_Requests];
ALTER TABLE [dbo].[RequestAlignmentEntityAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_RequestAlignmentEntityAttributeCache_TerritoryTypes];
ALTER TABLE [dbo].[RequestOrgUnitAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_RequestOrgUnitAttributeCache_OrgUnits];
ALTER TABLE [dbo].[RequestOrgUnitAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_RequestOrgUnitAttributeCache_AttributeTypes];
ALTER TABLE [dbo].[RequestOrgUnitAttributeCache] WITH CHECK CHECK CONSTRAINT [FK_RequestOrgUnitAttributeCache_Requests];
ALTER TABLE [dbo].[Requests] WITH CHECK CHECK CONSTRAINT [FK_Requests_ApprovalProgressTrackers];
ALTER TABLE [dbo].[RoleProducts] WITH CHECK CHECK CONSTRAINT [FK_RoleProducts_Products];
ALTER TABLE [dbo].[RoleProducts] WITH CHECK CHECK CONSTRAINT [FK_RoleProducts_Roles];
ALTER TABLE [dbo].[Roles] WITH CHECK CHECK CONSTRAINT [FK_Roles_Teams];
ALTER TABLE [dbo].[TerritoryShapes] WITH CHECK CHECK CONSTRAINT [FK_TerritoryShapes_OrgUnits];
ALTER TABLE [dbo].[TerritoryTypePermissions] WITH CHECK CHECK CONSTRAINT [FK_TerritoryTypePermissions_Permissions];
ALTER TABLE [dbo].[TerritoryTypePermissions] WITH CHECK CHECK CONSTRAINT [FK_TerritoryTypePermissions_TerritoryTypes];
ALTER TABLE [dbo].[TerritoryTypes] WITH CHECK CHECK CONSTRAINT [FK_TerritoryTypes_OrgUnitLevels];
ALTER TABLE [dbo].[TerritoryTypes] WITH CHECK CHECK CONSTRAINT [FK_TerritoryTypes_OrgUnits];
ALTER TABLE [dbo].[UpsertAlignmentEntityOverrides] WITH CHECK CHECK CONSTRAINT [FK_UpsertAlignmentEntityOverrides_NewAlignmentEntityState];
ALTER TABLE [dbo].[UpsertAlignmentEntityOverrides] WITH CHECK CHECK CONSTRAINT [FK_UpsertAlignmentEntityOverrides_OldAlignmentEntityState];
ALTER TABLE [dbo].[UpsertAlignmentEntityOverrides] WITH CHECK CHECK CONSTRAINT [FK_UpsertAlignmentEntityOverrides_Requests];
ALTER TABLE [dbo].[UpsertPersonnelOverrides] WITH CHECK CHECK CONSTRAINT [FK_UpsertPersonnelOverrides_NewPersonnelState];
ALTER TABLE [dbo].[UpsertPersonnelOverrides] WITH CHECK CHECK CONSTRAINT [FK_UpsertPersonnelOverrides_OldPersonnelState];
ALTER TABLE [dbo].[UpsertPersonnelOverrides] WITH CHECK CHECK CONSTRAINT [FK_UpsertPersonnelOverrides_Request];
ALTER TABLE [dbo].[ValidationRanges] WITH CHECK CHECK CONSTRAINT [FK_ValidationRanges_Validations];
ALTER TABLE [dbo].[Validations] WITH CHECK CHECK CONSTRAINT [FK_Validations_AttributeTypes];
ALTER TABLE [dbo].[Validations] WITH CHECK CHECK CONSTRAINT [FK_Validations_Folders];
ALTER TABLE [dbo].DeleteAlignmentEntityOverrides WITH CHECK CHECK CONSTRAINT [FK_DeleteAlignmentEntityOverrides_Request];
ALTER TABLE [dbo].[DeletePersonnelOverrides] WITH CHECK CHECK CONSTRAINT FK_DeletePersonnelOverrides_Request;

PRINT N'Update complete.';



