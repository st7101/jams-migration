DECLARE @TableName NVARCHAR(100) = REPLACE(':TableName','_Temp','') --:TableName

SET @TableName = REPLACE(@TableName,'[','')
SET @TableName = REPLACE(@TableName,']','')

DECLARE @sqlCreate NVARCHAR(MAX) 

SET @sqlCreate='CREATE TABLE [dbo].['+@TableName+'](
	[CUSTOMER_EXTERNALID] [NVARCHAR](100) NOT NULL,
	[CUSTOMER_ID] [BIGINT] NOT NULL,
 CONSTRAINT [PK_'+@TableName+'] PRIMARY KEY CLUSTERED 
(
	[CUSTOMER_ID] ASC
)WITH (DATA_COMPRESSION = PAGE)
) ON [PRIMARY]'

exec (  @sqlCreate)

DECLARE @sqlInsert NVARCHAR(MAX)
SET @SqlInsert ='
INSERT INTO [dbo].['+@TableName+']
           ([CUSTOMER_EXTERNALID]
           ,[CUSTOMER_ID])
SELECT 
	ae.AlignmentEntity_ExternalId,ae.AlignmentEntity_Id
	
FROM  dbo.AlignmentEntitys ae 
where ae.AlignmentEntity_ExternalId IN (select [CUSTOMER_ID] from :TableName)'
exec (@sqlInsert)

