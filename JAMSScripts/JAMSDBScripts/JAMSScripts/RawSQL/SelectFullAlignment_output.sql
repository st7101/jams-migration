-- get explicit alignments
select 
	alignments.AlignmentEntity_Id as AlignmentEntity_Id, 
	territoryTypes.TerritoryType_Id as TerritoryType_Id,
	alignments.OrgUnit_Id as OrgUnit_Id, 
	alignments.OrgUnitAlignmentEntity_Weight as OrgUnitAlignmentEntity_Weight
	{1}
from [{0}].dbo.OrgUnitAlignmentEntitys alignments
join [{0}].dbo.AlignmentEntitys entitys on alignments.AlignmentEntity_Id = entitys.AlignmentEntity_Id
join [{0}].dbo.OrgUnits orgUnits on alignments.OrgUnit_Id = orgUnits.OrgUnit_Id
join [{0}].dbo.TerritoryTypes territoryTypes on orgUnits.OrgUnitLevel_Id = territoryTypes.OrgUnitLevel_Id
where entitys.AlignmentEntity_Status = 'Normal'

union all

-- get derived alignments
select 
	grouped.AlignmentEntity_Id as AlignmentEntity_Id, 
	grouped.TerritoryType_Id as TerritoryType_Id, 
	alignments.OrgUnit_Id as OrgUnit_Id, 
	alignments.OrgUnitAlignmentEntity_Weight as OrgUnitAlignmentEntity_Weight
	{1}
from [{0}].dbo.GroupedEntities grouped
join [{0}].dbo.AlignmentEntitys entitys on grouped.alignmentEntity_Id = entitys.alignmentEntity_Id
join [{0}].dbo.AlignmentEntitys parents on entitys.AlignmentEntity_ParentId = parents.AlignmentEntity_Id
join [{0}].dbo.OrgUnitAlignmentEntitys alignments on entitys.alignmentEntity_ParentId = alignments.AlignmentEntity_Id
join [{0}].dbo.OrgUnits orgUnits on alignments.OrgUnit_Id = OrgUnits.OrgUnit_Id
join [{0}].dbo.TerritoryTypes territoryTypes on orgUnits.OrgUnitLevel_Id = territoryTypes.OrgUnitLevel_Id
	and territoryTypes.TerritoryType_Id = grouped.TerritoryType_Id
where entitys.AlignmentEntity_Status = 'Normal'
	and parents.AlignmentEntity_Status = 'Normal';