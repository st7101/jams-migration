PRINT N'Starting rebuilding table [dbo].[AlignmentEntitys]...';


/*Base table structure */
DECLARE @CreateTable VARCHAR(MAX) ,
    @InsertTable VARCHAR(MAX)
DECLARE @GetIDs VARCHAR(MAX)



SET @CreateTable = '
CREATE TABLE {DBIdentifier}.[dbo].[tmp_ms_xx_AlignmentEntitys] (
	[AlignmentEntity_Id] bigint not null,
	[AlignmentEntity_ExternalId] [nvarchar] (100) NOT NULL,
	[AlignmentEntity_Name] [nvarchar](255) NULL,
	[AlignmentEntity_Status] [nvarchar](255) NULL,
	[AlignmentEntity_Latitude] [float] NULL,
	[AlignmentEntity_Longitude] [float] NULL,
	[AlignmentEntity_ParentExternalId] [nvarchar](100) NULL,
	[AlignmentEntity_ParentId] [bigint] NULL,
	[AlignmentEntityType_Id] [bigint] NOT NULL,
	[TileX_20] AS ([dbo].[ConvertLongitudeToTileX] (AlignmentEntity_Longitude,20)) PERSISTED,
	[TileY_20] AS ([dbo].[ConvertLatitudeToTileY] (AlignmentEntity_Latitude,20)) PERSISTED,
 ' + CHAR(13)
 /*add metric columns*/
SELECT  @CreateTable = @CreateTable + QUOTENAME(clmns.name) + ' '
        + 'NUMERIC (' + CAST(clmns.PRECISION AS VARCHAR) + ','
        + CAST(clmns.SCALE AS VARCHAR) + ')' + ' ' + CASE clmns.is_nullable
                                                       WHEN 1 THEN 'NULL'
                                                       ELSE 'NOT NULL'
                                                     END + ', ' + CHAR(13)
FROM    {DBIdentifier}.sys.tables AS tbl
INNER JOIN {DBIdentifier}.sys.all_columns AS clmns ON clmns.object_Id = tbl.object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.types AS baset ON ( baset.user_type_Id = clmns.system_type_Id
                                        AND baset.user_type_Id = baset.system_type_Id
                                      )
                                      OR ( ( baset.system_type_Id = clmns.system_type_Id )
                                           AND ( baset.user_type_Id = clmns.user_type_Id )
                                           AND ( baset.is_user_defined = 0 )
                                           AND ( baset.is_assembly_type = 1 )
                                         )
LEFT OUTER JOIN {DBIdentifier}.sys.types AS usrt ON usrt.user_type_Id = clmns.user_type_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS d ON d.object_Id = clmns.default_object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS r ON r.object_Id = clmns.rule_object_Id
WHERE   ( tbl.name = 'AlignmentEntitys'
          AND SCHEMA_NAME(tbl.schema_Id) = 'dbo'
          AND baset.NAME = 'numeric'
        )
ORDER BY clmns.column_Id ASC

/*add attribute columns*/
SELECT  @CreateTable = @CreateTable + QUOTENAME(clmns.name) + ' '
        + 'NVARCHAR (' + CAST(clmns.max_length / 2 AS VARCHAR) + ')' + ' '
        + CASE clmns.is_nullable
            WHEN 1 THEN 'NULL'
            ELSE 'NOT NULL'
          END + ', ' + CHAR(13)
FROM    {DBIdentifier}.sys.tables AS tbl
INNER JOIN {DBIdentifier}.sys.all_columns AS clmns ON clmns.object_Id = tbl.object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.types AS baset ON ( baset.user_type_Id = clmns.system_type_Id
                                        AND baset.user_type_Id = baset.system_type_Id
                                      )
                                      OR ( ( baset.system_type_Id = clmns.system_type_Id )
                                           AND ( baset.user_type_Id = clmns.user_type_Id )
                                           AND ( baset.is_user_defined = 0 )
                                           AND ( baset.is_assembly_type = 1 )
                                         )
LEFT OUTER JOIN {DBIdentifier}.sys.types AS usrt ON usrt.user_type_Id = clmns.user_type_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS d ON d.object_Id = clmns.default_object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS r ON r.object_Id = clmns.rule_object_Id
WHERE   ( tbl.name = 'AlignmentEntitys'
          AND SCHEMA_NAME(tbl.schema_Id) = 'dbo'
          AND baset.NAME = 'nvarchar'
          AND clmns.max_length = 4000
        )
ORDER BY clmns.column_Id ASC
SET @CreateTable = @CreateTable
    + '
CONSTRAINT [tmp_ms_xx_PK_AlignmentEntitys] PRIMARY KEY CLUSTERED ([AlignmentEntity_Id] ASC) WITH (DATA_COMPRESSION = PAGE)
);'

EXEC (@CreateTable)

/* creating the insert list*/
SET @InsertTable = '
IF EXISTS (SELECT TOP 1 1 FROM {DBIdentifier}.[dbo].[AlignmentEntitys])
BEGIN
INSERT INTO {DBIdentifier}.[dbo].[tmp_ms_xx_AlignmentEntitys] WITH (TABLOCK) ([AlignmentEntity_Id],[AlignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ParentExternalId],[AlignmentEntityType_id],'

/*adding metrics to the insert list*/
SELECT  @InsertTable = @InsertTable + QUOTENAME(clmns.name) + ','
FROM    {DBIdentifier}.sys.tables AS tbl
INNER JOIN {DBIdentifier}.sys.all_columns AS clmns ON clmns.object_Id = tbl.object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.types AS baset ON ( baset.user_type_Id = clmns.system_type_Id
                                        AND baset.user_type_Id = baset.system_type_Id
                                      )
                                      OR ( ( baset.system_type_Id = clmns.system_type_Id )
                                           AND ( baset.user_type_Id = clmns.user_type_Id )
                                           AND ( baset.is_user_defined = 0 )
                                           AND ( baset.is_assembly_type = 1 )
                                         )
LEFT OUTER JOIN {DBIdentifier}.sys.types AS usrt ON usrt.user_type_Id = clmns.user_type_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS d ON d.object_Id = clmns.default_object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS r ON r.object_Id = clmns.rule_object_Id
WHERE   ( tbl.name = 'AlignmentEntitys'
          AND SCHEMA_NAME(tbl.schema_Id) = 'dbo'
          AND baset.NAME = 'numeric'
        )
ORDER BY clmns.column_Id ASC

/* adding attributes to the insert list */		
SELECT  @InsertTable = @InsertTable + QUOTENAME(clmns.name) + ','
FROM    {DBIdentifier}.sys.tables AS tbl
INNER JOIN {DBIdentifier}.sys.all_columns AS clmns ON clmns.object_Id = tbl.object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.types AS baset ON ( baset.user_type_Id = clmns.system_type_Id
                                        AND baset.user_type_Id = baset.system_type_Id
                                      )
                                      OR ( ( baset.system_type_Id = clmns.system_type_Id )
                                           AND ( baset.user_type_Id = clmns.user_type_Id )
                                           AND ( baset.is_user_defined = 0 )
                                           AND ( baset.is_assembly_type = 1 )
                                         )
LEFT OUTER JOIN {DBIdentifier}.sys.types AS usrt ON usrt.user_type_Id = clmns.user_type_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS d ON d.object_Id = clmns.default_object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS r ON r.object_Id = clmns.rule_object_Id
WHERE   ( tbl.name = 'AlignmentEntitys'
          AND SCHEMA_NAME(tbl.schema_Id) = 'dbo'
          AND baset.NAME = 'nvarchar'
          AND clmns.max_length = 4000
        )
ORDER BY clmns.column_Id ASC	

/* finish the insert list*/
SET @InsertTable = SUBSTRING(@InsertTable, 1, LEN(@InsertTable) - 1) +  ')'
/* start the select list */
SET @insertTable = @inserttable + ' 
SELECT ROW_NUMBER() OVER (ORDER BY alignmentEntity_Id ASC) ,[AlignmentEntity_Id],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ParentId],[AlignmentEntityType_id],'
/*adding metrics to the select list*/
SELECT  @InsertTable = @InsertTable + QUOTENAME(clmns.name) + ','
FROM    {DBIdentifier}.sys.tables AS tbl
INNER JOIN {DBIdentifier}.sys.all_columns AS clmns ON clmns.object_Id = tbl.object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.types AS baset ON ( baset.user_type_Id = clmns.system_type_Id
                                        AND baset.user_type_Id = baset.system_type_Id
                                      )
                                      OR ( ( baset.system_type_Id = clmns.system_type_Id )
                                           AND ( baset.user_type_Id = clmns.user_type_Id )
                                           AND ( baset.is_user_defined = 0 )
                                           AND ( baset.is_assembly_type = 1 )
                                         )
LEFT OUTER JOIN {DBIdentifier}.sys.types AS usrt ON usrt.user_type_Id = clmns.user_type_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS d ON d.object_Id = clmns.default_object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS r ON r.object_Id = clmns.rule_object_Id
WHERE   ( tbl.name = 'AlignmentEntitys'
          AND SCHEMA_NAME(tbl.schema_Id) = 'dbo'
          AND baset.NAME = 'numeric'
        )
ORDER BY clmns.column_Id ASC

/* adding attributes to the select list */		
SELECT  @InsertTable = @InsertTable + QUOTENAME(clmns.name) + ','
FROM    {DBIdentifier}.sys.tables AS tbl
INNER JOIN {DBIdentifier}.sys.all_columns AS clmns ON clmns.object_Id = tbl.object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.types AS baset ON ( baset.user_type_Id = clmns.system_type_Id
                                        AND baset.user_type_Id = baset.system_type_Id
                                      )
                                      OR ( ( baset.system_type_Id = clmns.system_type_Id )
                                           AND ( baset.user_type_Id = clmns.user_type_Id )
                                           AND ( baset.is_user_defined = 0 )
                                           AND ( baset.is_assembly_type = 1 )
                                         )
LEFT OUTER JOIN {DBIdentifier}.sys.types AS usrt ON usrt.user_type_Id = clmns.user_type_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS d ON d.object_Id = clmns.default_object_Id
LEFT OUTER JOIN {DBIdentifier}.sys.objects AS r ON r.object_Id = clmns.rule_object_Id
WHERE   ( tbl.name = 'AlignmentEntitys'
          AND SCHEMA_NAME(tbl.schema_Id) = 'dbo'
          AND baset.NAME = 'nvarchar'
          AND clmns.max_length = 4000
        )
ORDER BY clmns.column_Id ASC
/* finish the insert list*/
SET @InsertTable = SUBSTRING(@InsertTable, 1, LEN(@InsertTable) - 1) +  '
FROM {DBIdentifier}.dbo.AlignmentEntitys ae 
END
'



EXEC (@insertTable)
DROP TABLE {DBIdentifier}.[dbo].[AlignmentEntitys];

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntitys]', N'AlignmentEntitys';

EXECUTE {DBIdentifier}.sys.sp_rename N'[dbo].[tmp_ms_xx_PK_AlignmentEntitys]', N'PK_AlignmentEntitys', N'OBJECT';

