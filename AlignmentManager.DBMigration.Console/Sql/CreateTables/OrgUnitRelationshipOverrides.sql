
PRINT N'Starting rebuilding table [dbo].[OrgUnitRelationshipOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitRelationshipOverrides]
    (
      [OrgUnitRelationshipOverride_Id] BIGINT NOT NULL ,
      [Request_Id] BIGINT NULL ,
      [OrgUnitRelationshipOverride_ExternalDrivingOrgUnitId] NVARCHAR(255)
        NULL ,
      [OrgUnitRelationshipOverride_DrivingOrgUnitId] BIGINT NULL ,
      [NewOrgUnitRelationshipState_Id] BIGINT NULL ,
      [OldOrgUnitRelationshipState_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitRelationshipOverrides] PRIMARY KEY CLUSTERED
        ( [OrgUnitRelationshipOverride_Id] ASC )
        WITH ( DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitRelationshipOverrides] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitRelationshipOverrides] WITH (TABLOCK)
                ( [OrgUnitRelationshipOverride_Id] ,
                  [Request_Id] ,
                  [OrgUnitRelationshipOverride_ExternalDrivingOrgUnitId] ,
                  [NewOrgUnitRelationshipState_Id] ,
                  [OldOrgUnitRelationshipState_Id]
                )
                SELECT  [OrgUnitRelationshipOverride_Id] ,
                        [Request_Id] ,
                        [OrgUnitRelationshipOverride_DrivingOrgUnitId] ,
                        [NewOrgUnitRelationshipState_Id] ,
                        [OldOrgUnitRelationshipState_Id]
                FROM    [dbo].[OrgUnitRelationshipOverrides]
                ORDER BY [OrgUnitRelationshipOverride_Id] ASC;
    END
DROP TABLE [dbo].[OrgUnitRelationshipOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitRelationshipOverrides]',
    N'OrgUnitRelationshipOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitRelationshipOverrides]',
    N'PK_OrgUnitRelationshipOverrides', N'OBJECT';
