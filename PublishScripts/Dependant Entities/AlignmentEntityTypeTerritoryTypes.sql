INSERT INTO [dbo].[AlignmentEntityTypeTerritoryTypes]
           ([AlignmentEntityTypeTerritoryType_Id]
           ,[ToleratePartialAlignment]
           ,[AlignmentEntityType_Id]
           ,[AlignmentEntityType_ParentId]
           ,[TerritoryType_ExternalId]
           ,[TerritoryType_Id])
SELECT 
	ROW_NUMBER() OVER (ORDER BY aettt.TERRITORYTYPE_ID),
	aettt.TOLERATEPARTIALALIGNMENT,
	aettt.ALIGNMENTENTITYTYPE_ID,
	aettt.ALIGNMENTENTITYTYPE_PARENTID,
	aettt.TERRITORYTYPE_ID,
	tt.TerritoryType_Id
 FROM dbo.AlignmentEntityTypeTerritoryTypes_Temp aettt
 INNER JOIN dbo.TerritoryTypes tt ON aettt.TERRITORYTYPE_ID=tt.TerritoryType_ExternalId

 