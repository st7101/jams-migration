INSERT INTO [AlignmentEntityNumberAttributeCache] WITH(TABLOCK) 
(AlignmentEntityNumberAttributeCache_Id, AlignmentEntityNumberAttributeCache_Value, AlignmentEntity_id, 
AttributeType_id, TerritoryType_id)
SELECT (AlignmentEntityNumberAttributeCache_Id + (SELECT ISNULL(MAX(AlignmentEntityNumberAttributeCache_Id),0)+1 
				FROM [AlignmentEntityNumberAttributeCache])), AlignmentEntityNumberAttributeCache_Value, 
AlignmentEntity_id, AttributeType_id, TerritoryType_id FROM 
[TempAlignmentEntityNumberAttributeCache]; 

/* Drop Temporary Table */
DROP TABLE [TempAlignmentEntityNumberAttributeCache]
DROP TABLE [ChildEntityPerTerritoryTypeMappings]
DROP TABLE [IndependentEntitys]

/* Update HiLo table */
DELETE FROM HiLoUniqueKey
WHERE ObjectType = 'AlignmentEntityNumberAttributeCache'

DELETE FROM HiLoUniqueKey
WHERE ObjectType = 'AlignmentEntityAttributeCache'

INSERT INTO HiLoUniqueKey (NextHi, ObjectType)
Values ((SELECT ISNULL(MAX(AlignmentEntityNumberAttributeCache_Id),0)+1 
				FROM [AlignmentEntityNumberAttributeCache]), 'AlignmentEntityNumberAttributeCache');