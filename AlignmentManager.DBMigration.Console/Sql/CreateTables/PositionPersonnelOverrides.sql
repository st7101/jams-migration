
PRINT N'Starting rebuilding table [dbo].[PositionPersonnelOverrides]...';
CREATE TABLE [dbo].[tmp_ms_xx_PositionPersonnelOverrides] (
    [PositionPersonnelOverride_Id]                      BIGINT             NOT NULL,
    [EffectiveDate]                                     DATETIMEOFFSET (7) NULL,
    [PositionPersonnelOverride_RefinementOperationType] NVARCHAR (100)     NULL,
    [PositionPersonnelOverride_ExternalPositionId]              NVARCHAR (255)     NULL,
    [PositionPersonnelOverride_PositionId]       BIGINT             NULL,
    [Personnel_Id]                                      NVARCHAR (100)     NULL,
    [Request_Id]                                        BIGINT             NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_PositionPersonnelOverrides] PRIMARY KEY CLUSTERED ([PositionPersonnelOverride_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[PositionPersonnelOverrides])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_PositionPersonnelOverrides] WITH (TABLOCK) ([PositionPersonnelOverride_Id], [EffectiveDate], [PositionPersonnelOverride_RefinementOperationType], [PositionPersonnelOverride_ExternalPositionId], [Personnel_Id], [Request_Id])
        SELECT   [PositionPersonnelOverride_Id],
                 [EffectiveDate],
                 [PositionPersonnelOverride_RefinementOperationType],
                 [PositionPersonnelOverride_PositionId],
                 [Personnel_Id],
                 [Request_Id]
        FROM     [dbo].[PositionPersonnelOverrides]
        ORDER BY [PositionPersonnelOverride_Id] ASC;
    END

DROP TABLE [dbo].[PositionPersonnelOverrides];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PositionPersonnelOverrides]', N'PositionPersonnelOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_PositionPersonnelOverrides]', N'PK_PositionPersonnelOverrides', N'OBJECT';
