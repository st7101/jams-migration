EXECUTE  [dbo].PersonnelSearchQuery
  @treeLeft= :treeLeft
  ,@treeRight= :treeRight
  ,@territoryTypeId=:terrTypeId
  ,@MaxResult=:maxResult
  ,@SearchString=:searchString
  ,@PersonnelStatus=:personnelStatus