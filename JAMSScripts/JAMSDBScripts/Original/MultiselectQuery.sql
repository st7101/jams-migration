/********* This query gets selected entities list ********************/

WITH AllEntities AS
(
	SELECT DISTINCT ae.AlignmentEntity_id
	FROM [AlignmentEntitys] ae
		inner join OrgUnitAlignmentEntitys oae
		ON ae.AlignmentEntity_id = oae.AlignmentEntity_id
		inner join [OrgUnits] ou
		ON oae.OrgUnit_id = ou.OrgUnit_id
		inner join [TerritoryTypes] tt
        ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id
	WHERE
		tt.TerritoryType_Id = :territoryTypeId
		and ae.AlignmentEntity_Latitude between :minLat and :maxLat
		and ae.AlignmentEntity_Longitude between :minLng and :maxLng
		and ((:showGeos = 1 and ae.AlignmentEntityType_Id = 1) or (:showCustomers = 1 and ae.AlignmentEntityType_Id <> 1))
		and ae.AlignmentEntity_Status = :aeStatus
		and (ae.AlignmentEntity_Latitude <> 0 and ae.AlignmentEntity_Longitude <> 0)
		and (ae.AlignmentEntity_Latitude Is NOT NULL and ae.AlignmentEntity_Longitude Is NOT NULL)
	    and ((:showAssigned = 1 and 
						ou.OrgUnit_TreeLeft>= :treeLeft and
						ou.OrgUnit_TreeRight<= :treeRight)
					  or
					  (:showUnassigned = 1 and ou.OrgUnit_IsUnassigned = 1))
)

SELECT ae.*,  oae.*
FROM   AlignmentEntitys ae
       inner join OrgUnitAlignmentEntitys oae
         ON ae.AlignmentEntity_Id = oae.AlignmentEntity_Id
       inner join [OrgUnits] ou
         ON ou.OrgUnit_Id = oae.OrgUnit_id
       inner join [TerritoryTypes] tt
         ON ou.OrgUnitLevel_id = tt.OrgUnitLevel_id
            and tt.TerritoryType_Id = :territoryTypeId /* @p8 */
WHERE EXISTS (SELECT 1 FROM AllEntities sae
         WHERE sae.AlignmentEntity_id = ae.AlignmentEntity_Id)
