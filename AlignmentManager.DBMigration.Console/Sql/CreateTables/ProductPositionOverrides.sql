
PRINT N'Starting rebuilding table [dbo].[ProductPositionOverrides]...';
CREATE TABLE [dbo].[tmp_ms_xx_ProductPositionOverrides] (
    [ProductPositionOverride_Id]                BIGINT         NOT NULL,
    [ProductPositionOverride_ExternalPositionId]        NVARCHAR (255) NOT NULL,
    [ProductPositionOverride_PositionId] BIGINT         NULL,
    [Request_Id]                                BIGINT         NULL,
    [OldProductBasket_Id]                       BIGINT         NULL,
    [NewProductBasket_Id]                       BIGINT         NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_ProductPositionOverrides] PRIMARY KEY CLUSTERED ([ProductPositionOverride_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ProductPositionOverrides])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_ProductPositionOverrides] WITH (TABLOCK) ([ProductPositionOverride_Id], [ProductPositionOverride_ExternalPositionId], [Request_Id], [OldProductBasket_Id], [NewProductBasket_Id])
        SELECT   [ProductPositionOverride_Id],
                 [ProductPositionOverride_PositionId],
                 [Request_Id],
                 [OldProductBasket_Id],
                 [NewProductBasket_Id]
        FROM     [dbo].[ProductPositionOverrides]
        ORDER BY [ProductPositionOverride_Id] ASC;
    END

DROP TABLE [dbo].[ProductPositionOverrides];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ProductPositionOverrides]', N'ProductPositionOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_ProductPositionOverrides]', N'PK_ProductPositionOverrides', N'OBJECT';
