DECLARE @TTName nvarchar(255);
DECLARE @OrgUnitLevel nvarchar(100);
DECLARE @OrgUnitId nvarchar(100);
DECLARE @OrgUnitName nvarchar(255);
DECLARE @TerritoryTypeId nvarchar(100);
DECLARE @Count int

DECLARE TTId_Cursor CURSOR FOR
SELECT TerritoryType_Id FROM [{0}].[dbo].[TerritoryTypes]
WHERE UnassignedOrgUnit_id IS NULL

OPEN TTId_Cursor
FETCH NEXT FROM TTId_Cursor INTO @TerritoryTypeId

WHILE @@FETCH_STATUS = 0
BEGIN
	SELECT @TTName = TerritoryType_Name, @OrgUnitLevel = OrgUnitLevel_Id
	FROM [{0}].[dbo].[TerritoryTypes] WHERE TerritoryType_Id = @TerritoryTypeId
	
	SET @OrgUnitId = 'ORG_' + REPLACE(@TerritoryTypeId, ' ','_') + '_UNASSIGNED' 
	SET @OrgUnitName = @TTName + ' UNASSIGNED'
	
	SELECT @Count = COUNT(*) FROM [{0}].[dbo].[OrgUnits]
	WHERE OrgUnit_Id = @OrgUnitId
	
	IF @Count = 0
	BEGIN	
		INSERT INTO OrgUnits (OrgUnit_Id, OrgUnit_IsUnassigned, OrgUnit_LegacyId, OrgUnit_Name, 
		OrgUnit_ParentId, OrgUnit_TreeLeft , OrgUnit_TreeRight, OrgUnitLevel_id)
		Values
		(@OrgUnitId, 1, @OrgUnitId, @OrgUnitName, NULL, 0, 0, @OrgUnitLevel)
	END
	
	UPDATE TerritoryTypes
	SET UnassignedOrgUnit_id = @OrgUnitId
	WHERE TerritoryType_Id = @TerritoryTypeId
	
	FETCH NEXT FROM TTId_Cursor INTO @TerritoryTypeId
END


CLOSE TTId_Cursor   
DEALLOCATE TTId_Cursor
