SELECT DISTINCT TOP (:maxResult) p.*
FROM   [Personnel] p
       inner join [PositionPersonnel] pp
         on p.Personnel_Id = pp.Personnel_id
       inner join Positions pos
         on pp.Position_id = pos.Position_Id
       inner join OrgUnits ou
         on pos.OrgUnit_id = ou.OrgUnit_Id
         and (ou.OrgUnit_TreeLeft >= :treeLeft
         and ou.OrgUnit_TreeRight <= :treeRight)
       inner join OrgUnitLevels ol
         on ou.OrgUnitLevel_id = ol.OrgUnitLevel_Id
       inner join TerritoryTypes tt
         on ol.OrgUnitLevel_Id = tt.OrgUnitLevel_id
         and tt.TerritoryType_Id = :terrTypeId
WHERE p.Personnel_Status = :personnelStatus
	and ((p.Personnel_Name like :searchString +'%' escape '\'
              or p.Personnel_Name like '% ' + :searchString +'%' escape '\')
             or p.Personnel_Id like :searchString +'%' escape '\')

OPTION (RECOMPILE)