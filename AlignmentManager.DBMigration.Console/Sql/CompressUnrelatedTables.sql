DECLARE @sql VARCHAR(MAX) =''
PRINT N'Dropping Indexes...';
SET @SQL = ''
SELECT  @sql = @sql + 'DROP INDEX ' + QUOTENAME(name) + ' ON '
        + QUOTENAME(OBJECT_SCHEMA_NAME([object_id])) + '.'
        + QUOTENAME(OBJECT_NAME([object_id])) + ';
'
FROM    sys.indexes i
WHERE   index_id > 1
        AND OBJECTPROPERTY([object_id], 'IsMsShipped') = 0
		AND i.object_id IN (
SELECT p.object_id FROM sys.partitions p INNER JOIN sys.tables t ON t.object_id = p.object_id WHERE p.data_compression=0)

ORDER BY [object_id] ,
        index_id DESC;
EXEC (@sql)

SET @Sql=''
SELECT @sql=@sql+
   'ALTER TABLE [' 
   + s.name
   + '].[' 
   + o.[name] 
   + '] REBUILD WITH (DATA_COMPRESSION=PAGE);'+CHAR(13)
FROM sys.objects o 
INNER JOIN sys.schemas s ON s.schema_id = o.schema_id
 WHERE o.object_id IN (
SELECT i.object_id  
FROM sys.objects AS o WITH (NOLOCK)
INNER JOIN sys.indexes AS i WITH (NOLOCK)
   ON o.[object_id] = i.[object_id]
INNER JOIN sys.dm_db_partition_stats AS ps WITH (NOLOCK)
   ON i.[object_id] = ps.[object_id]
AND ps.[index_id] = i.[index_id]
WHERE o.[type] = 'U'
AND i.object_id IN (
SELECT p.object_id FROM sys.partitions p INNER JOIN sys.tables t ON t.object_id = p.object_id WHERE p.data_compression=0)
AND i.object_id NOT IN (SELECT object_id FROM sys.columns WHERE system_type_id =240))

EXEC (@sql)


