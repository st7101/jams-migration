DECLARE @isFirstTextAttribute bit;
SET @isFirstTextAttribute = 0;

/* DROP EXISTING TempAlignmentEntityTextAttributeCache TABLE */
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempAlignmentEntityTextAttributeCache') 
DROP TABLE [TempAlignmentEntityTextAttributeCache]

/* CREATE TEMPORARY TABLE */
CREATE TABLE [TempAlignmentEntityTextAttributeCache] 
( AlignmentEntityTextAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
AlignmentEntityTextAttributeCache_Value nvarchar(2000) NULL ,
AlignmentEntity_id nvarchar(100) NULL , 
AttributeType_id bigint NULL , 
CONSTRAINT PK_TempAlignmentEntityTextAttributeCache_AlignmentEntityTextAttributeCache_Id 
PRIMARY KEY CLUSTERED (AlignmentEntityTextAttributeCache_Id))

/* POPULATE ORIGINAL TABLE */
IF NOT EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'AlignmentEntityTextAttributeCache') 
BEGIN
	SET @isFirstTextAttribute = 1;

	CREATE TABLE [AlignmentEntityTextAttributeCache] 
	( AlignmentEntityTextAttributeCache_Id bigint NOT NULL, 
	AlignmentEntityTextAttributeCache_Value nvarchar(2000) NULL ,
	AlignmentEntity_id nvarchar(100) NULL , 
	AttributeType_id bigint NULL , 
	CONSTRAINT PK_AlignmentEntityTextAttributeCache_AlignmentEntityTextAttributeCache_Id 
	PRIMARY KEY CLUSTERED (AlignmentEntityTextAttributeCache_Id));
END

/* INSERT NEW ATTRIBUTE VALUES IN CACHE TABLE FOR ALL INDEPENDENT ENTITYS */
INSERT INTO [TempAlignmentEntityTextAttributeCache] with(tablock)
	(AlignmentEntity_id, AttributeType_id, AlignmentEntityTextAttributeCache_Value) 
	SELECT ae.AlignmentEntity_id AS AlignmentEntity_id, 
			-1 AS AttributeType_id, 
			ae.[{0}] AS AlignmentEntityTextAttributeCache_Value
			FROM [AlignmentEntitys] ae
			WHERE EXISTS (Select 1 from [OrgUnitAlignmentEntitys] oae 
						Where ae.AlignmentEntity_Id = oae.AlignmentEntity_id);


/* FIND OUT MAXIMUM VALUE OF ID IN CACHE TABLE */
DECLARE @maxIdInCache bigint;

Select @maxIdInCache = ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0) FROM [AlignmentEntityTextAttributeCache];

/* INSERT CACHE ENTRIES FROM TEMPORARY TABLE TO ACTUAL CACHE TABLE */

INSERT INTO [AlignmentEntityTextAttributeCache] with(tablock) 
(AlignmentEntityTextAttributeCache_Id, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id)
SELECT AlignmentEntityTextAttributeCache_Id + @maxIdInCache, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id 
FROM [TempAlignmentEntityTextAttributeCache]; 

IF @isFirstTextAttribute = 1
BEGIN
  /* ADD INDEXES ON TABLE */
CREATE NONCLUSTERED INDEX [idx_SortByTextValue] ON [dbo].[AlignmentEntityTextAttributeCache] 
(
	[AlignmentEntityTextAttributeCache_Value] ASC,
	[AlignmentEntity_id] ASC,
	[AttributeType_id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


CREATE NONCLUSTERED INDEX [idx_TextAttributeCache_Details] ON [dbo].[AlignmentEntityTextAttributeCache] 
(
	[AlignmentEntity_id] ASC
)
INCLUDE ( [AlignmentEntityTextAttributeCache_Value],
[AttributeType_id]) WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]

END

/* DROP TEMPORARY TABLE */
DROP TABLE [TempAlignmentEntityTextAttributeCache]

/* INSERT/UPDATE OBJECT IN HiLo TABLE */

DELETE FROM HiLoUniqueKey WHERE ObjectType = 'AlignmentEntityTextAttributeCache';

INSERT INTO HiLoUniqueKey
(NextHi, ObjectType)
VALUES
((SELECT ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0)+1 
				FROM [AlignmentEntityTextAttributeCache]), 'AlignmentEntityTextAttributeCache');
