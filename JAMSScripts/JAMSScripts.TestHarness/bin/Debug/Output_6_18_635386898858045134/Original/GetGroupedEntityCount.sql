select entity.AlignmentEntity_ParentId, count(entity.AlignmentEntity_Id) 
from AlignmentEntitys entity With (INDEX([ParentId]))
inner join GroupedEntities groupedEntity on entity.AlignmentEntity_Id = groupedEntity.AlignmentEntity_Id 
where entity.AlignmentEntity_Status = 'Normal' 
and entity.AlignmentEntity_ParentId in ('00664') 
and groupedEntity.TerritoryType_Id = '146' 
group by entity.AlignmentEntity_ParentId