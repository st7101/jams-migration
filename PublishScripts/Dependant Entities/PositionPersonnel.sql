
INSERT INTO [dbo].[PositionPersonnel]
           ([PositionPersonnel_Id]
           ,[EffectiveDate]
           ,[Position_Id]
           ,[Position_ExternalId]
           ,[Personnel_Id])
SELECT 
	ROW_NUMBER() OVER (ORDER BY ppTemp.POSITION_ID),
	ppTemp.EFFECTIVEDATE,
	p.Position_Id,
	ppTemp.POSITION_ID,

	ppTemp.PERSONNEL_ID

 FROM dbo.PositionPersonnel_Temp ppTemp
 INNER JOIN positions p ON p.Position_ExternalId=pptemp.POSITION_ID


