/*
Handle Parameters that the script has which are not required
:alignmentEntityTypeIds
*/
EXECUTE  [dbo].[SearchQuery] 
   @showAssigned=:showAssigned
  ,@showUnassigned=:showUnassigned
  ,@treeLeft= :treeLeft
  ,@treeRight= :treeRight
  ,@territoryTypeId=:terrTypeId
  ,@aeStatus=:aeStatus
  ,@MaxResult=:maxResult
  ,@GeoEntityTypeId=:geoEntityTypeId
  ,@SearchString=:searchString


