IF (TYPE_ID('[StringArray]') IS NULL)
CREATE TYPE dbo.StringArray AS TABLE 
(
	String NVARCHAR(100) NOT NULL,
	PRIMARY KEY  (String)
)

IF (TYPE_ID('[IntArray]') IS NULL)
CREATE TYPE dbo.IntArray AS TABLE 
(
	String NVARCHAR(100) NOT NULL,
	PRIMARY KEY  (String)
)
