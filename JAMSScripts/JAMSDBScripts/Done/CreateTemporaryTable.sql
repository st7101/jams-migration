﻿SELECT  o.OrgUnit_id as OrgUnitId
		,[AlignmentEntity_Latitude] as ClusterLat
		,[AlignmentEntity_Longitude] as ClusterLong
		,Count([AlignmentEntity_Latitude]) as ClusterCount
FROM [AlignmentEntitys] as c
inner join 
OrgUnitAlignmentEntitys as o
on c.AlignmentEntity_Id = o.AlignmentEntity_id
inner join Orgunits as org
on o.OrgUnit_Id = org.OrgUnit_Id
inner join AlignmentEntityTypes as at
on at.AlignmentEntityType_Id = c.AlignmentEntityType_id
inner join TerritoryTypes tt 
			ON ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
	and tt.TerritoryType_Id =:territoryTypeId
where
 c.AlignmentEntity_Latitude < :topRightLatitude and c.AlignmentEntity_Latitude > :bottomLeftLatitude and c.AlignmentEntity_Longitude < :topRightLongitude and c.AlignmentEntity_Longitude > :bottomLeftLongitude
and ((:showAssigned = 1 and org.OrgUnit_TreeLeft >= :treeLeft and org.OrgUnit_TreeRight <= :treeRight) or (:showUnassigned = 1 and org.OrgUnit_IsUnassigned = 1))
and at.AlignmentEntityType_Name  = 'Geo' and AlignmentEntity_Latitude is not null
group by o.OrgUnit_id, [AlignmentEntity_Latitude] ,[AlignmentEntity_Longitude]
