INSERT INTO [dbo].[Personnel]
           ([Personnel_Id]
           ,[Personnel_Name]
           ,[Personnel_Login]
           ,[Personnel_Address1]
           ,[Personnel_Address2]
           ,[Personnel_City]
           ,[Personnel_State]
           ,[Personnel_Country]
           ,[Personnel_PostalCode]
           ,[Personnel_Latitude]
           ,[Personnel_Longitude]
           ,[Personnel_Quality]
           ,[Personnel_Status])

SELECT
	pT.PERSONNEL_ID,
	pT.PERSONNEL_NAME,
	pT.PERSONNEL_LOGIN,
	pT.PERSONNEL_ADDRESS1,
	pT.PERSONNEL_ADDRESS2,
	pT.PERSONNEL_CITY,
	pT.PERSONNEL_STATE,
	pT.PERSONNEL_COUNTRY,
	pT.PERSONNEL_POSTALCODE,
	pT.PERSONNEL_LATITUDE,
	pT.PERSONNEL_LONGITUDE,
	pT.PERSONNEL_QUALITY,
	'Normal'
FROM dbo.Personnel_Temp pT
GO


