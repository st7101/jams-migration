SELECT * 
FROM   positions pos 
INNER JOIN (
	SELECT listone.position_id 
    FROM ( 
		SELECT pospers.* 
		FROM positionpersonnel pospers 
        INNER JOIN (
			SELECT TEMP.position_id, TEMP.effectivedate 
            FROM  (
				SELECT pos.*, ppos.effectivedate 
                FROM   personnel pers 
                INNER JOIN positionpersonnel ppos ON pers.personnel_id = ppos.personnel_id 
                INNER JOIN positions pos ON pos.position_id = ppos.position_id  
					AND ppos.effectivedate <=  '6/18/2014 12:04:47 PM' 
					AND pers.personnel_id = '10001573'
			) TEMP 
			GROUP  BY TEMP.position_id, TEMP.effectivedate
		) sortedpositions ON sortedpositions.position_id = pospers.position_id
	) AS listone 
    INNER JOIN (
		SELECT MAX(pospers.effectivedate) AS effectivedate, pospers.position_id 
        FROM   positionpersonnel pospers 
        INNER JOIN(
			SELECT TEMP.position_id, TEMP.effectivedate 
			FROM  (
				SELECT pos.*, ppos.effectivedate 
				FROM   personnel pers 
				INNER JOIN positionpersonnel ppos ON pers.personnel_id = ppos.personnel_id 
				INNER JOIN positions pos ON pos.position_id = ppos.position_id 
					AND ppos.effectivedate <= '6/18/2014 12:04:47 PM' 
					AND pers.personnel_id = '10001573'
			) TEMP 
			GROUP BY TEMP.position_id, TEMP.effectivedate
		) sortedpositions ON sortedpositions.position_id = pospers.position_id  
		and pospers.EffectiveDate<= '6/18/2014 12:04:47 PM'	 
		GROUP  BY pospers.position_id
	) AS listtwo 
	ON listone.effectivedate = listtwo.effectivedate 
		AND listone.position_id = listtwo.position_id 
		AND listone.personnel_id = '10001573' 
) AS posperslist ON pos.position_id = posperslist.position_id