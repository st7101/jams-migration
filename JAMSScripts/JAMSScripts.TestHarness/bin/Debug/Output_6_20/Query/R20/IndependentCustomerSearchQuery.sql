/*
Handle Parameters that the script has which are not required
'1,2'
*/
EXECUTE  [dbo].IndependantCustomerSearchQuery 
   @showAssigned=0
  ,@showUnassigned=0
  ,@treeLeft= 1
  ,@treeRight= 10430
  ,@territoryTypeId=6
  ,@aeStatus='Normal'
  ,@MaxResult=100
  ,@GeoEntityTypeId=1
  ,@alignmentEntityTypeIds='1,2'
  ,@SearchString='p'


