﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlTypes;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace JAMSScripts.TestHarness
{
    class TestCase
    {
        private string _ScriptName;

        public string ScriptName
        {
            get { return _ScriptName; }
            set { _ScriptName = value; }
        }
        private string _Name;

        public string Name
        {
            get { return _Name; }
            set { _Name = value; }
        }
        private Dictionary<string, string[]> _DefaultParams;

        public Dictionary<string, string[]> DefaultParams
        {
            get { return _DefaultParams; }
            set { _DefaultParams = value; }
        }
        private Dictionary<string, string> _OldParams;

        public Dictionary<string, string> OldParams
        {
            get { return _OldParams; }
            set { _OldParams = value; }
        }
        private Dictionary<string, string> _NewParams;

        public Dictionary<string, string> NewParams
        {
            get { return _NewParams; }
            set { _NewParams = value; }
        }

        public TestCase(string scriptName, string name, Dictionary<string, string[]> defaultParams, Dictionary<string, string> oldParmas, Dictionary<string, string> newParams)
        {
            _ScriptName = scriptName;
            _Name = name;
            _DefaultParams = defaultParams;
            _OldParams = oldParmas;
            _NewParams = newParams;
        }

        public TestCase(DataRow dr, TestParameters tp)
        {
            _DefaultParams = new Dictionary<string, string[]>();
            _OldParams = new Dictionary<string, string>();
            _NewParams = new Dictionary<string, string>();

            _ScriptName = dr[2].ToString();
            _Name = dr[1].ToString();
            XDocument xd = XDocument.Parse(dr[3].ToString());




            foreach (XElement xe in xd.Root.Elements("DefaultParam"))
            {
                _DefaultParams.Add(xe.Attributes("Name").First().Value, tp.GetParameterMapping(xe.Attributes("Tag").First().Value));


            }



            xd = XDocument.Parse(dr[4].ToString());

            foreach (XElement xe in xd.Root.Elements("OriginalParam"))
            {
                if (xe.Attributes("Value").Count() == 0)

                    _OldParams.Add(xe.Attributes("Name").First().Value, tp.GetParameterValue(xe.Attributes("Tag").First().Value, true));
                else
                    _OldParams.Add(xe.Attributes("Name").First().Value, xe.Attributes("Value").First().Value);

            }


            xd = XDocument.Parse(dr[5].ToString());
            foreach (XElement xe in xd.Root.Elements("R20Param"))
            {
                if (xe.Attributes("Value").Count() == 0)

                    _NewParams.Add(xe.Attributes("Name").First().Value, tp.GetParameterValue(xe.Attributes("Tag").First().Value, true));
                else
                    _NewParams.Add(xe.Attributes("Name").First().Value, xe.Attributes("Value").First().Value);

            }

        }

        public string PopulateSQL(string sql, bool legacy)
        {
            string returnVal = sql;
            foreach (var item in _DefaultParams)
            {
                string temp = String.Format(@"{0}\b", item.Key);
                returnVal = Regex.Replace(returnVal, temp, item.Value[legacy ? 0 : 1], RegexOptions.IgnoreCase);
            }
            foreach (var item in (legacy) ? _OldParams : _NewParams)
            {
                string temp = String.Format(@"{0}\b", item.Key);
                returnVal = Regex.Replace(returnVal, temp, item.Value, RegexOptions.IgnoreCase);
            }
            return returnVal;

        }
    }
}
