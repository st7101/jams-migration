
PRINT N'Starting rebuilding table [dbo].[OrgUnitRelationships]...';


CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitRelationships]
    (
      [Driving_ExternalOrgUnitId] NVARCHAR(100) NOT NULL ,
      [Driving_OrgUnitId] BIGINT NOT NULL ,
      [Affected_ExternalOrgUnitId] NVARCHAR(100) NOT NULL ,
      [Affected_OrgUnitId] BIGINT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitRelationships] PRIMARY KEY CLUSTERED
        ( [Driving_OrgUnitId], [Affected_OrgUnitId] )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[OrgUnitRelationships] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_OrgUnitRelationships] WITH (TABLOCK)
                ( [Driving_ExternalOrgUnitId] ,
                  [Driving_OrgUnitId],
				  [Affected_ExternalOrgUnitId],
				  [Affected_OrgUnitId]
                )
                SELECT  [Driving_OrgUnitId] ,
                        ROW_NUMBER() OVER ( ORDER BY Driving_OrgUnitId ASC ) ,
						[Affected_OrgUnitId],
						ROW_NUMBER() OVER ( ORDER BY Driving_OrgUnitId ASC )
                FROM    [dbo].[OrgUnitRelationships];
    END
DROP TABLE [dbo].[OrgUnitRelationships];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitRelationships]',
    N'OrgUnitRelationships';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitRelationships]',
    N'PK_OrgUnitRelationships', N'OBJECT';
