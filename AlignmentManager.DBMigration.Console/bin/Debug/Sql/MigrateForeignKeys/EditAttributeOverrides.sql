PRINT N'Updating EditAttributeOverrides ...';

UPDATE aeo
SET EditAttributeOverride_AlignmentEntityId=ae.AlignmentEntity_Id
FROM dbo.EditAttributeOverrides aeo INNER JOIN 
dbo.AlignmentEntitys ae ON aeo.EditAttributeOverride_ExternalAlignmentEntityId= ae.AlignmentEntity_ExternalID

