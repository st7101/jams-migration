IF (OBJECT_ID('[PersonnelSearchQuery]') IS NOT NULL)
  DROP PROCEDURE [PersonnelSearchQuery]

DECLARE @SqlCommand NVARCHAR(MAX)= '
-- =============================================
-- Author:		Steve Tetrault
-- Create date: Jun. 11, 2014
-- Description:	Modified Logic for PersonnelSearchQuery SP
-- =============================================
CREATE PROCEDURE [dbo].[PersonnelSearchQuery]
	-- Add the parameters for the stored procedure here
    @treeLeft INT ,
    @treeRight INT ,
    @territoryTypeId BIGINT ,
    @MaxResult INT ,
    @SearchString NVARCHAR(100) = NULL ,
    @PersonnelStatus NVARCHAR(100)
AS
    BEGIN

        DECLARE @sql NVARCHAR(MAX)= ''
SELECT DISTINCT TOP ('' + CAST (@maxResult AS VARCHAR) + '') p.*
FROM   [Personnel] p
       inner join [PositionPersonnel] pp
         on p.Personnel_Id = pp.Personnel_id
       inner join Positions pos
         on pp.Position_id = pos.Position_Id
       inner join OrgUnits ou
         on pos.OrgUnit_id = ou.OrgUnit_Id
		 and ( ou.OrgUnit_TreeLeft >= '' + CAST(@treeLeft AS NVARCHAR)
            + '' and ou.OrgUnit_TreeRight <= '' + CAST(@treeRight AS NVARCHAR)
            + '')
       inner join TerritoryTypes tt
         on ou.OrgUnitLevel_Id = tt.OrgUnitLevel_id
		and tt.TerritoryType_Id ='' + CAST(@territoryTypeId AS NVARCHAR) + ''
		and p.Personnel_Status = '''''' + @PersonnelStatus + '''''''' + CHAR(13)
        IF ( @SearchString IS NOT NULL
             AND @SearchString != ''''
           )
            BEGIN
                SET @sql = @sql + ''	and (LEFT(p.Personnel_Name,''
                    + CAST(LEN(@SearchString) AS NVARCHAR) + '') = ''''''
                    + @SearchString + '''''''' + CHAR(13)
                    + '' or LEFT(p.Personnel_Id,''
                    + CAST(LEN(@SearchString) AS NVARCHAR) + '') = ''''''
                    + @SearchString + '''''' or p.Personnel_Name like  '''''' + ''%''
                    + @SearchString + ''%'''')'' + CHAR(13)
            END

        EXEC (@sql)
    END
'
EXEC (@sqlCommand)
