SELECT e.DbIdentifier 'DBName',e.EffectiveDate 'EventStartDate' FROM dbo.Events e
INNER join MASTER.sys.databases db ON e.DbIdentifier =db.name
 WHERE PublishStatus ='Succeeded'
 ORDER BY e.EffectiveDate
