﻿/* Query to find out those Parent Entities Territory Type combinations which are not present in OrgUnitAlignmentEntitys, 
but whose children are present in either GroupedEntities table or OrgUnitAlignmentEntitys table for that territorytype */

with 
alignmentEntityTerritoryTypes as
	(
		select ouae.AlignmentEntity_id, ouae.TerritoryType_Id 
		from OrgUnitAlignmentEntitys ouae 
	) , 
alignmentEntityTerritoryTypesUniverse as 
	(
		select * 
		from alignmentEntityTerritoryTypes
		union 
		select AlignmentEntity_id ,TerritoryType_id
		from GroupedEntities
	), 
missingParentAlignmentEntityTerritoryTypes as 
	(
		select distinct ae.AlignmentEntity_ParentId,alignmentEntityTerritoryTypesUniverse.TerritoryType_Id 
		from alignmentEntityTerritoryTypesUniverse 
		inner join AlignmentEntitys ae 
			on ae.AlignmentEntity_Id = alignmentEntityTerritoryTypesUniverse.AlignmentEntity_id and ae.AlignmentEntity_ParentId is not null  
		inner join AlignmentEntityTypeTerritoryTypes aettt 
		on alignmentEntityTerritoryTypesUniverse.TerritoryType_id = aettt.TerritoryType_id and aettt.AlignmentEntityType_ParentId is not null
			except 
		select AlignmentEntity_id,TerritoryType_Id 
		from alignmentEntityTerritoryTypes
	) 
select * from missingParentAlignmentEntityTerritoryTypes

