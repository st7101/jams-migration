/* Fetch the alignment entities along with their alignments to org units which are belonging to territory type 
that does not support alignmnt entity type of the entity*/

select ae.AlignmentEntity_Id, ouae.OrgUnit_Id, ouae.TerritoryType_Id 
from AlignmentEntitys ae
inner join OrgUnitAlignmentEntitys ouae on ouae.AlignmentEntity_id = ae.AlignmentEntity_Id
INNER JOIN  OrgUnits as OU ON OU.OrgUnit_Id = ouae.OrgUnit_Id
INNER JOIN OrgUnitLevels AS OUL
ON OU.OrgUnitLevel_id = OUL.OrgUnitLevel_id
INNER JOIN dbo.TerritoryTypes tt ON tt.OrgUnitLevel_Id = OUL.OrgUnitLevel_Id
WHERE Not Exists (Select 1 from alignmentEntityTypeTerritoryTypes aettt 
where aettt.AlignmentEntityType_id = ae.AlignmentEntityType_id and aettt.TerritoryType_id = ouae.TerritoryType_Id)