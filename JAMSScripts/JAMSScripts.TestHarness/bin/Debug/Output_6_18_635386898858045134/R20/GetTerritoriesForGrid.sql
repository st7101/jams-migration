EXECUTE [dbo].[GetTerritoriesForGrid] 
   @minLat=42.6969776153564
  ,@maxLat=47.4596862792969
  ,@minLng=47.4596862792969
  ,@maxLng=-66.9500045776367
  ,@territoryTypeId=19
  ,@showAssigned=0
  ,@showUnassigned=0
  ,@TreeLeft=1
  ,@treeRight=10430
