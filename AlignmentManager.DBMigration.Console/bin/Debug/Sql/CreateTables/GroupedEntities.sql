

PRINT N'Starting rebuilding table [dbo].[GroupedEntities]...';
CREATE TABLE [dbo].[tmp_ms_xx_GroupedEntities]
    (
      [TerritoryType_Id] BIGINT NOT NULL ,
      [AlignmentEntity_Id] BIGINT NOT NULL ,
      CONSTRAINT [tmp_ms_xx_PK_GroupedEntities] PRIMARY KEY CLUSTERED
        ( [TerritoryType_Id]	ASC ,[AlignmentEntity_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE ) ON PS_TT(territorytype_id)
    ) 
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[GroupedEntities] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_GroupedEntities] WITH (TABLOCK)
                ( [AlignmentEntity_Id] ,
                  [TerritoryType_Id]
                )
                SELECT 
                        ae.[AlignmentEntity_Id] ,
                        tt.[TerritoryType_Id]
                FROM    [dbo].[GroupedEntities] ge INNER JOIN 
dbo.AlignmentEntitys ae ON ge.AlignmentEntity_ID= ae.AlignmentEntity_ExternalID
INNER JOIN dbo.TerritoryTypes tt ON ge.TerritoryType_ID=tt.TerritoryType_ExternalID
;
    END
DROP TABLE [dbo].[GroupedEntities];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_GroupedEntities]', N'GroupedEntities';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PK_GroupedEntities]',
    N'PK_GroupedEntities', N'OBJECT';
