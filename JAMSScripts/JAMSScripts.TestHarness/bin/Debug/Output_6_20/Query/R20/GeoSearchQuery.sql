/*
Handle Parameters that the script has which are not required
'1,2'
*/
EXECUTE  [dbo].[SearchQuery] 
   @showAssigned=0
  ,@showUnassigned=0
  ,@treeLeft= 1
  ,@treeRight= 10430
  ,@territoryTypeId=40
  ,@aeStatus='Normal'
  ,@MaxResult=100
  ,@GeoEntityTypeId=1
  ,@SearchString='j'


