/*
Remove Constraints that will get in the way of changing the schema
*/


        PRINT N'Dropping OAESuperView...';
        IF EXISTS ( SELECT TOP 1
                            1
                    FROM    sys.views
                    WHERE   name = 'OAESuperView' )
            BEGIN
                DROP VIEW [dbo].[OAESuperView]
            END



DECLARE @SQL VARCHAR(MAX)= ''

PRINT N'Dropping Unique Constraints...';
SELECT  @sql = @sql + 'ALTER TABLE ' + OBJECT_NAME(so.parent_obj)
        + ' DROP CONSTRAINT ' + QUOTENAME(so.name) + ';' + CHAR(13)
FROM    sysobjects so
WHERE   so.xtype = 'UQ' 

        EXEC (@sql)


SET @SQL = ''
PRINT N'Dropping Default Constraints...';
SELECT  @sql = @sql + 'ALTER TABLE ' + OBJECT_NAME(dc.parent_object_id)
        + ' DROP CONSTRAINT ' + QUOTENAME(dc.name) + ';' + CHAR(13)
FROM    sys.default_constraints dc

        EXEC (@sql)

PRINT N'Dropping Indexes...';
SET @SQL = ''
SELECT  @sql = @sql + 'DROP INDEX ' + QUOTENAME(name) + ' ON '
        + QUOTENAME(OBJECT_SCHEMA_NAME([object_id])) + '.'
        + QUOTENAME(OBJECT_NAME([object_id])) + ';
'
FROM    sys.indexes
WHERE   index_id > 1
        AND OBJECTPROPERTY([object_id], 'IsMsShipped') = 0

ORDER BY [object_id] ,
        index_id DESC;

        --EXEC (@sql)



PRINT N'Dropping Foreign Keys...';
SET @SQL = ''
SELECT  @SQL = @SQL + 'ALTER TABLE ' + FK.TABLE_NAME + ' DROP CONSTRAINT ['
        + RTRIM(C.CONSTRAINT_NAME) + '];' + CHAR(13)
FROM    INFORMATION_SCHEMA.REFERENTIAL_CONSTRAINTS C
        INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS FK ON C.CONSTRAINT_NAME = FK.CONSTRAINT_NAME
        INNER JOIN INFORMATION_SCHEMA.TABLE_CONSTRAINTS PK ON C.UNIQUE_CONSTRAINT_NAME = PK.CONSTRAINT_NAME
        INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE CU ON C.CONSTRAINT_NAME = CU.CONSTRAINT_NAME
        INNER JOIN ( SELECT i1.TABLE_NAME ,
                            i2.COLUMN_NAME
                     FROM   INFORMATION_SCHEMA.TABLE_CONSTRAINTS i1
                            INNER JOIN INFORMATION_SCHEMA.KEY_COLUMN_USAGE i2 ON i1.CONSTRAINT_NAME = i2.CONSTRAINT_NAME
                     WHERE  i1.CONSTRAINT_TYPE = 'PRIMARY KEY'
                   ) PT ON PT.TABLE_NAME = PK.TABLE_NAME
        EXEC (@sql)




