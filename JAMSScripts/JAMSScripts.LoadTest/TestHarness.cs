﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace JAMSScripts.LoadTest
{
    class LoadTestHarness : IDisposable
    {
        public static string Sql2008Connection = @"Server=SA-QSDSQL13.zssd.local;Database=DEV_AlignmentManager_ColumnStore_Test;Trusted_Connection=True;";
        public static string Sql2008ConnectionPage = @"Server=SA-QSDSQL13.zssd.local;Database=DEV_AlignmentManager_ColumnStore_Test_Page;Trusted_Connection=True;";
        public static string Sql2008ConnectionTDE = @"Server=SA-QSDSQL13.zssd.local;Database=DEV_AlignmentManager_ColumnStore_Test_TDE;Trusted_Connection=True;";
        public static string Sql2008ConnectionPageTDE = @"Server=SA-QSDSQL13.zssd.local;Database=DEV_AlignmentManager_ColumnStore_Test_Page_TDE;Trusted_Connection=True;";
        public static string Sql2014Connection = @"Server=sa-qsql08.zssd.local;Database=QA_AlignmentManager_0012e_CSTest;Trusted_Connection=True;";
        public static string Sql2014ConnectionTDE = @"Server=sa-qsql08.zssd.local;Database=QA_AlignmentManager_0012e_CSTest_TDE;Trusted_Connection=True;";

        public static string onemetric40 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae40Metrics] with (nolock) ORDER BY abc_calls1";
        public static string onemetric80 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae80] with (nolock) ORDER BY abc_calls2";
        public static string onemetric120 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae120] with (nolock) ORDER BY abc_calls3";
        public static string twometric40 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae40Metrics] with (nolock) ORDER BY abc_calls1,PCP1_CALLS1 ";
        public static string twometric80 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae80] with (nolock) ORDER BY abc_calls1,PCP1_CALLS2 ";
        public static string twometric120 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae120] with (nolock) ORDER BY abc_calls1,PCP1_CALLS3 ";
        public static string fourmetric40 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae40Metrics] with (nolock) ORDER BY abc_calls1,PCP1_CALLS1,ABC_TARGETS1,PCP1_TARGETS1";
        public static string fourmetric80 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae80] with (nolock) ORDER BY abc_calls1,PCP1_CALLS2,ABC_TARGETS2,PCP1_TARGETS1 ";
        public static string fourmetric120 = @"SELECT TOP 250 [AlignmentEntity_Id],[alignmentEntity_ExternalId],[AlignmentEntity_Name],[AlignmentEntity_Status],[AlignmentEntity_Latitude],[AlignmentEntity_Longitude],[AlignmentEntity_ExternalParentId],[AlignmentEntity_ParentId],[AlignmentEntityType_Id],[ABC_CALLS1],[ABC_TARGETS1]	  ,[ALL_SPACES1],[CUSTOMER_CITY1],[CUSTOMER_POSTALCODE1],[CUSTOMER_STATE1],[PCP1_CALLS1],[PCP1_TARGETS1]  FROM [dbo].[ae120] with (nolock) ORDER BY abc_calls2,PCP1_CALLS2,ABC_TARGETS1,PCP1_TARGETS3";
        private LimitedConcurrencyTaskScheduler _ts;
        private TaskFactory _factory;
        private DataTable _dtStats = new DataTable();
        private string _logConnectionString = @"Server=(local);Database=JAMSPerfTests;Trusted_Connection=True;";
        private StreamWriter _log;
        ConcurrentQueue<Exception> _exceptions;
        TaskFactory _taskFact ;
        List<Task> _taskList;

        
        public LoadTestHarness()
        {

            _log = new StreamWriter(String.Format("LoadTest_{0}_{1}_{2}_{3}.txt", DateTime.Now.Month, DateTime.Now.Day, DateTime.Now.Hour, DateTime.Now.Minute));

        }

        public void RunConcurrent(int concurrent)
        {
            _exceptions = new ConcurrentQueue<Exception>();
            _taskFact = new TaskFactory(new LimitedConcurrencyTaskScheduler(concurrent));
            _taskList = new List<Task>();
            try
            {
                RunTest(false, false, false, 40, 1, concurrent);
                RunTest(false, true, false, 40, 1, concurrent);
                RunTest(true, false, false, 40, 1, concurrent);
                RunTest(true, true, false, 40, 1, concurrent);
                RunTest(false, false, true, 40, 1, concurrent);
                RunTest(true, false, true, 40, 1, concurrent);
                RunTest(false, false, false, 80, 1, concurrent);
                RunTest(false, true, false, 80, 1, concurrent);
                RunTest(true, false, false, 80, 1, concurrent);
                RunTest(true, true, false, 80, 1, concurrent);
                RunTest(false, false, true, 80, 1, concurrent);
                RunTest(true, false, true, 80, 1, concurrent);
                RunTest(false, false, false, 120, 1, concurrent);
                RunTest(false, true, false, 120, 1, concurrent);
                RunTest(true, false, false, 120, 1, concurrent);
                RunTest(true, true, false, 120, 1, concurrent);
                RunTest(false, false, true, 120, 1, concurrent);
                RunTest(true, false, true, 120, 1, concurrent);
                RunTest(false, false, false, 40, 2, concurrent);
                RunTest(false, true, false, 40, 2, concurrent);
                RunTest(true, false, false, 40, 2, concurrent);
                RunTest(true, true, false, 40, 2, concurrent);
                RunTest(false, false, true, 40, 2, concurrent);
                RunTest(true, false, true, 40, 2, concurrent);
                RunTest(false, false, false, 80, 2, concurrent);
                RunTest(false, true, false, 80, 2, concurrent);
                RunTest(true, false, false, 80, 2, concurrent);
                RunTest(true, true, false, 80, 2, concurrent);
                RunTest(false, false, true, 80, 2, concurrent);
                RunTest(true, false, true, 80, 2, concurrent);
                RunTest(false, false, false, 120, 2, concurrent);
                RunTest(false, true, false, 120, 2, concurrent);
                RunTest(true, false, false, 120, 2, concurrent);
                RunTest(true, true, false, 120, 2, concurrent);
                RunTest(false, false, true, 120, 2, concurrent);
                RunTest(true, false, true, 120, 2, concurrent);
                RunTest(false, false, false, 40, 4, concurrent);
                RunTest(false, true, false, 40, 4, concurrent);
                RunTest(true, false, false, 40, 4, concurrent);
                RunTest(true, true, false, 40, 4, concurrent);
                RunTest(false, false, true, 40, 4, concurrent);
                RunTest(true, false, true, 40, 4, concurrent);
                RunTest(false, false, false, 80, 4, concurrent);
                RunTest(false, true, false, 80, 4, concurrent);
                RunTest(true, false, false, 80, 4, concurrent);
                RunTest(true, true, false, 80, 4, concurrent);
                RunTest(false, false, true, 80, 4, concurrent);
                RunTest(true, false, true, 80, 4, concurrent);
                RunTest(false, false, false, 120, 4, concurrent);
                RunTest(false, true, false, 120, 4, concurrent);
                RunTest(true, false, false, 120, 4, concurrent);
                RunTest(true, true, false, 120, 4, concurrent);
                RunTest(false, false, true, 120, 4, concurrent);
                RunTest(true, false, true, 120, 4, concurrent);
            }
            finally
            {
                SaveStats();
            }
        }

        private void RunTest(bool isTde, bool isPageCompressed, bool is2014, int metrics, int sortMetrics,int concurrent)
        {
            
            LogTestRunMessage(isTde, isPageCompressed, is2014, metrics, sortMetrics, concurrent);
            for (int i = 1; i <= concurrent; i++)
            {
                _taskList.Add(_taskFact.StartNew(() =>
                {
                    try
                    {
                        Run(isTde, isPageCompressed, is2014, metrics, sortMetrics, concurrent);

                    }
                    catch (Exception ex)
                    {
                        LogMessage(String.Format("\n\n\nFailed with error:\n{0}", ex.Message));
                        _exceptions.Enqueue(ex);

                    }
                }));
            }
            Task.WaitAll(_taskList.ToArray());
            _taskList.Clear();
        }


        public void Run(bool isTde, bool isPageCompressed, bool is2014, int metrics, int sortMetrics, int thread)
        {
            string sqlCon = "";
            string query = "";
            #region select connection and query
            if (is2014 & isTde)
            {
                if (isTde)
                    sqlCon = Sql2014ConnectionTDE;
                else
                    sqlCon = Sql2014Connection;
            }
            else
            {
                if (isTde)
                {
                    if (isPageCompressed)
                        sqlCon = Sql2008ConnectionPageTDE;
                    else
                        sqlCon = Sql2008ConnectionTDE;
                }
                else
                {
                    if (isPageCompressed)
                        sqlCon = Sql2008ConnectionPage;
                    else
                        sqlCon = Sql2008Connection;

                }
            }

            if (metrics == 40)
            {
                switch (sortMetrics)
                {
                    case 1:
                        query = onemetric40;
                        break;
                    case 2:
                        query = twometric40;
                        break;
                    case 4:
                        query = fourmetric40;
                        break;
                }
            }
            else if (metrics == 80)
            {
                switch (sortMetrics)
                {
                    case 1:
                        query = onemetric80;
                        break;
                    case 2:
                        query = twometric80;
                        break;
                    case 4:
                        query = fourmetric80;
                        break;
                }
            }
            else if (metrics == 120)
            {
                switch (sortMetrics)
                {
                    case 1:
                        query = onemetric120;
                        break;
                    case 2:
                        query = twometric120;
                        break;
                    case 4:
                        query = fourmetric120;
                        break;
                }
            }
            #endregion

            using (SqlConnection con = new SqlConnection(sqlCon))
            {
                con.Open();
                con.StatisticsEnabled = true;
                for (int i = 1; i <= 5;i++ )
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandTimeout = 0;
                        cmd.ExecuteNonQuery();
                        var statistics = con.RetrieveStatistics();
                        lock (_dtStats)
                        {
                            if (_dtStats.Columns.Count == 0)
                                SetupStatsTable(statistics);
                            DataRow dr = _dtStats.NewRow();
                            dr[0] = thread;
                            dr[1] = metrics;
                            dr[2] = isTde;
                            dr[3] = isPageCompressed;
                            dr[4] = (is2014) ? "2014" : "2008";
                            dr[5] = sortMetrics;
                            dr[6] = i;
                            for (int j = 7; j < 25; j++)
                                dr[j] = statistics[_dtStats.Columns[j].ColumnName].ToString();
                            _dtStats.Rows.Add(dr);

                        }
                            con.ResetStatistics();
                    }
            }

        }

        private void SaveStats()
        {
            using (SqlConnection con = new SqlConnection(_logConnectionString))
            {
                string tableName = String.Format("Stats_{0}_{1}_{2}_{3}_{4}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day,DateTime.Now.Hour,DateTime.Now.Minute);
                #region add sql table
                StringBuilder sql = new StringBuilder();
                sql.AppendFormat("IF EXISTS (select top 1 1 FROM sys.tables where name ='{0}') BEGIN DROP TABLE {0}; END  CREATE TABLE [{0}] (", tableName);
                for (int i = 0; i < _dtStats.Columns.Count; i++)
                {
                    sql.AppendFormat(" {0} varchar(255) null,", _dtStats.Columns[i].ColumnName);
                }
                sql.Remove(sql.Length - 1, 1).Append(")");
                con.Open();
                using (SqlCommand cmd = new SqlCommand(sql.ToString(), con))
                    cmd.ExecuteNonQuery();
                #endregion
                using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                {

                    bulk.DestinationTableName = tableName;
                    bulk.WriteToServer(_dtStats);
                }
            }
        }

        private void SetupStatsTable(System.Collections.IDictionary statistics)
        {
            _dtStats.Columns.Add("ThreadNumber");
            _dtStats.Columns.Add("Metrics");
            _dtStats.Columns.Add("TDE");
            _dtStats.Columns.Add("Compression");
            _dtStats.Columns.Add("Version");
            _dtStats.Columns.Add("SortMetrics");
            _dtStats.Columns.Add("Iteration");

            foreach (var stat in statistics.Keys)
                _dtStats.Columns.Add(stat.ToString());
        }

        private void LogMessage(string message)
        {
            lock (_log)
            {
                System.Console.WriteLine(String.Format("{0}: {1}", DateTime.Now.ToLongTimeString(), message));
                _log.WriteLine(String.Format("{0}: {1}", DateTime.Now.ToLongTimeString(), message));
            }
        }

        private void LogTestRunMessage(bool isTde, bool isPageCompressed, bool is2014, int metrics, int sortMetrics, int concurrent)
        {
            string msg= string.Format("{0}\nSql Version:{1}\nTDE:{2}\nCompression:{3}\nTotal Metrics:{4}\nSort Metrics:{5}\nConcurrent Users:{6}\n",DateTime.Now.ToShortTimeString(),((is2014)?"2014":"2008"),isTde,isPageCompressed,metrics,sortMetrics,concurrent);
            LogMessage(msg);
        }





        public void Dispose()
        {
            _log.Close();
        }

    }
}