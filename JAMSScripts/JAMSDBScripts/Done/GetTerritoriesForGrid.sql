﻿EXECUTE [dbo].[GetTerritoriesForGrid] 
   @minLat=:minlatitude
  ,@maxLat=:maxlatitude
  ,@minLng=:maxlatitude
  ,@maxLng=:maxlongitude
  ,@territoryTypeId=:territoryTypeId
  ,@showAssigned=:showAssigned
  ,@showUnassigned=:showUnassigned
  ,@TreeLeft=:treeLeft
  ,@treeRight=:treeRight
