-- get explicit alignments
select 
	alignments.AlignmentEntity_Id as AlignmentEntity_Id, 
	territoryTypes.TerritoryType_Id as TerritoryType_Id,
	alignments.OrgUnit_Id as OrgUnit_Id, 
	alignments.OrgUnitAlignmentEntity_Weight as OrgUnitAlignmentEntity_Weight
	
from dbo.OrgUnitAlignmentEntitys alignments
join dbo.AlignmentEntitys entitys on alignments.AlignmentEntity_Id = entitys.AlignmentEntity_Id
join dbo.OrgUnits orgUnits on alignments.OrgUnit_Id = orgUnits.OrgUnit_Id
join dbo.TerritoryTypes territoryTypes on orgUnits.OrgUnitLevel_Id = territoryTypes.OrgUnitLevel_Id
where entitys.AlignmentEntity_Status = 'Normal'

union all

-- get derived alignments
select 
	grouped.AlignmentEntity_Id as AlignmentEntity_Id, 
	grouped.TerritoryType_Id as TerritoryType_Id, 
	alignments.OrgUnit_Id as OrgUnit_Id, 
	alignments.OrgUnitAlignmentEntity_Weight as OrgUnitAlignmentEntity_Weight
	
from dbo.GroupedEntities grouped
join dbo.AlignmentEntitys entitys on grouped.alignmentEntity_Id = entitys.alignmentEntity_Id
join dbo.AlignmentEntitys parents on entitys.AlignmentEntity_ParentId = parents.AlignmentEntity_Id
join dbo.OrgUnitAlignmentEntitys alignments on entitys.alignmentEntity_ParentId = alignments.AlignmentEntity_Id
join dbo.OrgUnits orgUnits on alignments.OrgUnit_Id = OrgUnits.OrgUnit_Id
join dbo.TerritoryTypes territoryTypes on orgUnits.OrgUnitLevel_Id = territoryTypes.OrgUnitLevel_Id
	and territoryTypes.TerritoryType_Id = grouped.TerritoryType_Id
where entitys.AlignmentEntity_Status = 'Normal'
	and parents.AlignmentEntity_Status = 'Normal';