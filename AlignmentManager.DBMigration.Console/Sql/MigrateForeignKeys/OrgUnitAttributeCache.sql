
PRINT N'Updating OrgUnitAttributeCache ...';

UPDATE ouac
SET ouac.[OrgUnit_Id]=ou.OrgUnit_Id
FROM dbo.OrgUnitAttributeCache ouac INNER JOIN 
dbo.OrgUnits ou ON ouac.[OrgUnit_ExternalId]=ou.OrgUnit_ExternalID