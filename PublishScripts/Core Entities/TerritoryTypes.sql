INSERT INTO [dbo].[TerritoryTypes]
           ([TerritoryType_ExternalId]
           ,[TerritoryType_Id]
           ,[TerritoryType_Name]
           ,[OrgUnitLevel_ExternalId]
           ,[UnassignedOrgUnit_ExternalId]
           ,[TerritoryType_SingleAssignmentWeightEnabled]
           ,[TerritoryType_ZeroWeightToleranceEnabled]
           ,[TerritoryType_WeightSumNotEqualTo100Enabled])
SELECT 
	tTemp.TERRITORYTYPE_ID,
	ROW_NUMBER() OVER (ORDER BY TERRITORYTYPE_ID ASC),
	tTemp.TERRITORYTYPE_NAME,
	tTemp.ORGUNITLEVEL_ID,
	tTemp.UNASSIGNEDORGUNIT_ID,
	tTemp.TERRITORYTYPE_SINGLEASSIGNMENTWEIGHTENABLED,
	tTemp.TERRITORYTYPE_ZEROWEIGHTTOLERANCEENABLED,
	tTemp.TERRITORYTYPE_WEIGHTSUMNOTEQUALTO100ENABLED
 FROM dbo.TerritoryTypes_Temp tTemp
GO

UPDATE t
SET OrgUnitLevel_Id=oul.OrgUnitLevel_Id, UnassignedOrgUnit_Id=ou.OrgUnit_Id
FROM dbo.TerritoryTypes t
LEFT JOIN orgunitlevels oul ON t.OrgUnitLevel_ExternalId=oul.OrgUnitLevel_ExternalId
LEFT JOIN orgunits ou ON t.UnassignedOrgUnit_ExternalId=ou.OrgUnit_ExternalId
GO
