SELECT TOP (:maxResult) ou.OrgUnit_Id as Id,
                 ou.OrgUnit_Name as Name,
				 ou.OrgUnit_LegacyId as LegacyId,
				 ou.OrgUnit_ColorId as ColorId               
FROM   OrgUnits ou
       inner join OrgUnitLevels ol
         on ou.OrgUnitLevel_id = ol.OrgUnitLevel_Id
       inner join TerritoryTypes tt
         on ol.OrgUnitLevel_Id = tt.OrgUnitLevel_id
         and tt.TerritoryType_Id = :terrTypeId
WHERE  ((:showAssigned = 1 and ou.OrgUnit_TreeLeft >= :treeLeft and ou.OrgUnit_TreeRight <= :treeRight) 
		or (:showUnassigned = 1 and ou.OrgUnit_IsUnassigned = 1))
       and (ou.OrgUnit_Name like :searchString +'%' escape '\'
             or (ou.OrgUnit_Id like :searchString +'%' escape '\'
                  or ou.OrgUnit_LegacyId like :searchString +'%' escape '\'))

OPTION (RECOMPILE)