/* READ UPSERTED ENTITIES INTO TEMPORARY TABLE */
SELECT * INTO UpsertedEntitysEntityTypes FROM
(SELECT DISTINCT uaeo.UpsertAlignmentEntityOverride_AlignmentEntityId as AlignmentEntity_Id,
 uaeo.AlignmentEntityType_Id as AlignmentEntityType_Id 
 FROM UpsertAlignmentEntityOverrides uaeo 
 where uaeo.Request_Id = 100
 ) b

/* READ TERRITORYTYPES INTO TEMPORARY TABLE */ 
SELECT * INTO TerritoryTypesWithDerivedAlignment FROM
(SELECT DISTINCT aettt.TerritoryType_id AS TerritoryType_Id, 
 aettt.AlignmentEntityType_ParentId AS AlignmentEntityType_id 
 FROM AlignmentEntityTypeTerritoryTypes aettt
 WHERE aettt.AlignmentEntityType_ParentId IS NOT NULL) a
			
DECLARE @attributeTypeId VARCHAR(200) -- Attribute Type Id
DECLARE @attributeTypeName VARCHAR(200) -- Attribute Type Name
DECLARE @sql NVARCHAR(MAX)
DECLARE @maxIdValue bigint;

/* CREATE TEMPORARY TABLES */
CREATE TABLE [TempAlignmentEntityNumberAttributeCache] 
( AlignmentEntityNumberAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
AlignmentEntityNumberAttributeCache_Value float NULL , 
AlignmentEntity_id nvarchar(100) NULL , 
AttributeType_id bigint NULL , 
TerritoryType_id nvarchar(100) NULL , 
CONSTRAINT PK_TempAlignmentEntityNumberAttributeCache_AlignmentEntityNumberAttributeCache_Id 
PRIMARY KEY CLUSTERED (AlignmentEntityNumberAttributeCache_Id));

CREATE TABLE [TempAlignmentEntityTextAttributeCache] 
( AlignmentEntityTextAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
AlignmentEntityTextAttributeCache_Value nvarchar(2000) NULL ,
AlignmentEntity_id nvarchar(100) NULL , 
AttributeType_id bigint NULL , 
CONSTRAINT PK_TempAlignmentEntityTextAttributeCache_AlignmentEntityTextAttributeCache_Id 
PRIMARY KEY CLUSTERED (AlignmentEntityTextAttributeCache_Id))

/* Populate Number attribute table*/
DECLARE Attribute_Cursor CURSOR FOR
SELECT AttributeType_Id, AttributeType_Name FROM [AttributeTypes]
WHERE AttributeType_Entity = 'AlignmentEntity' AND
	(AttributeType_DataType = 'Number' OR AttributeType_DataType = 'Currency')

OPEN Attribute_Cursor
FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @sql = N'
	INSERT INTO [TempAlignmentEntityNumberAttributeCache] with(tablock)
	(AlignmentEntity_id, AttributeType_id, AlignmentEntityNumberAttributeCache_Value,TerritoryType_id) 
	(SELECT ueet.AlignmentEntity_id AS AlignmentEntity_id,' + 
			@attributeTypeId + ' AS AttributeType_id,			
			0 AS AlignmentEntityNumberAttributeCache_Value,			 
			pae.TerritoryType_id AS TerritoryType_id
			FROM UpsertedEntitysEntityTypes ueet
			LEFT JOIN TerritoryTypesWithDerivedAlignment pae 
			ON ueet.AlignmentEntityType_id = pae.AlignmentEntityType_id
	 UNION SELECT ueet.AlignmentEntity_id AS AlignmentEntity_id,' + 
			@attributeTypeId + ' AS AttributeType_id,			
			0 AS AlignmentEntityNumberAttributeCache_Value, 
			NULL AS TerritoryType_id
			FROM UpsertedEntitysEntityTypes ueet
			where exists (
			select distinct TerritoryType_Id from TerritoryTypes where TerritoryType_Id
			not in (select TerritoryType_Id from TerritoryTypesWithDerivedAlignment)));'

	exec sp_executesql @sql;

	FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName
END

SELECT @maxIdValue = ISNULL(MAX(AlignmentEntityNumberAttributeCache_Id),0) FROM [AlignmentEntityNumberAttributeCache];

		 /* INSERT ENTRIES INTO ACTUAL CACHE TABLE FROM TEMP CACHE TABLE */
		MERGE [AlignmentEntityNumberAttributeCache] WITH(TABLOCK) AS TARGET
		USING (SELECT AlignmentEntityNumberAttributeCache_Id + @maxIdValue AS AlignmentEntityNumberAttributeCache_Id, 
		AlignmentEntityNumberAttributeCache_Value, AlignmentEntity_id, AttributeType_id, 
		TerritoryType_id FROM [TempAlignmentEntityNumberAttributeCache]) AS SOURCE
		ON Target.AlignmentEntity_id = SOURCE.AlignmentEntity_id and Target.AttributeType_Id = SOURCE.AttributeType_Id and
		(Target.TerritoryType_id = SOURCE.TerritoryType_id OR (Target.TerritoryType_id IS NULL and SOURCE.TerritoryType_id IS NULL)) 
		 WHEN NOT MATCHED THEN 
        INSERT (AlignmentEntityNumberAttributeCache_Id,AlignmentEntity_id,AlignmentEntityNumberAttributeCache_Value,TerritoryType_id,AttributeType_id) 
        VALUES (SOURCE.AlignmentEntityNumberAttributeCache_Id,SOURCE.AlignmentEntity_id,SOURCE.AlignmentEntityNumberAttributeCache_Value,
        SOURCE.TerritoryType_id,SOURCE.AttributeType_id); 

		/* Update HiLo table */
		DELETE FROM HiLoUniqueKey
		WHERE ObjectType = 'AlignmentEntityNumberAttributeCache'

		INSERT INTO HiLoUniqueKey (NextHi, ObjectType)
		Values ((SELECT ISNULL(MAX(AlignmentEntityNumberAttributeCache_Id),0)+1 
					FROM [AlignmentEntityNumberAttributeCache]), 'AlignmentEntityNumberAttributeCache');

		DROP TABLE [TempAlignmentEntityNumberAttributeCache]
		CLOSE Attribute_Cursor   
		DEALLOCATE Attribute_Cursor
		
/* Populate text attribute table*/
DECLARE Attribute_Cursor CURSOR FOR
SELECT AttributeType_Id, AttributeType_Name FROM [AttributeTypes]
WHERE AttributeType_Entity = 'AlignmentEntity' AND AttributeType_DataType = 'Text'

OPEN Attribute_Cursor
FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @sql = N'
	INSERT INTO [TempAlignmentEntityTextAttributeCache] with(tablock)
	(AlignmentEntity_id, AttributeType_id, AlignmentEntityTextAttributeCache_Value) 
	(SELECT ueet.AlignmentEntity_id AS AlignmentEntity_id,' + 
			@attributeTypeId + ' AS AttributeType_id,			
			NULL AS AlignmentEntityTextAttributeCache_Value 
			FROM UpsertedEntitysEntityTypes ueet);'

	exec sp_executesql @sql;

	FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName
END

SELECT @maxIdValue = ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0) FROM [AlignmentEntityTextAttributeCache];

INSERT INTO [AlignmentEntityTextAttributeCache] with(tablock) (AlignmentEntityTextAttributeCache_Id, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id)
SELECT AlignmentEntityTextAttributeCache_Id + @maxIdValue, AlignmentEntityTextAttributeCache_Value, AlignmentEntity_id, AttributeType_id FROM [TempAlignmentEntityTextAttributeCache]; 

/* Drop Temporary Table */
DROP TABLE [TempAlignmentEntityTextAttributeCache]

/* Insert object in HiLo table */

DELETE FROM HiLoUniqueKey WHERE ObjectType = 'AlignmentEntityTextAttributeCache';

INSERT INTO HiLoUniqueKey
(NextHi, ObjectType)
VALUES
((SELECT ISNULL(MAX(AlignmentEntityTextAttributeCache_Id),0)+1 
				FROM [AlignmentEntityTextAttributeCache]), 'AlignmentEntityTextAttributeCache');
						
CLOSE Attribute_Cursor   
DEALLOCATE Attribute_Cursor
drop table UpsertedEntitysEntityTypes
drop table TerritoryTypesWithDerivedAlignment


