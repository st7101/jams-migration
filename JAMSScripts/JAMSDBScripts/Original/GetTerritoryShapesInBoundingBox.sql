﻿
select shapes.OrgUnit_Id as OrgUnitId, org.OrgUnit_Name as OrgUnitName, org.OrgUnit_ColorId as OrgUnitColorId,
Shape = CASE WHEN :zoomLevel between 7 and 10 and shapes.geom_medium IS NOT NULL THEN shapes.geom_medium.STIntersection(:boundingBoxWKT).Reduce(:reduceTolerance).STAsBinary() 
                     WHEN :zoomLevel between 3 and 6 and shapes.geom_low IS NOT NULL Then shapes.geom_low.STIntersection(:boundingBoxWKT).Reduce(:reduceTolerance).STAsBinary()
					  ELSE shapes.geom.STIntersection(:boundingBoxWKT).Reduce(:reduceTolerance).STAsBinary() END 
from TerritoryShapes as shapes
inner join Orgunits as org
on shapes.OrgUnit_Id = org.OrgUnit_Id
inner join TerritoryTypes as t
on org.OrgUnitLevel_id = t.OrgUnitLevel_id
where
t.TerritoryType_Id = :territoryTypeId 
and ((:showAssigned = 1 and org.OrgUnit_TreeLeft >= :treeLeft and org.OrgUnit_TreeRight <= :treeRight) or (:showUnassigned = 1 and org.OrgUnit_IsUnassigned = 1))
and geom.STEnvelope().STIntersects(:boundingBoxWKT) = 1