﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace JAMSScripts.TestHarness
{
    class TestHarness : IDisposable
    {
        private string[] _args;
        private string _eventDbConnectionString;
        private string _logConnectionString = @"Server=(local);Database=JAMSPerfTests;Trusted_Connection=True;";
        private string _scriptFolder;
        private string _originalScripts;
        private string _r20Scripts;
        private string _legacyDbConnectionString;
        private string _legacyScriptFolder;
        private int _version;
        private TestParameters _params;
        private Dictionary<string, int> _variables;

        private List<string> _scriptFiles;
        private StreamWriter _log;
        private DataTable _eventInfo = new DataTable();
        private DataTable _dtStats = new DataTable();
        private List<TestCase> _tests = new List<TestCase>();
        public TestHarness(string[] argv)
        {
            _args = argv;
            ParseArguments(_args);
            _version = 1000;
            _variables = new Dictionary<string, int>();
            _log = new StreamWriter(_scriptFolder + @"\TestHarnessLog.txt");
            _scriptFiles = new List<string>();
            _params = new TestParameters(_eventDbConnectionString);
            LoadTests();

        }

        private void LoadTests()
        {
            DataTable dt = new DataTable();
            using (SqlConnection con = new SqlConnection(_logConnectionString))
            {
                con.Open();
                using (SqlDataAdapter da = new SqlDataAdapter("select * from tests where testgroup = 'Query'", con))
                {
                    da.Fill(dt);
                }

            }

            foreach (DataRow dr in dt.Rows)
            {
                _tests.Add(new TestCase(dr, _params));
            }
        }

        public void WriteTestXml()
        {
            DirectoryInfo outputDir;
            if (Directory.Exists(String.Format("Output_{0}_{1}_Xml", DateTime.Now.Month, DateTime.Now.Day)))
                Directory.Delete(String.Format("Output_{0}_{1}_Xml", DateTime.Now.Month, DateTime.Now.Day), true);

            outputDir = Directory.CreateDirectory(String.Format("Output_{0}_{1}_Xml", DateTime.Now.Month, DateTime.Now.Day));

            foreach (TestCase tc in _tests)
            {

                XDocument doc = new XDocument(new XElement("TestCase", new XElement("DefaultParams"), new XElement("OriginalParams"), new XElement("R20Params")));
                XElement rootDoc = doc.Root.Elements("DefaultParams").First();
                XElement rootOld = doc.Root.Elements("OriginalParams").First();
                XElement rootNew = doc.Root.Elements("R20Params").First();
                foreach (var p in tc.DefaultParams)
                {
                    XElement temp = new XElement("DefaultParam");
                    temp.SetAttributeValue("Name", p.Key);
                    temp.SetAttributeValue("Tag", p.Key.ToUpper().TrimStart(':'));
                    rootDoc.Add(temp);
                }

                foreach (var p in tc.OldParams)
                {
                    XElement temp = new XElement("OldParam");
                    temp.SetAttributeValue("Name", p.Key);
                    temp.SetAttributeValue("Tag", p.Key.ToUpper().TrimStart(':'));
                    rootOld.Add(temp);
                }

                foreach (var p in tc.NewParams)
                {
                    XElement temp = new XElement("R20Param");
                    temp.SetAttributeValue("Name", p.Key);
                    temp.SetAttributeValue("Tag", p.Key.ToUpper().TrimStart(':'));
                    rootNew.Add(temp);
                }

                File.WriteAllText(outputDir + "\\" + tc.Name + "_Default.xml", doc.ToString());
                using (SqlConnection con = new SqlConnection(_logConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO [dbo].[Tests] ([Title],[ScriptName],[DefaultParams] ,[OriginalParams],[R20Params]) VALUES (@Title ,@ScriptName ,@DefaultParams ,@OriginalParams ,@R20Params)", con))
                    {
                        cmd.Parameters.AddWithValue("@Title", tc.Name);
                        cmd.Parameters.AddWithValue("@ScriptName", tc.ScriptName);
                        cmd.Parameters.AddWithValue("@DefaultParams", rootDoc.ToString());
                        cmd.Parameters.AddWithValue("@OriginalParams", rootOld.ToString());
                        cmd.Parameters.AddWithValue("@R20Params", rootNew.ToString());
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }

        public void WriteTestXmlExtended()
        {
            DirectoryInfo outputDir;
            if (Directory.Exists(String.Format("Output_{0}_{1}_Xml", DateTime.Now.Month, DateTime.Now.Day)))
                Directory.Delete(String.Format("Output_{0}_{1}_Xml", DateTime.Now.Month, DateTime.Now.Day), true);

            outputDir = Directory.CreateDirectory(String.Format("Output_{0}_{1}_Xml", DateTime.Now.Month, DateTime.Now.Day));
            DirectoryInfo scriptsRaw = new DirectoryInfo(_scriptFolder);
            DirectoryInfo originalRaw = new DirectoryInfo(_legacyScriptFolder);

            foreach (var file in scriptsRaw.GetFiles("*.sql"))
            {
                XDocument doc = new XDocument(new XElement("TestCase", new XElement("DefaultParams"), new XElement("OriginalParams"), new XElement("R20Params")));
                XElement rootDoc = doc.Root.Elements("DefaultParams").First();
                XElement rootOld = doc.Root.Elements("OriginalParams").First();
                XElement rootNew = doc.Root.Elements("R20Params").First();
                FileInfo originalFile = originalRaw.GetFiles(file.Name)[0];
                string originalSql = originalFile.OpenText().ReadToEnd();
                foreach (Match match in Regex.Matches(originalSql, @"(?<!\w):\w+"))
                {

                    XElement temp = new XElement("DefaultParam");
                    temp.SetAttributeValue("Name", match.Value);
                    temp.SetAttributeValue("Tag", match.Value.ToUpper().TrimStart(':'));
                    rootDoc.Add(temp);
                }


                File.WriteAllText(outputDir + "\\" + file.Name + "_Default.xml", doc.ToString());
                using (SqlConnection con = new SqlConnection(_logConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("INSERT INTO [dbo].[Tests] ([Title],[ScriptName],[DefaultParams] ,[OriginalParams],[R20Params]) VALUES (@Title ,@ScriptName ,@DefaultParams ,@OriginalParams ,@R20Params)", con))
                    {
                        cmd.Parameters.AddWithValue("@Title", file.Name);
                        cmd.Parameters.AddWithValue("@ScriptName", file.Name);
                        cmd.Parameters.AddWithValue("@DefaultParams", rootDoc.ToString());
                        cmd.Parameters.AddWithValue("@OriginalParams", rootOld.ToString());
                        cmd.Parameters.AddWithValue("@R20Params", rootNew.ToString());
                        cmd.ExecuteNonQuery();
                    }
                }
            }
        }


        public void Run()
        {
            for (int k = 1; k < 2; k++)
            {
                var output = GenerateScripts(k);
                foreach (var file in Directory.GetFiles(output + "\\Original", "*.sql"))

                    using (SqlConnection con = new SqlConnection(_legacyDbConnectionString))
                    {
                        con.Open();
                        con.StatisticsEnabled = true;

                        var f = new FileInfo(file);
                        string sql = f.OpenText().ReadToEnd();
                        for (int i = 0; i < 1; i++)
                        {
                            try
                            {
                                using (SqlCommand cmd = new SqlCommand(sql, con))
                                {
                                    cmd.CommandTimeout = 0;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            catch (SqlException e)
                            {
                                LogMessage("Error in sciprt:" + f.FullName + @"\n\n" + sql + @"\n\n" + e.Message);
                                continue;
                            }
                            var statistics = con.RetrieveStatistics();
                            if (_dtStats.Columns.Count == 0)
                                SetupStatsTable(statistics);
                            DataRow dr = _dtStats.NewRow();

                            dr[0] = "Before";
                            dr[1] = f.Name;
                            dr[2] = i;

                            for (int j = 3; j < 21; j++)
                                dr[j] = statistics[_dtStats.Columns[j].ColumnName].ToString();
                            _dtStats.Rows.Add(dr);

                            con.ResetStatistics();
                        }

                    }
                foreach (var file in Directory.GetFiles(_r20Scripts, "*.sql"))

                    using (SqlConnection con = new SqlConnection(_eventDbConnectionString))
                    {
                        con.Open();
                        con.StatisticsEnabled = true;

                        var f = new FileInfo(file);
                        string sql = f.OpenText().ReadToEnd();
                        for (int i = 0; i < 1; i++)
                        {
                            try
                            {

                                using (SqlCommand cmd = new SqlCommand(sql, con))
                                {
                                    cmd.CommandTimeout = 0;
                                    cmd.ExecuteNonQuery();
                                }
                            }
                            catch (SqlException e)
                            {
                                LogMessage("Error in sciprt:" + f.FullName + @"\n\n" + sql + @"\n\n" + e.Message);
                                continue;
                            }
                            var statistics = con.RetrieveStatistics();
                            if (_dtStats.Columns.Count == 0)
                                SetupStatsTable(statistics);
                            DataRow dr = _dtStats.NewRow();

                            dr[0] = "After";
                            dr[1] = f.Name;
                            dr[2] = i;
                            for (int j = 3; j < 21; j++)
                                dr[j] = statistics[_dtStats.Columns[j].ColumnName].ToString();
                            _dtStats.Rows.Add(dr);

                            con.ResetStatistics();
                        }

                    }

                SaveStats();
            }
        }

        public void RunSpecificTests()
        {
            var output = GenerateScripts(_tests);
            foreach (var file in Directory.GetFiles(output + "\\Original", "*.sql"))

                using (SqlConnection con = new SqlConnection(_legacyDbConnectionString))
                {
                    con.Open();
                    con.StatisticsEnabled = true;

                    var f = new FileInfo(file);
                    string sql = f.OpenText().ReadToEnd();
                    for (int i = 0; i < 1; i++)
                    {
                        try
                        {
                            using (SqlCommand cmd = new SqlCommand(sql, con))
                            {
                                cmd.CommandTimeout = 0;
                                cmd.ExecuteNonQuery();
                            }
                        }
                        catch (SqlException e)
                        {
                            LogMessage("Error in sciprt:" + f.FullName + @"\n\n" + sql + @"\n\n" + e.Message);
                            continue;
                        }
                        var statistics = con.RetrieveStatistics();
                        if (_dtStats.Columns.Count == 0)
                            SetupStatsTable(statistics);
                        DataRow dr = _dtStats.NewRow();

                        dr[0] = "Before";
                        dr[1] = f.Name;
                        dr[2] = i;

                        for (int j = 3; j < 21; j++)
                            dr[j] = statistics[_dtStats.Columns[j].ColumnName].ToString();
                        _dtStats.Rows.Add(dr);

                        con.ResetStatistics();
                    }

                }
            foreach (var file in Directory.GetFiles(_r20Scripts, "*.sql"))

                using (SqlConnection con = new SqlConnection(_eventDbConnectionString))
                {
                    con.Open();
                    con.StatisticsEnabled = true;

                    var f = new FileInfo(file);
                    string sql = f.OpenText().ReadToEnd();
                    for (int i = 0; i < 1; i++)
                    {
                        try
                        {

                            using (SqlCommand cmd = new SqlCommand(sql, con))
                            {
                                cmd.CommandTimeout = 0;
                                cmd.ExecuteNonQuery();
                            }
                        }
                        catch (SqlException e)
                        {
                            LogMessage("Error in sciprt:" + f.FullName + @"\n\n" + sql + @"\n\n" + e.Message);
                            continue;
                        }
                        var statistics = con.RetrieveStatistics();
                        if (_dtStats.Columns.Count == 0)
                            SetupStatsTable(statistics);
                        DataRow dr = _dtStats.NewRow();

                        dr[0] = "After";
                        dr[1] = f.Name;
                        dr[2] = i;
                        for (int j = 3; j < 21; j++)
                            dr[j] = statistics[_dtStats.Columns[j].ColumnName].ToString();
                        _dtStats.Rows.Add(dr);

                        con.ResetStatistics();
                    }

                }

            SaveStats();

        }
        private void SaveStats()
        {
            using (SqlConnection con = new SqlConnection(_logConnectionString))
            {
                string tableName = String.Format("Stats_{0}_{1}_{2}", DateTime.Now.Year, DateTime.Now.Month, DateTime.Now.Day);
                #region add sql table
                StringBuilder sql = new StringBuilder();
                sql.AppendFormat("IF EXISTS (select top 1 1 FROM sys.tables where name ='{0}') BEGIN DROP TABLE {0}; END  CREATE TABLE [{0}] (", tableName);
                for (int i = 0; i < _dtStats.Columns.Count; i++)
                {
                    sql.AppendFormat(" {0} varchar(255) null,", _dtStats.Columns[i].ColumnName);
                }
                sql.Remove(sql.Length - 1, 1).Append(")");
                con.Open();
                using (SqlCommand cmd = new SqlCommand(sql.ToString(), con))
                    cmd.ExecuteNonQuery();
                #endregion
                using (SqlBulkCopy bulk = new SqlBulkCopy(con))
                {

                    bulk.DestinationTableName = tableName;
                    bulk.WriteToServer(_dtStats);
                }
            }
        }

        private void SetupStatsTable(System.Collections.IDictionary statistics)
        {
            _dtStats.Columns.Add("State");
            _dtStats.Columns.Add("Query");
            _dtStats.Columns.Add("Iteration");
            foreach (var stat in statistics.Keys)
                _dtStats.Columns.Add(stat.ToString());
        }


        public DirectoryInfo GenerateScripts(int iteration)
        {

            DirectoryInfo outputDir;
            if (Directory.Exists(String.Format("Output_{0}_{1}_{2}", DateTime.Now.Month, DateTime.Now.Day, iteration)))
                Directory.Delete(String.Format("Output_{0}_{1}_{2}", DateTime.Now.Month, DateTime.Now.Day, iteration), true);

            outputDir = Directory.CreateDirectory(String.Format("Output_{0}_{1}_{2}", DateTime.Now.Month, DateTime.Now.Day, iteration));
            if (outputDir.GetDirectories("Query").Count() > 1)
                outputDir.GetDirectories("Query").First().Delete(true);
            outputDir = outputDir.CreateSubdirectory("Query");


            _originalScripts = outputDir.CreateSubdirectory("Original").FullName;
            _r20Scripts = outputDir.CreateSubdirectory("R20").FullName;

            _params = new TestParameters(_eventDbConnectionString);

            DirectoryInfo scriptsRaw = new DirectoryInfo(_scriptFolder);
            DirectoryInfo originalRaw = new DirectoryInfo(_legacyScriptFolder);

            foreach (FileInfo file in scriptsRaw.GetFiles("*.sql"))
            {
                try
                {

                    if (File.Exists(originalRaw.FullName + "\\" + file.Name))
                    {
                        _params.ClearMapping();
                        FileInfo originalFile = originalRaw.GetFiles(file.Name)[0];
                        string originalSql = originalFile.OpenText().ReadToEnd();
                        string newOriginalSql = ReplaceVariablesInScript(originalSql, true);
                        File.WriteAllText(_originalScripts + "\\" + file.Name, newOriginalSql);
                        string sql = file.OpenText().ReadToEnd();
                        string newSql = ReplaceVariablesInScript(sql, false);
                        File.WriteAllText(_r20Scripts + "\\" + file.Name, newSql);

                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    LogMessage("Unable to replace all variables in script\n" + e.Message);

                }

            }
            return outputDir;
        }

        public DirectoryInfo GenerateScripts(List<TestCase> tests)
        {

            DirectoryInfo outputDir;
            if (Directory.Exists(String.Format("Output_{0}_{1}_TestRun", DateTime.Now.Month, DateTime.Now.Day)))
                Directory.Delete(String.Format("Output_{0}_{1}_TestRun", DateTime.Now.Month, DateTime.Now.Day), true);

            outputDir = Directory.CreateDirectory(String.Format("Output_{0}_{1}_TestRun", DateTime.Now.Month, DateTime.Now.Day));

            _originalScripts = outputDir.CreateSubdirectory("Original").FullName;
            _r20Scripts = outputDir.CreateSubdirectory("R20").FullName;



            DirectoryInfo scriptsRaw = new DirectoryInfo(_scriptFolder);
            DirectoryInfo originalRaw = new DirectoryInfo(_legacyScriptFolder);

            foreach (TestCase tc in tests)
            {
                try
                {

                    if (File.Exists(originalRaw.FullName + "\\" + tc.ScriptName))
                    {
                        FileInfo originalFile = originalRaw.GetFiles(tc.ScriptName)[0];
                        string originalSql = originalFile.OpenText().ReadToEnd();
                        string newOriginalSql = tc.PopulateSQL(originalSql, true);
                        File.WriteAllText(_originalScripts + "\\" + tc.Name + ".sql", newOriginalSql);
                        FileInfo newFile = scriptsRaw.GetFiles(tc.ScriptName)[0];
                        string sql = newFile.OpenText().ReadToEnd();
                        string newSql = tc.PopulateSQL(sql, false);
                        File.WriteAllText(_r20Scripts + "\\" + tc.Name + ".sql", newSql);

                    }
                }
                catch (IndexOutOfRangeException e)
                {
                    LogMessage("Unable to replace all variables in script\n" + e.Message);

                }

            }
            return outputDir;
        }



        public void ParseArguments(string[] argv)
        {
            _eventDbConnectionString = argv[0];
            _scriptFolder = argv[1];
            _legacyDbConnectionString = argv[2];
            _legacyScriptFolder = argv[3];

        }
        private string GetEventConnectionString(string metaDbString, string title)
        {
            var connList = metaDbString.Split(';');
            var newConnString = "";

            foreach (var segment in connList)
            {
                if (segment.ToLower().Contains("database"))
                {
                    newConnString += "Database=" + title + ";";
                }
                else
                {
                    if (!String.IsNullOrWhiteSpace(segment))
                    {
                        newConnString += segment + ";";
                    }
                }
            }
            return newConnString;
        }


        //{
        //    return new SqlConnection((String.IsNullOrEmpty(eventDbConnString) ? metaDbConnString : eventDbConnString));
        //}

        private string GetSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/" + file);
            return f.OpenText().ReadToEnd();
        }
        private string GetSqlFromFileForEvent(string file, string dBIdentifier)
        {
            FileInfo f = new FileInfo("sql/" + file);
            return f.OpenText().ReadToEnd().Replace("{DBIdentifier}", dBIdentifier);
        }
        private string GetCreateSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/CreateTables/" + file);
            return f.OpenText().ReadToEnd();
        }
        private string GetMigrateSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/MigrateForeignKeys/" + file);
            return f.OpenText().ReadToEnd();
        }
        private string[] SplitSqlFromFile(string file)
        {
            FileInfo f = new FileInfo("sql/" + file);
            return f.OpenText().ReadToEnd().Split('\n');
        }
        private void LogMessage(string message)
        {
            System.Console.WriteLine(String.Format("{0}: {1}", DateTime.Now.ToLongTimeString(), message));
            _log.WriteLine(String.Format("{0}: {1}", DateTime.Now.ToLongTimeString(), message));
        }

        private string[] GetVariablesFromScript(string sqlScript)
        {
            string[] returnVal = null;
            foreach (Match match in Regex.Matches(sqlScript, @"(?<!\w):\w+"))
            {
                if (_variables.ContainsKey(match.Value.ToUpper()))
                {
                    _variables[match.Value.ToUpper()]++;
                }
                else
                {
                    _variables.Add(match.Value.ToUpper(), 1);

                }
                Console.WriteLine(match.Value);
            }
            return returnVal;
        }

        private string ReplaceVariablesInScript(string sqlScript, bool legacy)
        {
            string returnVal = sqlScript;
            try
            {
                foreach (Match match in Regex.Matches(sqlScript, @"(?<!\w):\w+"))
                {
                    string variable = match.Value.Substring(1).ToUpper();
                    if (variable == "ATTRIBUTETYPEID")
                    {
                        if (Regex.Matches(sqlScript, @"TextAttribute").Count > 0)
                        {
                            var exceptions = Regex.Matches(sqlScript, @"(?<!\w){\w+}");
                            string[] att = _params.GetParameterValue("ATTRIBUTETYPEID_Text", legacy).Split(new string[] { "," }, StringSplitOptions.None);
                            if (exceptions.Count > 0)
                                foreach (Match e in exceptions)
                                    if (e.Value.Contains("0"))
                                        returnVal = returnVal.Replace(e.Value, att[1]);

                            returnVal = returnVal.Replace(match.Value, att[0]);
                        }
                        else
                        {
                            var exceptions = Regex.Matches(sqlScript, @"(?<!\w){\w+}");
                            string[] att = _params.GetParameterValue("ATTRIBUTETYPEID_Number", legacy).Split(new string[] { "," }, StringSplitOptions.None);
                            if (exceptions.Count > 0)
                                foreach (Match e in exceptions)
                                    if (e.Value.Contains("0"))
                                        returnVal = returnVal.Replace(e.Value, att[1]);
                            returnVal = returnVal.Replace(match.Value, att[0]);

                        }
                    }
                    else
                    {
                        string temp = String.Format(@"{0}\b", match.Value);
                        string t2 = _params.GetParameterValue(variable, legacy);
                        returnVal = Regex.Replace(returnVal, temp, t2, RegexOptions.IgnoreCase);
                    }
                }
            }
            catch (IndexOutOfRangeException e)
            {
                throw e;
            }

            //remove unwanted symbols
            returnVal = returnVal.Replace(".[{0}]", "");
            returnVal = returnVal.Replace("{1}", "");

            return returnVal;
        }


        private void PrintVariables()
        {
            foreach (var variable in _variables)
            {
                Console.WriteLine(String.Format("Variable:{0} Count:{1}", variable.Key, variable.Value));
                _log.WriteLine(String.Format("{0}\t{1}", variable.Key, variable.Value));
            }
        }

        private void PrintScriptFiles()
        {
            using (var scripts = new StreamWriter(_scriptFolder + "TestHarnessScripts.txt"))
                foreach (var file in _scriptFiles)
                {
                    Console.WriteLine(String.Format("Script:{0}", file));
                    scripts.WriteLine(String.Format("Script:{0}", file));
                }

        }

        public void Dispose()
        {
            _log.Close();
        }

        public void DocumentScripts()
        {
            StringBuilder sb = new StringBuilder();
            DirectoryInfo dir = new DirectoryInfo(@"D:\Code\JAMS_Original_Scripts\Original");
            sb.Append(@"<style>#hor-zebra {font-family: ""Lucida Sans Unicode"", ""Lucida Grande"", Sans-Serif;	font-size: 12px;			text-align: left;	border-collapse: collapse; 	border-top: 3px solid #9baff1; border-bottom: 3px solid #9baff1; } #hor-zebra th {	padding: 8px; 	font-size: 14px;	font-weight: normal;text-align: left;	color: #039; 	border-bottom: 2px solid #6678b1;	border-right: 1px solid #9baff1; border-left: 1px solid #9baff1; } #hor-zebra td {	padding-left: 8px; padding-right: 8px; 	border-right: 1px solid #aabcfe; border-left: 1px solid #aabcfe;		color: #669; } #hor-zebra .odd { 	background: #e8edff; }</style>");

            foreach (FileInfo file in dir.GetFiles("*.sql"))
            {
                sb.AppendFormat(@"<table id=""hor-zebra""><tr><th>Script: {0}</th></tr><tr><th>Input Parameters</th></tr>", file.Name);
                var sqlScript = file.OpenText().ReadToEnd();
                List<string> vars = new List<string>();
                bool odd = true;
                
                foreach (Match match in Regex.Matches(sqlScript, @"(?<!\w):\w+"))
                {
                    if (!vars.Contains(match.Value))
                    {
                        if (odd)
                            sb.AppendFormat(@"<tr class=""odd""><td>{0}</td></tr>",match.Value);
                        else
                            sb.AppendFormat("<tr><td>{0}</td></tr>", match.Value);
                        vars.Add(match.Value);
                        odd = !odd;
                    }
                }
                sb.Append(@"<tr><th>Output</th></tr>");
                sb.Append(@"<tr class=""odd""><td></td></tr>");
     
                if (sqlScript.Contains("EXEC") || sqlScript.Contains("execute"))
                {
                    sb.Append(@"<tr><th>Stored Procedure</th></tr>");
                    sb.Append(@"<tr class=""odd""><td></td></tr>");
                }
                sb.Append(@"<tr><th>Changes</th></tr>");
                sb.Append(@"<tr class=""odd""><td></td></tr>");
               sb.Append("</table><br/><br/>");      
            }

            File.WriteAllText("JAMSScripts.html", sb.ToString());

        }
    }
}
