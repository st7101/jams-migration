IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempOrgUnitAttributeCache') 
DROP TABLE [TempOrgUnitAttributeCache]

IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempDerivedAlignmentsTerritoryTypes') 
DROP TABLE [TempDerivedAlignmentsTerritoryTypes]

/* CREATE TEMPORARY TABLE */
CREATE TABLE [TempOrgUnitAttributeCache] 
( OrgUnitAttributeCache_Id bigint NOT NULL IDENTITY(1,1), 
OrgUnitAttributeCache_Value float NULL , 
OrgUnit_id nvarchar(100) NULL , 
AttributeType_id bigint NULL , 
CONSTRAINT PK_TempOrgUnitAttributeCache_Id 
PRIMARY KEY CLUSTERED (OrgUnitAttributeCache_Id))

SELECT * INTO [TempDerivedAlignmentsTerritoryTypes] FROM
(SELECT DISTINCT aettt.TerritoryType_id as TerritoryType_Id, 
					aettt.AlignmentEntityType_ParentId as AlignmentEntityType_id 
 FROM AlignmentEntityTypeTerritoryTypes aettt
 WHERE aettt.AlignmentEntityType_ParentId IS NOT NULL) pae

/* Populate Temporary table */
INSERT INTO [TempOrgUnitAttributeCache] with(tablock) (OrgUnit_id, AttributeType_id, OrgUnitAttributeCache_Value) 
(SELECT oae.OrgUnit_id, aenac.AttributeType_id AS AttributeType_id, SUM(aenac.AlignmentEntityNumberAttributeCache_Value * oae.OrgUnitAlignmentEntity_Weight/100) AS OrgUnitAttributeCache_Value 
	FROM AlignmentEntityNumberAttributeCache aenac
	INNER JOIN OrgUnitAlignmentEntitys oae ON aenac.AlignmentEntity_id = oae.AlignmentEntity_id
	INNER JOIN AlignmentEntitys ae ON oae.AlignmentEntity_id = ae.AlignmentEntity_Id
	INNER JOIN OrgUnits o ON oae.OrgUnit_id = o.OrgUnit_Id
	INNER JOIN TerritoryTypes tt ON tt.OrgUnitLevel_id = o.OrgUnitLevel_id
	INNER JOIN AttributeTypes at ON at.AttributeType_id = aenac.AttributeType_id
	LEFT OUTER JOIN  TempDerivedAlignmentsTerritoryTypes da ON 
	da.TerritoryType_Id = tt.TerritoryType_Id AND ae.AlignmentEntityType_id = da.AlignmentEntityType_id
	WHERE 
	at.AttributeType_Name in ('METRIC3')
	AND
	((ae.AlignmentEntityType_id = da.AlignmentEntityType_id AND tt.TerritoryType_Id = aenac.TerritoryType_id) 
	OR 
	(da.AlignmentEntityType_id IS NULL AND aenac.TerritoryType_id IS NULL))
	AND ae.AlignmentEntity_Status = 'Normal'
	AND at.AttributeType_Entity = 'AlignmentEntity' 
	AND (at.AttributeType_DataType = 'Number' OR at.AttributeType_DataType = 'Currency')
	GROUP BY oae.OrgUnit_id, aenac.AttributeType_id
)

INSERT INTO [TempOrgUnitAttributeCache] with(tablock) (OrgUnit_id, AttributeType_id, OrgUnitAttributeCache_Value)
(
SELECT parent.orgunit_Id as oid, oc.AttributeType_id as aid, sum(oc.OrgUnitAttributeCache_Value) as val from TempOrgUnitAttributeCache as oc
inner JOIN OrgUnits child ON child.OrgUnit_Id = oc.Orgunit_Id
INNER JOIN OrgUnits parent ON child.OrgUnit_TreeLeft >  parent.OrgUnit_TreeLeft AND child.OrgUnit_TreeRight < parent.OrgUnit_TreeRight
GROUP BY parent.orgunit_Id, oc.AttributeType_id
)


INSERT INTO [OrgUnitAttributeCache] with(tablock) (OrgUnitAttributeCache_Id, OrgUnitAttributeCache_Value, OrgUnit_id, AttributeType_id)
Select (OrgUnitAttributeCache_Id + (SELECT ISNULL(MAX(OrgUnitAttributeCache_Id),0)+1 
				FROM [OrgUnitAttributeCache])), 
OrgUnitAttributeCache_Value, OrgUnit_id, AttributeType_id FROM [TempOrgUnitAttributeCache]; 

 /* Drop Temporary Table */
DROP TABLE [TempOrgUnitAttributeCache]
DROP TABLE [TempDerivedAlignmentsTerritoryTypes]

/* Update HiLo table */
DELETE FROM HiLoUniqueKey
WHERE ObjectType = 'OrgUnitAttributeCache'

INSERT INTO HiLoUniqueKey (NextHi, ObjectType)
Values ((SELECT ISNULL(MAX(OrgUnitAttributeCache_Id),0)+1 
				FROM [OrgUnitAttributeCache]), 'OrgUnitAttributeCache');