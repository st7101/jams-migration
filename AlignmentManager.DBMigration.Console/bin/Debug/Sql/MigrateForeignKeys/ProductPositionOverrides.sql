
PRINT N'Updating ProductPositionOverrides ...';

UPDATE ppo
SET ProductPositionOverride_PositionId=p.Position_Id
FROM dbo.ProductPositionOverrides ppo INNER JOIN 
dbo.Positions p ON ppo.ProductPositionOverride_ExternalPositionId=p.Position_ExternalID
