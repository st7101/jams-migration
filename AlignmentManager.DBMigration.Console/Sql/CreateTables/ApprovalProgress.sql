
PRINT N'Starting rebuilding table [dbo].[ApprovalProgress]...';


CREATE TABLE [dbo].[tmp_ms_xx_ApprovalProgress] (
    [ApprovalProgress_Id]          BIGINT         NOT NULL,
    [ApprovalProgress_ActionTaken] NVARCHAR (100) NULL,
    [Position_ExternalId]                  NVARCHAR (100) NULL,
    [Position_Id]           BIGINT         NULL,
    CONSTRAINT [tmp_ms_xx_PK_ApprovalProgress] PRIMARY KEY CLUSTERED ([ApprovalProgress_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[ApprovalProgress])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_ApprovalProgress] WITH (TABLOCK) ([ApprovalProgress_Id], [ApprovalProgress_ActionTaken], [Position_ExternalId])
        SELECT   [ApprovalProgress_Id],
                 [ApprovalProgress_ActionTaken],
                 [Position_Id]
        FROM     [dbo].[ApprovalProgress]
        ORDER BY [ApprovalProgress_Id] ASC;
    END

DROP TABLE [dbo].[ApprovalProgress];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_ApprovalProgress]', N'ApprovalProgress';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_PK_ApprovalProgress]', N'PK_ApprovalProgress', N'OBJECT';

