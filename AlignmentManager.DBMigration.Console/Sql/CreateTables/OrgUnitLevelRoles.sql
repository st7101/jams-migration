PRINT N'Starting rebuilding table [dbo].[OrgUnitLevelRoles]...';

CREATE TABLE [dbo].[tmp_ms_xx_OrgUnitLevelRoles] (
    [OrgUnitLevelRole_Id]          BIGINT         NOT NULL,
    [OrgUnitLevelRole_MaxPosition] INT            NULL,
    [OrgUnitLevelRole_MinPosition] INT            NULL,
    [UsesEffort]                   BIT            CONSTRAINT DF_tmp_ms_xx_OrgUnitLevelRoles_UsesEffort DEFAULT ((0)) NOT NULL,
    [OrgUnitLevel_ExternalId]              NVARCHAR (100) NULL,
    [OrgUnitLevel_Id]       BIGINT         NULL,
    [Role_Id]                      NVARCHAR (100) NULL,
    CONSTRAINT [tmp_ms_xx_constraint_PK_OrgUnitLevelRoles] PRIMARY KEY CLUSTERED ([OrgUnitLevelRole_Id] ASC) WITH ( DATA_COMPRESSION = PAGE)
);

IF EXISTS (SELECT TOP 1 1 
           FROM   [dbo].[OrgUnitLevelRoles])
    BEGIN
        INSERT INTO [dbo].[tmp_ms_xx_OrgUnitLevelRoles] WITH (TABLOCK) ([OrgUnitLevelRole_Id], [OrgUnitLevelRole_MaxPosition], [OrgUnitLevelRole_MinPosition], [UsesEffort], [OrgUnitLevel_ExternalId], [Role_Id])
        SELECT   [OrgUnitLevelRole_Id],
                 [OrgUnitLevelRole_MaxPosition],
                 [OrgUnitLevelRole_MinPosition],
                 [UsesEffort],
                 [OrgUnitLevel_Id],
                 [Role_Id]
        FROM     [dbo].[OrgUnitLevelRoles]
        ORDER BY [OrgUnitLevelRole_Id] ASC;
    END

DROP TABLE [dbo].[OrgUnitLevelRoles];

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_OrgUnitLevelRoles]', N'OrgUnitLevelRoles';

EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_OrgUnitLevelRoles]', N'PK_OrgUnitLevelRoles', N'OBJECT';
EXECUTE sp_rename N'[dbo].[DF_tmp_ms_xx_OrgUnitLevelRoles_UsesEffort]', N'DF_OrgUnitLevelRoles_UsesEffort', N'OBJECT';

