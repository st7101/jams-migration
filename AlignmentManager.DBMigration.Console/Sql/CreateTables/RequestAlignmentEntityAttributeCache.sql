
PRINT N'Starting rebuilding table [dbo].[RequestAlignmentEntityAttributeCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_RequestAlignmentEntityAttributeCache]
    (
      [RequestAlignmentEntityAttributeCache_Id] BIGINT NOT NULL ,
      [RequestAlignmentEntityAttributeCache_Value] FLOAT(53) NOT NULL ,
      [Request_Id] BIGINT NULL ,
      [AttributeType_Id] BIGINT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_RequestAlignmentEntityAttributeCache] PRIMARY KEY CLUSTERED
        ( [RequestAlignmentEntityAttributeCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[RequestAlignmentEntityAttributeCache] )
    BEGIN
        INSERT  INTO [dbo].[tmp_ms_xx_RequestAlignmentEntityAttributeCache] WITH (TABLOCK)
                ( [RequestAlignmentEntityAttributeCache_Id] ,
                  [RequestAlignmentEntityAttributeCache_Value] ,
                  [Request_Id] ,
                  [AttributeType_Id] ,
                  [TerritoryType_ExternalId] ,
                  [AlignmentEntity_ExternalId]
                )
                SELECT  [RequestAlignmentEntityAttributeCache_Id] ,
                        [RequestAlignmentEntityAttributeCache_Value] ,
                        [Request_Id] ,
                        [AttributeType_Id] ,
                        [TerritoryType_Id] ,
                        [AlignmentEntity_Id]
                FROM    [dbo].[RequestAlignmentEntityAttributeCache]
                ORDER BY [RequestAlignmentEntityAttributeCache_Id] ASC;
    END
DROP TABLE [dbo].[RequestAlignmentEntityAttributeCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_RequestAlignmentEntityAttributeCache]',
    N'RequestAlignmentEntityAttributeCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_RequestAlignmentEntityAttributeCache]',
    N'PK_RequestAlignmentEntityAttributeCache', N'OBJECT';
