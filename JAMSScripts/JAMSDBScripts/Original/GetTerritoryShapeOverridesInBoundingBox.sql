﻿select shapes.OrgUnit_Id as OrgUnitId, org.OrgUnit_Name as OrgUnitName, org.OrgUnit_ColorId as OrgUnitColorId, 
Shape = CASE WHEN :zoomLevel between 7 and 10 and shapes.geom_medium IS NOT NULL THEN shapes.geom_medium.STIntersection(:boundingBoxWKT).Reduce(:reduceTolerance).STAsBinary() 
                     WHEN :zoomLevel between 3 and 6 and shapes.geom_low IS NOT NULL Then shapes.geom_low.STIntersection(:boundingBoxWKT).Reduce(:reduceTolerance).STAsBinary()
					  ELSE shapes.geom.STIntersection(:boundingBoxWKT).Reduce(:reduceTolerance).STAsBinary() END
from TerritoryShapeOverrides as shapes
left outer join Orgunits as org
on shapes.OrgUnit_Id = org.OrgUnit_Id
left outer join OrgUnitOverrides as orgOverride
on shapes.OrgUnit_Id=orgOverride.OrgUnitOverride_OrgUnitId
inner join TerritoryTypes as t
on org.OrgUnitLevel_id = t.OrgUnitLevel_id or orgOverride.Level_id=t.OrgUnitLevel_id 
where Position_Id = :positionId and t.TerritoryType_Id = :territoryTypeId 
and ((:showAssigned = 1 and ((org.OrgUnit_TreeLeft >= :treeLeft and org.OrgUnit_TreeRight <= :treeRight) 
		 or (orgOverride.OrgUnitOverride_RefinementOperationType='Add')))
		 or (:showUnassigned = 1 and org.OrgUnit_IsUnassigned = 1))