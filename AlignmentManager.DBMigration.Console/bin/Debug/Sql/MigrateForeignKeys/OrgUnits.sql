
PRINT N'Updating OrgUnits ...';

UPDATE ou
SET OrgUnit_ParentId=ouParent.OrgUnit_Id,OrgUnitLevel_Id=oul.OrgUnitLevel_Id
FROM dbo.OrgUnits ou INNER JOIN 
 dbo.OrgUnitLevels oul ON ou.OrgUnitLevel_ExternalID=oul.OrgUnitLevel_ExternalID
LEFT OUTER JOIN dbo.OrgUnits ouParent ON ou.OrgUnit_ParentExternalId=ouParent.OrgUnit_ExternalID
