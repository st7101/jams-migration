PRINT N'Updating OrgUnitRelationshipStateEntrys ...';

UPDATE ourse
SET [OrgUnitRelationshipStateEntry_AffectedOrgUnitId]=ouA.OrgUnit_Id
FROM dbo.OrgUnitRelationshipStateEntrys ourse INNER JOIN 
dbo.OrgUnits ouA ON ourse.[OrgUnitRelationshipStateEntry_ExternalAffectedOrgUnitId]=ouA.OrgUnit_ExternalID
