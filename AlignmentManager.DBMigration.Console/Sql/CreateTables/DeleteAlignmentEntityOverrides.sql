
PRINT 'Updating DeleteAlignmentEntityOverrides'

CREATE TABLE dbo.tmp_ms_xx_DeleteAlignmentEntityOverrides
    (
      DeleteAlignmentEntityOverride_Id BIGINT NOT NULL ,
      Request_Id BIGINT NULL ,
      DeleteAlignmentEntityOverride_ExternalAlignmentEntityId NVARCHAR(100)
        NULL ,
      DeleteAlignmentEntityOverride_AlignmentEntityId BIGINT NULL ,
      AlignmentEntityType_Id BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_DeleteAlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( DeleteAlignmentEntityOverride_Id ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    )
ON  [PRIMARY]
IF EXISTS ( SELECT TOP 1
                    1
            FROM    dbo.DeleteAlignmentEntityOverrides )
    BEGIN
        INSERT  INTO dbo.tmp_ms_xx_DeleteAlignmentEntityOverrides WITH (TABLOCK)
                ( DeleteAlignmentEntityOverride_Id ,
                  Request_Id ,
                  DeleteAlignmentEntityOverride_ExternalAlignmentEntityId ,
                  AlignmentEntityType_Id
                )
                SELECT  DeleteAlignmentEntityOverride_Id ,
                        Request_Id ,
                        DeleteAlignmentEntityOverride_AlignmentEntityId ,
                        AlignmentEntityType_Id
                FROM    dbo.DeleteAlignmentEntityOverrides 
    END
DROP TABLE dbo.DeleteAlignmentEntityOverrides
EXECUTE sp_rename N'dbo.tmp_ms_xx_DeleteAlignmentEntityOverrides',
    N'DeleteAlignmentEntityOverrides'
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_DeleteAlignmentEntityOverrides]',
    N'[PK_DeleteAlignmentEntityOverrides', N'OBJECT';
