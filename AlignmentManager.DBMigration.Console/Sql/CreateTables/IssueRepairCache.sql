PRINT N'Starting rebuilding table [dbo].[IssueRepairCache]...';


CREATE TABLE [dbo].[tmp_ms_xx_IssueRepairCache]
    (
      [IssueRepairCache_Id] BIGINT NOT NULL IDENTITY(1, 1),
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_IssueRepairCache] PRIMARY KEY CLUSTERED
        ( [IssueRepairCache_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[IssueRepairCache] )
    BEGIN
 
        INSERT  INTO [dbo].[tmp_ms_xx_IssueRepairCache] WITH (TABLOCK)
                ( 
					[AlignmentEntity_ExternalId] ,
					[TerritoryType_ExternalId]
                )
                SELECT  [AlignmentEntity_Id] ,
                        [TerritoryType_Id]
                FROM    [dbo].[IssueRepairCache]
                ORDER BY [IssueRepairCache_Id] ASC;
    END

DROP TABLE [dbo].[IssueRepairCache];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_IssueRepairCache]', N'IssueRepairCache';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_IssueRepairCache]',
    N'PK_IssueRepairCache', N'OBJECT';

