/********* This query gets clusters in Assigned/Both display mode ********************/
EXEC dbo.GetAlignmentEntityClustersOfTypeInBoundingBox
	@showAssigned=0,@showUnassigned=0 , @isGeo=0,  @treeLeft=1, @treeRight=10430, @territoryTypeId=11,
	@minLat=42.6969776153564, @maxLat=47.4596862792969, @minLng=-73.4377403259277, @maxLng=47.4596862792969, @ZoomLevel=19,@aeStatus='Normal';