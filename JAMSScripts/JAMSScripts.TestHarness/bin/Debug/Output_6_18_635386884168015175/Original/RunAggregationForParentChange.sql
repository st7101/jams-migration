
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedEntitys') 
DROP TABLE [TempUpdatedEntitys];

CREATE TABLE [dbo].[TempUpdatedEntitys](
	[AlignmentEntity_Id] [nvarchar](100) NOT NULL PRIMARY KEY
);


WITH AffectedAlignmentEntity as
(
	SELECT AlignmentEntity_Id FROM AlignmentEntitys  
	WHERE AlignmentEntity_Id in (22)
)

INSERT INTO [TempUpdatedEntitys] SELECT * from AffectedAlignmentEntity

--------------------

DECLARE @sql AS VARCHAR(MAX)='';
SELECT @sql = @sql + 
'ALTER INDEX ' + sys.indexes.name + ' ON  ' + sys.objects.name + ' DISABLE ' +CHAR(13)+CHAR(10)+ ';'
FROM 
    sys.indexes
JOIN 
    sys.objects 
    ON sys.indexes.object_id = sys.objects.object_id
WHERE sys.indexes.type_desc = 'NONCLUSTERED'
  AND sys.objects.type_desc = 'USER_TABLE'
  AND sys.objects.name = 'AlignmentEntityNumberAttributeCache'
  AND sys.indexes.name <> 'idx_AttributeTypeId_AEAttributeCacheId_AEId_TTId';
exec(@sql)

------------------------------

IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempUpdatedOrgUnitIds') 
DROP TABLE [TempUpdatedOrgUnitIds];

CREATE TABLE [dbo].[TempUpdatedOrgUnitIds](
	[OrgUnit_Id] [nvarchar](100) NOT NULL PRIMARY KEY
);


INSERT INTO TempUpdatedOrgUnitIds 
	SELECT DISTINCT ouae.OrgUnit_id 
	FROM TempUpdatedEntitys tue
	inner join OrgUnitAlignmentEntitys ouae
		ON ouae.AlignmentEntity_id = tue.AlignmentEntity_id
	inner join OrgUnits ou
		ON ou.OrgUnit_Id = ouae.OrgUnit_id


---------------------



/* DROP TEMPORARY TABLES IF THEY EXIST*/
IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'ChildEntityPerTerritoryTypeMappings') 
DROP TABLE [ChildEntityPerTerritoryTypeMappings];

IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'IndependentEntitys') 
DROP TABLE [IndependentEntitys];



SELECT * INTO ChildEntityPerTerritoryTypeMappings FROM
(
SELECT ae_child.AlignmentEntity_ParentId AS ParentAlignmentEntity_Id, ae_child.AlignmentEntity_Id as AlignmentEntity_Id, 
ge.TerritoryType_Id AS TerritoryType_id
FROM [AlignmentEntitys] ae_child
INNER JOIN [GroupedEntities] ge ON ae_child.AlignmentEntity_id = ge.AlignmentEntity_id -- join only child records to GE to sum them per territory type
INNER JOIN [TempUpdatedEntitys] tue ON tue.AlignmentEntity_id = ae_child.AlignmentEntity_ParentId 
WHERE ae_child.AlignmentEntity_Status in ('Normal')
) a


SELECT * INTO IndependentEntitys FROM
(
Select ae.AlignmentEntity_Id
FROM [AlignmentEntitys] ae
INNER JOIN [TempUpdatedEntitys] tue ON tue.AlignmentEntity_id = ae.AlignmentEntity_Id
WHERE EXISTS (Select 1 from [OrgUnitAlignmentEntitys] oae
WHERE ae.AlignmentEntity_Id = oae.AlignmentEntity_id )
and Exists 
(select distinct territorytype_id from AlignmentEntityTypeTerritoryTypes aet2
where aet2.AlignmentEntityType_id = ae.AlignmentEntityType_id
except
select aet1.TerritoryType_id from AlignmentEntityTypeTerritoryTypes aet1
where aet1.AlignmentEntityType_ParentId = ae.AlignmentEntityType_id
)
) b



IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[ChildEntityPerTerritoryTypeMappings]') AND name = N'idx_AEID_ParentAEId_TType')
DROP INDEX [idx_AEID_ParentAEId_TType] ON [dbo].[ChildEntityPerTerritoryTypeMappings]
CREATE NONCLUSTERED INDEX [idx_AEID_ParentAEId_TType] ON [dbo].[ChildEntityPerTerritoryTypeMappings] 
(
	[AlignmentEntity_Id] ASC
)
INCLUDE ( [ParentAlignmentEntity_Id],
[TerritoryType_id])

IF  EXISTS (SELECT * FROM sys.indexes WHERE object_id = OBJECT_ID(N'[dbo].[IndependentEntitys]') AND name = N'PK_IndependentEntitys')
ALTER TABLE [dbo].[IndependentEntitys] DROP CONSTRAINT [PK_IndependentEntitys]
ALTER TABLE [dbo].[IndependentEntitys] ADD  CONSTRAINT [PK_IndependentEntitys] PRIMARY KEY CLUSTERED 
(
	[AlignmentEntity_Id] ASC
)

/* Populate Temporary table */
DECLARE @attributeTypeId VARCHAR(200) -- Attribute Type Id
DECLARE @attributeTypeName VARCHAR(200) -- Attribute Type Name
DECLARE @sql1 NVARCHAR(MAX)

DECLARE Attribute_Cursor CURSOR FOR
SELECT AttributeType_Id, AttributeType_Name FROM [AttributeTypes]
WHERE AttributeType_Entity = 'AlignmentEntity' AND
	(AttributeType_DataType = 'Number' OR AttributeType_DataType = 'Currency')

OPEN Attribute_Cursor
FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName



WHILE @@FETCH_STATUS = 0
BEGIN
	
	SET @sql1 = N'
	With TempAlignmentEntityNumberAttributeCache as
	(SELECT em.ParentAlignmentEntity_Id AS AlignmentEntity_id, ' + 
			@attributeTypeId + ' AS AttributeType_Id, 
			em.TerritoryType_id AS TerritoryType_id, 
			SUM(ISNULL(ae.[' + @attributeTypeName + '], 0)) AS AlignmentEntityNumberAttributeCache_Value FROM
			AlignmentEntitys ae
			INNER JOIN ChildEntityPerTerritoryTypeMappings em ON em.AlignmentEntity_Id = ae.AlignmentEntity_Id
			GROUP BY em.ParentAlignmentEntity_Id, em.TerritoryType_id
		UNION
	SELECT ae.AlignmentEntity_Id AS AlignmentEntity_id, ' + 
			@attributeTypeId + ' AS AttributeType_Id,
			pae.TerritoryType_id AS TerritoryType_id,
			0 AS AlignmentEntityNumberAttributeCache_Value 
			FROM AlignmentEntitys ae
			INNER JOIN TempUpdatedEntitys uae
			on uae.AlignmentEntity_Id = ae.AlignmentEntity_Id
			INNER JOIN (SELECT DISTINCT aettt.TerritoryType_id AS TerritoryType_Id, 
								aettt.AlignmentEntityType_ParentId AS AlignmentEntityType_id 
			FROM AlignmentEntityTypeTerritoryTypes aettt
			WHERE aettt.AlignmentEntityType_ParentId IS NOT NULL) pae 
			ON ae.AlignmentEntityType_id = pae.AlignmentEntityType_id
			WHERE NOT EXISTS (SELECT 1 FROM ChildEntityPerTerritoryTypeMappings em WHERE em.ParentAlignmentEntity_Id = ae.AlignmentEntity_Id and em.TerritoryType_id = pae.TerritoryType_Id)
			AND ae.AlignmentEntity_Status in (''Normal'',''Updated'')
			
		UNION
	SELECT ae.AlignmentEntity_id AS AlignmentEntity_id, ' + 
			@attributeTypeId + ' AS AttributeType_id, 
			NULL AS TerritoryType_id, 
			ISNULL(ae.[' + @attributeTypeName + '], 0) AS AlignmentEntityNumberAttributeCache_Value
			FROM [AlignmentEntitys] ae
			INNER JOIN IndependentEntitys ie ON ae.AlignmentEntity_Id = ie.AlignmentEntity_Id
			WHERE ae.AlignmentEntity_Status in (''Normal'',''Updated'')	
	UNION
	SELECT ae.AlignmentEntity_id AS AlignmentEntity_id, ' + 
			@attributeTypeId + ' AS AttributeType_id, 
			NULL AS TerritoryType_id, 
			0 AS AlignmentEntityNumberAttributeCache_Value
			FROM [AlignmentEntitys] ae
			INNER JOIN IndependentEntitys ie ON ae.AlignmentEntity_Id = ie.AlignmentEntity_Id
			WHERE ae.AlignmentEntity_Status = ''SoftDelete''
	)	
	UPDATE aenac WITH (TABLOCK)
		SET aenac.AlignmentEntityNumberAttributeCache_Value = taenac.AlignmentEntityNumberAttributeCache_Value 
	FROM AlignmentEntityNumberAttributeCache  aenac
	INNER join TempAlignmentEntityNumberAttributeCache taenac
		ON taenac.AlignmentEntity_id = aenac.AlignmentEntity_id
		AND taenac.AttributeType_id = aenac.AttributeType_id
		AND (taenac.TerritoryType_id = aenac.TerritoryType_id 
		OR (taenac.TerritoryType_id is null and aenac.TerritoryType_id is null))
	;'

	exec sp_executesql @sql1;

	FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId, @attributeTypeName
END

CLOSE Attribute_Cursor   
DEALLOCATE Attribute_Cursor

DROP TABLE [ChildEntityPerTerritoryTypeMappings]
DROP TABLE [IndependentEntitys]
DROP TABLE [TempUpdatedEntitys]


IF EXISTS (SELECT * FROM sys.objects o 
			WHERE o.name = 'TempDerivedAlignmentsTerritoryTypes') 
DROP TABLE [TempDerivedAlignmentsTerritoryTypes]

SELECT * INTO [TempDerivedAlignmentsTerritoryTypes] FROM
(SELECT DISTINCT aettt.TerritoryType_id as TerritoryType_Id, 
					aettt.AlignmentEntityType_ParentId as AlignmentEntityType_id 
 FROM AlignmentEntityTypeTerritoryTypes aettt
 WHERE aettt.AlignmentEntityType_ParentId IS NOT NULL) pae
 
 
 
 /* Populate Temporary table */
DECLARE @attributeTypeId2 VARCHAR(200) -- Attribute Type Id
DECLARE @attributeTypeName2 VARCHAR(200) -- Attribute Type Name
DECLARE @sql3 NVARCHAR(MAX)

	
DECLARE Attribute_Cursor CURSOR FOR
SELECT AttributeType_Id, AttributeType_Name FROM [AttributeTypes]
WHERE AttributeType_Entity = 'AlignmentEntity' AND
	(AttributeType_DataType = 'Number' OR AttributeType_DataType = 'Currency')

OPEN Attribute_Cursor
FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId2, @attributeTypeName2

WHILE @@FETCH_STATUS = 0
BEGIN
	SET @sql3 = N'
	WITH UpdatedOrgUnitAttributeCache AS
	(
		SELECT oae.OrgUnit_id, ' + @attributeTypeId2 + ' AS AttributeType_id, 
			SUM(aenac.AlignmentEntityNumberAttributeCache_Value * oae.OrgUnitAlignmentEntity_Weight/100) AS OrgUnitAttributeCache_Value 
		FROM AlignmentEntityNumberAttributeCache aenac
			INNER JOIN OrgUnitAlignmentEntitys oae ON aenac.AlignmentEntity_id = oae.AlignmentEntity_id
			INNER JOIN AlignmentEntitys ae ON oae.AlignmentEntity_id = ae.AlignmentEntity_Id
			INNER JOIN OrgUnits o ON oae.OrgUnit_id = o.OrgUnit_Id
			INNER JOIN TerritoryTypes tt ON tt.OrgUnitLevel_id = o.OrgUnitLevel_id
			INNER JOIN TempUpdatedOrgUnitIds tuoui on tuoui.OrgUnit_id = o.OrgUnit_Id
			LEFT OUTER JOIN  TempDerivedAlignmentsTerritoryTypes da ON 
			da.TerritoryType_Id = tt.TerritoryType_Id AND ae.AlignmentEntityType_id = da.AlignmentEntityType_id
			WHERE 
			((ae.AlignmentEntityType_id = da.AlignmentEntityType_id AND tt.TerritoryType_Id = aenac.TerritoryType_id) 
			OR 
			(da.AlignmentEntityType_id IS NULL AND aenac.TerritoryType_id IS NULL))
			AND 
			aenac.AttributeType_id = ' + @attributeTypeId2 + ' 
			AND (ae.AlignmentEntity_Status = ''Normal'' OR  ae.AlignmentEntity_Status = ''Updated'')
			
		GROUP BY oae.OrgUnit_id 
	)

	UPDATE ouae WITH (TABLOCK) 
	SET
		ouae.OrgUnitAttributeCache_Value = uouae.OrgUnitAttributeCache_Value
	FROM [OrgUnitAttributeCache] ouae
	INNER JOIN UpdatedOrgUnitAttributeCache uouae
	ON ouae.OrgUnit_id = uouae.OrgUnit_id 
	AND ouae.AttributeType_id = uouae.AttributeType_id
	;'

	exec sp_executesql @sql3;

	FETCH NEXT FROM Attribute_Cursor INTO @attributeTypeId2, @attributeTypeName2
END

------

CLOSE Attribute_Cursor;  
DEALLOCATE Attribute_Cursor;


/* Update parents of updated leaf level org units */
SET @sql3 = N'
	WITH ParentsToBeUpdated AS
	(SELECT DISTINCT parents.* FROM OrgUnits parents, OrgUnits leaf 
							WHERE leaf.OrgUnit_Id in (select OrgUnit_Id from TempUpdatedOrgUnitIds)
							AND parents.OrgUnit_TreeLeft < leaf.OrgUnit_TreeLeft AND parents.OrgUnit_TreeRight > leaf.OrgUnit_TreeRight),

	UpdatedParentOrgUnitsCache AS
	(
	SELECT parent.orgunit_Id as OrgUnit_id, oc.AttributeType_id as AttributeType_id, SUM(oc.OrgUnitAttributeCache_Value) as OrgUnitAttributeCache_Value 
	FROM OrgUnitAttributeCache as oc
	INNER JOIN OrgUnits child ON child.OrgUnit_Id = oc.Orgunit_Id
	INNER JOIN OrgUnitLevels oul ON child.OrgUnitLevel_Id = oul.OrgUnitLevel_Id
	INNER JOIN ParentsToBeUpdated parent ON child.OrgUnit_TreeLeft >  parent.OrgUnit_TreeLeft AND child.OrgUnit_TreeRight < parent.OrgUnit_TreeRight
	WHERE oul.OrgUnitLevel_IsLeafLevel = 1
	GROUP BY parent.orgunit_Id, oc.AttributeType_id
	)

	UPDATE ouae WITH (TABLOCK) 
		SET
			ouae.OrgUnitAttributeCache_Value = upouc.OrgUnitAttributeCache_Value
		FROM [OrgUnitAttributeCache] ouae
		INNER JOIN UpdatedParentOrgUnitsCache upouc
		ON ouae.OrgUnit_id = upouc.OrgUnit_id 
		AND ouae.AttributeType_id = upouc.AttributeType_id';

exec sp_executesql @sql3;

 /* Drop Temporary Table */
DROP TABLE [TempDerivedAlignmentsTerritoryTypes]
DROP TABLE [TempUpdatedOrgUnitIds]



declare @currentMaxOrgUnitAttributeIdCount int;
declare @missingOrgUnitAttributeCacheCount int;

declare @sql4 varchar(4000);

Set @sql4='';
select @currentMaxOrgUnitAttributeIdCount= (MAX(OrgUnitAttributeCache_id)+1) from OrgUnitAttributeCache;

select OrgUnit_id,AttributeType_id,OrgUnitAttributeCache_Value  into  TempOrgUnitAttributeCache from OrgUnitAttributeCache where 1=2;

set @sql4='Alter Table TempOrgUnitAttributeCache 
ADD OrgUnitAttributeCache_id int Identity( ' + Cast(@currentMaxOrgUnitAttributeIdCount as CHAR) +' ,1) ;';

exec(@sql4);

insert into TempOrgUnitAttributeCache ( OrgUnit_id,AttributeType_id,OrgUnitAttributeCache_Value) 
select o.OrgUnit_Id,a.AttributeType_Id,0 from OrgUnits as o, AttributeTypes as a where not exists 
(select 1 from OrgUnitAttributeCache as oac where o.OrgUnit_Id=oac.OrgUnit_id and 
a.AttributeType_Id=oac.AttributeType_id) and a.AttributeType_Entity='AlignmentEntity' and a.AttributeType_DataType <> 'Text';

select @missingOrgUnitAttributeCacheCount= COUNT(1) from TempOrgUnitAttributeCache

IF @missingOrgUnitAttributeCacheCount > 0
BEGIN
	insert into OrgUnitAttributeCache (OrgUnitAttributeCache_Id, OrgUnit_id,AttributeType_id,OrgUnitAttributeCache_Value) 
	select OrgUnitAttributeCache_Id, OrgUnit_id,AttributeType_id,OrgUnitAttributeCache_Value from TempOrgUnitAttributeCache;
	
	update HiLoUniqueKey set NextHi= @missingOrgUnitAttributeCacheCount + NextHi*10 where
	ObjectType='OrgUnitAttributeCache'
END

drop table TempOrgUnitAttributeCache


---------------------------


DECLARE @sql5 AS VARCHAR(MAX)='';
SELECT @sql5 = @sql5 + 
'ALTER INDEX ' + sys.indexes.name + ' ON  ' + sys.objects.name + ' REBUILD ' +CHAR(13)+CHAR(10)+ ';'
FROM 
    sys.indexes
JOIN 
    sys.objects 
    ON sys.indexes.object_id = sys.objects.object_id
WHERE sys.indexes.type_desc = 'NONCLUSTERED'
  AND sys.objects.type_desc = 'USER_TABLE'
  AND sys.objects.name = 'AlignmentEntityNumberAttributeCache'
  AND sys.indexes.name <> 'idx_AttributeTypeId_AEAttributeCacheId_AEId_TTId';
exec(@sql5)






