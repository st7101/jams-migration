﻿Delete from ApprovalRoundOrgUnitLevelPermissions
Delete from OrgUnitLevelPermissions

Declare @PkConstraint nvarchar(255)
Declare @FkConstraint nvarchar(255)
Declare @sql nvarchar(max)

--Delete Foreign Key
SELECT @FkConstraint = (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'FOREIGN KEY' AND TABLE_NAME = 'ApprovalRoundOrgUnitLevelPermissions' AND CONSTRAINT_NAME like '%Org%')
Set @sql = N'Alter Table ApprovalRoundOrgUnitLevelPermissions
Drop CONSTRAINT ' + @FkConstraint
EXEC (@sql)

--Delete PrimaryKey
SELECT @PkConstraint = (SELECT TOP 1 CONSTRAINT_NAME FROM INFORMATION_SCHEMA.TABLE_CONSTRAINTS WHERE CONSTRAINT_TYPE = 'PRIMARY KEY' AND TABLE_NAME = 'OrgUnitLevelPermissions')
Set @sql = N'Alter Table OrgUnitLevelPermissions
Drop CONSTRAINT ' + @PkConstraint
EXEC (@sql)


Alter Table OrgUnitLevelPermissions
Add TempOrgUnitLevelPermission_Id bigint not null identity(1,1)

Insert Into OrgUnitLevelPermissions
(OrgUnitLevel_id, Permission_id, OrgUnitLevelPermission_IsEnabled, OrgUnitLevelPermission_Id)
(select oul.OrgUnitLevel_Id, p.Permission_Id, 1 as OrgUnitLevelPermission_IsEnabled, 0 as OrgUnitLevelPermission_Id from OrgUnitLevels oul, Permissions p
Where p.Permission_PermissionLevel = 'OrgUnitLevel')

SET @sql = N'
Update OrgUnitLevelPermissions
Set OrgUnitLevelPermission_Id = TempOrgUnitLevelPermission_Id'
EXEC (@sql)

Alter Table OrgUnitLevelPermissions
drop column TempOrgUnitLevelPermission_Id


--Add Primary Key again
ALTER TABLE [dbo].[OrgUnitLevelPermissions] ADD PRIMARY KEY CLUSTERED 
(
	[OrgUnitLevelPermission_Id] ASC
)WITH (PAD_INDEX  = OFF, STATISTICS_NORECOMPUTE  = OFF, SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, ONLINE = OFF, ALLOW_ROW_LOCKS  = ON, ALLOW_PAGE_LOCKS  = ON) ON [PRIMARY]


--Add Foreign Key again
ALTER TABLE [dbo].[ApprovalRoundOrgUnitLevelPermissions]  WITH CHECK ADD FOREIGN KEY([OrgUnitLevelPermission_id])
REFERENCES [dbo].[OrgUnitLevelPermissions] ([OrgUnitLevelPermission_Id])




