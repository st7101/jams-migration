﻿DECLARE @sql AS VARCHAR(MAX)='';
SELECT @sql = @sql + 
'ALTER INDEX ' + sys.indexes.name + ' ON  ' + sys.objects.name + ' REBUILD ' +CHAR(13)+CHAR(10)+ ';'
FROM 
    sys.indexes
JOIN 
    sys.objects 
    ON sys.indexes.object_id = sys.objects.object_id
WHERE sys.indexes.type_desc = 'NONCLUSTERED'
  AND sys.objects.type_desc = 'USER_TABLE'
  AND sys.objects.name = 'AlignmentEntityNumberAttributeCache'
  AND sys.indexes.name <> 'IDX_ALIGNMENTENTITYNUMBERATTRIBUTECACHE_AttributeTypeId_INC_AlignmentEntityId_TerritoryTypeId_AlignmentEntit____';
exec(@sql)
