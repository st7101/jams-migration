CREATE NONCLUSTERED INDEX [IDX_ALIGNMENTENTITYS_AlignmentEntityExternalId] ON [dbo].[AlignmentEntitys](	[AlignmentEntity_ExternalId] ASC ) WITH (DATA_COMPRESSION = PAGE)
CREATE NONCLUSTERED INDEX [IDX_TERRITORYTYPES_TerritoryTypeExternalId] ON [dbo].TerritoryTypes ([TerritoryType_ExternalId] ASC ) WITH (DATA_COMPRESSION = PAGE)
CREATE NONCLUSTERED INDEX [IDX_ORGUNITS_OrgUnitExternalId] ON [dbo].OrgUnits ([OrgUnit_ExternalId] ASC) WITH (DATA_COMPRESSION = PAGE)
CREATE NONCLUSTERED INDEX [IDX_ORGUNITLEVELS_OrgUnitLevelExternalId] ON [dbo].OrgUnitLevels ([OrgUnitLevel_ExternalId] ASC ) WITH (DATA_COMPRESSION = PAGE)
CREATE NONCLUSTERED INDEX [IDX_Positions_PositionExternalId] ON [dbo].Positions (Position_ExternalId ASC ) WITH (DATA_COMPRESSION = PAGE)