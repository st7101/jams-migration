﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace JAMSScripts.LoadTest
{
    class Program
    {

        static void Main(string[] args)
        {
            
            using (LoadTestHarness load = new LoadTestHarness())
            {
                load.RunConcurrent(1);
                load.RunConcurrent(2);
                load.RunConcurrent(5);
                load.RunConcurrent(10);
            }
        }


    }
}
