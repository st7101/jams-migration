
PRINT N'Starting rebuilding table [dbo].[AlignmentEntityOverrides]...';


CREATE TABLE [dbo].[tmp_ms_xx_AlignmentEntityOverrides]
    (
      [AlignmentEntityOverride_Id] BIGINT NOT NULL ,
      [Request_Id] BIGINT NULL ,
      [AlignmentEntity_ExternalId] NVARCHAR(100) NULL ,
      [AlignmentEntity_Id] BIGINT NULL ,
      [TerritoryType_ExternalId] NVARCHAR(100) NULL ,
      [TerritoryType_Id] BIGINT NULL ,
      [NewAlignmentState_Id] BIGINT NULL ,
      [OldAlignmentState_Id] BIGINT NULL ,
      CONSTRAINT [tmp_ms_xx_constraint_PK_AlignmentEntityOverrides] PRIMARY KEY CLUSTERED
        ( [AlignmentEntityOverride_Id] ASC )
        WITH (  DATA_COMPRESSION = PAGE )
    );
IF EXISTS ( SELECT TOP 1
                    1
            FROM    [dbo].[AlignmentEntityOverrides] )
    BEGIN 
        INSERT  INTO [dbo].[tmp_ms_xx_AlignmentEntityOverrides] WITH (TABLOCK)
                ( [AlignmentEntityOverride_Id] ,
                  [Request_Id] ,
                  [AlignmentEntity_ExternalId] ,
                  [TerritoryType_ExternalId] ,
                  [NewAlignmentState_Id] ,
                  [OldAlignmentState_Id]
                )
                SELECT  [AlignmentEntityOverride_Id] ,
                        [Request_Id] ,
                        [AlignmentEntity_Id] ,
                        [TerritoryType_Id] ,
                        [NewAlignmentState_Id] ,
                        [OldAlignmentState_Id]
                FROM    [dbo].[AlignmentEntityOverrides]
                ORDER BY [AlignmentEntityOverride_Id] ASC;
    END
DROP TABLE [dbo].[AlignmentEntityOverrides];
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_AlignmentEntityOverrides]',
    N'AlignmentEntityOverrides';
EXECUTE sp_rename N'[dbo].[tmp_ms_xx_constraint_PK_AlignmentEntityOverrides]',
    N'PK_AlignmentEntityOverrides', N'OBJECT';
