INSERT INTO [dbo].[Positions]
           ([Position_ExternalId]
           ,[Position_Id]
           ,[Position_LegacyId]
           ,[Position_Effort]
           ,[OrgUnit_ExternalId]
           ,[Role_Id])
SELECT 
pTemp.POSITION_ID,
ROW_NUMBER() OVER (ORDER BY pTemp.POSITION_ID),
pTemp.POSITION_LEGACYID,
pTemp.POSITION_EFFORT,
ptemp.ORGUNIT_ID,
pTemp.ROLE_ID
FROM dbo.Positions_Temp pTemp
GO

UPDATE p
SET OrgUnit_Id=ou.OrgUnit_Id
FROM positions p
INNER JOIN orgunits ou ON p.OrgUnit_ExternalId=ou.OrgUnit_ExternalId
WHERE p.OrgUnit_ExternalId IS NOT NULL


GO
